#include "UFNBlueprintFunctionLibrary.h"
#include "FastNoise/UFastNoise.h"
#include "UFNSelectModule.h"
#include "UFN3SelectModule.h"
#include "UFNBlendModule.h"
#include "UFNScaleBiasModule.h"
#include "UFNAddModule.h"
#include "UFNConstantModule.h"
#include "UFNSplineGenerator.h"
#include "UFNWarpModule.h"
#include "UFNRadialModule.h"
#include "UFNShoreFilterModule.h"
#include "Components/SplineComponent.h"


UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateNoiseGenerator(UObject* outer, ENoiseType noiseType, ECellularDistanceFunction cellularDistanceFunction, ECellularReturnType cellularReturnType , EFractalType fractalType, EInterp interpolation, int32 seed, int32 octaves, float frequency, float lacunarity, float fractalGain)
{
	UFastNoise* noiseGen = NewObject<UFastNoise>(outer);

	noiseGen->SetNoiseType(noiseType);
	noiseGen->SetSeed(seed);
	noiseGen->SetFractalOctaves(octaves);
	noiseGen->SetFrequency(frequency);
	noiseGen->SetFractalType(fractalType);
	noiseGen->SetFractalLacunarity(lacunarity);
	noiseGen->SetFractalGain(fractalGain);
	noiseGen->SetCellularDistanceFunction(cellularDistanceFunction);
	noiseGen->SetCellularReturnType(cellularReturnType);
	noiseGen->SetInterp(interpolation);

	return noiseGen;
}



UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateSelectModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* selectModule, ESelectInterpType interpolationType, float falloff, float threshold, int32 numSteps)
{
	if (!(inputModule1 && inputModule2 && selectModule && outer)){
		return nullptr;
	}

	UUFNSelectModule* newSelectModule = NewObject<UUFNSelectModule>(outer);

	newSelectModule->inputModule1 = inputModule1;
	newSelectModule->inputModule2 = inputModule2;
	newSelectModule->selectModule = selectModule;
	newSelectModule->falloff = falloff;
	newSelectModule->threshold = threshold;
	newSelectModule->interpType = interpolationType;
	newSelectModule->numSteps = numSteps;

	return newSelectModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateBlendModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* selectModule, UCurveFloat* blendCurve)
{
	if (!(inputModule1 && inputModule2 && selectModule && outer)) {
		return nullptr;
	}

	UUFNBlendModule* blendModule = NewObject<UUFNBlendModule>(outer);

	blendModule->inputModule1 = inputModule1;
	blendModule->inputModule2 = inputModule2;
	blendModule->selectModule = selectModule;
	blendModule->blendCurve = blendCurve;

	return blendModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateScaleBiasModule(UObject* outer, UFNNoiseGenerator* inputModule, float scale, float bias)
{
	if (!(inputModule && outer)) {
		return nullptr;
	}

	UUFNScaleBiasModule* scaleBiasModule = NewObject<UUFNScaleBiasModule>(outer);

	scaleBiasModule->inputModule = inputModule;
	scaleBiasModule->scale = scale;
	scaleBiasModule->bias = bias;

	return scaleBiasModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateWarpModule(UObject* outer, UFNNoiseGenerator* inputModule, UFNNoiseGenerator* warpModule, float multiplier, EWarpIterations warpIterations)
{
	if (!(inputModule && outer)) {
		return nullptr;
	}

	UUFNWarpModule* thiswarpModule = NewObject<UUFNWarpModule>(outer);

	thiswarpModule->inputModule = inputModule;
	thiswarpModule->warpModule = warpModule;
	thiswarpModule->Iteration1XOffset = 1.6f;
	thiswarpModule->Iteration1YOffset = 2.5f;
	thiswarpModule->Iteration2XOffset1 = 5.7f;
	thiswarpModule->Iteration2YOffset1 = 3.4f;
	thiswarpModule->Iteration2XOffset2 = 2.1f;
	thiswarpModule->Iteration2YOffset2 = 3.5f;
	thiswarpModule->multiplier = multiplier;
	thiswarpModule->warpIterations = warpIterations;

	return thiswarpModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateAddModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* maskModule, float threshold)
{
	if (!(outer && inputModule1 && inputModule2))
	{
		return nullptr;
	}

	UUFNAddModule* noiseGen = NewObject<UUFNAddModule>(outer);

	noiseGen->inputModule1 = inputModule1;
	noiseGen->inputModule2 = inputModule2;
	noiseGen->maskModule = maskModule;
	noiseGen->threshold = threshold;

	return noiseGen;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateConstantModule(UObject* outer, float constantValue)
{
	UUFNConstantModule* noiseGen = NewObject<UUFNConstantModule>(outer);

	noiseGen->constantValue = constantValue;

	return noiseGen;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateSimpleNoiseGenerator(ESimpleNoiseType noiseType, int32 seed, float frequency, EInterp interpolation)
{
	UFastNoise* noiseGen = NewObject<UFastNoise>();

	switch (noiseType)
	{
		case ESimpleNoiseType::SimpleGradient:
			noiseGen->SetNoiseType(ENoiseType::Gradient);
			break;
		case ESimpleNoiseType::SimpleSimplex:
			noiseGen->SetNoiseType(ENoiseType::Simplex);
			break;
		case ESimpleNoiseType::SimpleValue:
			noiseGen->SetNoiseType(ENoiseType::Value);
			break;
		case ESimpleNoiseType::SimpleWhiteNoise:
			noiseGen->SetNoiseType(ENoiseType::WhiteNoise);
			break;
	}

	noiseGen->SetSeed(seed);
	noiseGen->SetFrequency(frequency);
	noiseGen->SetInterp(interpolation);

	return noiseGen;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateFractalNoiseGenerator(UObject* outer, EFractalNoiseType noiseType, int32 seed, float frequency, float fractalGain, EInterp interpolation, EFractalType fractalType, int32 octaves, float lacunarity)
{
	UFastNoise* noiseGen = NewObject<UFastNoise>(outer);

	switch (noiseType)
	{
	case EFractalNoiseType::FractalGradient:
		noiseGen->SetNoiseType(ENoiseType::GradientFractal);
		break;
	case EFractalNoiseType::FractalSimplex:
		noiseGen->SetNoiseType(ENoiseType::SimplexFractal);
		break;
	case EFractalNoiseType::FractalValue:
		noiseGen->SetNoiseType(ENoiseType::ValueFractal);
		break;
	}

	noiseGen->SetSeed(seed);
	noiseGen->SetFractalOctaves(octaves);
	noiseGen->SetFrequency(frequency);
	noiseGen->SetFractalType(fractalType);
	noiseGen->SetFractalGain(fractalGain);
	noiseGen->SetFractalLacunarity(lacunarity);
	noiseGen->SetInterp(interpolation);

	return noiseGen;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateCellularNoiseGenerator(UObject* outer, int32 seed, float frequency, ECellularDistanceFunction cellularDistanceFunction, ECellularReturnType cellularReturnType)
{
	UFastNoise* noiseGen = NewObject<UFastNoise>(outer);

	noiseGen->SetNoiseType(ENoiseType::Cellular);
	noiseGen->SetSeed(seed);
	noiseGen->SetFrequency(frequency);
	noiseGen->SetCellularDistanceFunction(cellularDistanceFunction);
	noiseGen->SetCellularReturnType(cellularReturnType);

	return noiseGen;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::Create3SelectModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* inputModule3, UFNNoiseGenerator* selectModule, float lowerThreshold /*= 0.0f*/, float upperThreshold /*= 0.0f*/, ESelectInterpType interpolationType /*= ESelectInterpType::None*/, float falloff /*= 0.0f*/, int32 steps /*= 4*/)
{
	if (!(inputModule1 && inputModule2 && inputModule3 && selectModule && outer)) {
		return nullptr;
	}

	UUFN3SelectModule* newSelectModule = NewObject<UUFN3SelectModule>(outer);

	newSelectModule->inputModule1 = inputModule1;
	newSelectModule->inputModule2 = inputModule2;
	newSelectModule->inputModule3 = inputModule3;
	newSelectModule->selectModule = selectModule;
	newSelectModule->falloff = falloff;
	newSelectModule->lowerThreshold = lowerThreshold;
	newSelectModule->upperThreshold = upperThreshold;
	newSelectModule->interpType = interpolationType;
	newSelectModule->numSteps = steps;

	return newSelectModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateSplineGenerator(UObject* outer, float MaxDistance, float MinDistance, TArray<USplineComponent*> Splines, UCurveFloat* falloffCurve)
{
	UUFNSplineGenerator* newSplineGenerator = NewObject<UUFNSplineGenerator>(outer);
	newSplineGenerator->MaximumDistance = MaxDistance;
	newSplineGenerator->MinimumDistance = MinDistance;
	newSplineGenerator->Splines = Splines;
	newSplineGenerator->FalloffCurve = falloffCurve;

	return newSplineGenerator;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateRadialModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, ESelectInterpType interpolationType, FVector origin, float radius, float falloff, int32 numSteps)
{
	if (!(inputModule1 && inputModule2  && outer)) {
		return nullptr;
	}

	UUFNRadialModule* newRadialModule = NewObject<UUFNRadialModule>(outer);

	newRadialModule->inputModule1 = inputModule1;
	newRadialModule->inputModule2 = inputModule2;	
	newRadialModule->falloff2 = falloff * falloff;
	newRadialModule->radius2 = radius * radius;
	newRadialModule->origin = origin;
	newRadialModule->interpType = interpolationType;
	newRadialModule->numSteps = numSteps;

	return newRadialModule;
}

UFNNoiseGenerator* UUFNBlueprintFunctionLibrary::CreateShoreFilterModule(UObject* outer, UFNNoiseGenerator* inputModule1, const float shoreHeight, const float threshold)
{
	if (!(inputModule1 && outer)) {
		return nullptr;
	}

	UUFNShoreFilterModule* newShoreFilterModule = NewObject<UUFNShoreFilterModule>(outer);

	newShoreFilterModule->myInputModule = inputModule1;
	newShoreFilterModule->myShoreHeight = shoreHeight;
	newShoreFilterModule->myThreshhold = threshold;

	return newShoreFilterModule;
}

UUFNBlueprintFunctionLibrary::UUFNBlueprintFunctionLibrary(const class FObjectInitializer& obj)
	: Super(obj)
{

}