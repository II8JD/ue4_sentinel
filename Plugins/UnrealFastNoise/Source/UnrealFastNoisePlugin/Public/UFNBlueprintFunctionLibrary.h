#pragma once
// Copyright 1998-2013 Epic Games, Inc. All Rights Reserved.
#include "FastNoise/UFastNoise.h"
#include "CoreMinimal.h"
#include "FUnrealFastNoisePlugin.h"
#include "UFNBlueprintFunctionLibrary.generated.h"

UCLASS()
class UNREALFASTNOISEPLUGIN_API UUFNBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()
	// Creates a new noise generator module. Note that not all parameters may be relevant e.g. Fractal noise types will ignore Cellular parameters
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateNoiseGenerator(UObject* outer, ENoiseType noiseType, ECellularDistanceFunction cellularDistanceFunction, ECellularReturnType cellularReturnType, EFractalType fractalType, EInterp interpolation, int32 seed = 1337, int32 octaves = 4, float frequency = 0.001f, float lacunarity = 2.0f, float fractalGain = 0.5f);
	// Creates a Select module. Returns a value either from input1 or input 2, depending on the value returned from the select module. Has sine in/out smooth falloff option (may be wonky)
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateSelectModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* selectModule, ESelectInterpType interpolationType = ESelectInterpType::None, float falloff = 0.0f, float threshold = 0.0f, int32 steps = 4);
	// Creates Blend modules. Returns a blended value from input1 and input 2, based on the value returned from the select module. Blend range is from -1.0 to 1.0;
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateBlendModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* selectModule, UCurveFloat* blendCurve = nullptr);
	// Creates a Scale/Bias modules. Applies a multiplier, and or additive value to the value returned from the input
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateScaleBiasModule(UObject* outer, UFNNoiseGenerator* inputModule, float scale = 1.0f, float bias = 0.0f);
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateWarpModule(UObject* outer, UFNNoiseGenerator* inputModule, UFNNoiseGenerator* warpModule, float multiplier, EWarpIterations warpIterations);
	// Creates an Add module. Adds two modules together, clamping the result and optionally accepting a third module as a mask with threshold
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateAddModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* maskModule = nullptr, float threshold = 1.0f);
	// Creates a Constant modules. Returns a constant value
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateConstantModule(UObject* outer, float constantValue);
	// Creates a simple (non-fractal) noise module
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateSimpleNoiseGenerator(ESimpleNoiseType noiseType, int32 seed = 12345, float frequency = 0.1f, EInterp interpolation = EInterp::InterpLinear);
	// Creates a Fractal noise module
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateFractalNoiseGenerator(UObject* outer, EFractalNoiseType noiseType, int32 seed = 12345, float frequency = 0.1f, float fractalGain = 0.5f, EInterp interpolation = EInterp::InterpLinear, EFractalType fractalType = EFractalType::Billow, int32 octaves = 4, float lacunarity = 2.0f);
	// Creates a Cellular (Voronoi) noise module
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateCellularNoiseGenerator(UObject* outer, int32 seed = 12345, float frequency = 0.1f, ECellularDistanceFunction cellularDistanceFunction = ECellularDistanceFunction::Euclidean, ECellularReturnType cellularReturnType = ECellularReturnType::CellValue);
	// Creates a spline module.
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* CreateSplineGenerator(UObject* outer, float MaxDistance, float MinDistance, TArray<class USplineComponent*> Splines, UCurveFloat* falloffCurve = nullptr);
	// Creates a Select module. Returns a value either from input1 or input 2 or input 3, depending on the value returned from the select module.
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
	static UFNNoiseGenerator* Create3SelectModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, UFNNoiseGenerator* inputModule3, UFNNoiseGenerator* selectModule, float lowerThreshold = 0.0f, float upperThreshold = 0.0f, ESelectInterpType interpolationType = ESelectInterpType::None, float falloff = 0.0f, int32 steps = 4);
	// Creates a Radial module. Returns a value either from input1 or input 2, depending on the distance from the origin. Has smooth falloff option (may be wonky)
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
		static UFNNoiseGenerator* CreateRadialModule(UObject* outer, UFNNoiseGenerator* inputModule1, UFNNoiseGenerator* inputModule2, ESelectInterpType interpolationType = ESelectInterpType::None, FVector origin = FVector(0.0f), float radius = 500.f, float falloff = 0.0f, int32 steps = 4);
	// Creates a Shore Filters module. Adjusts terrain near water level to encourage smooth beach like features.
	UFUNCTION(BlueprintPure, Category = "UnrealFastNoise")
		static UFNNoiseGenerator* CreateShoreFilterModule(UObject* outer, UFNNoiseGenerator* inputModule1, const float shoreHeight, const float threshold);

};