#pragma once

#include "UFNNoiseGenerator.h"
#include "CoreMinimal.h"
#include "UFNBlendModule.generated.h"

UCLASS()
class UNREALFASTNOISEPLUGIN_API UUFNBlendModule : public UFNNoiseGenerator
{
	GENERATED_UCLASS_BODY()
public:

	float GetNoise3D(float aX, float aY, float aZ) override;
	float GetNoise2D(float aX, float aY) override;
	UPROPERTY()
	UFNNoiseGenerator* inputModule1;
	UPROPERTY()
	UFNNoiseGenerator* inputModule2;
	UPROPERTY()
	UFNNoiseGenerator* selectModule;

	float falloff;
	UPROPERTY()
	UCurveFloat* blendCurve;

};