#pragma once

#include "UFNNoiseGenerator.h"
#include "CoreMinimal.h"
#include "UFN3SelectModule.generated.h"

UCLASS()
class UNREALFASTNOISEPLUGIN_API UUFN3SelectModule : public UFNNoiseGenerator
{
	GENERATED_UCLASS_BODY()
public:

	float GetNoise3D(float aX, float aY, float aZ) override;
	float GetNoise2D(float aX, float aY) override;

	UPROPERTY()
		UFNNoiseGenerator* inputModule1;
	UPROPERTY()
		UFNNoiseGenerator* inputModule2;
	UPROPERTY()
		UFNNoiseGenerator* inputModule3;
	UPROPERTY()
		UFNNoiseGenerator* selectModule;

	float upperThreshold;
	float lowerThreshold;

	float falloff;
	ESelectInterpType interpType;
	int32 numSteps;

};