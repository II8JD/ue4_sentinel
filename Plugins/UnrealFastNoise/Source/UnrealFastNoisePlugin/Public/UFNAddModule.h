#pragma once

#include "UFNNoiseGenerator.h"
#include "CoreMinimal.h"
#include "UFNAddModule.generated.h"

UCLASS()
class UUFNAddModule : public UFNNoiseGenerator
{
	GENERATED_UCLASS_BODY()
public:

	float GetNoise3D(float aX, float aY, float aZ) override;
	float GetNoise2D(float aX, float aY) override;
	UPROPERTY()
	UFNNoiseGenerator* inputModule1;
	UPROPERTY()
	UFNNoiseGenerator* inputModule2;
	UPROPERTY()
	UFNNoiseGenerator* maskModule;

	float threshold;
};