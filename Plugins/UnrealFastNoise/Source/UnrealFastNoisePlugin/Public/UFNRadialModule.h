#pragma once

#include "UFNNoiseGenerator.h"
#include "CoreMinimal.h"
#include "UFNRadialModule.generated.h"

UCLASS()
class UNREALFASTNOISEPLUGIN_API UUFNRadialModule : public UFNNoiseGenerator
{
	GENERATED_UCLASS_BODY()
public:

	float GetNoise3D(float aX, float aY, float aZ) override;
	float GetNoise2D(float aX, float aY) override;

	UPROPERTY()
	UFNNoiseGenerator* inputModule1;

	UPROPERTY()
	UFNNoiseGenerator* inputModule2;

	FVector origin;
	float radius2;
	float falloff2;

	ESelectInterpType interpType;
	int32 numSteps;
};