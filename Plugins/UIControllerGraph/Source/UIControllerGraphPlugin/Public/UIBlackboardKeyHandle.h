// Carl Peters

#pragma once

#include <BehaviorTree/BlackboardData.h>
#include <CoreMinimal.h>
#include <UObject/ObjectMacros.h>

#include "Engine/DataTable.h"

#include "UIBlackboardKeyHandle.generated.h"


/** Handle to a particular key on a blackboard*/
USTRUCT(BlueprintType)
struct UICONTROLLERGRAPHPLUGIN_API FUIBlackboardKeyHandle
{
	GENERATED_USTRUCT_BODY()

		FUIBlackboardKeyHandle()
		: BlackboardData(nullptr)
		, KeyName(NAME_None)
	{

	}

	/** Pointer to blueprint we want a key from */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UIBlackboardKeyHandle)
	const UBlackboardData* BlackboardData;

	/** Name of row in the table that we want */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UIBlackboardKeyHandle)
	FName KeyName;

	/** Returns true if this handle is specifically pointing to nothing */
	bool IsNull() const
	{
		return BlackboardData == nullptr && KeyName == NAME_None;
	}

	/** Get the key straight from the blackboard handle */
	template <class T>
	T* GetKey(const TCHAR* ContextString) const
	{
		if (BlackboardData == nullptr)
		{
			if (KeyName != NAME_None)
			{
				UE_LOG(LogDataTable, Warning, TEXT("FDataTableRowHandle::GetRow : No DataTable for row %s (%s)."), *RowName.ToString(), ContextString);
			}
			return nullptr;
		}

		return BlackboardData->FindRow<T>(RowName, ContextString);
	}

	template <class T>
	T* GetKey(const FString& ContextString) const
	{
		return GetKey<T>(*ContextString);
	}

	FString ToDebugString(bool bUseFullPath = false) const
	{
		if (BlackboardData == nullptr)
		{
			return FString::Printf(TEXT("No Data Table Specified, Row: %s"), *KeyName.ToString());
		}

		return FString::Printf(TEXT("Table: %s, Row: %s"), bUseFullPath ? *BlackboardData->GetPathName() : *BlackboardData->GetName(), *KeyName.ToString());
	}

	bool operator==(FUIBlackboardKeyHandle const& Other) const;
	bool operator!=(FUIBlackboardKeyHandle const& Other) const;
	void PostSerialize(const FArchive& Ar);
};

template<>
struct TStructOpsTypeTraits< FUIBlackboardKeyHandle > : public TStructOpsTypeTraitsBase2< FUIBlackboardKeyHandle >
{
	enum
	{
		WithPostSerialize = true,
	};
};

#define GETKEY_REPORTERROR(Handle, Template) Handle.GetKey<Template>(FString::Printf(TEXT("%s.%s"), *GetPathName(), TEXT(#Handle)))
#define GETROWOBJECT_REPORTERROR(Object, Handle, Template) Handle.GetRow<Template>(FString::Printf(TEXT("%s.%s"), *Object->GetPathName(), TEXT(#Handle)))