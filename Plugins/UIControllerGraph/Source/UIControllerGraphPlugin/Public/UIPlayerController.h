// Carl Peters

#pragma once

#include <BehaviorTree/BehaviorTreeTypes.h>
#include <CoreMinimal.h>
#include <GameFramework/PlayerController.h>

#include "UIPlayerController.generated.h"

class AAIController;
class UBehaviorTree;


/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class UICONTROLLERGRAPHPLUGIN_API AUIPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AUIPlayerController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "UMG")
	void ShowMenu(TSubclassOf<UUserWidget> MenuWidget, bool bShowCursor);

	UFUNCTION(BlueprintPure, Category = "UMG")
	UBlackboardComponent* GetUIBlackboard();

protected:
	UPROPERTY(BlueprintReadOnly, Category = "UMG")
	UUserWidget* CurrentMenuWidget{nullptr};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	UBehaviorTree* DefaultUITree{ nullptr };

	UPROPERTY(BlueprintReadOnly, Category = "UMG")
	AAIController* UIController{nullptr};

	/** Default class to use for UIAI. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (DisplayName = "AI Controller Class"), Category = UMG)
	TSubclassOf<AController> UIControllerClass;
};
