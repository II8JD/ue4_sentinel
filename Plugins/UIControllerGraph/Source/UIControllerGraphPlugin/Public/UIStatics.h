// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Kismet/BlueprintFunctionLibrary.h>

#include "UIStatics.generated.h"

class AUIPlayerController;
struct FUIBlackboardKeyHandle;

/**
 * 
 */
UCLASS()
class UICONTROLLERGRAPHPLUGIN_API UUIStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintPure, Category = "UI Graph Tools")
	static UBlackboardComponent* GetUIBlackboard(UObject* WorldContextObject);

	UFUNCTION(BlueprintPure, Category = "UI Graph Tools")
	static AUIPlayerController* GetPlayerController(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, Category = "UI Graph Tools")
	static void SetUIBlackboardEnum(UObject* WorldContextObject, const FUIBlackboardKeyHandle& Key, uint8 EnumValue);

	UFUNCTION(BlueprintCallable, Category = "UI Graph Tools")
	static void SetUIBlackboardBool(UObject* WorldContextObject, const FUIBlackboardKeyHandle& Key, bool bValue);
};
