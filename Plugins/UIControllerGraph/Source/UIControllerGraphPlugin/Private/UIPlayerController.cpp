﻿// Carl Peters


#include "UIPlayerController.h"

#include <AIController.h>
#include <BehaviorTree/BehaviorTree.h>
#include <BehaviorTree/BlackboardComponent.h>
#include <Blueprint/UserWidget.h>
#include <Blueprint/WidgetLayoutLibrary.h>
#include <Engine/StaticMeshActor.h>
#include <GameFramework/PlayerController.h>
#include <GameFramework/PlayerState.h>
#include <Kismet/KismetSystemLibrary.h>

AUIPlayerController::AUIPlayerController()
{
	bReplicates = false;
}

void AUIPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (!UKismetSystemLibrary::IsDedicatedServer(this))
	{
		if (ensure(UIControllerClass))
		{
			check(DefaultUITree);

			FActorSpawnParameters SpawnInfo;
			SpawnInfo.Instigator = GetInstigator();
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnInfo.OverrideLevel = GetLevel();
			SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save AI controllers into a map
			AController* NewController = GetWorld()->SpawnActor<AController>(UIControllerClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnInfo);
			NewController->SetOwner(this);
			UIController = Cast<AAIController>(NewController);
			if(UIController)
				UIController->RunBehaviorTree(DefaultUITree);
		}
	}
}

UBlackboardComponent* AUIPlayerController::GetUIBlackboard()
{
	//check(UIController);
	if (UIController)
		return UIController->GetBlackboardComponent();

	return nullptr;
}

void AUIPlayerController::ShowMenu(TSubclassOf<UUserWidget> MenuWidget, bool bShowCursor)
{
	if (CurrentMenuWidget)
		CurrentMenuWidget->RemoveFromViewport();

	if (bShowCursor)
	{
		FInputModeUIOnly InputMode;
		SetInputMode(InputMode);
	}
	else
	{
		FInputModeGameOnly InputMode;
		SetInputMode(InputMode);
	}
	bShowMouseCursor = bShowCursor;

	if (MenuWidget)
	{
		CurrentMenuWidget = CreateWidget<UUserWidget>(GetWorld(), MenuWidget);
		CurrentMenuWidget->AddToViewport();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No DefaultMainMenuWidget selected."))
	}
}