// Copyright Epic Games, Inc. All Rights Reserved.

#include "UIControllerGraphPlugin.h"

#define LOCTEXT_NAMESPACE "FUIControllerGraphPlugin"

void FUIControllerGraphPlugin::StartupModule()
{
}

void FUIControllerGraphPlugin::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FUIControllerGraphPlugin, UIControllerGraphPlugin)