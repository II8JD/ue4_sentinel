// Carl Peters

#include "UIStatics.h"

#include <UObject/UObjectIterator.h>

#include "UIPlayerController.h"


UBlackboardComponent* UUIStatics::GetUIBlackboard(UObject* WorldContextObject)
{
	AUIPlayerController* PC = UUIStatics::GetPlayerController(WorldContextObject);
	check(PC);
	return PC->GetUIBlackboard();
}

AUIPlayerController* UUIStatics::GetPlayerController(UObject* WorldContextObject)
{
	return WorldContextObject->GetWorld()->GetFirstPlayerController<AUIPlayerController>();
}

void UUIStatics::SetUIBlackboardEnum(UObject* WorldContextObject, const FUIBlackboardKeyHandle& Key, uint8 EnumValue)
{
	AUIPlayerController* PC = UUIStatics::GetPlayerController(WorldContextObject);
	check(PC);

	if (!UKismetSystemLibrary::IsDedicatedServer(WorldContextObject))
	{
		UBlackboardComponent* BB = UUIStatics::GetUIBlackboard(WorldContextObject);
		check(BB);
		BB->SetValueAsEnum(Key.KeyName, EnumValue);
	}
}

void UUIStatics::SetUIBlackboardBool(UObject* WorldContextObject, const FUIBlackboardKeyHandle& Key, bool bValue)
{
	AUIPlayerController* PC = UUIStatics::GetPlayerController(WorldContextObject);
	check(PC);

	if (!UKismetSystemLibrary::IsDedicatedServer(WorldContextObject))
	{
		UBlackboardComponent* BB = UUIStatics::GetUIBlackboard(WorldContextObject);
		check(BB);
		BB->SetValueAsBool(Key.KeyName, bValue);
	}
}