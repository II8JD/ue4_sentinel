// Carl Peters


#include "UIBlackboardKeyHandle.h"

bool FUIBlackboardKeyHandle::operator==(FUIBlackboardKeyHandle const& Other) const
{
	return BlackboardData == Other.BlackboardData && KeyName == Other.KeyName;
}

bool FUIBlackboardKeyHandle::operator != (FUIBlackboardKeyHandle const& Other) const
{
	return BlackboardData != Other.BlackboardData || KeyName != Other.KeyName;
}

void FUIBlackboardKeyHandle::PostSerialize(const FArchive& Ar)
{
	if (Ar.IsSaving() && !IsNull() && BlackboardData)
	{
		// Note which row we are pointing to for later searching
		Ar.MarkSearchableName(BlackboardData, KeyName);
	}
}