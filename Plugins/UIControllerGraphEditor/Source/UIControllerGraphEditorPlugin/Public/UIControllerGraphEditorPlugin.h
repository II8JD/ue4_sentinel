// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FUIControllerGraphEditorPlugin : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

private:
	void RegisterPropertyTypeCustomizations();

/**
* Registers a custom struct
*
* @param StructName				The name of the struct to register for property customization
* @param StructLayoutDelegate	The delegate to call to get the custom detail layout instance
*/
	void RegisterCustomPropertyTypeLayout(FName PropertyTypeName, FOnGetPropertyTypeCustomizationInstance PropertyTypeLayoutDelegate);

private:
	/** List of registered class that we must unregister when the module shuts down */
	TSet< FName > RegisteredPropertyTypes;
};
