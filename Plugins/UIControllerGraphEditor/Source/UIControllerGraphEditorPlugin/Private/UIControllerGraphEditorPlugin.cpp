// Copyright Epic Games, Inc. All Rights Reserved.

#include "UIControllerGraphEditorPlugin.h"

#include "UIBlackboardKeyCustomization.h"

#define LOCTEXT_NAMESPACE "FUIControllerGraphEditorPlugin"

void FUIControllerGraphEditorPlugin::StartupModule()
{
	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");

	RegisterPropertyTypeCustomizations();

	PropertyModule.NotifyCustomizationModuleChanged();
}

void FUIControllerGraphEditorPlugin::ShutdownModule()
{
	if (FModuleManager::Get().IsModuleLoaded("PropertyEditor"))
	{
		FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");

		// Unregister all structures
		for (auto It = RegisteredPropertyTypes.CreateConstIterator(); It; ++It)
		{
			if (It->IsValid())
			{
				PropertyModule.UnregisterCustomPropertyTypeLayout(*It);
			}
		}

		PropertyModule.NotifyCustomizationModuleChanged();
	}
}

void FUIControllerGraphEditorPlugin::RegisterPropertyTypeCustomizations()
{
	RegisterCustomPropertyTypeLayout("UIBlackboardKeyHandle", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FUIBlackboardKeyCustomizationLayout::MakeInstance));
}

void FUIControllerGraphEditorPlugin::RegisterCustomPropertyTypeLayout(FName PropertyTypeName, FOnGetPropertyTypeCustomizationInstance PropertyTypeLayoutDelegate)
{
	check(PropertyTypeName != NAME_None);

	RegisteredPropertyTypes.Add(PropertyTypeName);

	static FName PropertyEditor("PropertyEditor");
	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>(PropertyEditor);
	PropertyModule.RegisterCustomPropertyTypeLayout(PropertyTypeName, PropertyTypeLayoutDelegate);
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FUIControllerGraphEditorPlugin, UIControllerGraphEditorPlugin)