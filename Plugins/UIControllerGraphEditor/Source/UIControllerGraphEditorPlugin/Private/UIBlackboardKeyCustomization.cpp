// Carl Peters

#include "UIBlackboardKeyCustomization.h"

#include <BehaviorTree/BlackboardData.h>
#include <Editor.h>
#include <Framework/Application/SlateApplication.h>
#include <Framework/MultiBox/MultiBoxBuilder.h>
#include <Widgets/Input/SSearchBox.h>

#define LOCTEXT_NAMESPACE "FUIBlackboardKeyCustomizationLayout"

TSharedPtr<FName> FUIBlackboardKeyCustomizationLayout::InitWidgetContent()
{
	TSharedPtr<FName> InitialValue = MakeShared<FName>();

	FName KeyName;
	const FPropertyAccess::Result RowResult = KeyPropertyHandle->GetValue(KeyName);
	KeyNames.Empty();

	/** Get the properties we wish to work with */
	const UBlackboardData* Blackboard = nullptr;
	BlackboardPropertyHandle->GetValue((UObject*&)Blackboard);

	if (Blackboard != nullptr)
	{
		/** Extract all the row names from the RowMap */
		for (int i = 0; i < Blackboard->Keys.Num(); i++)
		{
			/** Create a simple array of the row names */
			TSharedRef<FName> KeyNameItem = MakeShared<FName>(Blackboard->Keys[i].EntryName);
			KeyNames.Add(KeyNameItem);

			/** Set the initial value to the currently selected item */
			if (Blackboard->Keys[i].EntryName == KeyName)
			{
				InitialValue = KeyNameItem;
			}
		}
	}

	/** Reset the initial value to ensure a valid entry is set */
	if (RowResult != FPropertyAccess::MultipleValues)
	{
		KeyPropertyHandle->SetValue(*InitialValue);
	}

	return InitialValue;
}

void FUIBlackboardKeyCustomizationLayout::CustomizeHeader(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
	this->StructPropertyHandle = InStructPropertyHandle;

	if (StructPropertyHandle->HasMetaData(TEXT("RowType")))
	{
		const FString& RowType = StructPropertyHandle->GetMetaData(TEXT("RowType"));
		RowTypeFilter = FName(*RowType);
	}

	FSimpleDelegate OnBlackboardChangedDelegate = FSimpleDelegate::CreateSP(this, &FUIBlackboardKeyCustomizationLayout::OnBlackboardChanged);
	StructPropertyHandle->SetOnPropertyValueChanged(OnBlackboardChangedDelegate);
	
	HeaderRow
	.NameContent()
	[
		InStructPropertyHandle->CreatePropertyNameWidget(FText::GetEmpty(), FText::GetEmpty(), false)
	];

	//FDataTableRowUtils::AddSearchForReferencesContextMenu(HeaderRow, FExecuteAction::CreateSP(this, &FUIBlackboardKeyCustomizationLayout::OnSearchForReferences));
}

void FUIBlackboardKeyCustomizationLayout::CustomizeChildren(TSharedRef<class IPropertyHandle> InStructPropertyHandle, class IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
	/** Get all the existing property handles */
	BlackboardPropertyHandle = InStructPropertyHandle->GetChildHandle("BlackboardData");
	KeyPropertyHandle = InStructPropertyHandle->GetChildHandle("KeyName");

	if (BlackboardPropertyHandle->IsValidHandle() && KeyPropertyHandle->IsValidHandle())
	{
		/** Queue up a refresh of the selected item, not safe to do from here */
		StructCustomizationUtils.GetPropertyUtilities()->EnqueueDeferredAction(FSimpleDelegate::CreateSP(this, &FUIBlackboardKeyCustomizationLayout::OnBlackboardChanged));

		/** Setup Change callback */
		FSimpleDelegate OnBlackboardChangedDelegate = FSimpleDelegate::CreateSP(this, &FUIBlackboardKeyCustomizationLayout::OnBlackboardChanged);
		BlackboardPropertyHandle->SetOnPropertyValueChanged(OnBlackboardChangedDelegate);

		/** Construct a asset picker widget with a custom filter */
		StructBuilder.AddCustomRow(LOCTEXT("UIBlackboard_BlackboardName", "UI Blackboard"))
			.NameContent()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("UIBlackboard_BlackboardName", "UI Blackboard"))
				.Font(StructCustomizationUtils.GetRegularFont())
			]
			.ValueContent()
			.MaxDesiredWidth(0.0f) // don't constrain the combo button width
			[
				SNew(SObjectPropertyEntryBox)
				.PropertyHandle(BlackboardPropertyHandle)
				.AllowedClass(UBlackboardData::StaticClass())
				.OnShouldFilterAsset(this, &FUIBlackboardKeyCustomizationLayout::ShouldFilterAsset)
			];

		/** Construct a combo box widget to select from a list of valid options */
		StructBuilder.AddCustomRow(LOCTEXT("UIBlackboard_KeyName", "Key Name"))
			.NameContent()
			[
				SNew(STextBlock)
				.Text(LOCTEXT("UIBlackboard_KeyName", "Key Name"))
				.Font(StructCustomizationUtils.GetRegularFont())
			]
			.ValueContent()
			.MaxDesiredWidth(0.0f) // don't constrain the combo button width
			[
				SAssignNew(RowNameComboButton, SComboButton)
				.ToolTipText(this, &FUIBlackboardKeyCustomizationLayout::GetRowNameComboBoxContentText)
				.OnGetMenuContent(this, &FUIBlackboardKeyCustomizationLayout::GetListContent)
				.OnComboBoxOpened(this, &FUIBlackboardKeyCustomizationLayout::HandleMenuOpen)
				.ContentPadding(FMargin(2.0f, 2.0f))
				.ButtonContent()
				[
					SNew(STextBlock)
					.Text(this, &FUIBlackboardKeyCustomizationLayout::GetRowNameComboBoxContentText)
				]
			];
	}
}

void FUIBlackboardKeyCustomizationLayout::HandleMenuOpen()
{
	FSlateApplication::Get().SetKeyboardFocus(SearchBox);
}

void FUIBlackboardKeyCustomizationLayout::OnSearchForReferences()
{
	if (CurrentSelectedItem.IsValid() && !CurrentSelectedItem->IsNone() && BlackboardPropertyHandle.IsValid() && BlackboardPropertyHandle->IsValidHandle())
	{
		UObject* SourceBlackboard;
		BlackboardPropertyHandle->GetValue(SourceBlackboard);
		
		TArray<FAssetIdentifier> AssetIdentifiers;
		AssetIdentifiers.Add(FAssetIdentifier(SourceBlackboard, *CurrentSelectedItem));

		FEditorDelegates::OnOpenReferenceViewer.Broadcast(AssetIdentifiers, FReferenceViewerParams());
	}
}

TSharedRef<SWidget> FUIBlackboardKeyCustomizationLayout::GetListContent()
{
	SAssignNew(RowNameComboListView, SListView<TSharedPtr<FName>>)
		.ListItemsSource(&KeyNames)
		.OnSelectionChanged(this, &FUIBlackboardKeyCustomizationLayout::OnSelectionChanged)
		.OnGenerateRow(this, &FUIBlackboardKeyCustomizationLayout::HandleRowNameComboBoxGenarateWidget)
		.SelectionMode(ESelectionMode::Single);

	// Ensure no filter is applied at the time the menu opens
	OnFilterTextChanged(FText::GetEmpty());

	if (CurrentSelectedItem.IsValid())
	{
		RowNameComboListView->SetSelection(CurrentSelectedItem);
	}

	return SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		.AutoHeight()
		[
			SAssignNew(SearchBox, SSearchBox)
			.OnTextChanged(this, &FUIBlackboardKeyCustomizationLayout::OnFilterTextChanged)
		]
		
		+ SVerticalBox::Slot()
		.FillHeight(1.f)
		[
			SNew(SBox)
			.MaxDesiredHeight(600)
			[
				RowNameComboListView.ToSharedRef()
			]
		];
}

void FUIBlackboardKeyCustomizationLayout::OnBlackboardChanged()
{
	CurrentSelectedItem = InitWidgetContent();
	if (RowNameComboListView.IsValid())
	{
		RowNameComboListView->SetSelection(CurrentSelectedItem);
		RowNameComboListView->RequestListRefresh();
	}
}

TSharedRef<ITableRow> FUIBlackboardKeyCustomizationLayout::HandleRowNameComboBoxGenarateWidget(TSharedPtr<FName> InItem, const TSharedRef<STableViewBase>& OwnerTable)
{
	return
		SNew(STableRow<TSharedPtr<FName>>, OwnerTable)
		[
			SNew(STextBlock).Text(FText::FromName(*InItem))
		];
}

FText FUIBlackboardKeyCustomizationLayout::GetRowNameComboBoxContentText() const
{
	FName RowNameValue;
	const FPropertyAccess::Result RowResult = KeyPropertyHandle->GetValue(RowNameValue);
	if (RowResult == FPropertyAccess::Success)
	{
		if (RowNameValue.IsNone())
		{
			return LOCTEXT("UIBlackboard_None", "None");
		}
		return FText::FromName(RowNameValue);
	}
	else if (RowResult == FPropertyAccess::Fail)
	{
		return LOCTEXT("UIBlackboard_None", "None");
	}
	else
	{
		return LOCTEXT("MultipleValues", "Multiple Values");
	}
}

void FUIBlackboardKeyCustomizationLayout::OnSelectionChanged(TSharedPtr<FName> SelectedItem, ESelectInfo::Type SelectInfo)
{
	if (SelectedItem.IsValid())
	{
		CurrentSelectedItem = SelectedItem;
		KeyPropertyHandle->SetValue(*SelectedItem);

		// Close the combo
		RowNameComboButton->SetIsOpen(false);
	}
}

void FUIBlackboardKeyCustomizationLayout::OnFilterTextChanged(const FText& InFilterText)
{
	FString CurrentFilterText = InFilterText.ToString();

	KeyNames.Empty();

	/** Get the properties we wish to work with */
	const UBlackboardData* Blackboard = nullptr;
	BlackboardPropertyHandle->GetValue((UObject*&)Blackboard);

	TArray<FName> AllRowNames;
	if (Blackboard != nullptr)
	{
		for (int i = 0; i < Blackboard->Keys.Num(); i++)
		{
			AllRowNames.Add(Blackboard->Keys[i].EntryName);
		}

		// Sort the names alphabetically.
		AllRowNames.Sort(FNameLexicalLess());
	}

	for (const FName& RowName : AllRowNames)
	{
		if (CurrentFilterText.IsEmpty() || RowName.ToString().Contains(CurrentFilterText))
		{
			TSharedRef<FName> RowNameItem = MakeShared<FName>(RowName);
			KeyNames.Add(RowNameItem);
		}
	}

	RowNameComboListView->RequestListRefresh();
}

bool FUIBlackboardKeyCustomizationLayout::ShouldFilterAsset(const struct FAssetData& AssetData)
{
	return false;
}

#undef LOCTEXT_NAMESPACE
