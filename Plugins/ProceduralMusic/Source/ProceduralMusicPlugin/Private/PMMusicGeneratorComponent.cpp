#include "PMMusicGeneratorComponent.h"
#include "SynthComponents/EpicSynth1Component.h"
#include "PMMelodyGeneratorInterface.h"


UPMMusicGeneratorComponent::UPMMusicGeneratorComponent(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//ModularSynthComponent = CreateDefaultSubobject<UModularSynthComponent>(TEXT("ModularSynthComponent"));
	//ModularSynthComponent->AttachToComponent(this, FAttachmentTransformRules::KeepRelativeTransform);

	PrimaryComponentTick.bCanEverTick = true;
}

void UPMMusicGeneratorComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bMelodyComplete)
	{
		bMelodyProcessing = false;
		bMelodyComplete = false;
		OnMelodyReadyCB.ExecuteIfBound();
	}
}

void UPMMusicGeneratorComponent::GenerateMelody(FOnMelodyReadyCB OnReadyCB)
{
	if (bMelodyProcessing)
	{
		UE_LOG(LogProceduralMusic, Error, TEXT("GenerateMelody called while generation still in progress. Exiting"));
		return;
	}

	bMelodyProcessing = true;

	if (CurrentMelodyGeneratorActor != nullptr)
	{
		CurrentMelodyGeneratorActor->Destroy();
	}
	CurrentMelodyGeneratorActor = GetWorld()->SpawnActor<AActor>(MelodyGenerators[0]);

	OnMelodyReadyCB = OnReadyCB;

	FOnMelodyTaskCompletionCB OnMelodyCompleteCB = FOnMelodyTaskCompletionCB();
	OnMelodyCompleteCB.BindUFunction(this, "OnMelodyComplete");

	(new FAutoDeleteAsyncTask<MelodyGenerateTask>(Melody, CurrentMelodyGeneratorActor, OnMelodyCompleteCB))->StartBackgroundTask();
}

void UPMMusicGeneratorComponent::OnMelodyComplete()
{
	bMelodyComplete = true;
}

void UPMMusicGeneratorComponent::PlayMelody(UModularSynthComponent* ModularSynth)
{
	if (bIsPlaying)
		return;

	ModularSynthComponent = ModularSynth;

	bIsPlaying = true;

	UpdateMelody();
}

void UPMMusicGeneratorComponent::StopMelody()
{
	bIsPlaying = false;
}

void UPMMusicGeneratorComponent::UpdateMelody()
{
	//while (bIsPlaying)
	{
		FModularSynthPreset Preset = PresetBank->Presets[Melody->Presets[PitchIndex]].Preset;
		float Note = Melody->Notes[PitchIndex];
		float Duration = Melody->Durations[DurationIndex];
		float Velocity = 127 * (Melody->Volumes[VelocityIndex] / 2);
		//AudioClip sample = melody.sounds[sampleindex];

		//AudioSource a_s;
		//if (Melody->bSpecifyChannels)
		//{
		//	a_s = audiosources[Melody->channels[ChannelIndex]];
		//}
		//else
		//{
		//	a_s = audiosources[ChannelIndex];
		//}

		//a_s.pitch = Pitch;
		//a_s.volume = Velocity;
		//a_s.PlayOneShot(Sample);
		//ModularSynthComponent->NoteOn(60 /*Note*/, 127/*Velocity*/, 0.1/*Duration*/);
		ModularSynthComponent->SetSynthPreset(Preset);
		ModularSynthComponent->NoteOn(Note, Velocity, Duration);

		PitchIndex = (PitchIndex + 1) % Melody->Notes.Num();
		DurationIndex = (DurationIndex + 1) % Melody->Durations.Num();
		VelocityIndex = (VelocityIndex + 1) % Melody->Volumes.Num();

		UE_LOG(LogProceduralMusic, Log, TEXT("Playing Note: %f Velocity: %f Duration: %f"), Note, Velocity, Duration);

		if (Duration <= 0.0f)
			UpdateMelody();
		else
			GetWorld()->GetTimerManager().SetTimer(PlaybackTimerHandle, this, &UPMMusicGeneratorComponent::UpdateMelody, Duration, false);

		//SampleIndex = (SampleIndex + 1) % Melody->Sounds.Num();
		//if (Melody->bSpecifyChannels)
		//{
		//	ChannelIndex = (ChannelIndex + 1) % Melody->Channels.Count;
		//}
		//else
		//{
		//	ChannelIndex = (ChannelIndex + 1) % audiosources.Length;
		//}
	}
}

//=======================================================================

void MelodyGenerateTask::DoWork()
{
	if (MelodyGenerator != nullptr &&
		MelodyGenerator->GetClass()->ImplementsInterface(UPMMelodyGeneratorInterface::StaticClass()))
	{
		IPMMelodyGeneratorInterface::Execute_GenerateMelody(MelodyGenerator, Melody);
	}
	else
	{
		UE_LOG(LogProceduralMusic, Error, TEXT("Actor does not implement LevelRandomiserInterface"));
	}

	OnCompletion.ExecuteIfBound();
}