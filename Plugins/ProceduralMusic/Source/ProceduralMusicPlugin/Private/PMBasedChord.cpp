// Carl Peters


#include "PMBasedChord.h"
#include "PMScale.h"
#include "IProceduralMusicPlugin.h"

FPMBasedChord::FPMBasedChord(FString Desc, TArray<FPMScale> ChordList)
{
	FString Suffix = "";
	bool bFound = false;
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EPMNote"), true);
	const int32 NumNotes = EnumPtr->NumEnums() - 1;
	for (int32 i = 0; i < NumNotes; i++)
	{
		FString NoteName = EnumPtr->GetNameStringByIndex(i);

		if (Desc.StartsWith(NoteName))
		{
			if (Desc == NoteName)
			{
				Desc += "major";
			}
			bFound = true;
			PitchClass = i;
			Suffix = Desc.RightChop(NoteName.Len());
			break;
		}
	}

	if (!bFound)
	{
		UE_LOG(LogProceduralMusic, Error, TEXT("ERROR ERROR BASEDCHORD NOT RIGHT - desc = %s"), *Desc);
		return;
	}

	bool bFoundChord = false;
	for (int i = 0; i < ChordList.Num(); i++)
	{
		FPMScale Chord = ChordList[i];
		if (Suffix == Chord.Name)
		{
			ChordIndex = i;
			bFoundChord = true;
			break;
		}
	}


	if (!bFoundChord)
	{
		UE_LOG(LogProceduralMusic, Error, TEXT("ERROR ERROR BASEDCHORD NOT RIGHT - desc = %s | %s"), *Desc, *Suffix);
		return;
	}

}

int32 UPMBasedChordLibrary::GetNote(const FPMBasedChord BasedChord, int32 Lower, int32 Upper, int32 Index, const TArray<FPMScale>& ChordList)
{
	const FPMScale Chord = ChordList[BasedChord.ChordIndex];
	//pretend in c and get lower note
	Lower -= BasedChord.PitchClass;
	Upper -= BasedChord.PitchClass;
	TArray<int32> ValidNotes = TArray<int32>();
	for (int32 i = Lower; i <= Upper; i++)
	{
		if (Chord.Notes[((i % 12) + 12) % 12])
		{
			ValidNotes.Add(i);
		}
	}
	const int32 ValidNoteNum = ValidNotes.Num();
	int32 Note = ValidNotes[((Index % ValidNoteNum) + ValidNoteNum) % ValidNoteNum];

	// transform to proper key
	Note += BasedChord.PitchClass;

	return Note;
}

int32 UPMBasedChordLibrary::GetRandomNote(const FPMBasedChord BasedChord, int32 Lower, int32 Upper, const TArray<FPMScale>& ChordList)
{
	const FPMScale Chord = ChordList[BasedChord.ChordIndex];
	//pretend in c and get lower note
	Lower -= BasedChord.PitchClass;
	Upper -= BasedChord.PitchClass;
	TArray<int32> ValidNotes = TArray<int32>();
	for (int32 i = Lower; i <= Upper; i++)
	{
		if (Chord.Notes[((i % 12) + 12) % 12])
		{
			ValidNotes.Add(i);
		}
	}

	int32 Note = ValidNotes[FMath::RandRange(0, ValidNotes.Num()-1)];

	// transform to proper key
	Note += BasedChord.PitchClass;

	return Note;
}

FPMBasedScale UPMBasedChordLibrary::FindScale(const FPMBasedChord BasedChord, const TArray<FPMScale>& ScaleList, const TArray<FPMScale>& ChordList, int32 KeyPitchClass)
{
	TArray<FPMBasedScale> CompatibleScales = TArray<FPMBasedScale>();

	bool bFoundScale = false;
	for (int t = 0; t < 12; t++)
	{
		const int32 ScaleNum = ScaleList.Num();
		for (int i = 0; i < ScaleNum; i++)
		{
			FPMBasedScale Scale = FPMBasedScale(KeyPitchClass + t, i);
			if (HasChord(BasedChord, ScaleList, ChordList, Scale))
			{
				CompatibleScales.Add(Scale);
				bFoundScale = true;
			}
		}
		if (bFoundScale)
			break;
	}
	//Debug.Log(Time.time.ToString() + " compatiblescales = " + compatiblescales.Count.ToString());
	return CompatibleScales[FMath::RandRange(0, CompatibleScales.Num()-1)];
}

const bool UPMBasedChordLibrary::HasChord(const FPMBasedChord BasedChord, const TArray<FPMScale>& ScaleList, const TArray<FPMScale>& ChordList, FPMBasedScale Scale)
{
	//chordnote+transform is in this scale's space
	bool Contained = true;

	//Debug.Log( this.Print(scales) + "\n" + chord.Print(chords) );

	for (int32 i = 0, NoteNum = ChordList[BasedChord.ChordIndex].Notes.Num(); i < NoteNum; i++)
	{
		//for each note in the chord
		if (ChordList[BasedChord.ChordIndex].Notes[i])
		{
			if (!UPMBasedScaleLibrary::HasNote(Scale, ScaleList, i + BasedChord.PitchClass))
			{
				//Debug.Log("disagree at note " + i.ToString());
				Contained = false;
				break;
			}
		}
	}

	return Contained;
}