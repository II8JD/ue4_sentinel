// Carl Peters


#include "PMMelodyGeneratorJazzy.h"
#include "PMProgression.h"
#include "PMBasedChord.h"
#include "PMGenericMelodyDataAsset.h"

const int32 APMMelodyGeneratorJazzy::INVALID_NOTE = 666;

// Sets default values
APMMelodyGeneratorJazzy::APMMelodyGeneratorJazzy()
{

}

void APMMelodyGeneratorJazzy::GenerateMelody_Implementation(UPMGenericMelodyDataAsset* MelodyDataAsset)
{
	TArray<int32> Melody = TArray<int32>();
	TArray<int32> Accompaniment = TArray<int32>();
	
	BuildProgressions(Melody, Accompaniment);

	TArray<int32> Presets;
	for (int i = 0; i < InstrumentBank->Presets.Num(); i++)
		Presets.Add(i);

	const int32 LeadInstrument = Presets[FMath::RandRange(0, Presets.Num() - 1)];
	Presets.RemoveAt(LeadInstrument);

	const int32 AccompanimentInstrument = Presets[FMath::RandRange(0, Presets.Num() - 1)];

	BuildMelody(MelodyDataAsset, Melody, 60, LeadInstrument, Accompaniment, 48, AccompanimentInstrument);
}

void APMMelodyGeneratorJazzy::BuildProgressions(TArray<int32>& Melody, TArray<int32>& Accompaniment)
{
	FPMProgression Progression = ProgressionDataAsset->Progressions[FMath::RandRange(0, ProgressionDataAsset->Progressions.Num()-1)];
	int32 KeyPitchClass = FMath::RandRange(0, 12);
	int32 TimeSignature = FMath::RandRange(3, 11);

	int32 BarHalfway = CalcHalfway(TimeSignature);

	bool bIsFirstHalf = true;
	int32 LastNote = -1000;
	int32 AccompanimentType = FMath::RandRange(0, 9);
	float MothEaten = FMath::RandRange(0.25f, 1.0f);
	UE_LOG(LogTemp, Log, TEXT("Moth = %f"), MothEaten);

	TArray<FVector2D> Ranges;
	Ranges.Add(FVector2D(-12, 0));
	Ranges.Add(FVector2D(0, 12));
	Ranges.Add(FVector2D(-6, 6));
	Ranges.Add(FVector2D(6, 18));
	Ranges.Add(FVector2D(-18, -6));

	int32 LeadRange = FMath::RandRange(0, Ranges.Num()-1);
	int32 AccompanimentRange = FMath::RandRange(0,Ranges.Num()-1);

	for (int32 i = 0; i < Progression.Chords.Num(); i++)
	{
		int32 NoteCount = bIsFirstHalf ? BarHalfway - 1 : TimeSignature - (BarHalfway - 1);
		for (int32 j = 0; j < NoteCount; j++)
		{
			if (j == 0)
			{
				Accompaniment.Add(Progression.Chords[i].PitchClass);
			}
			else
			{
				switch (AccompanimentType)
					{
					case 0:
						Accompaniment.Add(INVALID_NOTE);
						break;
					case 1:
						Accompaniment.Add(INVALID_NOTE);
						break;
					case 2:
						Accompaniment.Add(INVALID_NOTE);
						break;
					case 3:
						Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, j % 3, ChordsDataAsset->Scales));
						break;
					case 4:
						Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, (2 - j) % 3, ChordsDataAsset->Scales));
						break;
					case 5:
						Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, j % 4, ChordsDataAsset->Scales));
						break;
					case 6:
						Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, (3 - j) % 4, ChordsDataAsset->Scales));
						break;
					case 7:
						if (j == 1)
						{
							Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, j % 3, ChordsDataAsset->Scales));
						}
						else
						{
							Accompaniment.Add(INVALID_NOTE);
						}
						break;
					case 8:
						if (j % 2 == 0)
						{
							Accompaniment.Add(UPMBasedChordLibrary::GetNote(Progression.Chords[i], Ranges[AccompanimentRange].X, Ranges[AccompanimentRange].Y, j % 3, ChordsDataAsset->Scales));
						}
						else
						{
							Accompaniment.Add(INVALID_NOTE);
						}
						break;
					}
				}

				int32 Note = UPMBasedChordLibrary::GetRandomNote(Progression.Chords[i], Ranges[LeadRange].X, Ranges[LeadRange].Y, ChordsDataAsset->Scales);
				if (Note == LastNote)
					Note = UPMBasedChordLibrary::GetRandomNote(Progression.Chords[i], Ranges[LeadRange].X, Ranges[LeadRange].Y, ChordsDataAsset->Scales);

				if (j == 0 || FMath::FRand() < MothEaten)
				{
					Melody.Add(Note);
				}
				else
				{
					Melody.Add(INVALID_NOTE);
				}

				LastNote = Note;
			}

			bIsFirstHalf = !bIsFirstHalf;
		}

		// do second pass to add passing notes

		TArray<FPMBasedScale> ChordScales = TArray<FPMBasedScale>();
		for (int32 i = 0; i < Progression.Chords.Num(); i++)
			ChordScales.Add(UPMBasedChordLibrary::FindScale(Progression.Chords[i], ScalesDataAsset->Scales, ChordsDataAsset->Scales, KeyPitchClass));

		const float PassingChance = FMath::RandRange(0.6f,0.1f);
		const float AuxilliaryChance = FMath::RandRange(0.6f,0.1f);
		const float AuxilliaryUpperChance = FMath::FRand();
		const float ExtraChordChance = FMath::RandRange(0.0f,0.25f);
		
		for (int32 i = 0; i < (Melody.Num() - 1); i += 2)
		{
			const int32 NoteA = Melody[i];
			const int32 NoteB = Melody[i + 1];

			if (NoteA == INVALID_NOTE || NoteB == INVALID_NOTE)
			{
				Melody.Insert(INVALID_NOTE, i + 1);
				continue;
			}

			const int32 Bar = (i / 2) / TimeSignature;
			const int32 Beat = (i / 2) - Bar * TimeSignature;
			const int32 ChordIndex = Beat < BarHalfway ? 2 * Bar : 2 * Bar + 1;

			//Debug.Log(Random.value.ToString() + "chord index vs chord length " + bar.ToString()+"-"+beat.ToString()+" \t "+chord_index.ToString() + "," +  progression.chords.Count.ToString() + 
			//	" \t " + "barpos :" + beat.ToString() + "," + bar_halfway.ToString());
//			BasedChord chord = progression.chords[chord_index];
			FPMBasedScale Scale = ChordScales[ChordIndex];

			const int32 ScaleDistance = UPMBasedScaleLibrary::ScaleDistance(Scale, ScalesDataAsset->Scales, NoteA, NoteB);
			if (FMath::FRand() < ExtraChordChance)
			{
				const int32 Lower = FMath::Min(NoteA, NoteB);
				const int32 Upper = FMath::Max(NoteA, NoteB);
				const int32 Note = UPMBasedScaleLibrary::RandomNoteBetween(Scale, ScalesDataAsset->Scales, Lower - 5, Upper + 5);
				if (Note != NoteA && Note != NoteB)
				{
					Melody.Insert(Note, i + 1);
				}
				else
				{
					Melody.Insert(INVALID_NOTE, i + 1);
				}
			}
			else if (ScaleDistance == 2 && FMath::FRand() <= PassingChance)
			{
				const int32 Note = UPMBasedScaleLibrary::RandomNoteBetween(Scale, ScalesDataAsset->Scales, NoteA, NoteB);
				Melody.Insert(Note, i + 1);
			}
			else if (ScaleDistance == 0 && FMath::FRand() <= AuxilliaryChance)
			{
				const int32 Auxilliary = UPMBasedScaleLibrary::AuxilliaryNote(Scale, ScalesDataAsset->Scales, NoteA, FMath::FRand() < AuxilliaryUpperChance);
				Melody.Insert(Auxilliary, i + 1);
			}
			else
			{
				Melody.Insert(INVALID_NOTE, i + 1);
			}
		}
		Melody.Add(INVALID_NOTE);
		
		UE_LOG(LogTemp, Log, TEXT("melody length : %d - %d"), Melody.Num(), Accompaniment.Num());
}

int32 APMMelodyGeneratorJazzy::CalcHalfway(int32 TimeSignature)
{
	int32 Halfway;
	if (TimeSignature % 2 == 0)
	{
		Halfway = TimeSignature / 2;
		if (FMath::FRand() < 0.5)
		{
			if (FMath::FRand() < 0.5)
			{
				Halfway++;
			}
			else
			{
				Halfway--;
			}
		}
	}
	else
	{
		if (FMath::FRand() < 0.5)
		{
			Halfway = TimeSignature / 2;
		}
		else
		{
			Halfway = TimeSignature / 2 + 1;
		}
	}
	return Halfway;
}

void APMMelodyGeneratorJazzy::BuildMelody(UPMGenericMelodyDataAsset* MelodyDataAsset, 
	TArray<int32>& Melody, const int32 MelodyRoot, const int32 LeadInstrument,
	TArray<int32>& Accompaniment, const int32 AccompanimentRoot, const int32 AccompanimentInstrument)
{
	float TempoBpm = FMath::RandRange(60.0f, 160.0f);
	float AccompanyVol = FMath::RandRange(0.4f, 1.0f);
	int32 DottingBehaviour = FMath::RandRange(0, 5);//0,1,2 - normal, 3 - dotted, 4 - triplet
	float Half;
	if (DottingBehaviour <= 2)
	{
		Half = 60.0f / 2.0f;
	}
	else if (DottingBehaviour == 3)
	{
		Half = 60.0f * 3.0f / 4.0f;
	}
	else //if (dottingbehaviour==4)
	{
		Half = 60.0f * 2.0f / 3.0f;
	}

	MelodyDataAsset->Presets.Empty();
	MelodyDataAsset->Notes.Empty();
	MelodyDataAsset->Volumes.Empty();
	MelodyDataAsset->Durations.Empty();

	for (int32 i = 0; i < Melody.Num(); i += 2)
	{
		const int32 HalfIndex = i * 0.5f;
		if (HalfIndex < Accompaniment.Num() &&
			Accompaniment[HalfIndex] != INVALID_NOTE)
		{
			MelodyDataAsset->Presets.Add(AccompanimentInstrument);
			MelodyDataAsset->Notes.Add(AccompanimentRoot + Accompaniment[HalfIndex]);
			MelodyDataAsset->Volumes.Add(AccompanyVol);
			MelodyDataAsset->Durations.Add(0);
		}
		if (Melody[i + 1] == INVALID_NOTE)
		{
			if (Melody[i] == INVALID_NOTE)
			{
				MelodyDataAsset->Durations[MelodyDataAsset->Durations.Num() - 1] += 60.0f / TempoBpm;
			}
			else
			{
				MelodyDataAsset->Presets.Add(LeadInstrument);
				MelodyDataAsset->Volumes.Add(1.0F);
				MelodyDataAsset->Notes.Add(MelodyRoot + Melody[i]);
				MelodyDataAsset->Durations.Add(60.0f / TempoBpm);
			}
		}
		else
		{
			MelodyDataAsset->Presets.Add(LeadInstrument);
			MelodyDataAsset->Notes.Add(MelodyRoot + Melody[i]);
			MelodyDataAsset->Volumes.Add(1.0F);
			MelodyDataAsset->Durations.Add(Half / TempoBpm);

			MelodyDataAsset->Presets.Add(LeadInstrument);
			MelodyDataAsset->Notes.Add(MelodyRoot + Melody[i + 1]);
			MelodyDataAsset->Volumes.Add(1.0F);
			MelodyDataAsset->Durations.Add((60.0f - Half) / TempoBpm);
		}
	}
}