// Carl Peters


#include "PMBasedScale.h"
#include "PMBasedChord.h"
#include "PMScale.h"

const bool UPMBasedScaleLibrary::HasNote(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 Note)
{
	Note -= BasedScale.PitchClass;
	return ScaleList[BasedScale.ScaleIndex].Notes[((Note % 12) + 12) % 12];
}

const int32 UPMBasedScaleLibrary::ScaleDistance(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, int32 b)
{
	//convert to scale space
	a -= BasedScale.PitchClass;
	b -= BasedScale.PitchClass;
	if (a > b)
	{
		int32 c;
		c = a;
		a = b;
		b = c;
	}

	int32 dist = 0;
	//count number of 1s between a and b (inclusive of b)
	for (int i = a + 1; i <= b; i++)
	{
		if (ScaleList[BasedScale.ScaleIndex].Notes[(((i) % 12) + 12) % 12])
			dist++;
	}
	return dist;
}

const int32 UPMBasedScaleLibrary::AuxilliaryNote(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, bool bUpper)
{
	a -= BasedScale.PitchClass;
	while (true)
	{
		if (bUpper)
		{
			a++;
		}
		else
		{
			a--;
		}

		if (UPMBasedScaleLibrary::HasNote(BasedScale, ScaleList, a))
			return a + BasedScale.PitchClass;
	}
}

const int32 UPMBasedScaleLibrary::RandomNoteBetween(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, int32 b)
{
	//convert to scale space
	a -= BasedScale.PitchClass;
	b -= BasedScale.PitchClass;
	if (a > b)
	{
		int32 c;
		c = a;
		a = b;
		b = c;
	}

	TArray<int32> NotesBetween = TArray<int32>();
	//count number of 1s between a and b (inclusive of b)
	for (int i = a + 1; i < b; i++)
	{
		if (ScaleList[BasedScale.ScaleIndex].Notes[(((i) % 12) + 12) % 12])
			NotesBetween.Add(i + BasedScale.PitchClass);
	}
	return NotesBetween[FMath::RandRange(0, NotesBetween.Num()-1)];
}