// Carl Peters


#include "PMProgression.h"
#include "PMScale.h"
#include "PMBasedChord.h"

FPMProgression::FPMProgression()
{
}

FPMProgression::FPMProgression(FString* Description, TArray<FPMScale>& ChordList)
{
	//Dm7-G7,Cmaj7
	TArray<FString> BarsList;
	Description->ParseIntoArray(BarsList, TEXT("|"), true);

	TArray<FString> ChordsStr = TArray<FString>();

	for(FString Bar : BarsList)
	{
		if (Bar.Find(",") >= 0)
		{
			TArray<FString> SubBar;
			Bar.ParseIntoArray(SubBar, TEXT(","), true);
			ChordsStr.Add(SubBar[0]);
			ChordsStr.Add(SubBar[1]);
		}
		else
		{
			ChordsStr.Add(Bar);
			ChordsStr.Add(Bar);
		}
	}

	Chords = TArray<FPMBasedChord>();
	for(FString ChordDesc : ChordsStr)
	{
		ChordDesc.TrimStartAndEndInline();
		if (ChordDesc != "")
		{
			Chords.Add(FPMBasedChord(ChordDesc, ChordList));
		}
	}

	while (Chords.Num() < ProgressionLength)
	{
		TArray<FPMBasedChord> ChordCopy = TArray<FPMBasedChord>(Chords);
		Chords.Append(ChordCopy);
	}
}

#if WITH_EDITOR
void UPMProgressionDataAsset::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Progressions = TArray<FPMProgression>();
	//TArray<FString> Lines;
	//RawScales.ParseIntoArray(Lines, TEXT("\n"), true);
	for(FString l : RawScales)
	{
		if (l.TrimEnd() != "")
		{
			Progressions.Add(FPMProgression(&l, ChordsDataAsset->Scales));
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif