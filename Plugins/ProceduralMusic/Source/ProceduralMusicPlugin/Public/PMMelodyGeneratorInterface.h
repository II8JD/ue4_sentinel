// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "PMMelodyGeneratorInterface.generated.h"


// This class does not need to be modified.
UINTERFACE(Blueprintable, MinimalAPI)
class UPMMelodyGeneratorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROCEDURALMUSICPLUGIN_API IPMMelodyGeneratorInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void GenerateMelody(UPMGenericMelodyDataAsset* MelodyDataAsset);
};
