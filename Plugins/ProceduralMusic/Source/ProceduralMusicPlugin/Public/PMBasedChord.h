// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "PMBasedScale.h"
#include "PMBasedChord.generated.h"

struct FPMBasedScale;
struct FPMScale;

UENUM(BlueprintType)
enum class EPMNote : uint8
{
	C,
	Db,
	D,
	Eb,
	E,
	F,
	Gb,
	G,
	Ab,
	A,
	Bb,
	B
};

/**
 *
 */
USTRUCT(BlueprintType)
struct PROCEDURALMUSICPLUGIN_API FPMBasedChord
{
	GENERATED_BODY()

public:
	FPMBasedChord() : PitchClass(0), ChordIndex(0) {}

	FPMBasedChord(FString Desc, TArray<FPMScale> ChordList);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	int32 PitchClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	int32 ChordIndex;
};

UCLASS(BlueprintType)
class PROCEDURALMUSICPLUGIN_API UPMBasedChordLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	// Geometric calculations
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static int32 GetNote(const FPMBasedChord BasedChord, int32 Lower, int32 Upper, int32 Index, const TArray<FPMScale>& ChordList);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static int32 GetRandomNote(const FPMBasedChord BasedChord, int32 Lower, int32 Upper, const TArray<FPMScale>& ChordList);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static FPMBasedScale FindScale(const FPMBasedChord BasedChord, const TArray<FPMScale>& ScaleList, const TArray<FPMScale>& ChordList, int32 KeyPitchClass);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static const bool HasChord(const FPMBasedChord BasedChord, const TArray<FPMScale>& ScaleList, const TArray<FPMScale>& ChordList, FPMBasedScale Scale);
};