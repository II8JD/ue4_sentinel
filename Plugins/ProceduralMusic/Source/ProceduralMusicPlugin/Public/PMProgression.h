// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "PMProgression.generated.h"

class UPMBasedChord;
struct FPMScale;

/**
 *
 */
USTRUCT(BlueprintType)
struct PROCEDURALMUSICPLUGIN_API FPMProgression
{
	GENERATED_BODY()

public:
	FPMProgression();

	FPMProgression(FString* Description, TArray<FPMScale>& ChordList);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	int32 ProgressionLength = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	TArray<FPMBasedChord> Chords;
};

/**
 * 
 */
UCLASS(BlueprintType)
class PROCEDURALMUSICPLUGIN_API UPMProgressionDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	UPMScaleDataAsset* ChordsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	TArray<FString> RawScales;

	UPROPERTY(BlueprintReadOnly, Category = "Music Generator")
	TArray<FPMProgression> Progressions;
};
