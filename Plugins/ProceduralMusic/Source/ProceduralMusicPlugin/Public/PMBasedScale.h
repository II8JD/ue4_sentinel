// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "PMScale.h"
#include "PMBasedScale.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct PROCEDURALMUSICPLUGIN_API FPMBasedScale
{
	GENERATED_BODY()

public:
	FPMBasedScale() : PitchClass(0), ScaleIndex(0) {}

	FPMBasedScale(int32 PitchClass, int32 ScaleIndex) : PitchClass(PitchClass), ScaleIndex(ScaleIndex) {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	int32 PitchClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	int32 ScaleIndex;
};

UCLASS(BlueprintType)
class PROCEDURALMUSICPLUGIN_API UPMBasedScaleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static const bool HasNote(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 Note);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static const int32 ScaleDistance(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, int32 b);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static const int32 AuxilliaryNote(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, bool bUpper);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Music Generator")
	static const int32 RandomNoteBetween(const FPMBasedScale BasedScale, const TArray<FPMScale>& ScaleList, int32 a, int32 b);
};