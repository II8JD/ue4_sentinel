// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"
#include "PMMelodyGeneratorBase.generated.h"

UCLASS()
class PROCEDURALMUSICPLUGIN_API APMMelodyGeneratorBase : public AActor
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	APMMelodyGeneratorBase();

public:
	UFUNCTION(BlueprintImplementableEvent)
	void GenerateMelody(class UPMGenericMelodyDataAsset* MelodyDataAsset);
};
