// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Curves/CurveVector.h"
#include "PMGenericMelodyDataAsset.generated.h"

/**
 *
 */
UCLASS(BlueprintType)
class PROCEDURALMUSICPLUGIN_API UPMGenericMelodyDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	/** */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<int32> Presets;

	/** */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<float> Notes;

	/** */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<float> Durations;

	/** */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<float> Volumes;
};
