// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "PMScale.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct PROCEDURALMUSICPLUGIN_API FPMScale
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	TArray<bool> Notes;
};

UCLASS(BlueprintType)
class PROCEDURALMUSICPLUGIN_API UPMScaleDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	/** */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<FPMScale> Scales;
};