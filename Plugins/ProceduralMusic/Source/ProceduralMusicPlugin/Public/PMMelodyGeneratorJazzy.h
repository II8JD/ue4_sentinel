// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GameFramework/Actor.h"
#include "UObject/ObjectMacros.h"
#include "PMMelodyGeneratorInterface.h"
#include "SynthComponents/EpicSynth1Component.h"
#include "PMMelodyGeneratorJazzy.generated.h"

UCLASS()
class PROCEDURALMUSICPLUGIN_API APMMelodyGeneratorJazzy : public AActor, public IPMMelodyGeneratorInterface
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	APMMelodyGeneratorJazzy();

	void GenerateMelody_Implementation(class UPMGenericMelodyDataAsset* MelodyDataAsset) override;

protected:
	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void BuildProgressions(TArray<int32>& Melody, TArray<int32>& Accompaniment);

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	int32 CalcHalfway(int32 TimeSignature);

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void BuildMelody(UPMGenericMelodyDataAsset* MelodyDataAsset, 
		TArray<int32>& Melody, const int32 MelodyRoot, const int32 LeadInstrument,
		TArray<int32>& Accompaniment, const int32 AccompanimentRoot, const int32 AccompanimentInstrument);

	FORCEINLINE const float NoteToFreq(int32 n) const
	{
		return FMath::Pow(2.0f, (float)n / 12.0f);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	class UPMProgressionDataAsset* ProgressionDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	UPMScaleDataAsset* ScalesDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	UPMScaleDataAsset* ChordsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Music Generator")
	class UModularSynthPresetBank* InstrumentBank = nullptr;

	static const int32 INVALID_NOTE;
};
