#pragma once

#include "Components/ActorComponent.h"
#include "PMMusicGeneratorComponent.generated.h"

DECLARE_DYNAMIC_DELEGATE(FOnMelodyReadyCB);
DECLARE_DELEGATE(FOnMelodyTaskCompletionCB);

UCLASS(BlueprintType, Blueprintable, hidecategories = (Object, LOD, Physics, Collision), editinlinenew, meta = (BlueprintSpawnableComponent), ClassGroup = ProceduralMusic)
class PROCEDURALMUSICPLUGIN_API UPMMusicGeneratorComponent : public USceneComponent
{
	GENERATED_UCLASS_BODY()

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void GenerateMelody(FOnMelodyReadyCB OnReadyCB);

protected:
	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void OnMelodyComplete();

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void PlayMelody(class UModularSynthComponent* ModularSynth);

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void StopMelody();

	UFUNCTION(BlueprintCallable, Category = "Music Generator")
	void UpdateMelody();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	class UModularSynthPresetBank* PresetBank = nullptr;

	//UPROPERTY(VisibleAnywhere, meta=(AllowPrivateAccess="true"))
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	class UModularSynthComponent* ModularSynthComponent;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (MustImplement = "UPMMelodyGenerator"))
	TArray<TSubclassOf<AActor>> MelodyGenerators;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UPMGenericMelodyDataAsset* Melody;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bIsPlaying = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bMelodyComplete = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bMelodyProcessing = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 PitchIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 DurationIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 VelocityIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 SampleIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 ChannelIndex = 0;

	/** Handle for playback */
	FTimerHandle PlaybackTimerHandle;

	FOnMelodyReadyCB OnMelodyReadyCB;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	class AActor* CurrentMelodyGeneratorActor = nullptr;
	
};

//=======================================================================

class MelodyGenerateTask : public FNonAbandonableTask
{
public:
	MelodyGenerateTask(UPMGenericMelodyDataAsset* Melody, UObject* MelodyGenerator, FOnMelodyTaskCompletionCB OnCompletion) :
		Melody{ Melody },
		MelodyGenerator{ MelodyGenerator },
		OnCompletion{ OnCompletion }{}

	//required bu UE4
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(MelodyGenerateTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork();

private:
	UPMGenericMelodyDataAsset* Melody;
	UObject* MelodyGenerator;
	FOnMelodyTaskCompletionCB OnCompletion;
};