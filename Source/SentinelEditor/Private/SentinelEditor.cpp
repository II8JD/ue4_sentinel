#include "SentinelEditor.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FSentinelEditorModule, SentinelEditor);

DEFINE_LOG_CATEGORY(SentinelEditor)

#define LOCTEXT_NAMESPACE "SentinelEditor"

void FSentinelEditorModule::StartupModule()
{
	UE_LOG(SentinelEditor, Warning, TEXT("SentinelEditor: Log Started"));
}

void FSentinelEditorModule::ShutdownModule()
{
	UE_LOG(SentinelEditor, Warning, TEXT("ScentinelEditor: Log Ended"));
}

#undef LOCTEXT_NAMESPACE