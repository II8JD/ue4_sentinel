// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "GameFramework/Actor.h"
#include "SALevelRandomiserInterface.h"
#include "UObject/ObjectMacros.h"
#include "SALevelRandomiserBase.generated.h"

UCLASS()
class ASALevelRandomiserBase : public AActor
{
	GENERATED_BODY()
public:	
	// Sets default values for this actor's properties
	ASALevelRandomiserBase();

public:	
	UFUNCTION(BlueprintImplementableEvent)
	void GenerateHeights(const int32 Seed, const FSATerrainData& TerrainData, ASATerrainActor* TerrainActor);
};
