// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include <UObject/Interface.h>

#include "DataAssets/SALevelDataAsset.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "ProceduralMeshComponent.h"

#include "SALevelRandomiserInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable, MinimalAPI)
class USALevelRandomiserInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ISALevelRandomiserInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void GenerateHeights(const int32 Seed, const FSATerrainData& TerrainData, ASATerrainActor* TerrainActor);
};
