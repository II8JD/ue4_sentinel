// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SAWorldAlignScreenActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class USAWorldAlignScreenActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USAWorldAlignScreenActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void OnViewportResized(FViewport* Viewport, uint32 Unused);

	UFUNCTION()
	void PositionObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RadiusScale = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (UIMin = 0.0, UIMax = 1.0))
	float HorizontalOffset = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (UIMin = 0.0, UIMax = 1.0))
	float VerticalOffset = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FVector OffsetAngle;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
