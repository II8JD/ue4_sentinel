// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SASynthSourceActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class USASynthSourceActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USASynthSourceActorComponent();

	UFUNCTION()
	void PlayClip();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	class UModularSynthComponent* ModularSynthComponent;
};
