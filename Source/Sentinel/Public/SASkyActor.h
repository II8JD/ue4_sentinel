// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include <ProceduralMeshComponent.h>

#include "Utility/SAColourApplicatorInterface.h"

#include "SASkyActor.generated.h"

UCLASS()
class ASASkyActor : public AActor, public ISAColourApplicatorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASASkyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void OnConstruction(const FTransform& Transform) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Level Setup")
	void RefreshColours();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UExponentialHeightFogComponent* FogComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USkyAtmosphereComponent* SkyAtmosphereComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USkyLightComponent* SkyLightComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UDirectionalLightComponent* Sun = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UDirectionalLightComponent* Moon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class USAUVSphere* SkySphereMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* SkyMaterial;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite)
	class UMaterialInstanceDynamic* DynamicSkyMaterial;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 SkySphereStacks = 30;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int32 SkySphereSlices = 30;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	float SkySphereRadius = 1000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float SkySubdivisions = 20.0f;

	UPROPERTY()
	class USAIcosphere* UnitSphere = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTextureRenderTarget2D* CloudRenderTarget = nullptr;

	UPROPERTY()
	bool bGenerateCalled = false;

	//used to generate clod noise on launch
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* CloudMaterial = nullptr;

	UFUNCTION()
	void OnSkySphereGenerated();

	UFUNCTION()
	void CacheCloudTextures();
};
