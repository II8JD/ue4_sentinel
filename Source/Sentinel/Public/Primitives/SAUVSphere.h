// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <ProceduralMeshComponent.h>

#include "SAUVSphere.generated.h"

DECLARE_DELEGATE(FTaskCompletionCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUVSphereCompletionCB);

UCLASS()
class USAUVSphere : public UProceduralMeshComponent
{
    GENERATED_UCLASS_BODY()

public:
    UFUNCTION()
    void MakeUVSphereAsync(const int32 _segments = 75, const bool _bInverse = false);

    UFUNCTION()
    void MakeUVSphere(const int32 _segments = 75, const bool _bInverse = false);

    UPROPERTY(BlueprintAssignable)
    FUVSphereCompletionCB CompletionCB;

protected:
    UFUNCTION()
    void OnCalculationsComplete();

    UFUNCTION()
    void GenerateVertices();

    UFUNCTION()
    void GenerateTriangles();

    UFUNCTION()
    void GenerateUVs();

private:
    UPROPERTY()
    TArray<FVector> Vertices;

    UPROPERTY()
    TArray<int32> Triangles;

    UPROPERTY()
    TArray<FVector2D> UVMapping;

    UPROPERTY()
    int32 Segments;

    UPROPERTY()
    bool bInverse;
};

//=======================================================================

class UVSphereGenerateTask : public FNonAbandonableTask
{
public:
    UVSphereGenerateTask(const int32 Segments, const bool bInverse, USAUVSphere* UVSphere, FTaskCompletionCB OnCompletion) :
        Segments{ Segments },
        bInverse{ bInverse },
        UVSphere{ UVSphere },
        OnCompletion{ OnCompletion }{}

    //required bu UE4
    FORCEINLINE TStatId GetStatId() const
    {
        RETURN_QUICK_DECLARE_CYCLE_STAT(IcosphereGenerateTask, STATGROUP_ThreadPoolAsyncTasks);
    }

    void DoWork();

private:
    int32 Segments;
    bool bInverse;
    USAUVSphere* UVSphere;
    FTaskCompletionCB OnCompletion;
};