// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <ProceduralMeshComponent.h>

#include "SAIcosphere.generated.h"

DECLARE_DELEGATE(FTaskCompletionCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FIcosphereCompletionCB);

struct Triangle
{
    int vert[3];
};

namespace icosahedron
{

    const float X = .525731112119133606f;
    const float Z = .850650808352039932f;
    const float N = 0.f;

    static const FVector vertices[] =
    {
        {-X,N,Z}, {X,N,Z}, {-X,N,-Z}, {X,N,-Z},
        {N,Z,X}, {N,Z,-X}, {N,-Z,X}, {N,-Z,-X},
        {Z,X,N}, {-Z,X, N}, {Z,-X,N}, {-Z,-X, N}
    };

    static const Triangle triangles[] =
    {
        {0,4,1},{0,9,4},{9,5,4},{4,5,8},{4,8,1},
        {8,10,1},{8,3,10},{5,3,8},{5,2,3},{2,7,3},
        {7,10,3},{7,6,10},{7,11,6},{11,0,6},{0,1,6},
        {6,1,10},{9,0,11},{9,11,2},{9,2,5},{7,2,11}
    };
}

UCLASS()
class USAIcosphere : public UProceduralMeshComponent
{
    GENERATED_UCLASS_BODY()

public:
    UFUNCTION()
    void MakeIcosphereAsync(uint8 subdivisions, bool bInverse = false);

    UFUNCTION()
    void MakeIcosphere(uint8 subdivisions, bool bInverse = false);

    UPROPERTY(BlueprintAssignable)
    FIcosphereCompletionCB CompletionCB;

protected:
    UFUNCTION()
    void Normalize();

    UFUNCTION()
    uint32 VertexForEdge(uint32 vert_index_1, uint32 vert_index_2);

    UFUNCTION()
    void FindUV(const FVector& normal, FVector2D& uv);

    UFUNCTION()
    void Subdivide();

    UFUNCTION()
    void MapUV();

    UFUNCTION()
    void OnCalculationsComplete();

private:
    TArray<FVector> m_vertices;
    TArray<FVector2D> m_uvmapping;
    TArray<Triangle> m_triangles;
    TMap<TPair<uint32, uint32>, uint32> lookup;
};

//=======================================================================

class IcosphereGenerateTask : public FNonAbandonableTask
{
public:
    IcosphereGenerateTask(uint8 Subdivisions, bool bInverse, USAIcosphere* Icosphere, FTaskCompletionCB OnCompletion) :
        Subdivisions{ Subdivisions },
        bInverse{ bInverse },
        Icosphere{ Icosphere },
        OnCompletion{ OnCompletion }{}

    //required bu UE4
    FORCEINLINE TStatId GetStatId() const
    {
        RETURN_QUICK_DECLARE_CYCLE_STAT(IcosphereGenerateTask, STATGROUP_ThreadPoolAsyncTasks);
    }

    void DoWork();

private:
    uint8 Subdivisions;
    bool bInverse;
    USAIcosphere* Icosphere;
    FTaskCompletionCB OnCompletion;
};