#pragma once

#include <CoreMinimal.h>
#include <Engine/AssetManager.h>

#include "SAAssetManager.generated.h"

/**
 *
 */
UCLASS()
class SENTINEL_API USAAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:

	static USAAssetManager& Get();

	/** Starts initial load, gets called from InitializeObjectReferences */
	virtual void StartInitialLoading() override;
};