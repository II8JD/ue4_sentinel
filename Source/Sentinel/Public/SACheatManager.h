// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CheatManager.h"
#include "SACheatManager.generated.h"

/**
 * 
 */
UCLASS()
class USACheatManager : public UCheatManager
{
	GENERATED_BODY()
	
public:
	UFUNCTION(Exec)
	void SA_TogglePlayerIsInvulnerable();

	UFUNCTION(BlueprintCallable, Category = Cheats)
	bool GetPlayerIsInvulnerable();

	UFUNCTION(Exec)
	void SA_TogglePlayerInfinitePower();

	UFUNCTION(BlueprintCallable, Category = Cheats)
	bool GetPlayerInfinitePower();

	UFUNCTION(Exec)
	void SA_RobotInteract();

	static bool bPlayerInfinitePower;

	static bool bPlayerIsInvulnerable;
};
