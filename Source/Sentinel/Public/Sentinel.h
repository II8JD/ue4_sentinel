// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(TerrainLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(PowerObjectLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(SentinelAILog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(MenuPawnLog, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(PlaybackTimerHandle, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(GameModeLog, Log, All);