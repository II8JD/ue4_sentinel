// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Delegates/Delegate.h>
#include <GameFramework/Actor.h>

#include "LevelRandomisers/SALevelRandomiserinterface.h"
#include "SATerrainParams.h"
#include "SATerrainTileData.h"
#include "Utility/SAColourApplicatorInterface.h"

#include "SATerrainActor.generated.h"

class USATerrainDataAsset;
struct FSATerrainTileData;

DECLARE_DELEGATE(FTaskCompletionCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCompletionCB, ASATerrainActor*, Terrain);

UCLASS()
class SENTINEL_API ASATerrainActor : public AActor, public ISAColourApplicatorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASATerrainActor();

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, CallInEditor)
	void GenerateMesh(int32 Seed, FSATerrainData& NewTerrainData);

	const int32 GetTiles() const;

	float GetTileSize() const { return TerrainParams.TerrainData->TileSize; }

	const float GetMaxHeight() const;

	const bool GetTerrainGenerationInProgress() const { return bGenerationInProgress; }

	const bool GetTerrainGenerationComplete() const { return bGenerationComplete; }

	const int32 GetLevels() const { return TerrainParams.TerrainData->Levels; }

	const float GetTileHeight(const int32 Index) const { return Heights[Index]; }

	void SetTileHeight(const int32 Index, const float Height) { Heights[Index] = Height; }

	void ResetTileHeights() { FMemory::Memset(Heights.GetData(), 0, Heights.Num() * Heights.GetTypeSize()); }

	TArray<FVector>& GetFloorPoints() { return FloorPoints; };

	const float GetMeshHeight(const int index) const;

	UPROPERTY(BlueprintAssignable)
	FCompletionCB CompletionCB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	AActor* Randomiser = nullptr;

	UPROPERTY()
	FBox TerrainBounds;

	UPROPERTY(EditAnywhere, Transient, BlueprintReadWrite, Category = Attributes)
	TArray<int32> Heights;

protected:
	UFUNCTION()
	void OnCalculationsComplete();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UProceduralMeshComponent* CustomMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	class UMaterialInstanceDynamic* DynamicMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FTerrainParams TerrainParams;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	bool bGenerationInProgress = false;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	bool bGenerationComplete = false;

	UPROPERTY()
	TArray<FVector> FloorPoints;
};

//=======================================================================

class TerrainGenerateTask : public FNonAbandonableTask
{
public:
	TerrainGenerateTask(TArray<FVector>& FloorPoints, FTerrainParams& TerrainParams, int32 Seed, AActor *Randomiser, ASATerrainActor* TerrainActor, FTaskCompletionCB OnCompletion) :
		FloorPoints{ FloorPoints },
		TerrainParams{ TerrainParams }, 
		Seed{ Seed },
		Randomiser{ Randomiser }, 
		TerrainActor{ TerrainActor },
		OnCompletion{ OnCompletion }{}
	//TerrainGenerateTask();

	//required bu UE4
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(TerrainGenerateTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork();

private:
	TArray<FVector>& FloorPoints;
	FTerrainParams& TerrainParams;
	int32 Seed;
	AActor *Randomiser;
	ASATerrainActor* TerrainActor;
	FTaskCompletionCB OnCompletion;

	void GenerateVertices();
	//void GenerateQuad(int32 StartX, int32 StartZ, int32 Index);
	void GenerateTriangles();
	void GenerateUVs();
	void GenerateNormals();
	void Randomise();
	void CalculateFloorSections();
};