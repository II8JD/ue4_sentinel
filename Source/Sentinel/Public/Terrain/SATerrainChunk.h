// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>

#include "SATerrainChunk.generated.h"

struct FSATerrainTileData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPowerObjectUpdateCB);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class USATerrainChunk : public UActorComponent
{
	GENERATED_BODY()

public:
	USATerrainChunk();

	void BeginPlay() override;

	void BeginDestroy() override;

	void SetLevelData(FSALevelData* NewLevelData);

	FSALevelData& GetLevelData() { return LevelData; };

	void AddPowerObject(ASAPowerPawn& PowerObject, bool Add);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Attributes)
	TArray<int32> PowerObjectTiles;

	const bool GetTileCurrentPosition(FVector& Pos) const;

	const bool GetTileOriginalPosition(FVector& Pos) const;

	UFUNCTION()
	const bool IsPositionInTile(const FVector& Pos, const FVector& TargetPos) const;

	float GetMaxHeight() const;

	float GetObjectTargetHeight(ASAPowerPawn& PowerObject) const;

	/** Returns number of power objects on a given tile index */
	const uint32 GetTilePowerObjectCount(uint32 Index) const;

	/* Set up a power objects collision params so it can ignore all objects within a specific tile*/
	void InitTileCollisionParams(const FVector& Location, struct FCollisionQueryParams& InCollisionParams);

	const bool GetTileIndexFromLocation(const FVector& Location, int32& OutIndex) const;

	const bool GetTileLocationFromIndex(const uint32 Index, FVector& OutLocation) const;

	ASAPowerPawn* GetTileTopObject(FVector& Pos) const;

	template<class T>
	T* GetTileTopObject(FVector& Pos) const
	{
		return Cast<T>(GetTileTopObject(Pos));
	}

	ASAPowerPawn* GetTileTopObject(uint32 Index) const;

	template<class T>
	T* GetTileTopObject(uint32 Index) const
	{
		return Cast<T>(GetTileTopObject(Index));
	}

	ASAPowerPawn* GetTileAbsorbObject(FVector& Pos) const;

	template<class T>
	T* GetTileAbsorbObject(FVector& Pos) const
	{
		return Cast<T>(GetTileAbsorbObject(Pos));
	}

	const bool IsPositionValid(FVector& Pos, bool bExcludeSpawningInTiles = false) const;

	const int32 GetSentinelStartIndex() const { return LevelData.SentinelStartIndex; }

	void SetSentinelStartIndex(const int32 Index) { LevelData.SentinelStartIndex = Index; }

	const int32 GetPlayerStartIndex() const { return LevelData.PlayerStartIndex; }

	void SetPlayerStartIndex(const int32 Index) { LevelData.PlayerStartIndex = Index; }

	void AddTreeStartIndex(const int32 Index) { LevelData.TreeStartIndex.Add(Index); }

	void RemoveTreeAtTile(const int32 TileIndex)
	{
		for (int32 i = 0, TreeCount = LevelData.TreeStartIndex.Num(); i < TreeCount; i++)
		{
			if (LevelData.TreeStartIndex[i] == TileIndex)
			{
				LevelData.TreeStartIndex.RemoveAt(i);
				break;
			}
		}
	}

	void AddSentryStartIndex(const int32 Index) { LevelData.SentryStartIndex.Add(Index); }

	/**
* Get a random safe floor Tile coordinate
* @param
* @param Maximum floor height to search for (all if less than zero)
*/
	UFUNCTION(BlueprintCallable, CallInEditor)
	bool GetRandomPosition(FVector& OutValue, const bool bEmptyPositionOnly, const bool bSearchUp, const bool bFavourHigher, float MaxHeight = -1.0f) const;

	//UFUNCTION()
	void GetValidPositions(TArray<FSATerrainTileData>& OutValues, const bool bEmptyPositionOnly, const bool bbSearchUp, float const MaxHeight) const;

	ASATerrainActor* GetTerrainActor() const { return Terrain; };

	bool GetTerrainComplete() const;

	UPROPERTY(BlueprintAssignable)
	FPowerObjectUpdateCB PowerObjectUpdateCB;

protected:
	UFUNCTION()
	void OnTerrainGenerationComplete(ASATerrainActor* Terrain);

	UPROPERTY()
	TArray<FSATerrainTileData> FloorSections;

	ASATerrainActor* Terrain;

	UPROPERTY()
	FSALevelData LevelData;
};