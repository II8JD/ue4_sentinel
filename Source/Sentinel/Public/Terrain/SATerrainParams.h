// Carl Peters

#pragma once

#include <GameFramework/Actor.h>
#include <ProceduralMeshComponent.h>

#include "DataAssets/SATerrainDataAsset.h"

#include "SATerrainParams.generated.h"

USTRUCT(BlueprintType)
struct FTerrainParams
{
	GENERATED_BODY()

public:
	UPROPERTY()
	class UFastNoise* Noise = nullptr;

	FSATerrainData* TerrainData;

	/* The vertices of the mesh */
	UPROPERTY()
	TArray<FVector> Vertices;

	/* The triangles of the mesh */
	UPROPERTY()
	TArray<int32> Triangles;

	/* The UVs of the mesh */
	UPROPERTY()
	TArray<FVector2D> UV0;

	UPROPERTY()
	TArray<FVector> Normals;

	TArray<FProcMeshTangent> Tangents;

	UPROPERTY()
	TArray<FColor> Colours;
};
