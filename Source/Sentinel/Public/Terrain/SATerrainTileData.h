// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "SATerrainTileData.generated.h"

/** Each terrain chunk has ~32x32 tiles which can have objects piled upon them */
USTRUCT(BlueprintType)
struct SENTINEL_API FSATerrainTileData
{
	GENERATED_BODY()

public:
	FSATerrainTileData() : Location(FVector::ZeroVector), CurrentHeight(0.0f) {}

	FSATerrainTileData(const FVector Location) : Location(Location), CurrentHeight(Location.Z) {}

	//bool operator== (const FSATerrainTileData& Other)
	//{
	//	return Position == Other.Position;
	//}

	//friend uint32 GetTypeHash(const FSATerrainTileData& Other)
	//{
	//	return GetTypeHash(Other.Position);
	//}

	void AddPowerObject(ASAPowerPawn& PowerObject, bool Add);

	float GetPowerObjectHeight(ASAPowerPawn& PowerObject) const;
	
	FORCEINLINE void InitLocation(const FVector NewLocation) { Location = NewLocation; CurrentHeight = NewLocation.Z; }
	FORCEINLINE float GetCurrentHeight() const { return CurrentHeight; }
	FORCEINLINE bool LocationIsEmpty() const { return SectionPowerObjects.Num() == 0; }
	bool LocationCanStack(bool bExcludeSpawningInTiles = false) const;
	/** Gets a terrain Tiles position and current height (objects stacked on top) */
	FORCEINLINE FVector GetCurrentLocation() const { return FVector(Location.X, Location.Y, CurrentHeight); }
	/** Gets a terrain Tiles position and ground level */
	FORCEINLINE FVector GetOriginalLocation() const { return Location; }

	void DeleteAllPowerObjects();
	
	ASAPowerPawn* GetTopObject() const;

	void InitCollisionParams(struct FCollisionQueryParams& InCollisionParams);

	/** */
	FORCEINLINE ASAPowerPawn* GetAbsorbObject() const
	{
		if (CurrentHeight < 0)
			return nullptr;

		const int32 Count = SectionPowerObjects.Num();
		if (Count < 2)
			return nullptr;

		return SectionPowerObjects[Count - 2];
	}

	FORCEINLINE const uint8 GetPowerObjectNum() const { return SectionPowerObjects.Num(); }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FVector Location = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float CurrentHeight = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Attributes)
	TArray<ASAPowerPawn*> SectionPowerObjects;
};