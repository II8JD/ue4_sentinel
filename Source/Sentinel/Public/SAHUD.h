// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SAHUD.generated.h"

/**
 * 
 */
UCLASS()
class ASAHUD : public AHUD
{
	GENERATED_BODY()

public:
	//// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

	//UFUNCTION(BlueprintCallable, Category = "UMG")
	//void ShowMainMenu();
	//UFUNCTION(BlueprintCallable, Category = "UMG")
	//void TogglePause();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	TSubclassOf<UUserWidget> DefaultMainMenuWidget;
	UPROPERTY(BlueprintReadOnly, Category = "UMG")
	UUserWidget* MainMenuWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
		TSubclassOf<UUserWidget> DefaultLoadingWidget;
	UPROPERTY(BlueprintReadOnly, Category = "UMG")
		UUserWidget* LoadingWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
		TSubclassOf<UUserWidget> DefaultHUDWidget;
	UPROPERTY(BlueprintReadOnly, Category = "UMG")
		UUserWidget* HUDWidget;

	UPROPERTY()
	APlayerController* Controller;
};
