// Carl Peters

#pragma once

#include "Engine/LatentActionManager.h"
#include "LatentActions.h"
#include "GameFramework/Pawn.h"

/**
 * 
 */
class FActorPhysicsRestAction : public FPendingLatentAction
{
public:
	float TimeRemaining;
	AActor& TargetActor;
	FName ExecutionFunction;
	int32 OutputLink;
	FWeakObjectPtr CallbackTarget;

	FActorPhysicsRestAction(float Duration, AActor& Actor, const FLatentActionInfo& LatentInfo)
		: TimeRemaining(Duration)
		, TargetActor(Actor)
		, ExecutionFunction(LatentInfo.ExecutionFunction)
		, OutputLink(LatentInfo.Linkage)
		, CallbackTarget(LatentInfo.CallbackTarget)
	{
	}

	virtual void UpdateOperation(FLatentResponse& Response) override
	{
		if( FMath::IsNearlyZero(TargetActor.GetVelocity().Size()) )
			TimeRemaining -= Response.ElapsedTime();
		Response.FinishAndTriggerIf(TimeRemaining <= 0.0f, ExecutionFunction, OutputLink, CallbackTarget);
	}
};
