// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SAHuffmanCompression.generated.h"

// A Tree node
USTRUCT()
struct FHuffmanNode
{
	GENERATED_BODY()

public:
	FHuffmanNode() : Ch(), Freq(0), Left(nullptr), Right(nullptr) {}

	FHuffmanNode(int32 Ch, int32 Freq, FHuffmanNode* Left, FHuffmanNode* Right) : Ch(Ch), Freq(Freq), Left(Left), Right(Right){}

	int32 Ch;
	int32 Freq;
	FHuffmanNode* Left, * Right;
};

// Comparison object to be used to order the heap
//struct Comp
//{
//	bool operator()(Node* l, Node* r)
//	{
//		// highest priority item has lowest frequency
//		return l->Freq > r->Freq;
//	}
//};

/**
 * 
 */
UCLASS()
class USAHuffmanCompression : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	// Builds Huffman Tree and decode given input text
	static void BuildHuffmanTree(const TArray<int32>& Data);

protected:
	// traverse the Huffman Tree and store Huffman Codes in a map.
	static void Encode(FHuffmanNode* Root, FString Str, TMap<TCHAR, FString>& HuffmanCode);

	// traverse the Huffman Tree and decode the encoded string
	static void Decode(FHuffmanNode* Root, int32& Index, FString Str);
};
