// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SAStatics.generated.h"

/**
 * 
 */
UCLASS()
class USAStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Lerp Tools")
	static float Repeat(float _t, float _length);

	UFUNCTION(BlueprintCallable, Category = "Lerp Tools")
	static float PingPong(float _t, float _length);
};
