// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Materials/MaterialInterface.h>
#include <Subsystems/GameInstanceSubsystem.h>

#include "SACharacterImageGenerator.generated.h"

enum class EPowerObjectType : uint8;
class USAPowerDataAsset;

DECLARE_DELEGATE(FOnTaskCompletionCB);

UCLASS()
class USACharacterImageGenerator : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:	
	USACharacterImageGenerator();

	void GenerateImages(USAPowerDataAsset& PowerObjectsAsset, UMaterialInterface* ParentMaterial, FOnTaskCompletionCB OnCompletion);

	UFUNCTION(BlueprintCallable)
	FORCEINLINE UMaterialInstanceDynamic* GetImage(EPowerObjectType Type) const { return GeneratedImages.FindRef(Type); };

	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool IsImageGenerationComplete() const
	{
		if (GEngine->GetNetMode(GetWorld()) == NM_DedicatedServer)
			return true;

		return bImageGenerationComplete;
	};

private:

	UPROPERTY(VisibleInstanceOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USceneCaptureComponent2D* SceneCaptureComponent;

	UPROPERTY(VisibleInstanceOnly)
	TMap<EPowerObjectType, UMaterialInstanceDynamic*> GeneratedImages;

	UPROPERTY(VisibleInstanceOnly)
	bool bImageGenerationComplete = false;
};