// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BPFL_SAColour.generated.h"

/** Quad Colours */
UENUM()
enum class ERectangleColour : uint8
{
	Dominant = 0, //OriginalAntiClockwise
	OriginalAntiClockwise = 0,
	OriginalClockwise,
	ComplementaryAntiClockwise,
	ComplementaryClockwise,
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnColoursGeneratedCB);

/**
 * 
 */
UCLASS()
class UBPFL_SAColour : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Colour Tools")
	static void GenerateLevelColours(int32 Split);

	UFUNCTION(BlueprintPure, Category = "Colour Tools")
	static FColor GetComplementaryColour(FColor StartColour);

	UFUNCTION(BlueprintPure, Category = "Colour Tools")
	static FLinearColor GetRectangleColour(const ERectangleColour RectangleIndex);

	static TArray <FLinearColor> LevelRectangleColours;

	static FOnColoursGeneratedCB OnColoursGeneratedCB;
};
