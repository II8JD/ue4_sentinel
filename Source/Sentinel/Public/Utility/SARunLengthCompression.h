// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SARunLengthCompression.generated.h"

/**
 * 
 */
UCLASS()
class USARunLengthCompression : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	static TArray<int32> Encode(const TArray<int32>& Data)
	{
		TArray<int32> Output;

		const int32 DataLength = Data.Num();
		if (DataLength == 0)
		{
			return Output;
		}

		int32 StartData = Data[0];
		int32 RunLength = 1;
		Output.Add(StartData);

		for (int i = 1; i < DataLength; i++)
		{
			if (StartData != Data[i])
			{
				Output.Add(RunLength);
				StartData = Data[i];
				Output.Add(StartData);
				RunLength = 1;
			}
			else
			{
				RunLength++;
			}
		}
		Output.Add(RunLength);

		return Output;
	};

	static TArray<int32> Decode(const TArray<int32>& Data)
	{

	};
};
