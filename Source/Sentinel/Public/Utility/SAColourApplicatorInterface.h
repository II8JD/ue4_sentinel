// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include <UObject/Interface.h>

#include "SAColourApplicatorInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(Blueprintable, MinimalAPI)
class USAColourApplicatorInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ISAColourApplicatorInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ApplyColours();
};
