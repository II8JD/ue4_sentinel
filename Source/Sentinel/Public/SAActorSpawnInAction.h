// Carl Peters

#pragma once

#include "Engine/LatentActionManager.h"
#include "LatentActions.h"
#include "GameFramework/Pawn.h"
#include "NiagaraComponent.h"
#include "DrawDebugHelpers.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerPawn.h"
#include "DataAssets/SASpawnDataAsset.h"
#include "SynthComponents/EpicSynth1Component.h"

/**
 * 
 */
class SAFActorSpawnInAction : public FPendingLatentAction
{
public:
	float TimeRemaining = 0.0f;
	float StartTime = 0.0f;
	AActor& TargetActor;
	FName ExecutionFunction;
	int32 OutputLink = 0;
	FWeakObjectPtr CallbackTarget;
	FVector KillBoxSize = FVector::ZeroVector;
	FVector KillBoxOffset = FVector::ZeroVector;
	float KillBoxMaxHeight = 0.0f;
	UModularSynthComponent* ModularSynthComponent = nullptr;
	FSAActorSpawnParams& SpawnFX;

	//material paramiters
	//material paramiter sets fade position
	const FName MaterialFade = "StaticGradientPos"; 

	//niagara user paramiters
	//kill box position offset moves upwards as the object spawns in
	const FString ParticleBoxOffset = "KillBoxOffset"; 
	//kill box size fills entire object at start then shrinks to nothing at the end
	const FString ParticleBoxScale = "KillBozSize";
	//set mesh height in c++ used to calculate particle position based on fade not sure if this could be calculated in niagara i couldnt find a way
	const FString ParticleMeshHeight = "MeshHeight";
	//position of spawn in effect this is where particles converge onto mesh starts at zero ends at one
	const FString ParticleFade = "Fade";
	//set niagara loop tim to match duration of this latant action
	const FString ParticleLoopDuration = "LoopDuration";

	SAFActorSpawnInAction(FSAActorSpawnParams& FXParams, AActor& Actor, const FLatentActionInfo& LatentInfo)
		: TimeRemaining(FXParams.SpawnSetup->SpawnDuration)
		, StartTime(FXParams.SpawnSetup->SpawnDuration)
		, TargetActor(Actor)
		, ExecutionFunction(LatentInfo.ExecutionFunction)
		, OutputLink(LatentInfo.Linkage)
		, CallbackTarget(LatentInfo.CallbackTarget)
		, SpawnFX(FXParams)
	{
		const USASpawnDataAsset* SpawnSetup = SpawnFX.SpawnSetup;
		const FBox TargetBounds = TargetActor.GetComponentsBoundingBox();
		KillBoxSize = TargetBounds.GetExtent();

		KillBoxMaxHeight = KillBoxSize.Z;

		//set mesh height only need to do this once it feels like you should be able to do this in niagara but i couldnt find a way
		SpawnFX.SpawnParticleInstance->SetNiagaraVariableFloat(ParticleMeshHeight, KillBoxMaxHeight * 2.0f);

		//set particle loop duration to match material spawn in duration
		SpawnFX.SpawnParticleInstance->SetNiagaraVariableFloat(ParticleLoopDuration, FXParams.SpawnSetup->SpawnDuration);

		RefreshMaterial();

		SpawnFX.ModularSynthComponent->SetSynthPreset(SpawnSetup->SynthBank->Presets[0].Preset);
		SpawnFX.ModularSynthComponent->NoteOn(SpawnSetup->SynthNote, SpawnSetup->SynthVelocity, SpawnSetup->SpawnDuration);
		SpawnFX.ModularSynthComponent->SetPitchBend(0.0f);
	}

	virtual void UpdateOperation(FLatentResponse& Response) override
	{
		if( FMath::IsNearlyZero(TargetActor.GetVelocity().Size()) )
			TimeRemaining -= Response.ElapsedTime();
		if (TimeRemaining <= 0.0f)
		{
			SpawnFX.ModularSynthComponent->NoteOff(SpawnFX.SpawnSetup->SynthNote);

			//SpawnFX.SpawnParticleInstance->DestroyInstance();

			//TimeRemaining = 0.0f;

			Response.FinishAndTriggerIf(true, ExecutionFunction, OutputLink, CallbackTarget);
		}
		RefreshMaterial();
	}

	virtual void RefreshMaterial()
	{
		if (SpawnFX.SpawnParticleInstance == nullptr)
			return;

		const float Fade = FMath::InterpSinIn(0.0f, 1.0f, 1.0f - FMath::Clamp(TimeRemaining / StartTime, 0.0f, 1.0f));

		SpawnFX.ModularSynthComponent->SetPitchBend(Fade);

		SpawnFX.MatInstance->SetScalarParameterValue(MaterialFade, Fade);
		SpawnFX.SpawnParticleInstance->SetNiagaraVariableFloat(ParticleFade, Fade);

		KillBoxSize.Z = KillBoxMaxHeight * (1.0f-Fade);
		SpawnFX.SpawnParticleInstance->SetNiagaraVariableVec3(ParticleBoxScale, KillBoxSize);

		KillBoxOffset.Z = KillBoxMaxHeight + (KillBoxMaxHeight * Fade);
		SpawnFX.SpawnParticleInstance->SetNiagaraVariableVec3(ParticleBoxOffset, KillBoxOffset);

		DrawDebugBox(TargetActor.GetWorld(), TargetActor.GetActorLocation() + KillBoxOffset, KillBoxSize, FColor::Red);
	}
};
