// Carl Peters

#pragma once

#include <BehaviorTree/BehaviorTreeTypes.h>
#include <CoreMinimal.h>
#include <UIControllerGraphPlugin/Public/UIPlayerController.h>

#include <UIControllerGraphPlugin/Public/UIBlackboardKeyHandle.h>

#include "SAGameStateBase.h"

#include "SAPlayerControllerBase.generated.h"

class AAIController;
enum class ESAGameState : uint8;
class UBehaviorTree;

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class ASAPlayerControllerBase : public AUIPlayerController
{
	GENERATED_BODY()

public:
	ASAPlayerControllerBase();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	FUIBlackboardKeyHandle GameStateBlackboardKeyHandle;

	UFUNCTION()
	virtual void OnGameStateChanged(const ESAGameState NewState);
};
