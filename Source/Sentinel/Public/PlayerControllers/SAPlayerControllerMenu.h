// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "SAPlayerControllerMenu.generated.h"

/** Menu states */
UENUM()
enum class EMenuState : uint8
{
	None,
	/** Main menu provides options for new game, continue options and quit */
	MainMenu,
	/** We dont know if this might take longer in future/on slower hardware */
	Loading,
	/** player hud etc */
	Game,
	/** In game pause */
	Pause,
	/** Level select */
	LevelSelect,
	/** Game Over (dead) */
	GameOver,
	/** Game Win */
	Win,
	/** Preview map */
	PreviewMap,
	/** Options */
	Options,
	/** Select between single player, multi player and edit mode */
	Play,
	/** Select new game or continue */
	SinglePlayer,
	/** Select host or search */
	MultiPlayer,
};

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class ASAPlayerControllerMenu : public ASAPlayerControllerBase
{
	GENERATED_BODY()
	
public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
protected:
	virtual void OnGameStateChanged(const ESAGameState NewState) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class ASAHUD* HUD;

	UPROPERTY(BlueprintReadOnly, Category = "UMG")
	EMenuState TargetMenuState;

	/** Level select */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	TSubclassOf<UUserWidget> DefaultLevelSelectWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<class AText3DActor> DefaultTitleTextActor = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	AActor* TitleText = nullptr;

	UPROPERTY()
	class ASAGameStateBase* GameState = nullptr;
};
