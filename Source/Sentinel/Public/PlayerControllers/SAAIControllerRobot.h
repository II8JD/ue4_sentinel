#pragma once

#include <CoreMinimal.h>
#include <AIController.h>

#include <UIControllerGraphPlugin/Public/UIBlackboardKeyHandle.h>

#include "Sentinel/Public/PlayerControllers/SAAIControllerBase.h"

#include "SAAIControllerRobot.generated.h"

SENTINEL_API DECLARE_LOG_CATEGORY_EXTERN(LogAIControllerRobot, Log, All);

USTRUCT(BlueprintType)
struct SENTINEL_API FSABuildTileData
{
	GENERATED_BODY()

public:
	int32 TileIndex{ 0 };
	float AngleUntilDanger{ 0.0f };
};

/**
 *
 */
UCLASS()
class SENTINEL_API ASAAIControllerRobot : public ASAAIControllerBase
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

	virtual void ScanForTargets() override;

	virtual void RefreshTargetLists() override;

#if ENABLE_VISUAL_LOG
	//~ Begin IVisualLoggerDebugSnapshotInterface interface
	// Adds information about this Actor to the Visual Logger.
	virtual void GrabDebugSnapshot(FVisualLogEntry* Snapshot) const override;
	//~ End IVisualLoggerDebugSnapshotInterface interface
#endif

protected:
	virtual void BeginPlay() override;

	virtual bool InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset) override;

	virtual void AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb) override;

	UFUNCTION()
	void OnPowerUpdate(int32 CurrentPower);

protected:
	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle RobotCountBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle TargetCountBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle CanAbsorbSentinelBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle BuildLocationBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle PowerBlackboardKeyHandle;

private:
	UFUNCTION()
	void CalculateBuildTiles();

	UFUNCTION()
	float CalculateDangerAngle(int TileIndex);

	UFUNCTION()
	void UpdateBildTileDangerValues();

	UFUNCTION()
	void SortAndSetBuildTarget();

	UFUNCTION()
	bool GetTargetIsShield();

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> SentinelAllyTiles;

	///* list of all trees this character can see. boolean true means object can be absorbed */
	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TArray<int32 /*TileIndex*/> ShieldTiles;

	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> RobotTiles;

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	FSATileData RobotTileData;

	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> SentinelAllyTiles;

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TArray<FSABuildTileData> BuildTiles;

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	bool bCanAbsorbSentinel = false;

};