// Carl Peters

#pragma once

#include <CoreMinimal.h>

#include "SAPlayerControllerGame.h"

#include "SAPlayerControllerEdit.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class ASAPlayerControllerEdit : public ASAPlayerControllerGame
{
	GENERATED_BODY()

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	FORCEINLINE bool IsMouseCursorActive() const { return bShowMouseCursor; }

protected:
	UFUNCTION()
	void ToggleMouse();

	UFUNCTION()
	void TogglePlayMode();
};
