#pragma once

#include <CoreMinimal.h>
#include <AIController.h>

#include <UIControllerGraphPlugin/Public/UIBlackboardKeyHandle.h>

#include "SAAIControllerBase.generated.h"

struct FUIBlackboardKeyHandle;
class USAObjectManager;

USTRUCT(BlueprintType)
struct SENTINEL_API FSATileData
{
	GENERATED_BODY()

public:
	void Empty()
	{
		Tiles.Empty();
		AbsorbableTileCount = 0;
	};

	void AddTile(int32 TileIndex, bool bCanAbsorb)
	{
		if (bCanAbsorb)
			AbsorbableTileCount++;

		Tiles.Add(TileIndex, bCanAbsorb);
	};

	int32 GetAbsorbableTileCount() const { return AbsorbableTileCount; };

	bool ContainsTile(int32 tile) const { return Tiles.Contains(tile); };

	class ASAPowerPawn* FindClosestTile(const UWorld* World, const FVector _LookLocation, const FVector _LookDirection, float _HalfViewAngle, float& OutAngle_, bool& bOutCanAbsorb_);

private:
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32 /*TileIndex*/, bool /*bCanAbsorb*/> Tiles;

	int32 AbsorbableTileCount = 0;
};

/**
 *
 */
UCLASS()
class ASAAIControllerBase : public AAIController
{
	GENERATED_BODY()
public:
	void OnPossess(APawn* InPawn) override;

	//Called when an object is added/removed from the game
	//build lists of objects to use in ScanForTargets
	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	virtual void RefreshTargetLists();

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	virtual void ScanForTargets();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void FindClosestTarget(const FSATileData& _ObjectTiles, float& OutAngle_, bool& bOutCanAbsorb_) const;

	UFUNCTION()
	void SetBlackboardBool(FUIBlackboardKeyHandle& _handle, bool _bValue);

	UFUNCTION()
	void SetBlackboardFloat(FUIBlackboardKeyHandle& _handle, float _value);

	UFUNCTION()
	void SetBlackboardInt(FUIBlackboardKeyHandle& _handle, int32 _value);

	UFUNCTION()
	bool SetBlackboardVector(FUIBlackboardKeyHandle& _handle, FVector _value);

	/** Callback from terrain when a power object is added */
	UFUNCTION()
	void OnPowerObjectUpdate();

	UFUNCTION()
	virtual void AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb);

	//USAObjectManager* ObjectManager = nullptr;

	/* list of all targets this character can see. boolean true means object can be absorbed */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	FSATileData TargetTiles;

	/* list of all trees this character can see. boolean true means object can be absorbed */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> TreeTiles;

	/* list of all trees this character can see. boolean true means object can be absorbed */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TArray<int32 /*TileIndex*/> ShieldTiles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float HalfViewAngle = 10.0f;

	UPROPERTY()
	class ASAPowerPawn* ControlledRobot = nullptr;
};