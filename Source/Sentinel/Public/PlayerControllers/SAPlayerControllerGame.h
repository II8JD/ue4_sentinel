// Carl Peters

#pragma once

#include <CoreMinimal.h>

#include "SAPlayerControllerBase.h"

#include "SAPlayerControllerGame.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class ASAPlayerControllerGame : public ASAPlayerControllerBase
{
	GENERATED_BODY()
	
public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	FORCEINLINE const bool GetUseSentinelReturnsRules() const { return bUseSentinelReturnRules; }

	FORCEINLINE void SetUseSentinelReturnsRules(const bool bRules) { bUseSentinelReturnRules = bRules; }

	UFUNCTION(BlueprintCallable, Category = "UMG")
	void TogglePause(const bool bPause);

	UFUNCTION()
	void SetPlayerMarkerHidden(const bool bMarkerHidden);

	UFUNCTION()
	void SetPlayerMarkerLocation(const FVector Location);

	FORCEINLINE const bool GetPlayerIsSentinel() const
	{
		return bPlayerIsSentinel;
	}

	UFUNCTION(BlueprintCallable, Category = "UMG")
	void VotePlayAsRobot();

	UFUNCTION(BlueprintCallable, Category = "UMG")
	void VotePlayAsSentinel();

	UFUNCTION(BlueprintCallable, Category = "UMG")
	void DEBUGSpectate();

	/** Returns true if input should be frozen (whether UnFreeze timer is active) */
	virtual void AcknowledgePossession(class APawn* P) override;

	UFUNCTION()
	void SetGameOver(bool bSentinelWon);
	
protected:
	/** Called via input to pause the game */
	void OnInputPause();

	UFUNCTION(Server, Reliable)
	void Server_ReceiveVote(const bool bSentinel);

	UFUNCTION(Server, Reliable)
	void Server_DEBUGSpectate();

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_GameOver(bool bSentinelWon);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class ASAHUD* HUD;

	/** Players power at start of level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bUseSentinelReturnRules = true;

	UPROPERTY(BlueprintReadOnly, Category = Setup)
	class AStaticMeshActor* PlayerSpawnMarker = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bPlayerIsSentinel = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AStaticMeshActor> DefaultPlayerSpawnMarker;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	FUIBlackboardKeyHandle PreviewMapBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	FUIBlackboardKeyHandle CharacterTypeBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
	FUIBlackboardKeyHandle PauseBlackboardKeyHandle;
};
