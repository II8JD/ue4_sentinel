#pragma once

#include <CoreMinimal.h>
#include <AIController.h>

#include <UIBlackboardKeyHandle.h>

#include "Sentinel/Public/PlayerControllers/SAAIControllerBase.h"

#include "SAAIControllerSentinel.generated.h"

struct FUIBlackboardKeyHandle;
class USAObjectManager;

/**
 *
 */
UCLASS()
class ASAAIControllerSentinel : public ASAAIControllerBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void RefreshTargetLists() override;

public:
	void SetHasMeanie(bool bHasMeanie);

	void ConvertClosestTreeIntoMeanie();

private:
	UFUNCTION()
	void InitMeanieHeadOffset();

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> SentinelAllyTiles;

	/* build a list of possible meanie spawn locations on RefreshTargetLists then cycle through them when you have to */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TArray<uint32/*tile*/> MeanieTreeTiles;

	UPROPERTY(EditDefaultsOnly, Category = "AI Keys")
	FUIBlackboardKeyHandle HasMeanieBlackboardKeyHandle;

	/** Store meanie head height for placing meanies */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	float MeanieHeadOffsetPosition = 0.0f;

	/* Used to cycle every index in MeanieTreeTiles when spawning meanies */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	int32 CurrentMeanieTreeIndex = 0;
};