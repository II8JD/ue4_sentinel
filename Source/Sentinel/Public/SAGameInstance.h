// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SAGameInstance.generated.h"

class USAObjectManager;
class USAPowerDataAsset;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartupGeneratedCB);

/**
 * 
 */
UCLASS()
class USAGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	USAPowerDataAsset* GetPowerObjectsDataAsset() const { return PowerObjectsDataAsset; }

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bMusicGenerationComplete = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USAPowerDataAsset* PowerObjectsDataAsset;
};
