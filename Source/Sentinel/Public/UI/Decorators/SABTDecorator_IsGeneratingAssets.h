// Carl Peters

#pragma once

#include <BehaviorTree/BTDecorator.h>
#include <CoreMinimal.h>
#include <UObject/ObjectMacros.h>

#include "SABTDecorator_IsGeneratingAssets.generated.h"

/**
 */
UCLASS()
class SENTINEL_API USABTDecorator_IsGeneratingAssets : public UBTDecorator
{
	GENERATED_UCLASS_BODY()

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
