// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include <CoreMinimal.h>
#include <UObject/ObjectMacros.h>
#include <BehaviorTree/BTTaskNode.h>

#include "SABTTask_ShowWidget.generated.h"

/**
 */
UCLASS()
class SENTINEL_API USABTTask_ShowWidget : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(Category = ShowWidget, EditAnywhere)
	bool bShowCursor{ false };

	UPROPERTY(Category = ShowWidget, EditAnywhere)
	TSubclassOf<UUserWidget> MenuWidget{ nullptr };

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
