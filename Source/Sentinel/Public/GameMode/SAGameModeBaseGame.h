// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "SAGameModeBase.h"
#include "SAGameModeBaseGame.generated.h"

USTRUCT(BlueprintType)
struct FSALevelSkipData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float AbsorbPower = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int SkipLevels = 1;
};

/**
 * 
 */
UCLASS()
class ASAGameModeBaseGame : public ASAGameModeBase
{
	GENERATED_BODY()

public:
	ASAGameModeBaseGame();

	/** Called from ASAPowerRobotPawn when player presses hyperspace from the sentinels pedestal. sends player ahead x levels */
	UFUNCTION()
	void LevelHyperspace(const int PlayerPower);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<class ASAPreviewLevelPawn> DefaultPreviewPlayer = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<FSALevelSkipData> LevelSkipData;
};
