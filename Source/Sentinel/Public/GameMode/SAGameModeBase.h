// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "LevelRandomisers/SALevelRandomiserInterface.h"
#include "SAGameModeBase.generated.h"

//class ASACharacterImageGenerator;

/**
 * 
 */
UCLASS()
class SENTINEL_API ASAGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	void SetSeedAndLoadLevel(const int NewSeed, USALevelDataAsset* LevelData);

	UFUNCTION()
	virtual void SpawnGameObjects(const USATerrainChunk* Terrain);

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	virtual ASAPowerPawn* SpawnPowerObject(const TSubclassOf<ASAPowerPawn>& ObjectType, FVector& SpawnPos, bool bSetPossession = false);

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	const int GetUnlockedLevel() const;

	UFUNCTION()
	void UpdateVote(const FUniqueNetIdRepl& UniqueId, const bool bSentinel);

	UFUNCTION()
	virtual void OnTerrainGenerated(const USATerrainChunk* Terrain);

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	void InvokeTravel(const FString& FURL);

	UFUNCTION()
	void SetAIPossession(ASAPowerPawn* Pawn) const;

	UFUNCTION()
	void DebugSpectate();

	class AActor* EmptyActor = nullptr;

protected:

	UPROPERTY()
	TMap<FUniqueNetIdRepl /*PlayerId*/, int32 /*SentinelVotes*/> PlayerSentinelVotes;

	//UPROPERTY(BlueprintReadOnly, Category = Attributes)
	//ASACharacterImageGenerator* ImageGenerator = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USAPowerDataAsset* PowerObjectsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USALevelDataAsset* LevelDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	int32 UnlockedSeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int TotalWorldPower = 0;

	virtual void BeginPlay() override;	

	UFUNCTION()
	void DecideObjectLocation(const USATerrainChunk* Terrain, int32& LocationIndex, const bool bSearchUp, const bool bFavourHigher, float MaxHeight);

	UFUNCTION()
	void DecideObjectArrayLocations(const USATerrainChunk* Terrain, TArray<int32>& LocationArray, const bool bSearchUp, const bool bFavourHigher, FVector MinMaxNumCurve);

	UFUNCTION(BlueprintCallable, Category = "Level Setup")
	void SpawnObjectList(const USATerrainChunk* Terrain, const TSubclassOf<ASAPowerPawn>& ObjectType, const TArray<int32> &StartPositions);

	UFUNCTION(BlueprintCallable, Category = "Level Setup")
	class ASAPowerPawn* SpawnSingleObject(const USATerrainChunk* Terrain, const TSubclassOf<ASAPowerPawn>& ObjectType, const int32 StartPosition);

	UFUNCTION()
	void SetPlayerPossession(ASAPowerPawn* Pawn, AController* Controller) const;
};
