// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "SAGAmeModeBase.h"
#include "SAGameModeBaseEditor.generated.h"

class ASATerrainActor;

/**
 * 
 */
UCLASS()
class ASAGameModeBaseEditor : public ASAGameModeBase
{
	GENERATED_BODY()

public:
	virtual ASAPowerPawn* SpawnPowerObject(const TSubclassOf<ASAPowerPawn>& ObjectType, FVector& SpawnPos, bool bSetPossession) override;

	virtual void OnTerrainGenerated(const USATerrainChunk* Terrain) override;

	virtual void GenerateLevel();

	UFUNCTION()
	void TogglePlayMode();

	UFUNCTION()
	bool IsPlayMode();

protected:

	UFUNCTION()
	void StoreEditPlayerLocation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool ResetCameraOnTerrain = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FVector PlayerPosition = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FRotator PlayerRotation = FRotator::ZeroRotator;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	class ASAEditLevelPawn* EditorPlayer = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	bool PlayMode = false;

	//set material here because we have no USALevelDataAsset
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UMaterialInterface* Material; 

	//set material here because we have no USALevelDataAsset
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (MustImplement = "SALevelRandomiserInterface"))
	TSubclassOf<AActor> RandomiserInterface = nullptr;

private:
	void BeginPlay() override;
};
