
#pragma once

#include "SAActorSpawnParams.generated.h"

USTRUCT(BlueprintType)
struct FSAActorSpawnParams
{
	GENERATED_BODY()

public:
	UPROPERTY()
	class UModularSynthComponent* ModularSynthComponent = nullptr;

	UPROPERTY()
	UMaterialInstanceDynamic* MatInstance = nullptr;

	UPROPERTY()
	class UNiagaraComponent* SpawnParticleInstance = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USASpawnDataAsset* SpawnSetup = nullptr;
};