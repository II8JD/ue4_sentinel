// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Curves/CurveVector.h>
#include <Engine/DataAsset.h>

#include "SATerrainDataAsset.h"

#include "SALevelDataAsset.generated.h"

USTRUCT(BlueprintType)
struct SENTINEL_API FSALevelData : public FSATerrainData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 PlayerStartIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 SentinelStartIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UCurveVector* TreeCountCurve = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<int32> TreeStartIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UCurveVector* SentryCountCurve = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<int32> SentryStartIndex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FString CompressedHeights;
};

/**
 * 
 */
UCLASS(BlueprintType)
class SENTINEL_API USALevelDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	//bUSALevelDataAsset() : {}

//	USALevelDataAsset(FSALevelData LevelData) : LevelData{ LevelData } {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FSALevelData LevelData;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

};
