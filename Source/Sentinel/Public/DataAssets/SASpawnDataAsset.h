// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SASpawnDataAsset.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class SENTINEL_API USASpawnDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SpawnDuration = 3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UNiagaraSystem* SpawnFX = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	class UModularSynthPresetBank* SynthBank;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	float SynthNote = 60;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	int32 SynthVelocity = 127;
};
