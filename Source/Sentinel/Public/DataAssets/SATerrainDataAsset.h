// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Curves/CurveVector.h>
#include <Engine/DataAsset.h>

#include "SATerrainDataAsset.generated.h"

USTRUCT(BlueprintType)
struct SENTINEL_API FSATerrainData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (MustImplement = "SALevelRandomiserInterface"))
	TSubclassOf<AActor> RandomiserInterface = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 Seed = 0;

	/** How many tiles to use in a level */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 Tiles = 32;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float SampleDistance = 30;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float TileSize = 100.0f;

	/** how many height levels should be used */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 Levels = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UMaterialInterface* Material = nullptr;
};