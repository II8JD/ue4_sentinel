// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SAPowerDataAsset.generated.h"

class ASAPowerPawn;

UENUM(BlueprintType)
enum class EPowerObjectType : uint8
{
	Tree UMETA(DisplayName = "PO_Tree"),
	Rock UMETA(DisplayName = "PO_Rock"),
	Robot UMETA(DisplayName = "PO_Robot"),
	Sentinel UMETA(DisplayName = "PO_Sentinel"),
	Meanie UMETA(DisplayName = "PO_Meanie"),
	Sentry UMETA(DisplayName = "PO_Sentry"),
	Pedestal UMETA(DisplayName = "PO_Pedestal"),
	// make sure to update MaxPowerObjectType 
	MaxPowerObjectType UMETA(Hidden)
};

ENUM_RANGE_BY_COUNT(EPowerObjectType, EPowerObjectType::MaxPowerObjectType);

USTRUCT(BlueprintType)
struct FSAPowerObjectTypeData
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ASAPowerPawn> Blueprint;
};

/**
 * 
 */
UCLASS(BlueprintType)
class SENTINEL_API USAPowerDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Sentinel;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData SentinelPedestal;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Robot;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Rock;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Tree;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Meanie;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSAPowerObjectTypeData Sentry;

	UFUNCTION(BlueprintCallable, Category = "Power Objects")
	FORCEINLINE FSAPowerObjectTypeData GetPowerObjectTypeData(EPowerObjectType ObjectType) const
	{
		switch (ObjectType)
		{
		case EPowerObjectType::Tree:
			return Tree;

		case EPowerObjectType::Rock:
			return Rock;

		case EPowerObjectType::Robot:
			return Robot;

		case EPowerObjectType::Sentinel:
			return Sentinel;

		case EPowerObjectType::Meanie:
			return Meanie;

		case EPowerObjectType::Sentry:
			return Sentry;

		case EPowerObjectType::Pedestal:
			return SentinelPedestal;
		}

		UE_LOG(LogTemp, Error, TEXT("Failed to find objectType."))
		return Tree;
	}

	UFUNCTION(BlueprintCallable, Category = "Power Objects")
	FORCEINLINE const TSubclassOf<class ASAPowerPawn> GetPowerObjectBlueprint(EPowerObjectType ObjectType) const
	{ 
		return GetPowerObjectTypeData(ObjectType).Blueprint;
	}
};
