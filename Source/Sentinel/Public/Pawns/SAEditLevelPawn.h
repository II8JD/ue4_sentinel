// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SAEditLevelPawn.generated.h"

enum class EPowerObjectType : uint8;

UCLASS()
class SENTINEL_API ASAEditLevelPawn : public APawn
{
	GENERATED_BODY()

	DECLARE_DELEGATE_OneParam(FRaiseTerrainDelegate, bool)

public:
	// Sets default values for this pawn's properties
	ASAEditLevelPawn();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void ResetPosition();

	UFUNCTION(BlueprintCallable)
	int32 GetHeight(int32 Index) { if(Index < Heights.Num()) return Heights[Index]; return 0;};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void UpdateRayTest();

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	UFUNCTION()
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	UFUNCTION()
	void LookUpAtRate(float Rate);

	UFUNCTION()
	void RaiseTerrain(const bool Raise);

	UFUNCTION()
	void AddUpMovementInput(float Rate);

	UFUNCTION()
	void UpMovementAtRate(float Rate);

	UFUNCTION()
	void AddForwardMovementInput(float Rate);

	UFUNCTION()
	void ForwardMovementAtRate(float Rate);

	UFUNCTION()
	void AddRightMovementInput(float Rate);

	UFUNCTION()
	void RightMovementAtRate(float Rate);

	//UFUNCTION()
	//void OnTerrainComplete();

	void TrySpawnObject(EPowerObjectType ObjectType);
	template<EPowerObjectType ObjectType>
	void TrySpawnObject() { TrySpawnObject(ObjectType); }

	void ResetTerrain();

	void RemoveObject();

	UFUNCTION()
	void OnTerrainGenerated(const USATerrainChunk* Chunk);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UFloatingPawnMovement* MovementComponent = nullptr;

	/** First person camera. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent = nullptr;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY()
	class ASAPowerPawn* CurrentPowerObject;

	/** Updated in Tick UpdateRayTest. True if player is looking at a valid section of floor where an object can be spawned */
	UPROPERTY()
	bool bCurrentTileIsValid;

	/** Updated in Tick UpdateRayTest. Current floor grid location player is looking at. Used when player spawns objects. May be invalid check bCurrentBlockIsValid*/
	UPROPERTY()
	FVector CurrentTile;

	UPROPERTY()
	float TimeDelta;

	//UPROPERTY()
	//class ASAGameModeBase* GameMode;

	UPROPERTY()
	class USATerrainChunk* Chunk;

	UPROPERTY()
	FBox TerrainBounds;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USAPowerDataAsset* PowerObjectsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RayDistance = 5000.0f;

	struct FCollisionQueryParams CollisionParams;

	UPROPERTY()
	int32 SelectedTerrainIndex = 0;

	UPROPERTY()
	bool bHasSelectedTerrain = false;

	UPROPERTY()
	bool ResetRequired = false;

	UPROPERTY()
	TArray<int32> Heights;
};
