// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SAPreviewLevelPawn.generated.h"

UCLASS()
class SENTINEL_API ASAPreviewLevelPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASAPreviewLevelPawn();

protected:
	UFUNCTION()
	void UpdateRotation(float DeltaTime);

	UFUNCTION(Server, Reliable)
	void RPCPossessGamePawn(AController* NewController, class ASAPowerPawn* PawnToPossess);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	UPROPERTY()
	FBox TerrainBounds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RotateSpeed = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float PitchAngle = 20.0f;

	UPROPERTY()
	float Rotation = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RadiusScale = 1.5f;

	UPROPERTY()
	bool bBoundsSet = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
