// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/SpectatorPawn.h>

#include "SASpectatorPawn.generated.h"


UCLASS()
class SENTINEL_API ASASpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()

public:
	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	UFUNCTION()
	void OnInputResetView();

	UFUNCTION()
	void FocusBetweenRobotAndSentinel();

	float RadiusScale{ 1.5f };
};
