// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SABTTask_ScanForTargets.generated.h"

/**
 * Wait task node.
 * Wait for the specified time when executed.
 */
UCLASS()
class SENTINEL_API USABTTask_ScanForTargets : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector HasTargetBlackboardKey;

	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector CanAbsorbTargetBlackboardKey;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
