// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "BehaviorTree/Services/BTService_DefaultFocus.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"

#include "SABTTask_RotateToTarget.generated.h"

class UBlackboardComponent;

struct FRotateToTargetMemory
{
	/** Move request ID */
	FAIRequestID MoveRequestID;

	FDelegateHandle BBObserverDelegateHandle;
	FVector PreviousGoalLocation;

	//TWeakObjectPtr<USABTTask_RotateToTarget> Task;

	uint8 bObserverCanFinishTask : 1;
};

/**
 * Wait task node.
 * Wait for the specified time when executed.
 */
UCLASS( )
class SENTINEL_API USABTTask_RotateToTarget : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

public:
	//USABTTask_RotateToTarget(const FObjectInitializer& ObjectInitializer);

	virtual void PostInitProperties() override;
	virtual void PostLoad() override;

	/** initialize any asset related data */
	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	//virtual void DescribeRuntimeValues(const UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTDescriptionVerbosity::Type Verbosity, TArray<FString>& Values) const override;
	//virtual FString GetStaticDescription() const override;

	virtual uint16 GetInstanceMemorySize() const override { return sizeof(FRotateToTargetMemory); }

	EBlackboardNotificationResult OnBlackboardValueChange(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID);

protected:
	virtual EBTNodeResult::Type PerformRotateTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);

	/** blackboard key selector */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	struct FBlackboardKeySelector TargetBBKey;

	/** Success condition precision in degrees */
	UPROPERTY(Category = Node, EditAnywhere, meta = (ClampMin = "0.0"))
	float Precision;

	UPROPERTY(Category = Node, EditAnywhere, meta = (ClampMin = "0.0"))
	float RotationSpeed;

private:
	/** cached Precision tangent value */
	float PrecisionDot;

public:
#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
