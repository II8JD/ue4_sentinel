// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "SABTTask_WaitCooldown.h"

#include "SABTTask_RotateFractionCooldown.generated.h"

/**
 * Wait task node.
 * Wait for the specified time when executed.
 */
UCLASS()
class SENTINEL_API USABTTask_RotateFractionCooldown : public USABTTask_WaitCooldown
{
	GENERATED_UCLASS_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere)
	FBlackboardKeySelector RotationBlackboardKey;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
