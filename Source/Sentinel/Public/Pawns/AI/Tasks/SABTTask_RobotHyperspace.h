//Carl Peters
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "BehaviorTree/BTTaskNode.h"

#include "SABTTask_RobotHyperspace.generated.h"

/**
 * RobotHyperspace task node.
 * Activate hyperspace will teleport the robot to a random
 * position on the map or will end the level if robot is on sentinel pedistal
 */
UCLASS()
class SENTINEL_API USABTTask_RobotHyperspace : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
