//Carl Peters
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SABTTask_SpawnMeanie.generated.h"

/**
 * Spawn power object task node.
 * Spawns a powr object.
 */
UCLASS()
class SENTINEL_API USABTTask_SpawnMeanie : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	void OnMeanieDestroyed();

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
