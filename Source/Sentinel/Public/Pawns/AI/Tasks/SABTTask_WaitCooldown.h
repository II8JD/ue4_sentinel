#pragma once

#include <CoreMinimal.h>
#include <UObject/ObjectMacros.h>
#include <BehaviorTree/BTTaskNode.h>

#include "SABTTask_WaitCooldown.generated.h"

/**
 * Power ovbject absorber absorb task node.
 * Wait for the specified time when executed.
 */
UCLASS()
class SENTINEL_API USABTTask_WaitCooldown : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual void OnCooldownComplete();

	UPROPERTY(EditAnywhere)
	float CooldownTime = 5.0f;

	FTimerDelegate TimerDelegate;
	FTimerHandle TimerHandle;

	UPROPERTY()
	TObjectPtr<UBehaviorTreeComponent> MyOwnerComp;
};
