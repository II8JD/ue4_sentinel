#pragma once

#include "SABTTask_WaitCooldown.h"

#include "SABTTask_Absorb.generated.h"

class ASAPowerAbsorberPawn;

/**
 * Power ovbject absorber absorb task node.
 * Absorb target then wait for the specified time when executed.
 */
UCLASS()
class SENTINEL_API USABTTask_Absorb : public USABTTask_WaitCooldown
{
	GENERATED_UCLASS_BODY()

	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR

protected:
	void OnCooldownComplete() override;

private:
	ASAPowerAbsorberPawn* GetValidPowerObject();
};
