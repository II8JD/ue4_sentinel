// Copyright Epic Games, Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "BehaviorTree/BTTaskNode.h"
#include "SABTTask_SpawnPowerObject.generated.h"

enum class EPowerObjectType : uint8;

/**
 * Spawn power object task node.
 * Spawns a powr object.
 */
UCLASS()
class SENTINEL_API USABTTask_SpawnPowerObject : public UBTTaskNode
{
	GENERATED_UCLASS_BODY()

	/** Object type to spawn */
	UPROPERTY(Category = SpawnPowerObject, EditAnywhere)
	EPowerObjectType PowerObjectType;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

#if WITH_EDITOR
	virtual FName GetNodeIconName() const override;
#endif // WITH_EDITOR
};
