// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SAMainMenuPawn.generated.h"

UCLASS()
class SENTINEL_API ASAMainMenuPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASAMainMenuPawn();

	virtual void BeginDestroy() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UFUNCTION()
	void UpdateCameraLocation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RotateSpeed = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float PitchAngle = 20.0f;

	UPROPERTY()
	float Rotation = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RadiusScale = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes, meta = (UIMin = 0.0, UIMax = 1.0))
	float HorizontalOffset = 1.0f;

	UPROPERTY()
	bool bBoundsSet = false;

	UPROPERTY()
	FVector TerrainCenter = FVector::ZeroVector;

};
