// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <UIBlackboardKeyHandle.h>

#include "SAPowerAbsorberPawn.h"

#include "SAPowerSentryPawn.generated.h"

/**
 * 
 */
UCLASS()
class SENTINEL_API ASAPowerSentryPawn : public ASAPowerAbsorberPawn
{
	GENERATED_BODY()

public:
	ASAPowerSentryPawn();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void AbsorbTarget() override;

	UFUNCTION()
	const float GetPlayerVisibility() const;

	UFUNCTION()
	void TriggerSwitchCharacter();

protected:
	UFUNCTION()
	void CalculateDirection();

	UFUNCTION()
	void OnRobotInteract();

	virtual void ApplyabsorbTarget() override;

	virtual void PossessTarget() override;

	//virtual void AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb) override;

	UFUNCTION()
	void OnInputSwitchCharacter();

	UFUNCTION(Server, Reliable)
	void Server_SwitchCharacter();

	/** how long to pause before scentinel starts absorbing something */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	TArray<float> AbsorbPauseTime;

	/** Access AbsorbPauseTime  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int CurrentAbsorbIndex = 0;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	float PlayerVisible = 0.0f;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	bool bRotateRight = false;
};

