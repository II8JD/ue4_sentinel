// Carl Peters

#pragma once

#include <Blueprint/UserWidget.h>
#include <CoreMinimal.h>

#include "SAPowerWidget.generated.h"

class SScaleBox;

/**
 * Player wiget when playing as athe robot
 * displays players visibility bar and total power
 */
UCLASS(BlueprintType, Blueprintable)
class USAPowerWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	class UImage *PlayerVisibleImage = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	UMaterialInterface* PlayerVisibleMaterial;

protected:
	UFUNCTION()
	void OnPowerUpdate(int32 CurrentPower);

	UFUNCTION()
	void UpdatePlayerVisibility();

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Materials)
	UMaterialInstanceDynamic* PlayerVisibleMatInstance = nullptr;

	UPROPERTY()
	class ASAPlayerControllerBase* PlayerController = nullptr;

	UPROPERTY()
	float PreviousVisibility = -1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USAPowerDataAsset* PowerObjectsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes, meta = (BindWidget))
	class UHorizontalBox* HorizontalBoxRemaingPower;

	//UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	//class USAGameInstance* GameInstance = nullptr;
};
