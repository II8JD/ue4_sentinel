// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SAActorSpawnParams.h"
#include "SAPowerPawn.generated.h"

enum class EPowerObjectType : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FObjectAbsorbedCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FObjectSpawnCompleteCB);

UCLASS()
class SENTINEL_API ASAPowerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASAPowerPawn();

	//virtual void SetActive(const bool bActive);

	//UFUNCTION()
	//void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	virtual void SetSelected(const bool bSelected);

	UFUNCTION()
	virtual void BecomeAbsorbed(const bool Immediate);

	UFUNCTION()
	virtual void EnablePhysics();

	UFUNCTION()
	void TriggerSpawnInFX();

	/** Returns true if power object can be absorbed.
	only the sentinels pedestal cannot be absorbed. */
	virtual bool CanBeAbsorbed();

	bool CanStack() const { return bStackable; }

	bool IsSpawningIn() const { return bIsSpawningIn; }

	int32 PowerLevel() const { return Power; }

	bool IsActive() const { return bIsActive; }

	float GetMeshHeight() const { return MeshHeight; }

	float GetPower() const { return Power; }

	EPowerObjectType GetType() const { return Type; }

	TSubclassOf<ASAPowerPawn> GetNext() const { return NextObject; }

	struct FCollisionQueryParams& GetCollisionParams() { return CollisionParams; }

	UPROPERTY(BlueprintAssignable)
	FObjectAbsorbedCB ObjectAbsorbedCB;

	UPROPERTY(BlueprintAssignable)
	FObjectSpawnCompleteCB ObjectSpawnCompleteCB;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void OnSpawninComplete();

	UFUNCTION(NetMulticast, Unreliable)
	void RPCTriggerSpawnInFX();

	/** Actually play the spawn in FX. Made it BlueprintNativeEvent just incase we want to set up in BP*/
	UFUNCTION(BlueprintCosmetic, BlueprintNativeEvent)
	void DoSpawnInFX();

	UFUNCTION(Server, Reliable)
	void Server_BecomeAbsorbed(const bool Immediate);

	UPROPERTY()
	class ASAGameStateBase* GameState = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 Power = 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* Body = nullptr;

	//True if this object can ave another object spawned on top of it e.g. the Rock
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bStackable = false;

	//Material colour change when player has this object selected
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FColor SelectedColour = FColor::Green;

	/** Material colour change when object is not selected */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FColor DefaultColour = FColor:: Blue;

	/** Defines the object to replace this one in the world if this object is slowly absobed e.g. by the sentinel.*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Attributes)
	TSubclassOf<ASAPowerPawn> NextObject;

	/** True if game object is active */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	bool bIsActive = true;

	/** Height of mesh. used for stacking */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	float MeshHeight = 0.0f;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	//float HalfViewAngle = 10.0f;

	/** up offset applied to objects as they spawn in. gives the appearence of popping in. */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	//float SpawnFallHeight = 10.0f;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Attributes)
	class ASAGameModeBase* GameMode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	EPowerObjectType Type;

	UPROPERTY()
	FLatentActionInfo SpawnInInfo;

	UPROPERTY(EditAnywhere, Category = Attributes)
	struct FSAActorSpawnParams FXParams;

	UPROPERTY()
	bool bIsSpawningIn = false;

	struct FCollisionQueryParams CollisionParams;
};
