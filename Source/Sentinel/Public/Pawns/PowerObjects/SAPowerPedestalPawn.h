// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "SAPowerPawn.h"
#include "SAPowerPedestalPawn.generated.h"

/**
 * This is pedestal that a sentinal sits on top of.
 * It cannot be absorbed by a player.
 * If a player creates a robot here, posesses it and activates hyperspace you go to the next level.
 */
UCLASS()
class ASAPowerPedestalPawn : public ASAPowerPawn
{
	GENERATED_BODY()
	
public:
	ASAPowerPedestalPawn();

	/** Returns true if power object can be absorbed.
	only the sentinels pedestal cannot be absorbed. */
	virtual bool CanBeAbsorbed() override;
};
