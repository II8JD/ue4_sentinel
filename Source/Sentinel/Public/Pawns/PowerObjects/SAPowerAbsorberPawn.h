// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Pawn.h>
#include <UIBlackboardKeyHandle.h>

#include "SAActorSpawnParams.h"
#include "SAPowerPawn.h"

#include "SAPowerAbsorberPawn.generated.h"

enum class EPowerObjectType : uint8;
struct FUIBlackboardKeyHandle;

UCLASS()
class SENTINEL_API ASAPowerAbsorberPawn : public ASAPowerPawn
{
	GENERATED_BODY()

public:
	ASAPowerAbsorberPawn();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void UpdateRayTest();

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	virtual void AbsorbTarget();

	UFUNCTION()
	void SetController();

	/** Converts Terrain tile index into Location and calls RPCTrySpawnObject*/
	UFUNCTION()
	virtual void TrySpawnObjectAtIndex(const EPowerObjectType ObjectType, const uint32 Index);

	UFUNCTION()
	virtual void TrySpawnObjectAtLocation(const EPowerObjectType ObjectType, FVector& SpawnLocation);

	bool CanAbsorbTile(int32 _tileIndex);

	class UBehaviorTree* GetBehaviorTree() const { return BehaviorTree; };

	bool HasAbsorbTarget() const { return CurrentPowerObject != nullptr; };

	bool CanAbsorbTarget() const { return bCanabsorbCurrentPowerObject; };

	bool GetCurrentTileIsValid() const { return bCurrentTileIsValid; };

	FVector GetCurrentTileLocation() const { return CurrentTile; };

	virtual float GetRotateDistance() const { return RotateDistance; };

	//const FVector GetHeadLocation() const;
	FVector GetHeadLocation() const;

	FVector GetHeadRotation() const { return LookDirection; };

	FVector GetCurrentTile() const { return CurrentTile; };

	void SetHeadPitch(float pitch);

	bool IsLookingAtTarget(FVector TargetLocation) const;

	ASAPowerPawn* CanAbsorbObject(const float StartHeight, const FVector& InStart, const FVector& InTarget, const struct FCollisionQueryParams& InCollisionParams, const bool bUseSentinelReturnRules, FVector& OutFoundMapTile);

	void SetCurrentPowerObject(ASAPowerPawn* newPowerObject, bool bCanAbsorb);

	void ClearCheckedTiles() { CheckedVisibleTiles.Empty(); };

protected:

	virtual void UpdatePossessTarget(const FVector& InStart, const FVector& InTarget, const struct FCollisionQueryParams& InCollisionParams);

	UFUNCTION()
	void SetAIControl();

	UFUNCTION()
	void SetPlayerControl();

	/**
	* Called when this Pawn is possessed. Only called on the server (or in standalone).	
	* @param NewController The controller possessing this pawn
	*/
	virtual void PossessedBy(AController* NewController) override;

	UFUNCTION()
	virtual void PossessTarget();

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	UFUNCTION()
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	UFUNCTION()
	void LookUpAtRate(float Rate);

	/** Called via input to absorb a target */
	UFUNCTION()
	void OnInputAbsorb();

	UFUNCTION(Server, Reliable)
	void Server_AbsorbTarget(ASAPowerPawn* TargetPowerObject);

	UFUNCTION()
	virtual void ApplyabsorbTarget() PURE_VIRTUAL(, );

	/** Called via input to possess a target robot */
	UFUNCTION()
	void OnInputPossess();

	UFUNCTION(Server, Reliable)
	void Server_SetPossessed(ASAPowerPawn* PossessTarget);

	UFUNCTION(Server, Reliable)
	void Server_TrySpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation);

	UFUNCTION()
	virtual ASAPowerPawn* FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation);

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	bool bRefreshTargetsrequired = false;

	/** modified by ScanForTargets true if CurrentPowerObject can be absorbed. */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	bool bCanabsorbCurrentPowerObject = false;

	/** Updated in Tick UpdateRayTest. True if player is looking at a valid section of floor where an object can be spawned */
	UPROPERTY()
	bool bCurrentTileIsValid;

	/** First person camera. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	const class UStaticMeshSocket* HeadSocket;

	/* list of all targets this character can see. boolean true means object can be absorbed */
	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32, bool> TargetTiles;

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	TMap<int32, bool> CheckedVisibleTiles;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class USAPowerDataAsset* PowerObjectsDataAsset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	class UBehaviorTree* BehaviorTree;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	class ASAPowerPawn* CurrentPossessObject = nullptr;

	/** modified by ScanForTargets stores pointer to what we are currently absorbing. */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	class ASAPowerPawn* CurrentPowerObject = nullptr;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY()
	float TimeDelta;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RayDistance = 5000.0f;

	/** How far to rotate in degrees on every turn when in the searching state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	float RotateDistance = 30.0f;

	/** Updated in Tick UpdateRayTest. Current floor grid location player is looking at. Used when player spawns objects. May be invalid check bCurrentBlockIsValid*/
	UPROPERTY()
	FVector CurrentTile;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle HasTargetBlackboardKeyHandle;

	UPROPERTY(EditAnywhere, Category = "AI Keys")
	FUIBlackboardKeyHandle CanAbsorbTargetBlackboardKeyHandle;

	UPROPERTY()
	FVector LookDirection;
};
