// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "SAPowerSentryPawn.h"
#include "SAPowerMeaniePawn.generated.h"

/**
 * 
 */
UCLASS()
class ASAPowerMeaniePawn : public ASAPowerSentryPawn
{
	GENERATED_BODY()
	
public:
	ASAPowerMeaniePawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void AbsorbTarget() override;

	virtual void TurnIntoTree();
};
