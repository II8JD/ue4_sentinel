// Carl Peters

#pragma once

#include <CoreMinimal.h>

#include "SAPowerSentryPawn.h"

#include "SAPowerSentinelPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSentinelAbsorbedCB);

/**
 * 
 */
UCLASS()
class SENTINEL_API ASAPowerSentinelPawn final : public ASAPowerSentryPawn
{
	GENERATED_BODY()

	UPROPERTY(BlueprintAssignable)
	FOnSentinelAbsorbedCB OnSentinelAbsorbedCB;

public:

	ASAPowerSentinelPawn();

	//// Called when the game starts or when spawned
	//virtual void BeginPlay() override;

	virtual void AbsorbTarget() override;

	//virtual void ConvertClosestTreeIntoMeanie();

	bool HasMeanie() const { return Meanie != nullptr; };

	bool CanSpawnMeanie() const { return Meanie != nullptr; };

	UFUNCTION()
	void ConvertTreeIntoMeanie(ASAPowerPawn* Tree);

	//FVector GetMeanieHeadOffsetPosition() const { return MeanieHeadOffsetPosition; };

protected:
	virtual void BecomeAbsorbed(const bool Immediate) override;

	//virtual void RefreshTargetLists() override;

	//virtual void AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb) override;

	ASAPowerPawn* FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation) override;

	//UFUNCTION()
	//void InitMeanieHeadOffset();

	UFUNCTION()
	void OnMeanieAbsorbed();

	UFUNCTION(Server, Reliable)
	void Server_ConvertTreeIntoMeanie(ASAPowerPawn* Tree);

	/* Remember your currently spawned meanie. not sure if original game limited to one meanie only too. */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	class ASAPowerMeaniePawn* Meanie = nullptr;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle HasMeanieBlackboardKeyHandle;

	///* build a list of possible meanie spawn locations on RefreshTargetLists then cycle through them when you have to */
	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TArray<uint32/*tile*/> MeanieTreeTiles;

	///* Used to cycle every index in MeanieTreeTiles when spawning meanies */
	//UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	//int32 CurrentMeanieTreeIndex = 0;

	///** Store meanie head height for placing meanies */
	//UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Attributes)
	//float MeanieHeadOffsetPosition = 0.0f;

	///* list of all trees this character can see. boolean true means object can be absorbed */
	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TMap<int32, bool> TreeTiles;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle HasMeanieBlackboardKeyHandle;

private:
	UFUNCTION()
	void SetMeanie(ASAPowerMeaniePawn* NewMeanie);
};