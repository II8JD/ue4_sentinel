// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "SAPowerAbsorberPawn.h"
#include "SAPowerRobotPawn.generated.h"

enum class EPowerObjectType : uint8;

//USTRUCT(BlueprintType)
//struct SENTINEL_API FSABuildTileData
//{
//	GENERATED_BODY()
//
//public:
//	int32 TileIndex{0};
//	float AngleUntilDanger{0.0f};
//};

/**
 * 
 */
UCLASS()
class SENTINEL_API ASAPowerRobotPawn : public ASAPowerAbsorberPawn
{
	GENERATED_BODY()
	
public:
	ASAPowerRobotPawn();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	float GetRotateDistance() const override;

	//virtual void ScanForTargets() override;

	//virtual void RefreshTargetLists() override;

	/** Called via input escape to random location / exit level when on Sentinel's Plinth */
	UFUNCTION()
	void OnInputHyperspace();

	UFUNCTION()
	void TriggerHyperspace();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void ApplyabsorbTarget() override;

	virtual void BecomeAbsorbed(const bool Immediate) override;

	virtual void TrySpawnObjectAtLocation(const EPowerObjectType ObjectType, FVector& SpawnPos) override;
	template<EPowerObjectType ObjectType>
	void TrySpawnObject() { TrySpawnObjectAtLocation(ObjectType, CurrentTile); }

	//virtual void AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb) override;

	/** Teleports the player to a random position on the map */
	UFUNCTION(Server, Reliable)
	void Server_Hyperspace();

	virtual void UpdatePossessTarget(const FVector& InStart, const FVector& InTarget, const struct FCollisionQueryParams& InCollisionParams) override;

	virtual void PossessTarget() override;

	/** Called when our Controller no longer possesses us. */
	virtual void UnPossessed() override;

	/**
	* Called when this Pawn is possessed. Only called on the server (or in standalone).
	* @param NewController The controller possessing this pawn
	*/
	virtual void PossessedBy(AController* NewController) override;

	UFUNCTION()
	void UpdatePlayerVisibility();

	ASAPowerPawn* FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation) override;

	//virtual void ScanForTargets() override;

	/** The CapsuleComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	//UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	//class UCapsuleComponent* CapsuleComponent;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	//class USAPowerDataAsset* PowerObjectsDataAsset;

	UPROPERTY()
	bool IsOnPedestal = false;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle TargetCountBlackboardKeyHandle;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle CanAbsorbSentinelBlackboardKeyHandle;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle BuildLocationBlackboardKeyHandle;

	//UPROPERTY(EditAnywhere, Category = "AI Keys")
	//FUIBlackboardKeyHandle PowerBlackboardKeyHandle;

private:
	//UFUNCTION()
	//bool GetTargetIsShield();

	UFUNCTION()
	float GetTileSafety(int32 _tileIndex);

	UFUNCTION()
	void CalculateTargetLocation();

	//UFUNCTION()
	//void CalculateBuildTiles();

	//UFUNCTION()
	//float CalculateDangerAngle(int TileIndex);

	//UFUNCTION()
	//void UpdateBildTileDangerValues();

	//UFUNCTION()
	//void SortAndSetBuildTarget();

	///* list of all trees this character can see. boolean true means object can be absorbed */
	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> TreeTiles;

	///* list of all trees this character can see. boolean true means object can be absorbed */
	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TArray<int32 /*TileIndex*/> ShieldTiles;

	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> RobotTiles;

	////UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	////TMap<int32 /*TileIndex*/, bool /*CanAbsorb*/> SentinelAllyTiles;

	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//TArray<FSABuildTileData> BuildTiles;

	//UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	//bool bCanAbsorbSentinel = false;

	UPROPERTY(VisibleInstanceOnly, Category = Attributes)
	int32 PrevBuildTileIndex = -1;

private:
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	void DEBUG_Tick() const;
#endif
};
