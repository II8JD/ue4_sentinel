// Carl Peters

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SAGameStateBase.generated.h"

class ASATerrainActor;
class ASASkyActor;


/** Menu states */
UENUM()
enum class ESAGameState : uint8
{
	None,
	/** One off generate assets at start of game. its just a silly challenge to not add textures directly to the project*/
	GenerateAssets UMETA(DisplayName = "GS_GenerateAssets"),
	/** Main menu provides options for new game, continue options and quit */
	LoadingMenu UMETA(DisplayName = "GS_LoadingMenu"),
	/** We dont know if this might take longer in future/on slower hardware */
	LoadingGame UMETA(DisplayName = "GS_LoadingGame"),
	/** */
	PlayGame UMETA(DisplayName = "GS_PlayGame"),
	/** */
	PostMatch UMETA(DisplayName = "GS_PostMatch"),
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameStateChangedCB, const ESAGameState, NewState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRobotInteractCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerPossessionChangeCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRobotPowerUpdateCB, const int32, RobotPower);

/**
 * 
 */
UCLASS()
class ASAGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ASAGameStateBase();

	/** Property replication */
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Game State")
	void SetMainMenuState();

	UFUNCTION()
	void ExitToMainMenu();

	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Game State")
	void SetLoadGameSatate(const int32 NewSeed);

	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Game State")
	void SetPlayGameState();

	UFUNCTION(BlueprintCallable, CallInEditor, Category = "Game State")
	const ESAGameState GetState() const;

	UFUNCTION()
	const bool GetRobotHasAbsorbAbility() const { return bRobotHasAbsorbAbility; }

	UFUNCTION()
	const bool GetSentinelCanMove() const
	{
		return bSentinelCanMove;
	}

	UFUNCTION()
	float GetRobotVisibility() const 
	{ 
		return RobotVisibility; 
	}

	void SetRobotVisibility(float Visible)
	{
		RobotVisibility = Visible;
	}

	UFUNCTION()
	void SetGameState(ESAGameState NewState);

	UFUNCTION()
	void SetSeed(int32 NewSeed);

	UFUNCTION()
	const int32 GetSeed() const { return Seed; }

	UFUNCTION()
	void SetLevelData(FSALevelData& NewLevelData);

	UFUNCTION()
	void SetExpectedObjectCount();

	UFUNCTION()
	void TryGenerateLevel();

	UFUNCTION()
	void TryClosePreview(int32 PlayerID);

	UFUNCTION(BlueprintCallable, Category = "Robot")
	void SetRobotIsSpawningObject(const bool bCanSpawn);

	UFUNCTION(BlueprintPure, Category = "Robot")
	bool GetRobotIsSpawningObject() const { return bRobotIsSpawningObject; }

	/** Setter for Current Health. Clamps the value at 0 and calls OnPowerUpdate. Should only be called on the server.*/
	UFUNCTION(BlueprintCallable, Category = "Robot")
	void SetRobotPower(const int32 NewPower);

	/** Getter for Current Robot Health.*/
	UFUNCTION(BlueprintPure, Category = "Robot")
	int32 GetRobotPower() const { return RobotPower; }

	/** Response to health being updated. Called on the server immediately after modification, and on clients in response to a RepNotify*/
	void OnRobotPowerUpdate();

	UFUNCTION(BlueprintCallable, Category = "Robot")
	void SetRobotHasAbsorbAbility(bool bHasPower);

	UPROPERTY(BlueprintAssignable, Category = "Robot")
	FRobotInteractCB RobotInteractCB;

	UPROPERTY(BlueprintAssignable, Category = "Robot")
	FPlayerPossessionChangeCB RobotPossessionChangeCB;

	UPROPERTY(BlueprintAssignable, Category = "Robot")
	FRobotPowerUpdateCB RobotPowerUpdateCB;

	FGameStateChangedCB GameStateChangedCB;

protected:
	UPROPERTY(ReplicatedUsing = OnRep_ExpectedObjectCount)
	int32 ExpectedObjectCount = -1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<ASATerrainActor> DefaultTerrain;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<ASASkyActor> DefaultSky;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Attributes)
	TSubclassOf<AActor> DefaultSynthAudio;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMaterialInterface* CharacterImagesMaterial;

	UPROPERTY()
	ESAGameState CurrentState = ESAGameState::GenerateAssets;

	/* sets which player is to be the sentinel -1 for AI*/
	//UPROPERTY(replicated)
	//int32 SentinelPlayerIndex = -1;

	UPROPERTY(replicated)
	FSALevelData LevelData;

	UPROPERTY(ReplicatedUsing = OnRep_Seed)
	int32 Seed = -1;

	UPROPERTY()
	bool bHasSeed = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	class USALevelDataAsset* MainMenuLevelData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	bool bSentinelCanMove = false;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	AActor* SynthAudio = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	float RobotVisibility = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	int32 RandomColourSplit = 20;

	UPROPERTY(BlueprintReadOnly, Category = Attributes)
	class ASALevelRandomiserBase* CurrentRandomiser = nullptr;

	UPROPERTY(Replicated)
	bool bRobotIsSpawningObject = false;

	UPROPERTY()
	bool bRobotHasAbsorbAbility = true;

	UPROPERTY(ReplicatedUsing = OnRep_RobotPower)
	int32 RobotPower = 0;

	/** Players power at start of level */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Attributes)
	int32 RobotStartPower = 10;

	UPROPERTY()
	bool bSpawnedObjectManager = false;

	virtual void BeginPlay() override;

	UFUNCTION()
	void TestIsReadyToBegin();

	UFUNCTION()
	void OnRep_ExpectedObjectCount();

	UFUNCTION(Server, Reliable)
	void Server_ExitToMainMenu();

	UFUNCTION(Server, Reliable)
	void Server_SetLoadGameSatate(const int32 NewSeed);

	/** RepNotify for changes made to current power.*/
	UFUNCTION()
	void OnRep_RobotPower();

	UFUNCTION(BlueprintCallable, Category = "Gameplay Events")
	void OnRobotInteract();

	/** RepNotify for changes made to current seed.*/
	UFUNCTION()
	void OnRep_Seed();

	UFUNCTION()
	void OnSeedUpdate();

	/**
	* Registered callback broadcast from ASAPowerSentinelPawn.
	* if you absorb the sentinel you cant absorb anything else in the level! (Harsh?)
	*/
	UFUNCTION()
	void OnSentinelAbsorbedCB();

	UFUNCTION()
	void SpawnSky();

	UFUNCTION()
	void OnCharacterImagesComplete();
};
