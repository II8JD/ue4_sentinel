// Carl Peters

#pragma once

#include <CoreMinimal.h>
#include <Subsystems/GameInstanceSubsystem.h>

#include "SAObjectManager.generated.h"

class ASATerrainActor;
class ASATerrainChunk;
class ASAPowerPawn;
class USALevelDataAsset;
enum class EPowerObjectType : uint8;

USTRUCT()
struct FObjectList
{
	GENERATED_BODY()

public:
	TArray<TWeakObjectPtr<ASAPowerPawn>> Pawns;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FObjectRegisteredCB);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTerrainGeneratedCB, const USATerrainChunk*, TerrainChunk);

UCLASS(DisplayName = "Object Manager Subsystem")
class USAObjectManager : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	virtual bool ShouldCreateSubsystem(UObject* Outer) const override { return true; };

	void RegisterTerrainObject(USATerrainChunk& Chunk, bool bRegister = true);

	void RegisterPowerObject(ASAPowerPawn& Pawn, bool bRegister);

	const TArray<ASAPowerPawn*> GetPowerObjects(const EPowerObjectType PawnType);

	ASAPowerPawn* GetRandomSentinel(ASAPowerPawn* ExcludeOBject);

	USATerrainChunk* GetChunk();

	USATerrainChunk* GetChunkAtLocation(const FVector& Location);

	void GenerateTerrainAtLocation(UWorld* World, UClass* DefaultTerrain, const FVector& Location, int32 Seed, FSALevelData& LevelData);

	UFUNCTION()
	void OnTerrainGenerationComplete(ASATerrainActor* Terrain);

	bool GetRandomPosition(FVector& OutValue, const bool bEmptyPositionOnly, const bool bSearchUp, const bool bFavourHigher, float MaxHeight = -1.0f);

	float GetPlayerVisibility();

	int32 GetObjectCount();

	int32 GetPendingObjectCount() { return PendingObjectList.Num(); }

	FObjectRegisteredCB ObjectRegisteredCB;

	FTerrainGeneratedCB TerrainGeneratedCB;

protected:

	TArray<TWeakObjectPtr<USATerrainChunk>> ChunkList;

	TMap<EPowerObjectType, FObjectList> PowerObjectList;

	TArray<TWeakObjectPtr<ASAPowerPawn>> PendingObjectList;

	//TArray<> PendingTerrainList;
};
