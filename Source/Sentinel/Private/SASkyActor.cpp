// Carl Peters


#include "SASkyActor.h"

#include <Components/DirectionalLightComponent.h>
#include <Components/SkyAtmosphereComponent.h>
#include <Components/ExponentialHeightFogComponent.h>
#include <Components/SkyLightComponent.h>
#include <Engine/TextureRenderTarget2D.h>
#include <Kismet/KismetRenderingLibrary.h>

#include "Primitives/SAUVSphere.h"

// Sets default values
ASASkyActor::ASASkyActor()
{
	bReplicates = true;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SkySphereMesh = CreateDefaultSubobject<USAUVSphere>("SkySphere");
	RootComponent = SkySphereMesh;
	SkySphereMesh->bUseAsyncCooking = true;
	SkySphereMesh->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	SkySphereMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SkySphereMesh->SetGenerateOverlapEvents(false);
	SkySphereMesh->SetMobility(EComponentMobility::Static);
	SkySphereMesh->SetComponentTickEnabled(false);
	SkySphereMesh->SetTickableWhenPaused(false);
	SkySphereMesh->SetEnableGravity(false);
	SkySphereMesh->SetIsReplicated(false);
	SkySphereMesh->SetRelativeScale3D(FVector(5000.0f));

	FogComponent = CreateDefaultSubobject<UExponentialHeightFogComponent>(TEXT("ExponentialHeightFog"));
	FogComponent->SetMobility(EComponentMobility::Static);
	FogComponent->Activate();

	SkyAtmosphereComponent = CreateDefaultSubobject<USkyAtmosphereComponent>(TEXT("SkyAtmosphere"));
	SkyAtmosphereComponent->SetMobility(EComponentMobility::Movable);
	SkyAtmosphereComponent->Activate();

	SkyLightComponent = CreateDefaultSubobject<USkyLightComponent>(TEXT("SkyLight"));
	SkyLightComponent->SetMobility(EComponentMobility::Movable);
	SkyLightComponent->Activate();

	Sun = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Sun"));
	Sun->SetMobility(EComponentMobility::Movable);
	//Sun->bUsedAsAtmosphereSunLight = true;
	Sun->SetupAttachment(RootComponent);

	//Moon = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Moon"));
}

// Called when the game starts or when spawned
void ASASkyActor::BeginPlay()
{
	Super::BeginPlay();

	UBPFL_SAColour::OnColoursGeneratedCB.AddDynamic(this, &ASASkyActor::ApplyColours);
	RefreshColours();
}

void ASASkyActor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (!bGenerateCalled)
	{
		bGenerateCalled = true;

		SkySphereMesh->CompletionCB.AddDynamic(this, &ASASkyActor::OnSkySphereGenerated);
		SkySphereMesh->MakeUVSphereAsync(SkySubdivisions, false);
	}
}

// Called every frame
void ASASkyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//TODO: maybe move sun and moon
}

void ASASkyActor::OnSkySphereGenerated()
{
	DynamicSkyMaterial = UMaterialInstanceDynamic::Create(SkyMaterial, this);
	SkySphereMesh->SetMaterial(0, DynamicSkyMaterial);
	//SkySphereMesh->SetMaterial(0, SkyMaterial);

	CacheCloudTextures();
	DynamicSkyMaterial->SetTextureParameterValue(FName(TEXT("CloudsTexture")), CloudRenderTarget);

	ISAColourApplicatorInterface::Execute_ApplyColours(this);
}

void ASASkyActor::CacheCloudTextures()
{
	const int32 RenderTargetSize = 1024;
	const bool bIsLinearSpace = true;

	CloudRenderTarget = NewObject<UTextureRenderTarget2D>(this);
	CloudRenderTarget->InitAutoFormat(RenderTargetSize, RenderTargetSize);
	CloudRenderTarget->bNeedsTwoCopies = false;
	CloudRenderTarget->AddressX = TextureAddress::TA_Clamp;
	CloudRenderTarget->AddressY = TextureAddress::TA_Clamp;
	//CloudRenderTarget->bHDR = false;
	//CloudRenderTarget->Filter = TextureFilter::TF_Default;
	CloudRenderTarget->ClearColor = FLinearColor::Transparent;
	//CloudRenderTarget->SRGB = bIsLinearSpace;
	//CloudRenderTarget->TargetGamma = 1;
	//CloudRenderTarget->InitCustomFormat(RenderTargetSize, RenderTargetSize, EPixelFormat::PF_A8, bIsLinearSpace);
	CloudRenderTarget->UpdateResourceImmediate(true);

	//UMaterialInstanceDynamic* CloudMID = UMaterialInstanceDynamic::Create(CloudMaterial, this);

	UKismetRenderingLibrary::ClearRenderTarget2D(this, CloudRenderTarget, FLinearColor::Transparent);
	UKismetRenderingLibrary::DrawMaterialToRenderTarget(this, CloudRenderTarget, CloudMaterial);

	//CloudRenderTarget->UpdateResourceImmediate(true);
}