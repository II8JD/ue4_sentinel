// Carl Peters

#include "Sentinel/Public/DataAssets/SALevelDataAsset.h"

#if WITH_EDITOR
void USALevelDataAsset::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.Property != nullptr)
	{
		const FName PropertyName(PropertyChangedEvent.Property->GetFName());

		if (PropertyName == GET_MEMBER_NAME_CHECKED(USALevelDataAsset, LevelData.Tiles))
		{
			const int Points = (LevelData.Tiles + 1) * (LevelData.Tiles + 1);
			const int Total = Points;
			//if (LevelData.Heights.Num() != Total)
				//LevelData.Heights.SetNum(Total);
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif