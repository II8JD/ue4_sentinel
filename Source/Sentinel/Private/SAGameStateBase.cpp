// Carl Peters


#include "SAGameStateBase.h"

#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

#include "GameMode/SAGameModeBase.h"
#include "Terrain/SATerrainActor.h"

ASAGameStateBase::ASAGameStateBase() :
	ExpectedObjectCount(-1), Seed(-1)
{
	bReplicates = true;

	CurrentState = ESAGameState::GenerateAssets;

	RobotPossessionChangeCB.AddDynamic(this, &ASAGameStateBase::OnRobotInteract); 
}

void ASAGameStateBase::BeginPlay()
{
	Super::BeginPlay();

	if (DefaultSynthAudio)
	{
		GetWorld()->SpawnActor<AActor>(DefaultSynthAudio->GetAuthoritativeClass(), FVector::ZeroVector, FRotator::ZeroRotator);

		UE_LOG(LogTemp, Log, TEXT("Spawned Synth Audio"))
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No DefaultSynthAudio selected."))
	}

	if (GEngine->GetNetMode(GetWorld()) < NM_Client)
		RobotPower = RobotStartPower;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->ObjectRegisteredCB.AddDynamic(this, &ASAGameStateBase::TestIsReadyToBegin);

	USAGameInstance* gameInstance = GetGameInstance<USAGameInstance>();

	FOnTaskCompletionCB onCompletion = FOnTaskCompletionCB();
	onCompletion.BindUObject(this, &ASAGameStateBase::OnCharacterImagesComplete);
	USACharacterImageGenerator* imageGenerator = GetGameInstance()->GetSubsystem<USACharacterImageGenerator>();
	imageGenerator->GenerateImages(*gameInstance->GetPowerObjectsDataAsset(), CharacterImagesMaterial, onCompletion);

//	if (GEngine->GetNetMode(GetWorld()) == NM_DedicatedServer)
//	{
//		TryGenerateLevel();
//	}
//	if (!imageGenerator->IsImageGenerationComplete())
//	{
//		FOnTaskCompletionCB onCompletion = FOnTaskCompletionCB();
//		onCompletion.BindUObject(this, &ASAGameStateBase::TryGenerateLevel);
//
////		imageGenerator->GenerateCharacterImages(*PowerObjectsDataAsset, CharacterImagesMaterial, onCompletion);
//	}
//	else
//	{
//		TryGenerateLevel();
//	}
}

void ASAGameStateBase::GetLifetimeReplicatedProps(TArray <FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASAGameStateBase, Seed);

	DOREPLIFETIME(ASAGameStateBase, RobotPower);

	DOREPLIFETIME(ASAGameStateBase, LevelData);

	DOREPLIFETIME(ASAGameStateBase, ExpectedObjectCount);

	DOREPLIFETIME(ASAGameStateBase, bRobotIsSpawningObject);
}

void ASAGameStateBase::OnRep_ExpectedObjectCount()
{
	TestIsReadyToBegin();
}

void ASAGameStateBase::SetExpectedObjectCount()
{
	if (GetNetMode() < ENetMode::NM_Client)
	{
		USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
		ExpectedObjectCount = objectManager->GetObjectCount();
		UE_LOG(LogTemp, Warning, TEXT("ExpectedObjectCount %d"), ExpectedObjectCount)

		TestIsReadyToBegin();
	}
}

void ASAGameStateBase::TestIsReadyToBegin()
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	UE_LOG(LogTemp, Warning, TEXT("ExpectedObjectCount %d current %d"), ExpectedObjectCount, objectManager->GetObjectCount())
	if (ExpectedObjectCount > 0 && objectManager != nullptr &&
		objectManager->GetPendingObjectCount() == 0 &&
		ExpectedObjectCount == objectManager->GetObjectCount())
	{
		SetGameState(ESAGameState::PlayGame);
	}
}

void ASAGameStateBase::OnRep_Seed()
{
	OnSeedUpdate();
}

void ASAGameStateBase::OnSeedUpdate()
{
	bHasSeed = true;

	FMath::RandInit(Seed);
	UBPFL_SAColour::GenerateLevelColours(RandomColourSplit);

	//TryGenerateLevel();
}

void ASAGameStateBase::SetSeed(int32 NewSeed)
{
	if (GetNetMode() < ENetMode::NM_Client)
	{
		Seed = NewSeed;
		OnSeedUpdate();
	}
}

void ASAGameStateBase::SetLevelData(FSALevelData& NewLevelData)
{
	if (GetNetMode() < ENetMode::NM_Client)
	{
		//cant pass a pointer to an object that only exists on the client!
		LevelData = NewLevelData;
	}
}

void ASAGameStateBase::TryGenerateLevel()
{
	UE_LOG(GameModeLog, Log, TEXT("TryGenerateLevel"));

	if (Seed < 0)
		return;

	UE_LOG(GameModeLog, Log, TEXT("TryGenerateLevel has seed"));

	if(CurrentState == ESAGameState::GenerateAssets)
		return;

	SpawnSky();

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->GenerateTerrainAtLocation(GetWorld(), DefaultTerrain->GetAuthoritativeClass(), FVector::ZeroVector, Seed, LevelData);
}

void ASAGameStateBase::SpawnSky()
{
	if (GetNetMode() == ENetMode::NM_DedicatedServer)
	{
		return;
	}

	if (DefaultSky)
	{
		GetWorld()->SpawnActor<ASASkyActor>(DefaultSky->GetAuthoritativeClass(), FVector::ZeroVector, FRotator::ZeroRotator);

		UE_LOG(LogTemp, Log, TEXT("Spawned Sky"))
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No DefaultSky selected."))
	}
}

void ASAGameStateBase::SetGameState(ESAGameState NewState)
{
	CurrentState = NewState;

	if (GameStateChangedCB.IsBound())
	{
		GameStateChangedCB.Broadcast(CurrentState);
	}
}

void ASAGameStateBase::ExitToMainMenu()
{
	Server_ExitToMainMenu();
}

void ASAGameStateBase::Server_ExitToMainMenu_Implementation()
{
	if (GetLocalRole() == ROLE_Authority)
	{
		CurrentState = ESAGameState::LoadingMenu;
	}
}

void ASAGameStateBase::SetMainMenuState()
{
	CurrentState = ESAGameState::LoadingMenu;

	ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
	GameMode->SetSeedAndLoadLevel(0, MainMenuLevelData);
}

void ASAGameStateBase::SetLoadGameSatate(const int32 NewSeed)
{
	Server_SetLoadGameSatate(NewSeed);
}

void ASAGameStateBase::Server_SetLoadGameSatate_Implementation(const int32 NewSeed)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		if (ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>())
		{
			GameMode->SetSeedAndLoadLevel(NewSeed, MainMenuLevelData);

			CurrentState = ESAGameState::LoadingGame;

			bSentinelCanMove = false;
		}
	}
}

//void ASAGameStateBase::SetPrepareGameState()
//{
//	CurrentState = EGameState::PrepareGame;
//}

void ASAGameStateBase::SetPlayGameState()
{
	CurrentState = ESAGameState::PlayGame;

	//TScriptDelegate <FWeakObjectPtr> OnRobotInteract;
	//OnRobotInteract.BindUFunction(this, "OnRobotInteract");
	//ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
	//if (!Terrain->OnPowerObjectUpdateCB.Contains(OnRobotInteract))
		//Terrain->OnPowerObjectUpdateCB.AddDynamic(this, &ASAGameStateBase::OnRobotInteract);
}

const ESAGameState ASAGameStateBase::GetState() const
{
	return CurrentState;
}

void ASAGameStateBase::OnRobotInteract()
{
	bSentinelCanMove = true;
}

void ASAGameStateBase::TryClosePreview(int32 PlayerID)
{
	//GetWorld()->GetPlayerController();
	//if (SentinelPlayerIndex == -1 || SentinelPlayerIndex != PlayerID)
	//{
	//	GetP
	//}
	//else
	//{
	//}
}

void ASAGameStateBase::SetRobotIsSpawningObject(const bool bCanSpawn)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		bRobotIsSpawningObject = false;
	}
}

void ASAGameStateBase::SetRobotPower(const int32 NewPower)
{
	if (GetLocalRole() == ROLE_Authority)
	{
		RobotPower = FMath::Clamp(NewPower, 0, 100);
		OnRobotPowerUpdate();
	}
}

void ASAGameStateBase::OnRep_RobotPower()
{
	OnRobotPowerUpdate();
}

void ASAGameStateBase::OnRobotPowerUpdate()
{
	RobotPowerUpdateCB.Broadcast(RobotPower);
}

void ASAGameStateBase::OnSentinelAbsorbedCB()
{
	bRobotHasAbsorbAbility = false;
}

void ASAGameStateBase::SetRobotHasAbsorbAbility(bool bHasAbility)
{
	bRobotHasAbsorbAbility = bHasAbility;
}

void ASAGameStateBase::OnCharacterImagesComplete()
{
	SetGameState(ESAGameState::LoadingGame);

	TryGenerateLevel();
}