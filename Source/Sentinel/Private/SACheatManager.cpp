// Carl Peters


#include "SACheatManager.h"

bool USACheatManager::bPlayerInfinitePower = false;
bool USACheatManager::bPlayerIsInvulnerable = false;

void USACheatManager::SA_TogglePlayerIsInvulnerable()
{
	bPlayerIsInvulnerable = !bPlayerIsInvulnerable;
}

bool USACheatManager::GetPlayerIsInvulnerable()
{
	return bPlayerIsInvulnerable;
}

void USACheatManager::SA_TogglePlayerInfinitePower()
{
	bPlayerInfinitePower = !bPlayerInfinitePower;
}

bool USACheatManager::GetPlayerInfinitePower()
{
	return bPlayerInfinitePower;
}

void USACheatManager::SA_RobotInteract()
{
	ASAGameStateBase* GameState = GetWorld() != NULL ? GetWorld()->GetGameState<ASAGameStateBase>() : NULL;
	GameState->RobotInteractCB.Broadcast();
}