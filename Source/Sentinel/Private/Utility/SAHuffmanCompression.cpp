// Carl Peters

#include "Sentinel/Public/Utility/SAHuffmanCompression.h"

void USAHuffmanCompression::Encode(FHuffmanNode* Root, FString Str, TMap<TCHAR, FString>& HuffmanCode)
{
	if (Root == nullptr)
		return;

	// found a leaf node
	if (!Root->Left && !Root->Right) 
	{
		HuffmanCode.Add(Root->Ch, Str);
	}

	Encode(Root->Left, Str + "0", HuffmanCode);
	Encode(Root->Right, Str + "1", HuffmanCode);
}

void USAHuffmanCompression::Decode(FHuffmanNode* Root, int32& Index, FString Str)
{
	if (Root == nullptr) {
		return;
	}

	// found a leaf node
	if (!Root->Left && !Root->Right)
	{
		return;
	}

	Index++;

	if (Str[Index] == '0')
		Decode(Root->Left, Index, Str);
	else
		Decode(Root->Right, Index, Str);
}

void USAHuffmanCompression::BuildHuffmanTree(const TArray<int32>& Data)
{
	// count frequency of appearance of each character
	// and store it in a map
	TMap<int32, int32> Freq;
	for (int32 Ch : Data) 
	{
		if (Freq.Contains(Ch))
		{
			int32* Val = Freq.Find(Ch);
			*Val = *Val+1;
		}
		else
		{
			Freq.Add(Ch, 1);
		}
	}

	// Create a priority queue to store live nodes of
	// Huffman tree;
	TArray<FHuffmanNode> pq;

	// Create a leaf node for each character and add it
	// to the priority queue.
	for (const TPair<int32, int32>& pair : Freq)
	{
		pq.Add(FHuffmanNode(pair.Key, pair.Value, nullptr, nullptr));
	}

	// do till there is more than one node in the queue
	while (pq.Num() != 1)
	{
		// Remove the two nodes of highest priority
		// (lowest frequency) from the queue
		FHuffmanNode left = pq[0]; 
		pq.RemoveAt(0); //<<

		FHuffmanNode right = pq[0]; 
		pq.RemoveAt(0);

		// Create a new internal node with these two nodes
		// as children and with frequency equal to the sum
		// of the two nodes' frequencies. Add the new node
		// to the priority queue.
		int32 sum = left.Freq + right.Freq;
		//pq.RemoveAt(0);
		pq.Insert(FHuffmanNode('\0', sum, &left, &right), 0);
	}

	// root stores pointer to root of Huffman Tree
	FHuffmanNode* root = &pq[0];

	// traverse the Huffman Tree and store Huffman Codes
	// in a map. Also prints them
	TMap<TCHAR, FString> HuffmanCode;
	Encode(root, "", HuffmanCode);

	//cout << "Huffman Codes are :\n" << '\n';
	//for (auto pair : huffmanCode) {
		//cout << pair.first << " " << pair.second << '\n';
	//}

	//cout << "\nOriginal string was :\n" << text << '\n';

	// print encoded string
	//FString str = "";
	//for (TCHAR ch : Text) {
		//str.Append(*HuffmanCode.Find(ch));
	//}

	//cout << "\nEncoded string is :\n" << str << '\n';

	// traverse the Huffman Tree again and this time
	// decode the encoded string
	int index = -1;
	//cout << "\nDecoded string is: \n";
	//while (index < (int)str.Len() - 2) {
		//Decode(root, index, str);
	//}
}