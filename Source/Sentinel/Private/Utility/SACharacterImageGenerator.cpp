// Carl Peters

#include "Sentinel/Public/Utility/SACharacterImageGenerator.h"

#include <Components/SceneCaptureComponent2D.h>
#include <Engine/CanvasRenderTarget2D.h>
#include <Kismet/KismetMaterialLibrary.h>
#include <Kismet/KismetMathLibrary.h>
#include <Kismet/KismetRenderingLibrary.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Materials/MaterialInstanceDynamic.h>

//take some photos of power objects for use on UI
//i didnt want to add any image assets to the project so this generates some
//on boot. its a bit of a waste but i just want to see how fiew assets i can add
//I had a go at setting this up as a background task but it has too many components that 
//need to happen on the main thread
USACharacterImageGenerator::USACharacterImageGenerator()
{
	//AActor* captureActor = CreateDefaultSubobject<AActor>(TEXT("SceneCaptureRoot"));
	//captureActor->SetActorLocationAndRotation(FVector::ZeroVector, FQuat::Identity);

	//SceneCaptureComponent = ConstructComponent<USceneCaptureComponent2D>(TEXT("SceneCaptureComponent"));
	//SceneCaptureComponent->bCaptureEveryFrame = false;
	//SceneCaptureComponent->bCaptureOnMovement = false;
	//captureActor->SetRootComponent(SceneCaptureComponent);
}

void USACharacterImageGenerator::GenerateImages(USAPowerDataAsset& PowerObjectsAsset, UMaterialInterface* ParentMaterial, FOnTaskCompletionCB OnCompletion)
{
	if (GEngine->GetNetMode(GetWorld()) != NM_DedicatedServer)
	{
		AActor* tempActor = GetWorld()->SpawnActor<AActor>(AActor::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);

		SceneCaptureComponent = NewObject<USceneCaptureComponent2D>(tempActor);
		SceneCaptureComponent->bCaptureEveryFrame = false;
		SceneCaptureComponent->bCaptureOnMovement = false;
		SceneCaptureComponent->SetWorldTransform(FTransform::Identity);
		SceneCaptureComponent->SetVisibleFlag(true);
		SceneCaptureComponent->SetVisibility(true, true);

		const FName ParameterName = "TextureParam";
		const int32 RenderTargetSize = 1024;
		const float RenderTargetAspectRatio = (float)RenderTargetSize / RenderTargetSize;
		const bool MultiplyByRatio = RenderTargetSize > 1.0f;

		FActorSpawnParameters SpawnParams;
		SpawnParams.bNoFail = true;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		for (EPowerObjectType PowerObject : TEnumRange<EPowerObjectType>())
		{
			if (PowerObject == EPowerObjectType::MaxPowerObjectType)
			{
				continue;
			}

			UCanvasRenderTarget2D* RenderTarget = UCanvasRenderTarget2D::CreateCanvasRenderTarget2D(GetWorld(), UCanvasRenderTarget2D::StaticClass(), RenderTargetSize, RenderTargetSize);

			//Set up render target
			UKismetRenderingLibrary::ClearRenderTarget2D(GetWorld(), RenderTarget, FLinearColor(1, 0, 0, 1));
			SceneCaptureComponent->TextureTarget = RenderTarget;

			float HalfFOVRadians = FMath::Tan(FMath::DegreesToRadians(SceneCaptureComponent->FOVAngle / 2.0f));

			UMaterialInstanceDynamic* MaterialInstanceDynamic = UKismetMaterialLibrary::CreateDynamicMaterialInstance(GetWorld(), ParentMaterial);
			GeneratedImages.Add(PowerObject, MaterialInstanceDynamic);
			MaterialInstanceDynamic->SetTextureParameterValue(ParameterName, RenderTarget);

			//set up actor
			AActor* TempActor = GetWorld()->SpawnActor<AActor>(PowerObjectsAsset.GetPowerObjectBlueprint(PowerObject), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);

			//Get actor size 
			UStaticMeshComponent* TempActorMesh = Cast<UStaticMeshComponent>(TempActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));
			FVector TempActorOrigin = FVector::ZeroVector;
			FVector TempActoRBoxExtent = FVector::ZeroVector;
			float TempActorRadius = 0.0f;
			UKismetSystemLibrary::GetComponentBounds(TempActorMesh, TempActorOrigin, TempActoRBoxExtent, TempActorRadius);
			float TempActorSize = TempActoRBoxExtent.Size();

			const float Radius = MultiplyByRatio ? TempActorSize * RenderTargetAspectRatio : TempActorSize;
			const float Distance = Radius / HalfFOVRadians;

			//offset the actors location to fit on camera
			FVector NewPos = FVector::ZeroVector + FVector::ForwardVector * Distance;
			NewPos.Z = 0.0f - TempActorOrigin.Z;
			TempActor->SetActorLocation(NewPos, false);

			//capture the scene
			SceneCaptureComponent->CaptureScene();

			TempActor->Destroy();
		}
	}

	bImageGenerationComplete = true;
	OnCompletion.ExecuteIfBound();
}