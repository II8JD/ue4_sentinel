// Carl Peters

#include "Sentinel/Public/Utility/SAStatics.h"

#include <UObject/UObjectIterator.h>

float USAStatics::Repeat(float _t, float _length)
{
	return FMath::Clamp(_t - FMath::Floor(_t / _length) * _length, 0.0f, _length);
}

// PingPongs the value t, so that it is never larger than length and never smaller than 0.
float USAStatics::PingPong(float t, float length)
{
	t = Repeat(t, length * 2.0f);
	return length - FMath::Abs(t - length);
}