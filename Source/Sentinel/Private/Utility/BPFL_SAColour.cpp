// Carl Peters

#include "Sentinel/Public/Utility/BPFL_SAColour.h"

TArray<FLinearColor> UBPFL_SAColour::LevelRectangleColours;
FOnColoursGeneratedCB UBPFL_SAColour::OnColoursGeneratedCB;

void UBPFL_SAColour::GenerateLevelColours(int32 Split)
{
	LevelRectangleColours.Empty();

	FLinearColor LinearColour = FLinearColor(FColor::MakeRandomColor());
	FLinearColor HSV = LinearColour.LinearRGBToHSV();

	FLinearColor NewHSV = HSV;

	NewHSV.R = HSV.R + (360 - Split);
	LevelRectangleColours.Add(NewHSV.HSVToLinearRGB());

	NewHSV.R = HSV.R + Split;
	LevelRectangleColours.Add(NewHSV.HSVToLinearRGB());

	NewHSV.R = HSV.R + (180 + Split);
	LevelRectangleColours.Add(NewHSV.HSVToLinearRGB());

	NewHSV.R = HSV.R + (180 - Split);
	LevelRectangleColours.Add(NewHSV.HSVToLinearRGB());

	OnColoursGeneratedCB.Broadcast();
}

FColor UBPFL_SAColour::GetComplementaryColour(const FColor StartColour)
{
	FLinearColor LinearColour = FLinearColor(StartColour);

	//conversion to HSV
	FLinearColor HSV = LinearColour.LinearRGBToHSV();
	float Hue = (int32)(HSV.R + 180) % 359;
	//const float Saturation = HSV.G;
	//const float Value = HSV.B;
	//const float A = HSV.A;

	return HSV.ToFColor(true);
}

FLinearColor UBPFL_SAColour::GetRectangleColour(const ERectangleColour RectangleIndex)
{
	if (LevelRectangleColours.Num() == 0 ||
		LevelRectangleColours.Num() <= (uint8)RectangleIndex)
		return FColor::Black;

	return LevelRectangleColours[(uint8)RectangleIndex];
}