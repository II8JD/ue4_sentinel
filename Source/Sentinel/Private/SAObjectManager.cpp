// Carl Peters

#include "SAObjectManager.h"

#include <Templates/SharedPointer.h>

#include "DataAssets/SAPowerDataAsset.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "Pawns/PowerObjects/SAPowerSentryPawn.h"
#include "Terrain/SATerrainActor.h"
#include "Terrain/SATerrainChunk.h"

void USAObjectManager::RegisterTerrainObject(USATerrainChunk& Chunk, bool bRegister)
{
	if (bRegister)
	{
		ChunkList.AddUnique(&Chunk);

		if (Chunk.GetTerrainComplete())
		{
			const int32 ListNum = PendingObjectList.Num();
			if (ListNum > 0)
			{
				while (PendingObjectList.Num() > 0)
					RegisterPowerObject(*PendingObjectList[PendingObjectList.Num() - 1], bRegister);
			}
		}
	}
	else
	{
		ChunkList.Remove(&Chunk);
	}
}

void USAObjectManager::RegisterPowerObject(ASAPowerPawn& Pawn, bool bRegister)
{
	FVector ActorLocation = Pawn.GetActorLocation();
	USATerrainChunk* Chunk = GetChunkAtLocation(ActorLocation);
	if (Chunk == nullptr || !Chunk->GetTerrainComplete())
	{
		if (bRegister)
		{
			//add to pending list
			if (!PendingObjectList.Contains(&Pawn))
			{
				PendingObjectList.Add(&Pawn);
			}

			return;
		}
	}
	else
	{
		Chunk->AddPowerObject(Pawn, bRegister);
	}

	if (PendingObjectList.Contains(&Pawn))
	{
		PendingObjectList.Remove(&Pawn);
	}

	const EPowerObjectType PawnType = Pawn.GetType();
	if (!PowerObjectList.Contains(PawnType))
	{
		PowerObjectList.Add(PawnType, FObjectList());
	}
	FObjectList* ObjectList = PowerObjectList.Find(PawnType);

	if (bRegister)
	{
		ObjectList->Pawns.Add(&Pawn);
		
		ObjectRegisteredCB.Broadcast();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Removing pawn %s"), *Pawn.GetName())

		ObjectList->Pawns.Remove(&Pawn);
	}
}

const TArray<ASAPowerPawn*> USAObjectManager::GetPowerObjects(const EPowerObjectType PawnType)
{
	TArray<ASAPowerPawn*> outList;
	FObjectList* objectList = PowerObjectList.Find(PawnType);
	if(objectList == nullptr || objectList->Pawns.Num() == 0)
		return outList;

	for (int i = objectList->Pawns.Num() - 1; i >= 0; i--)
	{
		if (objectList->Pawns[i].IsValid())
		{
			outList.Add(objectList->Pawns[i].Get());
		}
		else
		{
			objectList->Pawns.RemoveAt(i);
		}
	}

	return outList;
}

ASAPowerPawn* USAObjectManager::GetRandomSentinel(ASAPowerPawn* ExcludeOBject)
{
	TArray<TWeakObjectPtr<ASAPowerPawn>> PowerObjectTargets;
	FObjectList* ObjectList = PowerObjectList.Find(EPowerObjectType::Sentinel);
	if(ObjectList != nullptr)
		PowerObjectTargets.Append(ObjectList->Pawns);

	ObjectList = PowerObjectList.Find(EPowerObjectType::Meanie);
	if (ObjectList != nullptr)
		PowerObjectTargets.Append(ObjectList->Pawns);

	ObjectList = PowerObjectList.Find(EPowerObjectType::Sentry);
	if (ObjectList != nullptr)
		PowerObjectTargets.Append(ObjectList->Pawns);

	PowerObjectTargets.Remove(ExcludeOBject);

	for (int i = PowerObjectTargets.Num() - 1; i >= 0; i--)
	{
		if (!PowerObjectTargets[1].IsValid())
		{
			PowerObjectTargets.RemoveAt(i);
		}
	}

	return PowerObjectTargets[FMath::RandRange(0, PowerObjectTargets.Num() - 1)].Get();
}

USATerrainChunk* USAObjectManager::GetChunk()
{
	if (ChunkList.Num() == 0)
		return nullptr;

	for (int i = ChunkList.Num() - 1; i >= 0; i--)
	{
		if (ChunkList[i].IsValid())
		{
			return ChunkList[i].Get();
		}
		else
		{
			ChunkList.RemoveAt(i);
		}
	}

	return nullptr;
}

USATerrainChunk* USAObjectManager::GetChunkAtLocation(const FVector& Location)
{
	if (ChunkList.Num() == 0)
		return nullptr;

	return GetChunk();
}

void USAObjectManager::GenerateTerrainAtLocation(UWorld* World, UClass* DefaultTerrain, const FVector& Location, int32 Seed, FSALevelData& LevelData)
{
	USATerrainChunk* Chunk = GetChunkAtLocation(Location);

	if (Chunk == nullptr)
	{
		{
			UE_LOG(GameModeLog, Log, TEXT("TryGenerateLevel new terrain"));

			ASATerrainActor* Terrain = World->SpawnActor<ASATerrainActor>(DefaultTerrain, Location, FRotator::ZeroRotator);
			Chunk = NewObject<USATerrainChunk>(Terrain);
			Chunk->RegisterComponent();

			//Chunk->SetLevelData(LevelData);

			Terrain->CompletionCB.AddDynamic(this, &USAObjectManager::OnTerrainGenerationComplete);
			Terrain->GenerateMesh(Seed, LevelData);
		}
	}
	else
	{
		UE_LOG(GameModeLog, Log, TEXT("TryGenerateLevel has terrain"));

		//Chunk->SetLevelData(LevelData);

		ASATerrainActor* TerrainActor = Chunk->GetTerrainActor();
		TerrainActor->GenerateMesh(Seed, LevelData);
	}
}

void USAObjectManager::OnTerrainGenerationComplete(ASATerrainActor* Terrain)
{
	USATerrainChunk* Chunk = Terrain->FindComponentByClass<USATerrainChunk>();
	TerrainGeneratedCB.Broadcast(Chunk);

	const int32 ListNum = PendingObjectList.Num();
	if (ListNum > 0)
	{
		while (PendingObjectList.Num() > 0)
			RegisterPowerObject(*PendingObjectList[PendingObjectList.Num() - 1], true);
	}
}

bool USAObjectManager::GetRandomPosition(FVector& OutValue, const bool bEmptyPositionOnly, const bool bSearchUp, const bool bFavourHigher, float MaxHeight)
{
	if (ChunkList.Num() == 0)
		return false;

	USATerrainChunk* Chunk = GetChunk();
	return Chunk->GetRandomPosition(OutValue, bEmptyPositionOnly, bSearchUp, bFavourHigher, MaxHeight);
}

float USAObjectManager::GetPlayerVisibility()
{
	float CurrentVis = 0.0f;

	if (ChunkList.Num() == 0)
		return 0.0f;

	USATerrainChunk* Chunk = GetChunk();

	for (TArray<int32>::TConstIterator It = Chunk->PowerObjectTiles.CreateConstIterator(); It; ++It)
	{
		FVector TileLocation;
		Chunk->GetTileLocationFromIndex(*It, TileLocation);

		const ASAPowerSentryPawn* Sentry = Chunk->GetTileTopObject<ASAPowerSentryPawn>(TileLocation);
		if (Sentry == nullptr)
			continue;

		float Vis = Sentry->GetPlayerVisibility();
		if (Vis > CurrentVis)
		{
			CurrentVis = Vis;
			if (Vis == 1.0f)
				break;
		}
	}

	return CurrentVis;
}

int32 USAObjectManager::GetObjectCount() 
{
	int32 ObjectCount = ChunkList.Num();
	UE_LOG(LogTemp, Warning, TEXT("GetObjectCount terrains = %d"), ChunkList.Num())
	for (const TPair<EPowerObjectType, FObjectList>& pair : PowerObjectList)
	{
		ObjectCount += pair.Value.Pawns.Num();

		FText PowerObjectTypeName;
		if (const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EPowerObjectType"), true))
		{
			PowerObjectTypeName = EnumPtr->GetDisplayNameTextByIndex(EnumPtr->GetIndexByValue((int64)pair.Key));
		}

		UE_LOG(LogTemp, Warning, TEXT("GetObjectCount pawn %s = %d"), *PowerObjectTypeName.ToString(), pair.Value.Pawns.Num())
	}

	return ObjectCount;
}