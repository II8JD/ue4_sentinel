// Carl Peters

#include "Sentinel/Public/Primitives/SAIcosphere.h"

#include <array>

USAIcosphere::USAIcosphere(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void USAIcosphere::FindUV(const FVector& normal, FVector2D& uv)
{
    const float& x = normal.X;
    const float& y = normal.Y;
    const float& z = normal.Z;
    float normalisedX = 0;
    float normalisedZ = -1;
    if (((x * x) + (z * z)) > 0)
    {
        normalisedX = sqrt((x * x) / ((x * x) + (z * z)));
        if (x < 0)
        {
            normalisedX = -normalisedX;
        }
        normalisedZ = sqrt((z * z) / ((x * x) + (z * z)));
        if (z < 0)
        {
            normalisedZ = -normalisedZ;
        }
    }
    if (normalisedZ == 0)
    {
        uv.X = ((normalisedX * PI) / 2);
    }
    else
    {
        uv.X = atan(normalisedX / normalisedZ);
        if (normalisedX < 0)
        {
            uv.X = PI - uv.X;
        }
        if (normalisedZ < 0)
        {
            uv.X += PI;
        }
    }
    if (uv.X < 0)
    {
        uv.X += 2 * PI;
    }
    uv.X /= 2 * PI;
    uv.Y = (-y + 1) / 2;
}

void USAIcosphere::MakeIcosphereAsync(uint8 subdivisions, bool bInverse)
{
    FTaskCompletionCB onCompletion = FTaskCompletionCB();
    onCompletion.BindUObject(this, &USAIcosphere::OnCalculationsComplete);

    (new FAutoDeleteAsyncTask<IcosphereGenerateTask>(subdivisions, bInverse, this, onCompletion))->StartBackgroundTask();
}

void USAIcosphere::MakeIcosphere(uint8 subdivisions, bool bInverse)
{
    m_vertices.Reset(16);
    m_triangles.Reset(20);

    m_vertices.Append(icosahedron::vertices, UE_ARRAY_COUNT(icosahedron::vertices));
    m_triangles.Append(icosahedron::triangles, UE_ARRAY_COUNT(icosahedron::triangles));

    if (bInverse)
    {
        for (int i = 0; i < m_triangles.Num(); i++)
        {
            int tempX = m_triangles[i].vert[0];
            m_triangles[i].vert[0] = m_triangles[i].vert[2];
            m_triangles[i].vert[2] = tempX;
        }
    }

    Normalize(); //just to be sure
    for (int i = 0; i < subdivisions; ++i)
    {
        Subdivide();
    }
    MapUV();
}

void USAIcosphere::Normalize()
{
	for (FVector& each : m_vertices)
	{
		each.Normalize();
	}
}

uint32 USAIcosphere::VertexForEdge(uint32 vi1, uint32 vi2)
{
    uint32 a = FMath::Min(vi1, vi2);
    uint32 b = FMath::Max(vi1, vi2);

    TPair<uint32, uint32> key(a, b);
    int32 num = m_vertices.Num();
    if (lookup.Contains(key))
    {
       return *lookup.Find(key);
    }
    else
    {
        lookup.Add(key, num);
        FVector& edge0 = m_vertices[a];
        FVector& edge1 = m_vertices[b];
        auto point = edge0 + edge1;
        point.Normalize();
        m_vertices.Add(point);
    }
    return num;
}

void USAIcosphere::Subdivide()
{
    TArray<Triangle> swap_sphere;
    swap_sphere.Reserve(m_triangles.Num() * 3);

    for (auto&& triangle : m_triangles)
    {
        TArray<int32> mid;
        mid.Init(0, 3);
        for (int edge = 0; edge < 3; ++edge)
        {
            mid[edge] = VertexForEdge(triangle.vert[edge], triangle.vert[(edge + 1) % 3]);
        }

        swap_sphere.Add({ triangle.vert[0], mid[0], mid[2] });
        swap_sphere.Add({ triangle.vert[1], mid[1], mid[0] });
        swap_sphere.Add({ triangle.vert[2], mid[2], mid[1] });
        swap_sphere.Add({ mid[0], mid[1], mid[2] });
    }
    Swap(m_triangles, swap_sphere); // no new memory needed
    lookup.Reset();
}

//void USAIcosphere::MapUV()
//{
//    m_uvmapping.Empty(m_vertices.Num());
//    for (int32 i = 0; i < m_vertices.Num(); ++i)
//    {
//        FVector2D UV;
//        m_uvmapping.Add(UV);
//        FindUV(m_vertices[i], UV);
//    }
//}

void USAIcosphere::MapUV()
{
    m_uvmapping.Empty(m_vertices.Num());
    for (int32 i = 0; i < m_vertices.Num(); ++i)
    {
        m_uvmapping.Add(FVector2D(
            .5f - (FMath::Atan2(m_vertices[i].X, m_vertices[i].Y) / PI / 2),
            .5f - FMath::Asin(m_vertices[i].Z) / PI));
    }
}

void USAIcosphere::OnCalculationsComplete()
{
    FGraphEventRef Task = FFunctionGraphTask::CreateAndDispatchWhenReady([&]()
    {
        // Code placed here will run in the game thread
        TArray<int32> Triangles = TArray<int>((int*)m_triangles.GetData(), 3 * m_triangles.Num());
        TArray<FProcMeshTangent> Tangents;
        TArray<FColor> Colours;

        CreateMeshSection(0, m_vertices, Triangles, m_vertices, m_uvmapping, Colours, Tangents, true);

        // Code placed here will run in the game thread
        if (CompletionCB.IsBound())
        {
            CompletionCB.Broadcast();
        }

    }, TStatId(), NULL, ENamedThreads::GameThread);
}

//=======================================================================

void IcosphereGenerateTask::DoWork()
{
    Icosphere->MakeIcosphere(Subdivisions, bInverse);

    OnCompletion.ExecuteIfBound();
}