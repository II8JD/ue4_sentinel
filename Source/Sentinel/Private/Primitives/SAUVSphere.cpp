// Carl Peters

#include "Sentinel/Public/Primitives/SAUVSphere.h"

#include "Utility/SAStatics.h"

#include <KismetProceduralMeshLibrary.h>

USAUVSphere::USAUVSphere(const class FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void USAUVSphere::MakeUVSphereAsync(const int32 _segments, const bool _bInverse)
{
    FTaskCompletionCB onCompletion = FTaskCompletionCB();
    onCompletion.BindUObject(this, &USAUVSphere::OnCalculationsComplete);

    (new FAutoDeleteAsyncTask<UVSphereGenerateTask>(_segments, _bInverse, this, onCompletion))->StartBackgroundTask();
}

void USAUVSphere::MakeUVSphere(const int32 _segments, const bool _bInverse)
{
    Segments = _segments % 2 == 0 ? _segments + 1 : _segments;
    bInverse = _bInverse;

    GenerateVertices();
    GenerateTriangles();
    GenerateUVs();
}

void USAUVSphere::GenerateVertices()
{
    Vertices.Init(FVector::ZeroVector, Segments * Segments);
    float segSize = ((PI * (2.0999f + FLT_EPSILON)) / Segments);
    for (int32 i = 0; i < Segments; i++)
    {
        float lat = PI * (i / (float)(Segments-1)); //angle north or south
        lat = FMath::Clamp(lat, FLT_EPSILON, PI - FLT_EPSILON); //clamp fixes twisted poles
        for (int j = 0; j < Segments; j++)
        {
            //we dont want the uv of the final triangle to be 1.0 0.0
            // as this will cause those triangles to have the entier image
            // suased and reversed along one side of the sphere from one pole to the other
            // 
            //   0.0
            //1.0 |
            //  | |
            //|/|/|/|
            float lon = (((PI * (2.0f + FLT_EPSILON)) + segSize) //add some additional space ((PI * 2.0f) / Segments)
                * (j / (float)(Segments)));
            lon += PI * 1.5f; //rotate the triangles a bit to match up with UV mapping
            Vertices[Segments * i + j] = FVector(FMath::Sin(lat) * FMath::Cos(lon), FMath::Sin(lat) * FMath::Sin(lon), FMath::Cos(lat));
        }
    }
}

void USAUVSphere::GenerateTriangles()
{
    UKismetProceduralMeshLibrary::CreateGridMeshTriangles(Segments, Segments, !bInverse, Triangles);
}

void USAUVSphere::GenerateUVs()
{
    UVMapping.Empty(Vertices.Num());
    for (int32 i = 0; i < Vertices.Num(); ++i)
    {
        //UVMapping.Add(FVector2D(
        //    .5f - (FMath::Atan2(Vertices[i].X, Vertices[i].Y) / PI * 0.5f),
        //    .5f - FMath::Asin(Vertices[i].Z) / PI));

        UVMapping.Add(FVector2D(
            .5f - (FMath::Atan2(Vertices[i].X, Vertices[i].Y) / PI * 0.5f),
            USAStatics::PingPong((.5f - FMath::Asin(Vertices[i].Z) / PI) * 2.0f, 1.0f)) );
    }
}

void USAUVSphere::OnCalculationsComplete()
{
    FGraphEventRef Task = FFunctionGraphTask::CreateAndDispatchWhenReady([&]()
    {
        // Code placed here will run in the game thread
        TArray<FProcMeshTangent> Tangents;
        TArray<FColor> Colours;

        CreateMeshSection(0, Vertices, Triangles, Vertices, UVMapping, Colours, Tangents, true);

        // Code placed here will run in the game thread
        if (CompletionCB.IsBound())
        {
            CompletionCB.Broadcast();
        }

    }, TStatId(), NULL, ENamedThreads::GameThread);
}

//=======================================================================

void UVSphereGenerateTask::DoWork()
{
    UVSphere->MakeUVSphere(Segments, bInverse);

    OnCompletion.ExecuteIfBound();
}