// Carl Peters

#include "Sentinel/Public/PlayerControllers/SAAIControllerSentinel.h"

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "Sentinel/Public/SAGameInstance.h"
#include "Sentinel/Public/SAGameStateBase.h"
#include "Sentinel/Public/Terrain/SATerrainChunk.h"

void ASAAIControllerSentinel::BeginPlay()
{
	Super::BeginPlay();

	InitMeanieHeadOffset();
}

void ASAAIControllerSentinel::InitMeanieHeadOffset()
{
	USAGameInstance* gameInstance = GetGameInstance<USAGameInstance>();
	USAPowerDataAsset* powerObjectsDA = gameInstance->GetPowerObjectsDataAsset();

	//Get Meanie head offset
	const TSubclassOf<ASAPowerPawn> meanieBlueprint = powerObjectsDA->GetPowerObjectBlueprint(EPowerObjectType::Meanie);
	if (meanieBlueprint == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie prefab."))
			return;
	}

	const ASAPowerPawn* meaniePawn = meanieBlueprint->GetDefaultObject<ASAPowerPawn>();
	if (meaniePawn == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to get Meanie default object."))
			return;
	}

	const UStaticMeshComponent* meshComponent = meaniePawn->FindComponentByClass<UStaticMeshComponent>();
	if (meshComponent == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie mesh component."))
			return;
	}

	const UStaticMeshSocket* socket = meshComponent->GetSocketByName("HeadSocket");
	if (socket == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie HeadSocket."))
			return;
	}

	MeanieHeadOffsetPosition = socket->RelativeLocation.Z;
}

void ASAAIControllerSentinel::RefreshTargetLists()
{
	Super::RefreshTargetLists();

	CurrentMeanieTreeIndex = 0;
	MeanieTreeTiles.Empty();

	ASAPowerSentinelPawn* sentinel = GetPawn<ASAPowerSentinelPawn>();
	if (sentinel == nullptr)
		return;

	if (ControlledRobot == nullptr)
		return;

	const FVector TargetRobotPos = ControlledRobot->GetActorLocation();
	FVector OutFoundBlock = FVector::ZeroVector;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	//calculate good spots to spawn meanies
	//a meanie can only be spawned where there is a tree it temporeraly replaces it
	//im not sure what the original game does but the tree has to be absorbable too
	//all trees in TreeTiles can be absorbed search this list and add trees that can see 
	//the player to MeanieTreeTiles.
	TMap<uint32/*tile*/, float/*distance*/> DistanceSorted;
	for (const TPair<int32, bool>& pair : TreeTiles)
	{
		USATerrainChunk* Chunk = objectManager->GetChunk();
		ASAPowerPawn* TopObject = Chunk->GetTileTopObject(pair.Key);
		if (!TopObject)
			continue;

		FCollisionQueryParams TempCollisionParams;
		TempCollisionParams.AddIgnoredActor(TopObject);
		FVector TopObjectPos = TopObject->GetActorLocation();
		if (ControlledRobot == sentinel->CanAbsorbObject(TopObjectPos.Z, TopObjectPos + FVector::UpVector * MeanieHeadOffsetPosition, TargetRobotPos, TempCollisionParams, false, OutFoundBlock))
			DistanceSorted.Add(pair.Key, FVector::Dist(TopObjectPos, TargetRobotPos));
	}

	DistanceSorted.ValueSort([](const float& A, const float& B) {
		return A < B; // sort strings by length
		});

	DistanceSorted.GenerateKeyArray(MeanieTreeTiles);
}

void ASAAIControllerSentinel::SetHasMeanie(bool bHasMeanie)
{
	if (!ensureMsgf(GetNetMode() != ENetMode::NM_Client, TEXT("Server only function")))
	{
		return;
	}

	SetBlackboardBool(HasMeanieBlackboardKeyHandle, bHasMeanie);
}

void ASAAIControllerSentinel::ConvertClosestTreeIntoMeanie()
{
	ASAPowerSentinelPawn* myPawn = Cast<ASAPowerSentinelPawn>(GetPawn());
	if (myPawn == nullptr || !myPawn->HasMeanie())
		return;

	const int32 TreeCount = MeanieTreeTiles.Num();
	if (TreeCount == 0)
		return;

	if (CurrentMeanieTreeIndex >= TreeCount)
		CurrentMeanieTreeIndex = 0;

	ASAPowerAbsorberPawn* absorber = GetPawn<ASAPowerAbsorberPawn>();
	if (absorber == nullptr)
		return;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* Chunk = objectManager->GetChunkAtLocation(absorber->GetCurrentTile());
	ASAPowerPawn* TopObject = Chunk->GetTileTopObject(MeanieTreeTiles[CurrentMeanieTreeIndex]);
	if (TopObject == nullptr)
		return;

	CurrentMeanieTreeIndex++;

	
	myPawn->ConvertTreeIntoMeanie(TopObject);
}