// Carl Peters


#include "Sentinel/Public/PlayerControllers/SAPlayerControllerEdit.h"

#include <Blueprint/UserWidget.h>

#include "GameMode/SAGameModeBaseEditor.h"
#include "Pawns/SAEditLevelPawn.h"

void ASAPlayerControllerEdit::BeginPlay()
{
	Super::BeginPlay();

	FInputModeGameOnly InputMode;
	SetInputMode(InputMode);
	bShowMouseCursor = false;
}

// Called to bind functionality to input
void ASAPlayerControllerEdit::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("ToggleMouse", IE_Pressed, this, &ASAPlayerControllerEdit::ToggleMouse);
	InputComponent->BindAction("TogglePlayMode", IE_Pressed, this, &ASAPlayerControllerEdit::TogglePlayMode);
}

void ASAPlayerControllerEdit::ToggleMouse()
{
	bShowMouseCursor = !bShowMouseCursor;

	if (bShowMouseCursor)
	{
		FInputModeUIOnly InputMode;
		SetInputMode(InputMode);
	}
	else
	{
		FInputModeGameOnly InputMode;
		SetInputMode(InputMode);
	}
}

void ASAPlayerControllerEdit::TogglePlayMode()
{
	//ASAGameModeBaseEditor *GameMode = GetWorld()->GetAuthGameMode< ASAGameModeBaseEditor>();
	//if (GameMode != nullptr)
	//{
	//	GameMode->TogglePlayMode();

	//	if (CurrentMenuWidget)
	//		CurrentMenuWidget->RemoveFromViewport();

	//	if (GameMode->IsPlayMode())
	//	{
	//		if (DefaultHUDWidget != nullptr)
	//		{
	//			CurrentMenuWidget = CreateWidget<UUserWidget>(GetWorld(), DefaultHUDWidget);
	//			CurrentMenuWidget->AddToViewport();
	//		}
	//		else
	//		{
	//			UE_LOG(LogTemp, Warning, TEXT("DefaultHUDWidget is null."))
	//		}
	//	}
	//}
}