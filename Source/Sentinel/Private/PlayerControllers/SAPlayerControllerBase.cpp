﻿// Carl Peters

#include "Sentinel/Public/PlayerControllers/SAPlayerControllerBase.h"

#include <BehaviorTree/BehaviorTree.h>
#include <BehaviorTree/BlackboardComponent.h>
#include <Blueprint/UserWidget.h>
#include <Blueprint/WidgetLayoutLibrary.h>
#include <Engine/StaticMeshActor.h>
#include <GameFramework/PlayerController.h>
#include <GameFramework/PlayerState.h>
#include <Net/UnrealNetwork.h>
#include <Text3DActor.h>

#include <UIStatics.h>

#include "GameMode/SAGameModeBase.h"
#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"
#include "SAHUD.h"

#if WITH_EDITOR 
#include "SACheatManager.h"
#endif

ASAPlayerControllerBase::ASAPlayerControllerBase()
{
	bReplicates = true;

#if WITH_EDITOR
	CheatClass = USACheatManager::StaticClass();
#endif
}

void ASAPlayerControllerBase::BeginPlay()
{
	Super::BeginPlay();

	UWidgetLayoutLibrary::RemoveAllWidgets(this);

	ASAGameStateBase* GameState = GetWorld()->GetGameState<ASAGameStateBase>();
	GameState->GameStateChangedCB.AddDynamic(this, &ASAPlayerControllerBase::OnGameStateChanged);
}

void ASAPlayerControllerBase::OnGameStateChanged(const ESAGameState NewState)
{
	UUIStatics::SetUIBlackboardEnum(this, GameStateBlackboardKeyHandle, (uint8)NewState);
}