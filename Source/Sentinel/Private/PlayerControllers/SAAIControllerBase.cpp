// Carl Peters

#include "Sentinel/Public/PlayerControllers/SAAIControllerBase.h"

#include <UIControllerGraphPlugin/Public/UIBlackboardKeyHandle.h>

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerAbsorberPawn.h"
#include "Sentinel/Public/SAGameInstance.h"
#include "Sentinel/Public/SAGameStateBase.h"
#include "Sentinel/Public/Terrain/SATerrainChunk.h"

ASAPowerPawn* FSATileData::FindClosestTile(const UWorld* World, const FVector _LookLocation, const FVector _LookDirection, float _HalfViewAngle, float& OutAngle_, bool& bOutCanAbsorb_)
{
	ASAPowerPawn* bestObject = nullptr;
	USATerrainChunk* Chunk = World->GetGameInstance()->GetSubsystem<USAObjectManager>()->GetChunk();
	float BestAngle = FLT_MAX;
	bool bCanAbsorbBest = false;
	//const FVector HeadLocation = absorber->GetHeadLocation();

	for (const TPair<int32/*tile id*/, bool/*bCanAbsorb*/>& pair : Tiles)
	{
		FVector TileLocation;
		Chunk->GetTileLocationFromIndex(pair.Key, TileLocation);

		//see if this tile is within view angle
		FVector ToPosition = FVector(TileLocation.X, TileLocation.Y, _LookLocation.Z) - _LookLocation;
		//FVector DebugPos = ToPosition;
		ToPosition.Normalize();
		const float Dot = (1.0f - (0.5f * (1.0f + FVector::DotProduct(_LookDirection, ToPosition)))) * 180.0f;
		//UE_LOG(SentinelAILog, Verbose, TEXT("Tile Dot %f"), Dot)
		//DrawDebugLine(GetWorld(), HeadLocation, HeadLocation + ToPosition * DebugPos.Size(), FColor::Blue);
		if (Dot <= _HalfViewAngle && Dot < BestAngle)
		{
			ASAPowerPawn* TopObject = Chunk->GetTileTopObject(TileLocation);
			//if you can absorb the target or if you have a partial view of the player
			if (pair.Value/*bCanAbsorb*/ || (TopObject != nullptr && TopObject->IsPlayerControlled()))
			{
				//gameplay question if the sentinel sees a partial box/robot will he still try to spawn 
				//a meanie to hyperspace it? ir does it just ignore it?
				//current implementation: just ignore it

				BestAngle = Dot;
				bCanAbsorbBest = pair.Value;
				bestObject = TopObject;
			}
		}
	}
	bOutCanAbsorb_ = bCanAbsorbBest;
	OutAngle_ = BestAngle;

	return bestObject;
}

void ASAAIControllerBase::BeginPlay()
{
	Super::BeginPlay();

	ASAGameStateBase* const GameState = GetWorld() != nullptr ? GetWorld()->GetGameState<ASAGameStateBase>() : nullptr;
	if (GameState == nullptr)
	{
		GameState->RobotPossessionChangeCB.AddDynamic(this, &ASAAIControllerBase::OnPowerObjectUpdate);
	}
}

void ASAAIControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	OnPowerObjectUpdate();
}

void ASAAIControllerBase::OnPowerObjectUpdate()
{
	if (IsPendingKillPending())
		return;

	RefreshTargetLists();
	//bRefreshTargetsrequired = true;
	ScanForTargets();
}

/* Search list for target in angle range */
void ASAAIControllerBase::ScanForTargets()
{
	bool bCanAbsorbBest = false;
	float angle = FLT_MAX;
	FindClosestTarget(TargetTiles, angle, bCanAbsorbBest);

	ASAPowerAbsorberPawn* absorber = GetPawn<ASAPowerAbsorberPawn>();
	ASAPowerPawn* bestObject = TargetTiles.FindClosestTile(GetWorld(), absorber->GetHeadLocation(), absorber->GetHeadRotation(),
		HalfViewAngle, angle, bCanAbsorbBest);
	
	absorber->SetCurrentPowerObject(bestObject, bCanAbsorbBest);
	

	//bCanabsorbCurrentPowerObject = bCanAbsorbBest;

	//SetBlackboardBool(HasTargetBlackboardKeyHandle, CurrentPowerObject != nullptr);
	//SetBlackboardBool(CanAbsorbTargetBlackboardKeyHandle, bCanabsorbCurrentPowerObject);
}

void ASAAIControllerBase::FindClosestTarget(const FSATileData& _ObjectTiles, float& OutAngle_, bool& bOutCanAbsorb_) const
{
	//ASAPowerAbsorberPawn* absorber = GetPawn<ASAPowerAbsorberPawn>();
	//if (absorber == nullptr)
	//	return;

	//ASAPowerPawn* BestObject = nullptr;
	//USATerrainChunk* Chunk = ObjectManager->GetChunk();
	//FVector lookDirection = absorber->GetHeadRotation();
	//float BestAngle = FLT_MAX;
	//bool bCanAbsorbBest = false;
	//const FVector HeadLocation = absorber->GetHeadLocation();

	//for (const TPair<int32/*tile id*/, bool/*bCanAbsorb*/>& pair : _ObjectTiles)
	//{
	//	FVector TileLocation;
	//	Chunk->GetTileLocationFromIndex(pair.Key, TileLocation);

	//	//see if this tile is within view angle
	//	FVector ToPosition = FVector(TileLocation.X, TileLocation.Y, HeadLocation.Z) - HeadLocation;
	//	//FVector DebugPos = ToPosition;
	//	ToPosition.Normalize();
	//	const float Dot = (1.0f - (0.5f * (1.0f + FVector::DotProduct(lookDirection, ToPosition)))) * 180.0f;
	//	//UE_LOG(SentinelAILog, Verbose, TEXT("Tile Dot %f"), Dot)
	//	//DrawDebugLine(GetWorld(), HeadLocation, HeadLocation + ToPosition * DebugPos.Size(), FColor::Blue);
	//	if (Dot <= HalfViewAngle && Dot < BestAngle)
	//	{
	//		ASAPowerPawn* TopObject = Chunk->GetTileTopObject(TileLocation);
	//		//if you can absorb the target or if you have a partial view of the player
	//		if (pair.Value/*bCanAbsorb*/ || (TopObject != nullptr && TopObject->IsPlayerControlled()))
	//		{
	//			//gameplay question if the sentinel sees a partial box/robot will he still try to spawn 
	//			//a meanie to hyperspace it? ir does it just ignore it?
	//			//current implementation: just ignore it

	//			BestAngle = Dot;
	//			bCanAbsorbBest = pair.Value;
	//			BestObject = TopObject;
	//		}
	//	}
	//}
	//_bOutCanAbsorb = bCanAbsorbBest;
	//_OutAngle = BestAngle;


	//absorber->SetCurrentPowerObject(BestObject, _bOutCanAbsorb);
}

void ASAAIControllerBase::SetBlackboardBool(FUIBlackboardKeyHandle& _handle, bool _bValue)
{
	if (UBlackboardComponent* BB = GetBlackboardComponent())
		BB->SetValueAsBool(_handle.KeyName, _bValue);
}

void ASAAIControllerBase::SetBlackboardFloat(FUIBlackboardKeyHandle& _handle, float _value)
{
	if (UBlackboardComponent* BB = GetBlackboardComponent())
		BB->SetValueAsFloat(_handle.KeyName, _value);
}

void ASAAIControllerBase::SetBlackboardInt(FUIBlackboardKeyHandle& _handle, int32 _value)
{
	if (UBlackboardComponent* BB = GetBlackboardComponent())
		BB->SetValueAsInt(_handle.KeyName, _value);
}

bool ASAAIControllerBase::SetBlackboardVector(FUIBlackboardKeyHandle& _handle, FVector _value)
{
	if (UBlackboardComponent* BB = GetBlackboardComponent())
	{
		BB->SetValueAsVector(_handle.KeyName, _value);
		return true;
	}

	return false;
}

void ASAAIControllerBase::RefreshTargetLists()
{
	const UWorld* World = GetWorld();

#if WITH_EDITOR
	if (!World->IsPlayInEditor())
		return;
#endif

	ASAGameStateBase* gameState = GetWorld()->GetGameState<ASAGameStateBase>();
	if (gameState == nullptr || gameState->GetState() != ESAGameState::PlayGame)
		return;

	ASAPowerAbsorberPawn* myPawn = Cast<ASAPowerAbsorberPawn>(GetPawn());
	if (myPawn == nullptr)
		return;

	myPawn->ClearCheckedTiles();

	TargetTiles.Empty();

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunk();

	const FVector HeadLocation = myPawn->GetHeadLocation();
	FVector OutFoundBlock = FVector::ZeroVector;
	int32 MyIndex = 0;
	chunk->GetTileIndexFromLocation(HeadLocation, MyIndex);
	const float StartHeight = myPawn->GetActorLocation().Z;

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(myPawn);

	for (TArray<int32>::TConstIterator It = chunk->PowerObjectTiles.CreateConstIterator(); It; ++It)
	{
		if (*It == MyIndex || TargetTiles.ContainsTile(*It))
			continue;

		FVector TileLocation;
		chunk->GetTileLocationFromIndex(*It, TileLocation);

		ASAPowerPawn* TopObject = chunk->GetTileTopObject(TileLocation);
		if (!TopObject)
			continue;

		FVector TopObjectLocation = TopObject->GetActorLocation();
		const EPowerObjectType TargetType = TopObject->GetType();
		bool bCanAbsorb = TopObject == myPawn->CanAbsorbObject(StartHeight, HeadLocation, TopObjectLocation, CollisionParams, false, OutFoundBlock);
		if (bCanAbsorb)
		{
			//the object can be absorbed
			switch (TargetType)
			{
			case EPowerObjectType::Robot:
				//DrawDebugLine(World, HeadLocation, TopObjectLocation, FColor::Green, false, 10.0f);
				if (TopObject->Controller != nullptr)
					ControlledRobot = TopObject;
				break;
			}
		}
		else //cant see the base so cant absorb. see if it is a partial
		{
			FHitResult OutHit;
			FVector TargetLocation = TopObjectLocation + (FVector::UpVector * (TopObject->GetMeshHeight() * 0.8f));
			FVector Direction = TargetLocation - HeadLocation;
			float Distance = FVector::Dist(TargetLocation, HeadLocation);
			FVector End = HeadLocation + Direction.GetSafeNormal() * Distance * 1.1f;

#if WITH_EDITOR
			if (TargetType == EPowerObjectType::Robot)
				DrawDebugLine(World, HeadLocation, End, FColor::Red, true);
#endif

			if (!World->LineTraceSingleByChannel(OutHit, HeadLocation, End, ECC_Visibility, CollisionParams))
				continue;

			if (!chunk->IsPositionInTile(TileLocation, OutHit.ImpactPoint))
				continue;
		}

		AddTargetTile(TargetType, *It, bCanAbsorb);
	}
}

void ASAAIControllerBase::AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb)
{
	//the object is partly visible
	//TargetTiles.Add(_tileIndex, _bCanAbsorb);
	TargetTiles.AddTile(_tileIndex, _bCanAbsorb);
}