// Carl Peters


#include "Sentinel/Public/PlayerControllers/SAPlayerControllerMenu.h"

#include <Blueprint/UserWidget.h>
#include <Engine/StaticMeshActor.h>
#include <GameFramework/PlayerController.h>
#include <Net/UnrealNetwork.h>
#include <Text3DActor.h>

#include "GameMode/SAGameModeBase.h"
#include "SAHUD.h"

void ASAPlayerControllerMenu::BeginPlay()
{
	Super::BeginPlay();

	if (DefaultTitleTextActor != nullptr)
		TitleText = GetWorld()->SpawnActor<AActor>(DefaultTitleTextActor, FVector::ZeroVector, FRotator::ZeroRotator);
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No DefaultTitleTextActor selected."))
	}
}

void ASAPlayerControllerMenu::OnGameStateChanged(const ESAGameState NewState)
{
	//switch (NewState)
	//{
	//case ESAGameState::PlayGame:
	//	SetMenuState(EMenuState::MainMenu);
	//	break;
	//}
}
/*
void ASAPlayerControllerMenu::SetMenuState(EMenuState NewMenuState)
{
	if (CurrentMenuState == NewMenuState)
		return;

	PrevMenuState = CurrentMenuState;
	CurrentMenuState = NewMenuState;

	bool bShowCursor = true;
	bool bShowMenuText = false;

	switch (CurrentMenuState)
	{
	case EMenuState::MainMenu:
	{
		//SetPause(false);
		//ShowMenu(DefaultMainMenuWidget, true);
		bShowMenuText = true;
	}
	break;

	case EMenuState::Game:
	{
		//ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
		//if (GameMode != nullptr && GameMode->GetPlayerIsSentinel())
		//{
		//	if (CurrentMenuWidget)
		//		CurrentMenuWidget->RemoveFromViewport();
		//}
		//else
			ShowMenu(DefaultHUDWidget, false);

		bShowCursor = false;
	}
	break;

	case EMenuState::Loading:
		ShowMenu(DefaultLoadingWidget, true);
		break;

	case EMenuState::Pause:
		ShowMenu(DefaultPauseWidget, true);
		break;

	case EMenuState::LevelSelect:
		ShowMenu(DefaultLevelSelectWidget, true);
		break;

	case EMenuState::GameOver:
		ShowMenu(DefaultGameOverWidget, true);
		break;

	case EMenuState::Win:
		ShowMenu(DefaultWinWidget, true);
		break;

	case EMenuState::PreviewMap:
		bShowCursor = false;
		ShowMenu(DefaultPreviewMapWidget, false);
		break;

	case EMenuState::Options:
		ShowMenu(DefaultOptionsWidget, true);
		break;

	case EMenuState::Play:
		ShowMenu(DefaultPlayWidget, true);
		break;

	case EMenuState::SinglePlayer:
		ShowMenu(DefaultSinglePlayerWidget, true);
		break;

	case EMenuState::MultiPlayer:
		ShowMenu(DefaultMultiPlayerWidget, true);
		break;
	}

	if (TitleText != nullptr)
		TitleText->SetActorHiddenInGame(!bShowMenuText);
}*/

//const EMenuState ASAPlayerControllerMenu::GetMenuState() const
//{
//	return CurrentMenuState;
//}
//
//void ASAPlayerControllerMenu::ReturnToPreviousMenuState()
//{
//	SetMenuState(PrevMenuState);
//}