// Carl Peters

#include "Sentinel/Public/PlayerControllers/SAAIControllerRobot.h"

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerAbsorberPawn.h"
#include "Sentinel/Public/SAGameInstance.h"
#include "Sentinel/Public/SAGameStateBase.h"
#include "Sentinel/Public/Terrain/SATerrainChunk.h"

DEFINE_LOG_CATEGORY(LogAIControllerRobot);

void ASAAIControllerRobot::BeginPlay()
{
	Super::BeginPlay();

	ASAGameStateBase* const GameState = GetWorld() != nullptr ? GetWorld()->GetGameState<ASAGameStateBase>() : nullptr;
	if (GameState == nullptr)
		return;

	//refresh lists
	//Terrain->OnPowerObjectUpdateCB.AddDynamic(this, &ASAPowerAbsorberPawn::OnPowerObjectUpdate);
	//GameState->RobotPossessionChangeCB.AddDynamic(this, &ASAAIControllerRobot::OnPowerObjectUpdate);
}

#if ENABLE_VISUAL_LOG
void ASAAIControllerRobot::GrabDebugSnapshot(FVisualLogEntry* Snapshot) const
{
	Super::GrabDebugSnapshot(Snapshot);
	const int32 CatIndex = Snapshot->Status.AddZeroed();
	FVisualLogStatusCategory& PlaceableCategory = Snapshot->Status[CatIndex];
	PlaceableCategory.Category = TEXT("AI Robot");
	//PlaceableCategory.Add(TEXT("Projectile Class"), ProjectileClass != nullptr ? ProjectileClass->GetName() : TEXT("None"));
}
#endif

bool ASAAIControllerRobot::InitializeBlackboard(UBlackboardComponent& BlackboardComp, UBlackboardData& BlackboardAsset)
{
	if (!Super::InitializeBlackboard(BlackboardComp, BlackboardAsset))
		return false;

	//get the player auto spawned from default player pawn
	ASAGameStateBase* const GameState = GetWorld() != nullptr ? GetWorld()->GetGameState<ASAGameStateBase>() : nullptr;
	if (GameState)
	{
		GameState->RobotPowerUpdateCB.AddDynamic(this, &ASAAIControllerRobot::OnPowerUpdate);
		OnPowerUpdate(GameState->GetRobotPower());
	}

	return true;
}

void ASAAIControllerRobot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateBildTileDangerValues();

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	//DEBUG_Tick();
#endif
}

void ASAAIControllerRobot::OnPowerUpdate(int32 CurrentPower)
{
	SetBlackboardInt(PowerBlackboardKeyHandle, CurrentPower);
}

void ASAAIControllerRobot::RefreshTargetLists()
{
	const UWorld* World = GetWorld();

#if WITH_EDITOR
	if (!World->IsPlayInEditor())
		return;
#endif

	ASAGameStateBase* gameState = GetWorld()->GetGameState<ASAGameStateBase>();
	if (gameState == nullptr || gameState->GetState() != ESAGameState::PlayGame)
		return;

	RobotTileData.Empty();
	TargetTiles.Empty();
	TreeTiles.Empty();
	ShieldTiles.Empty();

	bCanAbsorbSentinel = false;

	Super::RefreshTargetLists();

	CalculateBuildTiles();
	//CalculateSafeTiles();
}

/* Search list for target in angle range */
void ASAAIControllerRobot::ScanForTargets()
{
	Super::ScanForTargets();

	SetBlackboardFloat(RobotCountBlackboardKeyHandle, RobotTileData.GetAbsorbableTileCount());
	SetBlackboardFloat(TargetCountBlackboardKeyHandle, TargetTiles.GetAbsorbableTileCount());
	SetBlackboardFloat(CanAbsorbSentinelBlackboardKeyHandle, bCanAbsorbSentinel);
}

void ASAAIControllerRobot::UpdateBildTileDangerValues()
{
	for (int i = 0; i < BuildTiles.Num(); i++)
	{
		BuildTiles[i].AngleUntilDanger = CalculateDangerAngle(BuildTiles[i].TileIndex);
	}

	SortAndSetBuildTarget();
}

void ASAAIControllerRobot::CalculateBuildTiles()
{
	ASAPowerAbsorberPawn* absorberPawn = Cast<ASAPowerAbsorberPawn>(GetPawn());
	if (absorberPawn == nullptr)
		return;

	BuildTiles.Empty();

	const FVector location = absorberPawn->GetActorLocation();
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunkAtLocation(location);

	TArray<FSATerrainTileData> validHeights;

	chunk->GetValidPositions(validHeights, true, true, location.Z);

	FCollisionQueryParams collisionParams;
	collisionParams.AddIgnoredActor(absorberPawn);

	const FVector start = absorberPawn->GetHeadLocation();

	FHitResult outHit;

	for (FSATerrainTileData tile : validHeights)
	{
		const FVector tileLocation = tile.GetCurrentLocation();
		int targetIndex;
		if (!chunk->GetTileIndexFromLocation(tileLocation, targetIndex))
			continue;

		if (!GetWorld()->LineTraceSingleByChannel(outHit, start, tileLocation - 0.1f, ECC_Visibility, collisionParams))
			continue;

		FVector hitTile = outHit.ImpactPoint;
		if (!chunk->IsPositionValid(hitTile, true))
			continue;

		AActor* HitActor = outHit.GetActor();
		if (Cast<ASATerrainActor>(HitActor) == nullptr)
			continue;

		int hitIndex;
		if (!chunk->GetTileIndexFromLocation(hitTile, hitIndex))
			continue;

		if (hitIndex != targetIndex)
			continue;

		BuildTiles.Add(FSABuildTileData{ targetIndex, CalculateDangerAngle(targetIndex) });
	}

	SortAndSetBuildTarget();
}

void ASAAIControllerRobot::SortAndSetBuildTarget()
{
	if (BuildTiles.Num() == 0)
		return;

	Algo::Sort(BuildTiles, [](const FSABuildTileData& A, const FSABuildTileData& B)
		{
			return A.AngleUntilDanger < B.AngleUntilDanger;
		});

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunk();
	FVector tileLocation;
	chunk->GetTileLocationFromIndex(BuildTiles[0].TileIndex, tileLocation);

	SetBlackboardVector(BuildLocationBlackboardKeyHandle, tileLocation);

#if ENABLE_VISUAL_LOG
	if (FVisualLogger::IsRecording())
	{
		UE_VLOG_ARROW(this, LogAIControllerRobot, Display, tileLocation + FVector::UpVector * 1000.0f, tileLocation, FColor::Red, TEXT(""));

		ASAPowerPawn* topObject = nullptr;
		const float tileSize = chunk->GetTerrainActor()->GetTileSize();
		const float halfTileSize = tileSize * 0.5f;
		const float ninetyTile = halfTileSize * 0.9f;
		for (const FSABuildTileData tile : BuildTiles)
		{
			chunk->GetTileLocationFromIndex(tile.TileIndex, tileLocation);

			topObject = chunk->GetTileTopObject(tile.TileIndex);
			if (IsValid(topObject))
			{
				float meshHeight = topObject->GetMeshHeight();
				switch (topObject->GetType())
				{
				case EPowerObjectType::Tree:
					//UE_VLOG_CONE(this, LogAIControllerRobot, Display, tileLocation, )
					break;

				case EPowerObjectType::Rock:
					UE_VLOG_BOX(
						this,
						LogAIControllerRobot,
						Display,
						FBox::BuildAABB(FVector(tileLocation.X, tileLocation.Y, tileLocation.Z - (meshHeight * 0.5f)), FVector(ninetyTile, ninetyTile, meshHeight)),
						FColor::Orange,
						TEXT("ROCK"));
					break;

				case EPowerObjectType::Robot:
					UE_VLOG_CAPSULE(
						this,
						LogAIControllerRobot,
						Display,
						tileLocation,
						ninetyTile,
						meshHeight,
						FQuat::Identity,
						FColor::Orange,
						TEXT("ROBOT"));
					break;
				}
			}

			UE_VLOG_BOX(
				this,
				LogAIControllerRobot,
				Display,
				FBox::BuildAABB(tileLocation, FVector(ninetyTile, ninetyTile, 0.0f)),
				FLinearColor::LerpUsingHSV(FColor::Green, FColor::Red, FMath::Abs(tile.AngleUntilDanger / 180.0f)).ToFColor(true),
				TEXT_EMPTY);
		}
	}
#endif
}

float ASAAIControllerRobot::CalculateDangerAngle(int TileIndex)
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunk();
	float dangerAngle = 180.0f;
	FVector TileLocation;

	chunk->GetTileLocationFromIndex(TileIndex, TileLocation);

	for (TPair<int32 /*TileIndex*/, bool /*CanAbsorb*/> Tile : SentinelAllyTiles)
	{
		if (ASAPowerAbsorberPawn* absorber = chunk->GetTileTopObject<ASAPowerAbsorberPawn>(Tile.Key))
		{
			if (!absorber->CanAbsorbTile(TileIndex))
				continue;

			FVector headLocation = absorber->GetHeadLocation();
			FVector ToPosition = FVector(TileLocation.X, TileLocation.Y, headLocation.Z) - headLocation;
			ToPosition.Normalize();

			const float dot = (1.0f - (0.5f * (1.0f + FVector::DotProduct(absorber->GetHeadRotation(), ToPosition)))) * 180.0f;
			if (dot < dangerAngle)
				dangerAngle = dot;
		}

	}

	return dangerAngle;
}

void ASAAIControllerRobot::AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb)
{
	switch (ObjectType)
	{
	case EPowerObjectType::Tree:
	{
		if (GetTargetIsShield())
			ShieldTiles.Add(_tileIndex);
		else
			TargetTiles.AddTile(_tileIndex, _bCanAbsorb);
	}
	break;

	case EPowerObjectType::Rock:
	{
		//if this rock is equal or higher than me i can use it to climb
		//otherwise i want to absorb it to get my power back
		USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
		USATerrainChunk* chunk = objectManager->GetChunk();
		FVector TileLocation;
		chunk->GetTileLocationFromIndex(_tileIndex, TileLocation);
		ASAPowerPawn* TopObject = chunk->GetTileTopObject(TileLocation);
		if (TopObject->GetActorLocation().Z >= GetPawn()->GetActorLocation().Z)
		{

		}
		else
			TargetTiles.AddTile(_tileIndex, _bCanAbsorb);
	}
	break;

	case EPowerObjectType::Robot:
	{
		RobotTileData.AddTile(_tileIndex, _bCanAbsorb);
	}
	break;

	case EPowerObjectType::Sentinel:
	case EPowerObjectType::Meanie:
	case EPowerObjectType::Sentry:
	{
		bCanAbsorbSentinel = _bCanAbsorb;

		SentinelAllyTiles.Add(_tileIndex, _bCanAbsorb);
	}
	break;

	default:
	{
		TargetTiles.AddTile(_tileIndex, _bCanAbsorb);
	}
	break;
	}
}

bool ASAAIControllerRobot::GetTargetIsShield()
{
	return false;
}