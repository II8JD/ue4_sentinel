// Carl Peters


#include "Sentinel/Public/PlayerControllers/SAPlayerControllerGame.h"

#include <Blueprint/UserWidget.h>
#include <Engine/StaticMeshActor.h>
#include <GameFramework/PlayerController.h>
#include <GameFramework/PlayerState.h>
#include <Net/UnrealNetwork.h>
#include <Text3DActor.h>

#include "GameMode/SAGameModeBase.h"
#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"

void ASAPlayerControllerGame::BeginPlay()
{
	Super::BeginPlay();

	//spawn a marker 
	check(DefaultPlayerSpawnMarker != nullptr);

	PlayerSpawnMarker = GetWorld()->SpawnActor<AStaticMeshActor>(DefaultPlayerSpawnMarker, FVector::ZeroVector, FRotator::ZeroRotator);
	if (PlayerSpawnMarker)
	{
		PlayerSpawnMarker->SetActorHiddenInGame(true);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to spawn PlayerSpawnMarker."))
	}

	UUIStatics::SetUIBlackboardBool(this, PreviewMapBlackboardKeyHandle, true);
}

// Called to bind functionality to input
void ASAPlayerControllerGame::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Pause", IE_Pressed, this, &ASAPlayerControllerGame::OnInputPause);
}

void ASAPlayerControllerGame::AcknowledgePossession(APawn* InPawn)
{
	Super::AcknowledgePossession(InPawn);

	ASAPowerAbsorberPawn* AbsorberPawn = Cast<ASAPowerAbsorberPawn>(InPawn);
	if (AbsorberPawn == nullptr)
		return;

	UUIStatics::SetUIBlackboardBool(this, PreviewMapBlackboardKeyHandle, false);
	UUIStatics::SetUIBlackboardEnum(this, CharacterTypeBlackboardKeyHandle, (uint8)AbsorberPawn->GetType());
	//GameState->OnRobotPossessionChangeCB.Broadcast();
}

void ASAPlayerControllerGame::OnInputPause()
{
	TogglePause(!IsPaused());
}

void ASAPlayerControllerGame::TogglePause(const bool bPause)
{
	if (IsPaused() == bPause)
		return;

	SetPause(bPause); //this does nothing on a client so cant use ISPAused() to tell if you are paused

	UUIStatics::SetUIBlackboardBool(this, PauseBlackboardKeyHandle, bPause);
}

void ASAPlayerControllerGame::SetPlayerMarkerHidden(const bool bMarkerHidden)
{
	if (PlayerSpawnMarker == nullptr)
		return;

	PlayerSpawnMarker->SetActorHiddenInGame(bMarkerHidden);
}

void ASAPlayerControllerGame::SetPlayerMarkerLocation(const FVector Location)
{
	if (PlayerSpawnMarker == nullptr)
		return;

	PlayerSpawnMarker->SetActorLocation(Location);
}

void ASAPlayerControllerGame::VotePlayAsRobot()
{
	Server_ReceiveVote(false);
}

void ASAPlayerControllerGame::VotePlayAsSentinel()
{
	Server_ReceiveVote(true);
}

void ASAPlayerControllerGame::DEBUGSpectate()
{
	Server_DEBUGSpectate();

	/*SpawnSpectatorPawn();*/
}

void ASAPlayerControllerGame::Server_ReceiveVote_Implementation(bool bSentinel)
{
	ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
	if (GameMode == nullptr)
		return;

	GameMode->UpdateVote(this->PlayerState->GetUniqueId(), bSentinel);
}

void ASAPlayerControllerGame::SetGameOver(bool bSentinelWon)
{
	Multicast_GameOver(bSentinelWon);
}

void ASAPlayerControllerGame::Multicast_GameOver_Implementation(bool bSentinelWon)
{
	if ((bPlayerIsSentinel && bSentinelWon) || (!bPlayerIsSentinel && !bSentinelWon))
	{
		//ShowMenu(DefaultVictoryWidget, true);
	}
	else
	{
		//ShowMenu(DefaultDefeatWidget, true);
	}
}

void ASAPlayerControllerGame::Server_DEBUGSpectate_Implementation()
{
	ASAGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
	if (GameMode == nullptr)
		return;

	GameMode->DebugSpectate();

	UUIStatics::SetUIBlackboardBool(this, PreviewMapBlackboardKeyHandle, false);

	SpawnSpectatorPawn();
	StartSpectatingOnly();
}