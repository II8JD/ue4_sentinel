// Carl Peters

#include "Sentinel/Public/Pawns/SASpectatorPawn.h"

#include <GameFramework/PlayerInput.h>

// Called to bind functionality to input
void ASASpectatorPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Hyperspace", IE_Pressed, this, &ASASpectatorPawn::OnInputResetView);
}

void ASASpectatorPawn::OnInputResetView()
{
	//ASAPlayerControllerSpectator* CurrentController = Cast<ASAPlayerControllerSpectator>(Controller);
	//if (!CurrentController)
		//return;

	FocusBetweenRobotAndSentinel();
}

void ASASpectatorPawn::FocusBetweenRobotAndSentinel()
{
	APlayerController* playerController = Cast<APlayerController>(GetController());
	if (!IsValid(playerController))
		return;

	APlayerCameraManager* cameraManager = playerController->PlayerCameraManager;
	if (!IsValid(cameraManager))
		return;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	if (!IsValid(objectManager))
		return;

	const TArray<ASAPowerPawn*> robots = objectManager->GetPowerObjects(EPowerObjectType::Robot);
	const TArray<ASAPowerPawn*> sentinels = objectManager->GetPowerObjects(EPowerObjectType::Sentinel);

	TArray<FVector> points;
	points.Reserve(robots.Num() + sentinels.Num());
	for(int i = 0; i < robots.Num(); i++)
	{
		FVector center;
		FVector box;
		robots[i]->GetActorBounds(true, center, box, false);
		
		points.Add(center + FVector(box.X, box.Y, box.Z));
		points.Add(center + FVector(box.X, -box.Y, box.Z));
		points.Add(center + FVector(-box.X, box.Y, box.Z));
		points.Add(center + FVector(-box.X, -box.Y, box.Z));

		points.Add(center + FVector(box.X, box.Y, -box.Z));
		points.Add(center + FVector(box.X, -box.Y, -box.Z));
		points.Add(center + FVector(-box.X, box.Y, -box.Z));
		points.Add(center + FVector(-box.X, -box.Y, -box.Z));

		DrawDebugBox(GetWorld(), center, box, FColor::Green, false, 10.0f);
	}
	for (int i = 0; i < sentinels.Num(); i++)
	{
		FVector center;
		FVector box;
		sentinels[i]->GetActorBounds(true, center, box, false);

		points.Add(center + FVector(box.X, box.Y, box.Z));
		points.Add(center + FVector(box.X, -box.Y, box.Z));
		points.Add(center + FVector(-box.X, box.Y, box.Z));
		points.Add(center + FVector(-box.X, -box.Y, box.Z));

		points.Add(center + FVector(box.X, box.Y, -box.Z));
		points.Add(center + FVector(box.X, -box.Y, -box.Z));
		points.Add(center + FVector(-box.X, box.Y, -box.Z));
		points.Add(center + FVector(-box.X, -box.Y, -box.Z));

		DrawDebugBox(GetWorld(), center, box, FColor::Green, false, 10.0f);
	}

	FBox bounds(points);
	FVector3d targetCenter = bounds.GetCenter();
	FMinimalViewInfo cameraCache = cameraManager->GetCameraCachePOV();
	float aspectToUse = cameraCache.AspectRatio;
	float targetRadius = bounds.GetExtent().Size() * RadiusScale;
	if (aspectToUse > 1.0f)
	{
		targetRadius *= aspectToUse;
	}

	const float halfFOVRadians = FMath::DegreesToRadians(cameraCache.FOV / 2.0f);
	const float distanceFromSphere = targetRadius / FMath::Tan(halfFOVRadians);
	FVector newLocation = targetCenter + (cameraManager->GetActorForwardVector() * -distanceFromSphere);

	SetActorLocation(newLocation);
	SetActorRotation(UKismetMathLibrary::FindLookAtRotation(newLocation, targetCenter));

	DrawDebugBox(GetWorld(), targetCenter, bounds.GetExtent(), FColor::Red, false, 10.0f);
}