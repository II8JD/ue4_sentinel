// Carl Peters

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerPawn.h"

#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameMode/SAGameModeBase.h"
#include "Terrain/SATerrainActor.h"
#include "DataAssets/SAPowerDataAsset.h"
#include "Engine/LatentActionManager.h"
#include "SAActorSpawnInAction.h"
#include "CollisionQueryParams.h"
#include "DrawDebugHelpers.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "SynthComponents/EpicSynth1Component.h"
#include "SAGameStateBase.h"

// Sets default values
ASAPowerPawn::ASAPowerPawn()
{
	bReplicates = true;
	bNetLoadOnClient = true;

 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false; //Base power objects like rocks and trees dont need a tick at the moment
	PrimaryActorTick.bStartWithTickEnabled = false;

	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	RootComponent = Body;

	FXParams.ModularSynthComponent = CreateDefaultSubobject<UModularSynthComponent>(TEXT("ModularSynthComponent"));
	FXParams.ModularSynthComponent->bAutoActivate = true;
	FXParams.ModularSynthComponent->bAllowSpatialization = true;

	//PhysicsRestInfo.CallbackTarget = this;
	//PhysicsRestInfo.ExecutionFunction = "DisablePhysics";
	//PhysicsRestInfo.Linkage = 0;
	//PhysicsRestInfo.UUID = GetUniqueID();

	SpawnInInfo.CallbackTarget = this;
	SpawnInInfo.ExecutionFunction = "OnSpawninComplete";
	SpawnInInfo.Linkage = 0;
	SpawnInInfo.UUID = GetUniqueID();

	bUseControllerRotationYaw = true;

	//if(Body)
		//Body->OnComponentHit.AddDynamic(this, &ASAPowerPawn::OnHit);		// set up a notification for when this component hits something blocking
}

// Called when the game starts or when spawned
void ASAPowerPawn::BeginPlay()
{
	Super::BeginPlay();

	GameMode = GetWorld()->GetAuthGameMode<ASAGameModeBase>();
	GameState = GetWorld()->GetGameState<ASAGameStateBase>();
	
	UMaterialInterface * Material = Body->GetMaterial(0);
	FXParams.MatInstance = Body->CreateDynamicMaterialInstance(0, Material);

	FBox box = GetComponentsBoundingBox();
	MeshHeight = box.GetSize().Z;

	SetSelected(false);

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->RegisterPowerObject(*this, true);
}

void ASAPowerPawn::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	UWorld* World = GetWorld();
	if (World != nullptr)
	{
		FLatentActionManager& LatentActionManager = World->GetLatentActionManager();
		LatentActionManager.RemoveActionsForObject(this);
	}

	if(FXParams.SpawnParticleInstance != nullptr)
		FXParams.SpawnParticleInstance->DestroyComponent();

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	if(objectManager != nullptr)
		objectManager->RegisterPowerObject(*this, false);
}

//void ASAPowerPawn::SetActive(const bool bActive)
//{
//	if (bIsActive == bActive)
//		return;
//
//	bIsActive = bActive;
//
//	SetActorHiddenInGame(!bActive);
//	SetActorEnableCollision(bActive);
//	SetActorTickEnabled(bActive);
//}

void ASAPowerPawn::BecomeAbsorbed(const bool Immediate)
{
	//if not absorbing immediate turn into next lower value power object
	//robots turn into rocks
	//rocks turn into trees
	//tress dont turn into anything
	Server_BecomeAbsorbed(Immediate);
}

void ASAPowerPawn::Server_BecomeAbsorbed_Implementation(const bool Immediate)
{
	ObjectAbsorbedCB.Broadcast();

	Destroy();

	if (!Immediate && NextObject)
	{
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		FVector SpawnPos = GetActorLocation();

		GameMode->SpawnPowerObject(NextObject, SpawnPos);
	}
}

void ASAPowerPawn::SetSelected(const bool bSelected)
{
	//if (FXParams.MatInstance)
		//FXParams.MatInstance->SetVectorParameterValue("Color", bSelected ? SelectedColour : DefaultColour);
}

void ASAPowerPawn::EnablePhysics()
{
	if (!Body)
		return;

	//Body->SetAllPhysicsPosition(GetActorLocation());
	Body->SetSimulatePhysics(true);
	Body->BodyInstance.bLockXRotation = Body->BodyInstance.bLockYRotation = Body->BodyInstance.bLockZRotation = true;
	Body->BodyInstance.bLockXTranslation = Body->BodyInstance.bLockYTranslation = true;
	Body->ComponentVelocity = FVector::ZeroVector;

	//FLatentActionManager& LatentActionManager = GetWorld()->GetLatentActionManager();
	//FActorPhysicsRestAction* Action = LatentActionManager.FindExistingAction<FActorPhysicsRestAction>(PhysicsRestInfo.CallbackTarget, PhysicsRestInfo.UUID);
	//if (Action == NULL)
	//{
		//LatentActionManager.AddNewAction(PhysicsRestInfo.CallbackTarget, PhysicsRestInfo.UUID, new FActorPhysicsRestAction(3.0f, *this, PhysicsRestInfo));
	//}
}

bool ASAPowerPawn::CanBeAbsorbed()
{
	return true;
}

void ASAPowerPawn::TriggerSpawnInFX()
{
	if (bIsSpawningIn)
		return;

	RPCTriggerSpawnInFX();
}

void ASAPowerPawn::RPCTriggerSpawnInFX_Implementation()
{
	DoSpawnInFX();
}

void ASAPowerPawn::DoSpawnInFX_Implementation()
{
	if (FXParams.SpawnSetup == nullptr)
	{
		UE_LOG(PowerObjectLog, Error, TEXT("Cant trigger spawn in FX no Spawn setup on %s"), *this->GetName())
		return;
	}

	FXParams.SpawnParticleInstance = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), FXParams.SpawnSetup->SpawnFX, GetActorLocation(), GetActorRotation());

	UNiagaraFunctionLibrary::OverrideSystemUserVariableStaticMeshComponent(FXParams.SpawnParticleInstance, "TargetMesh", Body);

	FLatentActionManager& LatentActionManager = GetWorld()->GetLatentActionManager();
	SAFActorSpawnInAction* Action = LatentActionManager.FindExistingAction<SAFActorSpawnInAction>(SpawnInInfo.CallbackTarget, SpawnInInfo.UUID);
	if (Action == NULL)
	{
		LatentActionManager.AddNewAction(SpawnInInfo.CallbackTarget, SpawnInInfo.UUID, new SAFActorSpawnInAction(FXParams, *this, SpawnInInfo));
	}

	bIsSpawningIn = true;
}

void ASAPowerPawn::OnSpawninComplete()
{
	bIsSpawningIn = false;

	ObjectSpawnCompleteCB.Broadcast();

	//UNiagaraFunctionLibrary::OverrideSystemUserVariableStaticMeshComponent(FXParams.SpawnParticleInstance, "TargetMesh", nullptr);

	if(FXParams.SpawnParticleInstance)
		FXParams.SpawnParticleInstance->DestroyComponent();
}