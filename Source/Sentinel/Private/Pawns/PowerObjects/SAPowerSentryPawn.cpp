// Carl Peters


#include "Sentinel/Public/Pawns/PowerObjects/SAPowerSentryPawn.h"

#include <Engine/StaticMeshSocket.h>
#include <Kismet/GameplayStatics.h>

#include <DrawDebugHelpers.h>

#include "DataAssets/SAPowerDataAsset.h"
#include "GameMode/SAGameModeBase.h"
#include "PlayerControllers/SAPlayerControllerEdit.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "Sentinel.h"
#include "Terrain/SATerrainActor.h"

ASAPowerSentryPawn::ASAPowerSentryPawn()
{
	Type = EPowerObjectType::Sentry;

	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called to bind functionality to input
void ASAPowerSentryPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Hyperspace", IE_Pressed, this, &ASAPowerSentryPawn::OnInputSwitchCharacter);
}

void ASAPowerSentryPawn::BeginPlay()
{
	Super::BeginPlay();

	CalculateDirection();

	if(GameState != nullptr)
		GameState->RobotInteractCB.AddDynamic(this, &ASAPowerSentryPawn::OnRobotInteract);
}

void ASAPowerSentryPawn::CalculateDirection()
{
	FVector PlayerLocation = FVector::ZeroVector;

	if (Cast<ASAPlayerControllerEdit>(GetWorld()->GetFirstPlayerController())) //if this is edit mode 
	{
		USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
		USATerrainChunk* chunk = objectManager->GetChunk();
		if (chunk == nullptr)
			return;

		const int StartIndex = chunk->GetPlayerStartIndex();
		if (StartIndex < 0)
			return;

		if (!chunk->GetTileLocationFromIndex(StartIndex, PlayerLocation))
			return;
	}
	else
	{
		APlayerController* PC = GetWorld()->GetFirstPlayerController();
		if (PC == nullptr)
			return;

		const AActor* Player = PC->GetPawn();
		if (Player == nullptr)
			return;

		PlayerLocation = Player->GetActorLocation();
	}

	//calculate turn direction and starting angle based on player position
	//I dont know what the real game does so i am going with this
	//start with your back to the player
	//turn diretion is whatever takes longest to get to player
	if (HeadSocket == nullptr)
	{
		UE_LOG(PowerObjectLog, Error, TEXT("HeadSocket is null. please check default pawn."))
			return;
	}

	FRotator Rotation = UKismetMathLibrary::ComposeRotators(GetActorRotation(), HeadSocket->RelativeRotation);
	const FVector HeadLocation = GetHeadLocation();
	FVector ToPosition = FVector(PlayerLocation.X, PlayerLocation.Y, HeadLocation.Z) - HeadLocation;
	ToPosition.Normalize();
	const float Dot = FVector::DotProduct(ToPosition, Rotation.Vector());
	const float Angle = FMath::RadiansToDegrees(FMath::Acos(Dot)) + 180.0f;
	//UE_LOG(SentinelAILog, Verbose, TEXT("Tile Dot %f"), Dot)

	float StartAngle = (int)(Angle / RotateDistance) * RotateDistance;
	FQuat YawQuat(FVector(0.0f, 0.0f, 1.0f), FMath::DegreesToRadians(StartAngle));
	SetActorRotation(YawQuat);

	bRotateRight = FVector::CrossProduct(ToPosition, Rotation.Vector()).Z >= 0 ? true : false;
}

void ASAPowerSentryPawn::OnInputSwitchCharacter()
{
	TriggerSwitchCharacter();
}

void ASAPowerSentryPawn::TriggerSwitchCharacter()
{
	Server_SwitchCharacter();
}

void ASAPowerSentryPawn::Server_SwitchCharacter_Implementation()
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	CurrentPossessObject = objectManager->GetRandomSentinel(this);

	//Cache controller because it will vanish when we unpossess!
	AController* CurrentController = Controller;
	if (!CurrentController)
		return;

	CurrentController->UnPossess();
	CurrentController->Possess(CurrentPossessObject);

	//look back in the direction you came from
	const FVector EndPos = CurrentPossessObject->GetActorLocation();
	const FVector StartPos = GetActorLocation();
	const FRotator Rotation = FRotationMatrix::MakeFromX((StartPos - EndPos).GetSafeNormal()).Rotator();
	CurrentController->SetControlRotation(Rotation);
}

void ASAPowerSentryPawn::OnRobotInteract()
{
	if (!IsActorTickEnabled())
		SetActorTickEnabled(true);

	SetController(); //start behaviour tree running
}

void ASAPowerSentryPawn::AbsorbTarget()
{
	if (CurrentPowerObject == nullptr || !bCanabsorbCurrentPowerObject)
		return;

	const EPowerObjectType CurrentObjectType = CurrentPowerObject->GetType();
	if (CurrentObjectType != EPowerObjectType::Rock &&
		CurrentObjectType != EPowerObjectType::Robot)
		return; //only absorb rocks and robots

	//absorb target
	Server_AbsorbTarget(CurrentPowerObject);
}

void ASAPowerSentryPawn::ApplyabsorbTarget()
{
	if (!CurrentPowerObject || !CurrentPowerObject->CanBeAbsorbed())
	{
		return;
	}

	if (!Controller || !GameState->GetRobotHasAbsorbAbility())
	{
		return;
	}

	CurrentPowerObject->BecomeAbsorbed(false);

	//place a tree smewhere in the level
	FVector RandomPosition = FVector::ZeroVector;
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->GetRandomPosition(RandomPosition, true, false, true);
	TrySpawnObjectAtLocation(EPowerObjectType::Tree, RandomPosition);
}

void ASAPowerSentryPawn::PossessTarget()
{
	//in the original game a robot can only possess another robot if you
	//can see its base. in other words there is only one rule for interaction 
	//regardless of if you want to absorb or interact a target
	//I am not sure we will try to do that for now with play as the sentinel mode 
	//it will almost always mean if you possess a sentry or a meanie you can never go back
	//it may also mean that sometimes a sentry may be out of reach
	//2 options
	//you could always use hyperspace to go back but doesnt feel comfortable to use one 
	//method to go out and another to come back again
	//
	//let players always possess regardless of height or partial. this breakes the interaction rule
	//but is the only way to let players possess every sentry

	//only allow robots to possess other robots
	if (CurrentPossessObject == nullptr)
		return;

	const EPowerObjectType PossessObjectType = CurrentPossessObject->GetType();
	if (PossessObjectType != EPowerObjectType::Meanie &&
		PossessObjectType != EPowerObjectType::Sentry &&
		PossessObjectType != EPowerObjectType::Sentinel)
		return;

	Super::PossessTarget();
}

const float ASAPowerSentryPawn::GetPlayerVisibility() const
{
	if (CurrentPowerObject == nullptr || !CurrentPowerObject->IsPlayerControlled())
		return 0.0f;

	return bCanabsorbCurrentPowerObject ? 1.0f : 0.5f;
}

//void ASAPowerSentryPawn::AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb)
//{
//	switch (ObjectType)
//	{
//	case EPowerObjectType::Rock:
//	case EPowerObjectType::Robot:
//		TargetTiles.Add(_tileIndex, _bCanAbsorb);
//		break;
//	}
//}