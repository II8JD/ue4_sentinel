// Carl Peters

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerSentinelPawn.h"

#include <AIController.h>

#include "Sentinel/Public/PlayerControllers/SAAIControllerSentinel.h"
#include "UIStatics.h"

ASAPowerSentinelPawn::ASAPowerSentinelPawn()
{
	Type = EPowerObjectType::Sentinel;
}

//void ASAPowerSentinelPawn::BeginPlay()
//{
//	Super::BeginPlay();
//
//	InitMeanieHeadOffset();
//}

//void ASAPowerSentinelPawn::InitMeanieHeadOffset()
//{
//	//Get Meanie head offset
//	const TSubclassOf<ASAPowerPawn> MeanieBlueprint = PowerObjectsDataAsset->GetPowerObjectBlueprint(EPowerObjectType::Meanie);
//	if (MeanieBlueprint == nullptr)
//	{
//		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie prefab."))
//			return;
//	}
//
//	const ASAPowerPawn* MeaniePawn = MeanieBlueprint->GetDefaultObject<ASAPowerPawn>();
//	if (MeaniePawn == nullptr)
//	{
//		UE_LOG(SentinelAILog, Error, TEXT("Failed to get Meanie default object."))
//			return;
//	}
//
//	const UStaticMeshComponent* MeshComponent = MeaniePawn->FindComponentByClass<UStaticMeshComponent>();
//	if (MeaniePawn == nullptr)
//	{
//		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie mesh component."))
//			return;
//	}
//
//	const UStaticMeshSocket* Socket = MeshComponent->GetSocketByName("HeadSocket");
//	if (Socket == nullptr)
//	{
//		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie HeadSocket."))
//			return;
//	}
//
//	MeanieHeadOffsetPosition = Socket->RelativeLocation.Z;
//}

void ASAPowerSentinelPawn::AbsorbTarget()
{
	if (CurrentPowerObject == nullptr || !bCanabsorbCurrentPowerObject)
		return;

	const EPowerObjectType CurrentObjectType = CurrentPowerObject->GetType();

	if (CurrentObjectType == EPowerObjectType::Tree)
	{
		ConvertTreeIntoMeanie(CurrentPowerObject);
		return;
	}

	Super::AbsorbTarget();
}

//void ASAPowerSentinelPawn::RefreshTargetLists()
//{
//	Super::RefreshTargetLists();
//
//	CurrentMeanieTreeIndex = 0;
//	MeanieTreeTiles.Empty();
//
//	if (ControlledRobot == nullptr)
//		return;
//
//	const FVector TargetRobotPos = ControlledRobot->GetActorLocation();
//	FVector OutFoundBlock = FVector::ZeroVector;
//
//	//calculate good spots to spawn meanies
//	//a meanie can only be spawned where there is a tree it temporeraly replaces it
//	//im not sure what the original game does but the tree has to be absorbable too
//	//all trees in TreeTiles can be absorbed search this list and add trees that can see 
//	//the player to MeanieTreeTiles.
//	TMap<uint32/*tile*/, float/*distance*/> DistanceSorted;
//	for (const TPair<int32, bool>& pair : TreeTiles)
//	{
//		USATerrainChunk* Chunk = ObjectManager->GetChunk();
//		ASAPowerPawn* TopObject = Chunk->GetTileTopObject(pair.Key);
//		if (!TopObject)
//			continue;
//
//		FCollisionQueryParams TempCollisionParams;
//		TempCollisionParams.AddIgnoredActor(TopObject);
//		FVector TopObjectPos = TopObject->GetActorLocation();
//		if (ControlledRobot == CanAbsorbObject(TopObjectPos.Z, TopObjectPos + FVector::UpVector * MeanieHeadOffsetPosition, TargetRobotPos, TempCollisionParams, false, OutFoundBlock))
//			DistanceSorted.Add(pair.Key, FVector::Dist(TopObjectPos, TargetRobotPos));
//	}
//
//	DistanceSorted.ValueSort([](const float& A, const float& B) {
//		return A < B; // sort strings by length
//	});
//
//	DistanceSorted.GenerateKeyArray(MeanieTreeTiles);
//}

//void ASAPowerSentinelPawn::AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb)
//{
//	switch (ObjectType)
//	{
//	case EPowerObjectType::Tree:
//		TreeTiles.Add(_tileIndex, _bCanAbsorb);
//		break;
//
//	case EPowerObjectType::Rock:
//	case EPowerObjectType::Robot:
//		TargetTiles.Add(_tileIndex, _bCanAbsorb);
//		break;
//	}
//}

void ASAPowerSentinelPawn::BecomeAbsorbed(const bool Immediate)
{
	Super::BecomeAbsorbed(Immediate);

	OnSentinelAbsorbedCB.Broadcast();
}

void ASAPowerSentinelPawn::ConvertTreeIntoMeanie(ASAPowerPawn* Tree)
{
	if (Tree == nullptr || Tree->GetType() != EPowerObjectType::Tree)
		return;

	Server_ConvertTreeIntoMeanie(Tree);
}

void ASAPowerSentinelPawn::Server_ConvertTreeIntoMeanie_Implementation(ASAPowerPawn* Tree)
{
	if (Tree == nullptr || Tree->GetType() != EPowerObjectType::Tree)
		return;

	if (Meanie != nullptr)
		Meanie->TurnIntoTree();

	FVector MeaniePos = Tree->GetActorLocation();
	Tree->BecomeAbsorbed(true);
	TrySpawnObjectAtLocation(EPowerObjectType::Meanie, MeaniePos);
}

//void ASAPowerSentinelPawn::ConvertClosestTreeIntoMeanie()
//{
//	if (Meanie != nullptr)
//		return;
//
//	const int32 TreeCount = MeanieTreeTiles.Num();
//	if (TreeCount == 0)
//		return;
//
//	if (CurrentMeanieTreeIndex >= TreeCount)
//		CurrentMeanieTreeIndex = 0;
//
//	USATerrainChunk* Chunk = ObjectManager->GetChunkAtLocation(CurrentTile);
//	ASAPowerPawn* TopObject = Chunk->GetTileTopObject(MeanieTreeTiles[CurrentMeanieTreeIndex]);
//	if (TopObject == nullptr)
//		return;
//
//	CurrentMeanieTreeIndex++;
//
//	ConvertTreeIntoMeanie(TopObject);
//}

void ASAPowerSentinelPawn::OnMeanieAbsorbed()
{
	SetMeanie(nullptr);
}

void ASAPowerSentinelPawn::SetMeanie(ASAPowerMeaniePawn* NewMeanie)
{
	if (!ensureMsgf(GetNetMode() != ENetMode::NM_Client, TEXT("Server only function")))
	{
		return;
	}

	if(NewMeanie != nullptr && NewMeanie != Meanie)
		Meanie->ObjectAbsorbedCB.AddDynamic(this, &ASAPowerSentinelPawn::OnMeanieAbsorbed);

	Meanie = NewMeanie;

	if (ASAAIControllerSentinel* AIController = GetController<ASAAIControllerSentinel>())
		AIController->SetHasMeanie(Meanie != nullptr);
}

ASAPowerPawn* ASAPowerSentinelPawn::FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation)
{
	if (!ensureMsgf(GetNetMode() != ENetMode::NM_Client, TEXT("Server only function")))
	{
		return nullptr;
	}

	ASAPowerPawn* SpawnedObj = Super::FinishSpawnObject(ObjectType, SpawnLocation);
	if (SpawnedObj == nullptr)
	{
		return nullptr;
	}

	if (ObjectType == EPowerObjectType::Meanie)
	{
		SetMeanie(Cast<ASAPowerMeaniePawn>(SpawnedObj));

		if (IsPlayerControlled())
		{

		}
		else
		{
			GameMode->SetAIPossession(Meanie);
		}
	}

	return SpawnedObj;
}