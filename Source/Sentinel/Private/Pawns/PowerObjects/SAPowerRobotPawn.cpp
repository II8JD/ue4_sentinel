// Carl Peters

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerRobotPawn.h"

#include <Components/CapsuleComponent.h>
#include <Components/InputComponent.h>
#include <Components/PrimitiveComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Engine/CollisionProfile.h>
#include <Engine/StaticMeshActor.h>
#include <PhysicsEngine/AggregateGeom.h>

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
#include <DrawDebugHelpers.h>
#endif

#include "DataAssets/SAPowerDataAsset.h"
#include "GameMode/SAGameModeBaseGame.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "Sentinel/Public/Sentinel.h"
#include "Terrain/SATerrainActor.h"

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
#include "SACheatManager.h"
#include "SAObjectManager.h"
#endif

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
static TAutoConsoleVariable<int32> CVarShowDebug(TEXT("Robot.ShowDebug"), 1, TEXT("Draw debug"));
#endif

ASAPowerRobotPawn::ASAPowerRobotPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Type = EPowerObjectType::Robot;
}

void ASAPowerRobotPawn::BeginPlay()
{
	Super::BeginPlay();

	IsOnPedestal = false;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	const TArray<ASAPowerPawn*> pedestal = objectManager->GetPowerObjects(EPowerObjectType::Pedestal);
	if (pedestal.Num() > 0)
	{
		USATerrainChunk* chunk = objectManager->GetChunkAtLocation(pedestal[0]->GetActorLocation());
		if(ensure(IsValid(chunk)))
			IsOnPedestal = chunk->IsPositionInTile(pedestal[0]->GetActorLocation(), GetActorLocation());
	}
}

// Called every frame
void ASAPowerRobotPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Controller)
		return;

	UpdatePlayerVisibility();
	//UpdateBildTileDangerValues();

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	DEBUG_Tick();
#endif
}

// Called to bind functionality to input
void ASAPowerRobotPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Hyperspace", IE_Pressed, this, &ASAPowerRobotPawn::OnInputHyperspace);

	PlayerInputComponent->BindAction("Tree", IE_Pressed, this, &ASAPowerRobotPawn::TrySpawnObject<EPowerObjectType::Tree>);
	PlayerInputComponent->BindAction("Rock", IE_Pressed, this, &ASAPowerRobotPawn::TrySpawnObject<EPowerObjectType::Rock>);
	PlayerInputComponent->BindAction("Robot", IE_Pressed, this, &ASAPowerRobotPawn::TrySpawnObject< EPowerObjectType::Robot>);
}

void ASAPowerRobotPawn::ApplyabsorbTarget()
{
	if (!CurrentPowerObject || !CurrentPowerObject->CanBeAbsorbed())
	{
		return;
	}

	ASAPlayerControllerGame* CurrentController = Cast<ASAPlayerControllerGame>(Controller);
	if (!CurrentController || !GameState->GetRobotHasAbsorbAbility())
	{
		return;
	}

	//if you absorb the sentinel you cant absorb anything else in the level! (Harsh?)
	ASAPowerSentinelPawn* Sentinel = Cast<ASAPowerSentinelPawn>(CurrentPowerObject);
	if (Sentinel)
	{
		GameState->SetRobotHasAbsorbAbility(false);
	}

	//bad guys absorb power one unit at a time but the player just absorbs everything in one go
	GameState->SetRobotPower(GameState->GetRobotPower() + CurrentPowerObject->PowerLevel());

	CurrentPowerObject->BecomeAbsorbed(true);

	GameState->RobotInteractCB.Broadcast();
}

void ASAPowerRobotPawn::BecomeAbsorbed(const bool Immediate)
{
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	if (Controller && USACheatManager::bPlayerIsInvulnerable)
		return;
#endif

	if (Controller) //if this robot is possessed
		GameState->SetRobotPower(GameState->GetRobotPower() - 1);
	else
		Super::BecomeAbsorbed(Immediate);
}

void ASAPowerRobotPawn::TrySpawnObjectAtLocation(const EPowerObjectType ObjectType, FVector& SpawnPos)
{
	if (!bCurrentTileIsValid || !Controller)
		return;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	if (!USACheatManager::bPlayerInfinitePower) //dont bother delaying object spawning if player has infinite power
#endif
	{
		if (GameState->GetRobotIsSpawningObject())
			return;
	}

	TSubclassOf<ASAPowerPawn> Blueprint = PowerObjectsDataAsset->GetPowerObjectBlueprint(ObjectType);

	const int32 RequiredPower = Blueprint->GetDefaultObject<ASAPowerPawn>()->GetPower();
	if (GameState->GetRobotPower() < RequiredPower)
		return;

	GameState->SetRobotIsSpawningObject(true);

	//spawn the object
	Super::TrySpawnObjectAtLocation(ObjectType, SpawnPos);
}

ASAPowerPawn* ASAPowerRobotPawn::FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnPos)
{
	GameState->SetRobotIsSpawningObject(false);

	//spawn the object
	ASAPowerPawn* SpawnedObj = Super::FinishSpawnObject(ObjectType, SpawnPos);
	if(!SpawnedObj)
	{
		return nullptr;
	}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	if (USACheatManager::bPlayerInfinitePower)
	{
		return nullptr;
	}
#endif

	TSubclassOf<ASAPowerPawn> Blueprint = PowerObjectsDataAsset->GetPowerObjectBlueprint(ObjectType);
	const int32 RequiredPower = Blueprint->GetDefaultObject<ASAPowerPawn>()->GetPower();
	GameState->SetRobotPower(GameState->GetRobotPower() - RequiredPower);

	return SpawnedObj;
}

void ASAPowerRobotPawn::OnInputHyperspace()
{
	if (!IsOnPedestal || GameState->GetRobotHasAbsorbAbility())
	{
		const int32 RequiredPower = PowerObjectsDataAsset->Robot.Blueprint->GetDefaultObject<ASAPowerPawn>()->GetPower();
		if (GameState->GetRobotPower() - RequiredPower < 0)
			return; //Not enough power to Hyperspace
	}

	TriggerHyperspace();
}

void ASAPowerRobotPawn::TriggerHyperspace()
{
	Server_Hyperspace();
}

void ASAPowerRobotPawn::Server_Hyperspace_Implementation()
{
	if (!Controller)
		return;

	if (IsOnPedestal && !GameState->GetRobotHasAbsorbAbility()) //if the sentinel has been absorbed and the player stands on his pedestal and presses hyperspace you go to the next level
	{
		ASAGameModeBaseGame* Game = Cast<ASAGameModeBaseGame>(GameMode);
		if (Game != nullptr)
			Game->LevelHyperspace(GameState->GetRobotPower());
		return;
	}

	const int32 RequiredPower = PowerObjectsDataAsset->Robot.Blueprint->GetDefaultObject<ASAPowerPawn>()->GetPower();
	GameState->SetRobotPower(GameState->GetRobotPower() - RequiredPower);
	if (GameState->GetRobotPower() <= 0)
		return; //dead

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	FVector RandomPos;
	if (!objectManager->GetRandomPosition(RandomPos, false, false, false, GetActorLocation().Z))
	{
		UE_LOG(PowerObjectLog, Error, TEXT("Hyper space failed. Failed to find random position."));
		return;
	}

	//FVector SpawnPos = RandomPos + FVector::UpVector * SpawnFallHeight;
	ASAPowerPawn* SpawnedObject = GameMode->SpawnPowerObject(PowerObjectsDataAsset->Robot.Blueprint, RandomPos);
	if (!SpawnedObject)
	{
		UE_LOG(PowerObjectLog, Error, TEXT("Failed to spawn power object %s"), PowerObjectsDataAsset->Robot.Blueprint)
			return;
	}

	ASAPowerRobotPawn* NewRobot = Cast<ASAPowerRobotPawn>(SpawnedObject);
	if (!NewRobot)
	{
		UE_LOG(PowerObjectLog, Warning, TEXT("Failed to find robot %s"), PowerObjectsDataAsset->Robot.Blueprint)
		return;
	}

	GameState->RobotInteractCB.Broadcast();

	Controller->Possess(NewRobot);
	NewRobot->SetOwner(Controller);
}

void ASAPowerRobotPawn::UpdatePossessTarget(const FVector& InStart, const FVector& InTarget, const FCollisionQueryParams& InCollisionParams)
{
	CurrentPossessObject = CurrentPowerObject;
}

void ASAPowerRobotPawn::PossessTarget()
{
	//only allow robots to possess other robots
	if (!CurrentPossessObject || CurrentPossessObject->GetType() != EPowerObjectType::Robot)
		return;

	Super::PossessTarget();
}

void ASAPowerRobotPawn::UnPossessed()
{
	Super::UnPossessed();

	SetActorTickEnabled(false);

	if(GameMode != nullptr)
		SetOwner(GameMode->EmptyActor);

	if(CameraComponent != nullptr && !CameraComponent)
		CameraComponent->Deactivate();
}

void ASAPowerRobotPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if(NewController)
		SetActorTickEnabled(true);

	if(GameState != nullptr)
		GameState->RobotPossessionChangeCB.Broadcast();
}

void ASAPowerRobotPawn::UpdatePlayerVisibility()
{
	if (GameState != nullptr)
	{
		USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
		GameState->SetRobotVisibility(objectManager->GetPlayerVisibility());
	}
}

///* Search list for target in angle range */
//void ASAPowerRobotPawn::ScanForTargets()
//{
//	Super::ScanForTargets();
//	//fix
//	//SetBlackboardFloat(TargetCountBlackboardKeyHandle, TargetTiles.Num());
//	//SetBlackboardFloat(CanAbsorbSentinelBlackboardKeyHandle, bCanAbsorbSentinel);
//}

float ASAPowerRobotPawn::GetRotateDistance() const
{
	return 0.0f;
}

//void ASAPowerRobotPawn::RefreshTargetLists()
//{
//	if (Controller == nullptr)
//		return;
//
//	const UWorld* World = GetWorld();
//
//#if WITH_EDITOR
//	if (!World->IsPlayInEditor())
//		return;
//#endif
//
//	if (GameState->GetState() != ESAGameState::PlayGame)
//		return;
//
//	TargetTiles.Empty();
//	TreeTiles.Empty();
//	ShieldTiles.Empty();
//
//	bCanAbsorbSentinel = false;
//
//	Super::RefreshTargetLists();
//
//	//CalculateBuildTiles();
//	//CalculateSafeTiles();
//}

//void ASAPowerRobotPawn::AddTargetTile(const EPowerObjectType ObjectType, int32 _tileIndex, bool _bCanAbsorb)
//{
//	switch (ObjectType)
//	{
//	case EPowerObjectType::Tree:
//	{
//		if (GetTargetIsShield())
//			ShieldTiles.Add(_tileIndex);
//		else
//			TargetTiles.Add(_tileIndex, _bCanAbsorb);
//	}
//	break;
//
//	case EPowerObjectType::Rock:
//	{
//		//if this rock is equal or higher than me i can use it to climb
//		//otherwise i want to absorb it to get my power back
//		USATerrainChunk* chunk = ObjectManager->GetChunk();
//		FVector TileLocation;
//		chunk->GetTileLocationFromIndex(_tileIndex, TileLocation);
//		ASAPowerPawn* TopObject = chunk->GetTileTopObject(TileLocation);
//		if (TopObject->GetActorLocation().Z >= GetActorLocation().Z)
//		{
//			
//		}
//		else
//			TargetTiles.Add(_tileIndex, _bCanAbsorb);
//	}
//	break;
//
//	case EPowerObjectType::Robot:
//	{
//		RobotTiles.Add(_tileIndex, _bCanAbsorb);
//	}
//	break;
//
//	case EPowerObjectType::Sentinel:
//	case EPowerObjectType::Meanie:
//	case EPowerObjectType::Sentry:
//	{
//		bCanAbsorbSentinel = _bCanAbsorb;
//
//		SentinelAllyTiles.Add(_tileIndex, _bCanAbsorb);
//	}
//	break;
//
//	default:
//	{
//		TargetTiles.Add(_tileIndex, _bCanAbsorb);
//	}
//	break;
//	}
//}

float ASAPowerRobotPawn::GetTileSafety(int32 _tileIndex)
{
	//most safety is just behind the sentinels gaze 
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunk();
	FVector TileLocation;
	chunk->GetTileLocationFromIndex(_tileIndex, TileLocation);

	return 0.0f;
}

//bool ASAPowerRobotPawn::GetTargetIsShield()
//{
//	return false;
//}

void ASAPowerRobotPawn::CalculateTargetLocation()
{
}

//void ASAPowerRobotPawn::CalculateBuildTiles()
//{
//	BuildTiles.Empty();
//
//	FVector location = GetActorLocation();
//	USATerrainChunk* chunk = ObjectManager->GetChunkAtLocation(location);
//
//	TArray<FSATerrainTileData> validHeights;
//
//	chunk->GetValidPositions(validHeights, true, true, location.Z);
//
//	FHitResult outHit;
//	const FVector start = GetTransform().TransformPosition(HeadSocket->RelativeLocation);
//
//	for (FSATerrainTileData tile : validHeights)
//	{
//		int targetIndex;
//		if (!chunk->GetTileIndexFromLocation(tile.GetCurrentPosition(), targetIndex))
//			continue;
//		
//		if (!GetWorld()->LineTraceSingleByChannel(outHit, start, tile.GetCurrentPosition()-0.1f, ECC_Visibility, CollisionParams))
//			continue;
//		
//		FVector hitTile = outHit.ImpactPoint;
//		if (!chunk->IsPositionValid(hitTile))
//			continue;
//		
//		AActor* HitActor = outHit.GetActor();
//		if (Cast<ASATerrainActor>(HitActor) == nullptr)
//			continue;
//
//		int hitIndex;
//		if (!chunk->GetTileIndexFromLocation(hitTile, hitIndex))
//			continue;
//
//		if (hitIndex != targetIndex)
//			continue;
//		
//		BuildTiles.Add(FSABuildTileData{ targetIndex, CalculateDangerAngle(targetIndex)});
//	}
//
//	SortAndSetBuildTarget();
//}

//float ASAPowerRobotPawn::CalculateDangerAngle(int TileIndex)
//{
//	USATerrainChunk* chunk = ObjectManager->GetChunk();
//	float dangerAngle = 180.0f;
//	FVector TileLocation;
//
//	chunk->GetTileLocationFromIndex(TileIndex, TileLocation);
//
//	for (TPair<int32 /*TileIndex*/, bool /*CanAbsorb*/> Tile : SentinelAllyTiles)
//	{
//		if (ASAPowerAbsorberPawn* absorber = chunk->GetTileTopObject<ASAPowerAbsorberPawn>(Tile.Key))
//		{
//			if (!absorber->CanAbsorbTile(TileIndex))
//				continue;
//
//			FVector headLocation = absorber->GetHeadLocation();
//			FVector ToPosition = FVector(TileLocation.X, TileLocation.Y, headLocation.Z) - headLocation;
//			ToPosition.Normalize();
//
//			const float dot = (1.0f - (0.5f * (1.0f + FVector::DotProduct(absorber->GetHeadRotation(), ToPosition)))) * 180.0f;
//			if(dot < dangerAngle)
//				dangerAngle = dot;
//		}
//
//	}
//
//	return dangerAngle;
//}

//void ASAPowerRobotPawn::UpdateBildTileDangerValues()
//{
//	if (!Controller)
//		return;
//
//	if (IsPlayerControlled())
//		return;
//
//	for(int i = 0; i < BuildTiles.Num(); i++)
//	{
//		BuildTiles[i].AngleUntilDanger = CalculateDangerAngle(BuildTiles[i].TileIndex);
//	}
//
//	SortAndSetBuildTarget();
//}

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
void ASAPowerRobotPawn::DEBUG_Tick() const
{
	if (USACheatManager::bPlayerInfinitePower)
		GEngine->AddOnScreenDebugMessage(1, 15.0f, FColor::Yellow, TEXT("PlayerInfinitePower"));

	if (USACheatManager::bPlayerIsInvulnerable)
		GEngine->AddOnScreenDebugMessage(2, 15.0f, FColor::Yellow, TEXT("PlayerIsInvulnerable"));

	if (CVarShowDebug.GetValueOnGameThread() <= 0)
		return;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunk();
	FVector tileLocation;

	for (TPair<int32 /*TileIndex*/, bool /*CanAbsorb*/> Tile : TargetTiles)
	{
		chunk->GetTileLocationFromIndex(Tile.Key, tileLocation);
		DrawDebugLine(GetWorld(), tileLocation, tileLocation + FVector::UpVector * 10000.0f,
			FColor::Green, false, -1, 0, 12.333);
	}

	//FVector location = GetActorLocation();
	//TArray<FSATerrainTileData> validHeights;
	//chunk->GetValidPositions(validHeights, true, true, location.Z);
	//for (FSATerrainTileData tile : validHeights)
	//{
	//	int targetIndex;
	//	if (!chunk->GetTileIndexFromLocation(tile.GetCurrentPosition(), targetIndex))
	//		continue;

	//	DrawDebugLine(GetWorld(), tile.GetCurrentPosition(), tile.GetCurrentPosition() + FVector::UpVector * 10000.0f,
	//		FColor::Blue, false, -1, 0, 12.333);
	//}
	
	//for (FSABuildTileData tileData : BuildTiles)
	//{
	//	chunk->GetTileLocationFromIndex(tileData.TileIndex, tileLocation);
	//	DrawDebugLine(GetWorld(), tileLocation, tileLocation + FVector::UpVector * (1000.0f * (tileData.AngleUntilDanger / 180.0f)),
	//		FColor::Blue, false, -1, 0, 12.333);
	//}
}
#endif