// Carl Peters


#include "Sentinel/Public/Pawns/PowerObjects/SAPowerWidget.h"

#include <Components/HorizontalBox.h>
#include <Components/HorizontalBoxSlot.h>
#include <Components/Image.h>
#include <Components/TextBlock.h>
#include <Materials/MaterialInstanceDynamic.h>

#include "DataAssets/SAPowerDataAsset.h"
#include "GameMode/SAGameModeBase.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "SAGameInstance.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerPawn.h"
#include "Utility/SACharacterImageGenerator.h"

void USAPowerWidget::NativeConstruct()
{
	// Call the Blueprint "Event Construct" node
	Super::NativeConstruct();

	PlayerVisibleMatInstance = UMaterialInstanceDynamic::Create(PlayerVisibleMaterial, PlayerVisibleImage);
	PlayerVisibleImage->SetBrushFromMaterial(PlayerVisibleMatInstance);

	PlayerVisibleMatInstance->SetScalarParameterValue("Visible", 0);

	//get the player auto spawned from default player pawn
	ASAGameStateBase* const GameState = GetWorld() != nullptr ? GetWorld()->GetGameState<ASAGameStateBase>() : nullptr;
	if (GameState)
	{
		GameState->RobotPowerUpdateCB.AddDynamic(this, &USAPowerWidget::OnPowerUpdate);
		OnPowerUpdate(GameState->GetRobotPower());
	}
}

void USAPowerWidget::OnPowerUpdate(int32 CurrentPower)
{
	HorizontalBoxRemaingPower->ClearChildren();

	const USACharacterImageGenerator* imageGen = GetGameInstance()->GetSubsystem<USACharacterImageGenerator>();

	int32 TempPower = CurrentPower;
	TSubclassOf<ASAPowerPawn> CurrentBlueprint = PowerObjectsDataAsset->GetPowerObjectBlueprint(EPowerObjectType::Robot);
	while (TempPower > 0)
	{
		const ASAPowerPawn* CurrentPowerPawn = CurrentBlueprint->GetDefaultObject<ASAPowerPawn>();
		const int32 RequiredPower = CurrentPowerPawn->GetPower();

		const UMaterialInstanceDynamic* CurrentMaterial = imageGen->GetImage(CurrentPowerPawn->GetType());
		//const 
		while (TempPower >= RequiredPower)
		{
			UImage* TempImage = NewObject<UImage>();
			TempImage->SetBrushFromMaterial((UMaterialInstanceDynamic*)CurrentMaterial);
			HorizontalBoxRemaingPower->AddChild(TempImage);
			UHorizontalBoxSlot* ImageSlot = Cast<UHorizontalBoxSlot>(TempImage->Slot);
			if (ImageSlot)
			{
				ImageSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);
				ImageSlot->SetHorizontalAlignment(EHorizontalAlignment::HAlign_Fill);
				ImageSlot->SetPadding(FMargin(0));
			}

			TempPower -= RequiredPower;
		}
		CurrentBlueprint = CurrentPowerPawn->GetNext();
		if (CurrentBlueprint == nullptr)
			break;
	}
}

void USAPowerWidget::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry, DeltaTime);

	UpdatePlayerVisibility();

}

void USAPowerWidget::UpdatePlayerVisibility()
{
	ASAGameStateBase* const GameState = GetWorld() != nullptr ? GetWorld()->GetGameState<ASAGameStateBase>() : nullptr;
	if (GameState)
	{
		const float CurrentVisibility = GameState->GetRobotVisibility();
		if (CurrentVisibility != PreviousVisibility)
		{
			PreviousVisibility = CurrentVisibility;
			PlayerVisibleMatInstance->SetScalarParameterValue("Visible", CurrentVisibility);
		}
	}
}