// Carl Peters

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerMeaniePawn.h"

#include <Engine/StaticMeshSocket.h>
#include <Terrain/SATerrainActor.h>

#include "DataAssets/SAPowerDataAsset.h"
#include "GameMode/SAGameModeBase.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerRobotPawn.h"

ASAPowerMeaniePawn::ASAPowerMeaniePawn()
{
	Type = EPowerObjectType::Meanie;
}

void ASAPowerMeaniePawn::BeginPlay()
{
	Super::BeginPlay();

	if (GameState != nullptr && GameState->GetSentinelCanMove())
		SetController();
}

void ASAPowerMeaniePawn::AbsorbTarget()
{
	if (CurrentPowerObject == nullptr || !bCanabsorbCurrentPowerObject)
		return;

	if (CurrentPowerObject != nullptr && bCanabsorbCurrentPowerObject)
	{
		ASAPowerRobotPawn* Robot = Cast<ASAPowerRobotPawn>(CurrentPowerObject);
		if (Robot != nullptr)
			Robot->TriggerHyperspace();
	}
}

void ASAPowerMeaniePawn::TurnIntoTree()
{
	FVector Location = GetActorLocation();
	BecomeAbsorbed(true);
	GameMode->SpawnPowerObject(PowerObjectsDataAsset->Tree.Blueprint, Location);
}

//void ASAPowerMeaniePawn::ScanForTargets()
//{
//	if (IsPendingKillPending())
//		return;
//
//	Super::ScanForTargets();
//
//	if (CurrentPowerObject == nullptr)
//		TurnIntoTree();
//}