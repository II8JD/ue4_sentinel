// Carl Peters


#include "Sentinel/Public/Pawns/PowerObjects/SAPowerPedestalPawn.h"

ASAPowerPedestalPawn::ASAPowerPedestalPawn()
{
	Type = EPowerObjectType::Pedestal;
}

bool ASAPowerPedestalPawn::CanBeAbsorbed()
{
	return false;
}