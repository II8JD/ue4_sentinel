// Carl Peters

#include "Sentinel/Public/Pawns/PowerObjects/SAPowerAbsorberPawn.h"

#include <Blueprint/AIBlueprintHelperLibrary.h>
#include <Camera/CameraComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Engine/StaticMeshSocket.h>
#include <Kismet/KismetMathLibrary.h>

#include <UIStatics.h>

#include "AIController.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "SAObjectManager.h"

ASAPowerAbsorberPawn::ASAPowerAbsorberPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	// Create a CameraComponent
	// this would normally only be specific to SAPowerRobotPawn,
	// but i want to allow players to play as the sentinel too.
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetupAttachment(Body);
	CameraComponent->SetRelativeLocation(FVector(.0f, .0f, 64.f)); // Position the camera
	CameraComponent->bUsePawnControlRotation = true;
	CameraComponent->Deactivate();
}

void ASAPowerAbsorberPawn::BeginPlay()
{
	Super::BeginPlay();

	HeadSocket = Body->GetSocketByName("HeadSocket");
	if (!HeadSocket)
		UE_LOG(PowerObjectLog, Error, TEXT("Failed to find HeadSocket on %s. please check default pawn."), *this->GetName())

	LookDirection = GetHeadRotation();

	CollisionParams.AddIgnoredActor(this);

	//if (GameState == nullptr)
	//	return;

	////refresh lists
	////Terrain->OnPowerObjectUpdateCB.AddDynamic(this, &ASAPowerAbsorberPawn::OnPowerObjectUpdate);
	//GameState->RobotPossessionChangeCB.AddDynamic(this, &ASAPowerAbsorberPawn::OnPowerObjectUpdate);
}

// Called to bind functionality to input
void ASAPowerAbsorberPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Absorb", IE_Pressed, this, &ASAPowerAbsorberPawn::OnInputAbsorb);
	PlayerInputComponent->BindAction("Possess", IE_Pressed, this, &ASAPowerAbsorberPawn::OnInputPossess);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASAPowerAbsorberPawn::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASAPowerAbsorberPawn::LookUpAtRate);
}

// Called every frame
void ASAPowerAbsorberPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Controller)
		return;

	TimeDelta = DeltaTime;

	UpdateRayTest();
}

void ASAPowerAbsorberPawn::UpdateRayTest()
{
	bCanabsorbCurrentPowerObject = false;
	if (!Controller)
		return;

	if (IsPlayerControlled())
		LookDirection = CameraComponent->GetForwardVector();

	FHitResult OutHit;
	const FVector Start = GetHeadLocation();
	FVector End = (LookDirection * RayDistance) + Start;
	ASAPowerPawn* prevPowerObject = CurrentPowerObject;
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	//DrawDebugLine(GetWorld(), Start, End, FColor::Blue);

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		CurrentTile = OutHit.ImpactPoint;

		//DrawDebugLine(GetWorld(), Start, CurrentTile, FColor::Red);
		USATerrainChunk* Chunk = objectManager->GetChunkAtLocation(CurrentTile);
		Chunk->IsPositionValid(CurrentTile);

		AActor* HitActor = OutHit.GetActor();

		if (Cast<ASAPowerPawn>(HitActor) != nullptr)
			CurrentTile = HitActor->GetActorLocation();
	}

	USATerrainChunk* Chunk = objectManager->GetChunkAtLocation(CurrentTile);
	bCurrentTileIsValid = Chunk->IsPositionValid(CurrentTile, false);

	ASAPlayerControllerGame* CurrentController = Cast<ASAPlayerControllerGame>(Controller);
	if (CurrentController == nullptr)
		return;
	CurrentController->SetPlayerMarkerHidden(!bCurrentTileIsValid);
	if (bCurrentTileIsValid)
		CurrentController->SetPlayerMarkerLocation(CurrentTile);
	//DrawDebugLine(GetWorld(), CurrentTile, CurrentTile + FVector::UpVector * 1000.0f, FColor::Blue);

	FVector InTile = CurrentTile;
	CurrentPowerObject = CanAbsorbObject(GetActorLocation().Z, Start, CurrentTile, CollisionParams, CurrentController->GetUseSentinelReturnsRules(), CurrentTile);
	if (CurrentPowerObject != prevPowerObject)
	{
		if (prevPowerObject)
			prevPowerObject->SetSelected(false);

		if (CurrentPowerObject)
			CurrentPowerObject->SetSelected(true);
	}

	bCanabsorbCurrentPowerObject = CurrentPowerObject != nullptr;

	UpdatePossessTarget(Start, InTile, CollisionParams);
}

void ASAPowerAbsorberPawn::UpdatePossessTarget(const FVector& InStart, const FVector& InTarget, const FCollisionQueryParams& InCollisionParams)
{
	CurrentPossessObject = nullptr;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* Chunk = objectManager->GetChunk();

	FVector TileLocation = InTarget;
	ASAPowerPawn* TopObject = Chunk->GetTileTopObject(TileLocation);
	if (!TopObject)
		return;

	//didnt hit anything see if we can find a possession target for play as sentinel mode
	const EPowerObjectType TopObjectType = TopObject->GetType();
	if (TopObjectType == EPowerObjectType::Sentry ||
		TopObjectType == EPowerObjectType::Sentinel ||
		TopObjectType == EPowerObjectType::Meanie)
	{
		//FVector End = TopObject->GetActorLocation() + (TopObject->GetMeshHeight() * 0.8f);

		//FHitResult OutHit;

		//if (!GetWorld()->LineTraceSingleByChannel(OutHit, InStart, End, ECC_Visibility, InCollisionParams))
		//	return; //hit nothing

		//ASAPowerPawn* HitObject = Cast<ASAPowerPawn>(OutHit.GetActor());
		//if (!HitObject)
		//	return;

		//CurrentPossessObject = HitObject == TopObject ? HitObject : nullptr;
		CurrentPossessObject = TopObject;
	}
}

void ASAPowerAbsorberPawn::OnInputAbsorb()
{
	if (!Controller)
		return;

	AbsorbTarget();
}

void ASAPowerAbsorberPawn::AbsorbTarget()
{
	Server_AbsorbTarget(CurrentPowerObject);
}
	
void ASAPowerAbsorberPawn::Server_AbsorbTarget_Implementation(ASAPowerPawn* TargetPowerObject)
{
	CurrentPowerObject = TargetPowerObject;
	ApplyabsorbTarget();
}

void ASAPowerAbsorberPawn::OnInputPossess()
{
	if (!CurrentPossessObject)
		return;

	AController* CurrentController = Controller;
	if (!CurrentController)
		return;

	PossessTarget();
}

void ASAPowerAbsorberPawn::PossessTarget()
{
	AController* CurrentController = Controller;
	if (!CurrentController)
		return;

	if (!CurrentPossessObject)
		return;

	Server_SetPossessed(CurrentPossessObject);
}

void ASAPowerAbsorberPawn::Server_SetPossessed_Implementation(ASAPowerPawn* PossessTarget)
{
	if (!PossessTarget)
		return;

	if (PossessTarget->IsPlayerControlled())
		return; //target is already possessed

	//Cache controller because it will vanish when we unpossess!
	AController* CurrentController = Controller;
	if (!CurrentController)
		return;

	//CurrentController->UnPossess();
	CurrentController->Possess(PossessTarget);

	//look back in the direction you came from
	const FVector EndPos = PossessTarget->GetActorLocation();
	const FVector StartPos = GetActorLocation();
	const FRotator Rotation = FRotationMatrix::MakeFromX((StartPos - EndPos).GetSafeNormal()).Rotator();
	CurrentController->SetControlRotation(Rotation);
}

void ASAPowerAbsorberPawn::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * TimeDelta);
}

void ASAPowerAbsorberPawn::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * TimeDelta);
}

ASAPowerPawn* ASAPowerAbsorberPawn::CanAbsorbObject(const float StartHeight, const FVector& InStart, const FVector& InTarget, const struct FCollisionQueryParams& InCollisionParams, 
	const bool bUseSentinelReturnRules, FVector& OutFoundMapTile)
{
	OutFoundMapTile = InTarget;
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* Chunk = objectManager->GetChunkAtLocation(InTarget);
	if (Chunk == nullptr)
		return nullptr;

	FVector TilePos = InTarget;
	if (!Chunk->GetTileCurrentPosition(TilePos) || TilePos.Y < 0)
		return nullptr; //terrain section isnt valid so it cant have an object on it.

	ASAPowerPawn* TopObject = Chunk->GetTileTopObject(TilePos);
	if (!TopObject)
		return nullptr; //no object in this map location

	FHitResult OutHit;

	//add some distance on the ray trace otherwise you may under hit
	if (TopObject)
	{
		TilePos = TopObject->GetActorLocation();
		Chunk->GetTileOriginalPosition(TilePos);
	}

	FVector End = TilePos;

	//DrawDebugLine(GetWorld(), InStart, End, FColor::Green, true);
	//DrawDebugLine(GetWorld(), End, End + FVector::UpVector * 1000.0f, FColor::Green, true);
	//DrawDebugLine(GetWorld(), TilePos, TilePos + FVector::UpVector * 1000.0f, FColor::Red, true);

	//see if we can hit the correct tile
	if (!GetWorld()->LineTraceSingleByChannel(OutHit, InStart, End, ECC_Visibility, InCollisionParams))
		return nullptr; //hit nothing

	ASAPowerPawn* HitObject = Cast<ASAPowerPawn>(OutHit.GetActor());
	if (HitObject) //hit a power object
	{
		//only objects at the top of a stack can be absorbed
		//in the original sentinel you have to look at the base of an object to absorb it even the sides dont count
		//in sentinel returns you can absorb from the sides or from any valid rock

		OutFoundMapTile = HitObject->GetActorLocation();

		ASAPowerPawn* AbsorbObject = Chunk->GetTileAbsorbObject(OutFoundMapTile);
		if (AbsorbObject == nullptr) //this tile hasnt got an absorb object the player must be able to see the ground to absorb the object
		{
			End = OutFoundMapTile;

			if (StartHeight < End.Z)
				return nullptr; //if the start point is lower than the target tile you cant absorb any objects on it

			//DrawDebugLine(GetWorld(), End, OutFoundMapTile, FColor::Blue);
		}
		else //see if we can see the rock this object is on top of
		{
			if (!AbsorbObject->CanBeAbsorbed())
			{
				if(TopObject)
					End = TopObject->GetActorLocation();// +FVector::UpVector * AbsorbObject->MeshHeight;

				if (StartHeight < End.Z)
					return nullptr; //if the start point is lower than the target tile you cant absorb any objects on it
			}
			else
			{
				if (!bUseSentinelReturnRules && HitObject != AbsorbObject)
					return nullptr; //in original sentinel you have to look directly at the object

				End = AbsorbObject->GetActorLocation() + FVector::UpVector * AbsorbObject->GetMeshHeight();
			}
			//DrawDebugLine(GetWorld(), End, OutFoundMapTile, FColor::Yellow);
		}

		if (GetWorld()->LineTraceSingleByChannel(OutHit, InStart, End, ECC_Visibility, InCollisionParams))
		{
			//DrawDebugLine(GetWorld(), InStart, End, FColor::Green);
			//DrawDebugLine(GetWorld(), OutHit.ImpactPoint, OutHit.ImpactPoint + FVector::UpVector, FColor::Red);

			if (Chunk->IsPositionInTile(OutFoundMapTile, OutHit.ImpactPoint))
			{
				TopObject = Chunk->GetTileTopObject(OutFoundMapTile); //absorb the top object
				if (TopObject && TopObject->CanBeAbsorbed())
					return TopObject;
			}
		}
	}
	else //hit terrain
	{
		OutFoundMapTile = OutHit.ImpactPoint;

		if (!Chunk->IsPositionInTile(InTarget, OutFoundMapTile))
			return nullptr; //didnt hit anything on the target tile

		TopObject = Chunk->GetTileTopObject(OutFoundMapTile); //absorb the top object
		if (TopObject && TopObject->CanBeAbsorbed())
		{
			if (StartHeight < TopObject->GetActorLocation().Z)
				return nullptr; //if the start point is lower than the target tile you cant absorb any objects on it

			return TopObject;
		}
	}

	return nullptr;
}

bool ASAPowerAbsorberPawn::CanAbsorbTile(int32 _tileIndex)
{
	if (TargetTiles.Contains(_tileIndex))
		return true;

	if (CheckedVisibleTiles.Contains(_tileIndex))
		return *CheckedVisibleTiles.Find(_tileIndex);

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* chunk = objectManager->GetChunkAtLocation(CurrentTile);
	FVector targetTilePos = FVector::ZeroVector;
	FVector hitTile = FVector::ZeroVector;
	bool bHitTile = false;

	if (chunk->GetTileLocationFromIndex(_tileIndex, targetTilePos) && targetTilePos.Y >= 0)
	{
		targetTilePos.Z -= 1.0f;

		FHitResult OutHit;

		//see if we can hit the correct tile
		if (GetWorld()->LineTraceSingleByChannel(OutHit, GetHeadLocation(), targetTilePos, ECC_Visibility, CollisionParams))
		{

			int32 hitIndex = -1;
			ASAPowerPawn* HitObject = Cast<ASAPowerPawn>(OutHit.GetActor());
			FVector hitPoint = OutHit.ImpactPoint;

			if (HitObject) //hit a power object
			{
				hitPoint = HitObject->GetActorLocation();
			}

			if (chunk->GetTileIndexFromLocation(hitPoint, hitIndex))
			{
				bHitTile = hitIndex == _tileIndex;
			}
		}
	}
	CheckedVisibleTiles.Add(_tileIndex, bHitTile);

	return bHitTile;
}

void ASAPowerAbsorberPawn::SetController()
{
	//switch (Type)
	//{
	//case EPowerObjectType::Robot:
	//	if (!GameMode->GetPlayerIsSentinel())
	//	{
	//		SetPlayerControl();
	//		return;
	//	}
	//	break;

	//case EPowerObjectType::Sentinel:
	//	if (GameMode->GetPlayerIsSentinel())
	//	{
	//		SetPlayerControl();
	//		return;
	//	}
	//	break;
	//}
	//	
	//SetAIControl();
}

void ASAPowerAbsorberPawn::SetAIControl()
{
	AAIController* AIC = UAIBlueprintHelperLibrary::GetAIController(this);
	if (AIC == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AI Controller %s."), *this->GetName())
		return;
	}

	if (BehaviorTree == nullptr)
	{
		UE_LOG(SentinelAILog, Error, TEXT("BehaviourRree is null please chack blueprint %s."), *this->GetName())
		return;
	}

	AIC->RunBehaviorTree(BehaviorTree);
}

void ASAPowerAbsorberPawn::SetPlayerControl()
{
	ASAPlayerControllerGame* WorldController = Cast<ASAPlayerControllerGame>(GetWorld()->GetFirstPlayerController());

	if (Controller == WorldController)
		return; //nothing to do

	WorldController->UnPossess();
	WorldController->Possess(this);
}

void ASAPowerAbsorberPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (NewController->IsPlayerController())
	{
		SetOwner(NewController);
		CameraComponent->Activate();
	}

	SetActorTickEnabled(NewController->IsPlayerController());
}

void ASAPowerAbsorberPawn::TrySpawnObjectAtIndex(const EPowerObjectType ObjectType, const uint32 Index)
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* Chunk = objectManager->GetChunk();
	if (!Chunk)
		return;

	FVector SpawnPos = FVector::ZeroVector;
	if (!Chunk->GetTileLocationFromIndex(Index, SpawnPos))
		return;

	TrySpawnObjectAtLocation(ObjectType, SpawnPos);
}

void ASAPowerAbsorberPawn::TrySpawnObjectAtLocation(const EPowerObjectType ObjectType, FVector& SpawnLocation)
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	USATerrainChunk* Chunk = objectManager->GetChunk();
	if (!Chunk)
		return;

	Chunk->IsPositionValid(SpawnLocation);

	Server_TrySpawnObject(ObjectType, SpawnLocation);
}

void ASAPowerAbsorberPawn::Server_TrySpawnObject_Implementation(const EPowerObjectType ObjectType, FVector SpawnPos)
{
	FinishSpawnObject(ObjectType, SpawnPos);
}

ASAPowerPawn* ASAPowerAbsorberPawn::FinishSpawnObject(const EPowerObjectType ObjectType, FVector SpawnLocation)
{
	if (!ensureMsgf(GetNetMode() != ENetMode::NM_Client, TEXT("Server only function")))
	{
		return nullptr;
	}

	ASAPowerPawn* SpawnedObject = nullptr;

	TSubclassOf<ASAPowerPawn> DefaultObject = PowerObjectsDataAsset->GetPowerObjectBlueprint(ObjectType);

	SpawnedObject = GameMode->SpawnPowerObject(DefaultObject, SpawnLocation);
	if (!SpawnedObject)
	{
		UE_LOG(PowerObjectLog, Error, TEXT("Failed to spawn power object %s"), *UEnum::GetDisplayValueAsText(ObjectType).ToString())
		return nullptr;
	}

	SpawnedObject->TriggerSpawnInFX();

	return SpawnedObject;
}

//const FVector GetHeadLocation() const { return GetTransform().TransformPosition(HeadSocket->RelativeLocation); };
FVector ASAPowerAbsorberPawn::GetHeadLocation() const
{ 
	return GetActorLocation() + FVector::UpVector * HeadSocket->RelativeLocation.Z; 
}

void ASAPowerAbsorberPawn::SetHeadPitch(float pitch)
{
	FRotator rot = UKismetMathLibrary::ComposeRotators(GetActorRotation(), HeadSocket->RelativeRotation);
	rot.Pitch = pitch;
	LookDirection = rot.Vector();
}

bool ASAPowerAbsorberPawn::IsLookingAtTarget(FVector TargetLocation) const
{
	if (!GetCurrentTileIsValid())
		return false;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	USATerrainChunk* currentChunk = objectManager->GetChunkAtLocation(CurrentTile);
	if (!currentChunk)
		return false;

	USATerrainChunk* targetChunk = objectManager->GetChunkAtLocation(TargetLocation);
	if (!targetChunk)
		return false;

	int32 targetIndex = 0;
	int32 currentIndex = 0;
	if(!targetChunk->GetTileIndexFromLocation(CurrentTile, currentIndex) ||
	!targetChunk->GetTileIndexFromLocation(TargetLocation, targetIndex))
		return false;

	return targetIndex == currentIndex;
}

void ASAPowerAbsorberPawn::SetCurrentPowerObject(ASAPowerPawn* newPowerObject, bool bCanAbsorb)
{
	if (newPowerObject == nullptr)
	{
		CurrentPowerObject = nullptr;
		bCanabsorbCurrentPowerObject = false;
		return;
	}

	CurrentPowerObject = newPowerObject;

	bCanabsorbCurrentPowerObject = bCanAbsorb;
}