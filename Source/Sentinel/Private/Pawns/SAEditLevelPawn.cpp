// Carl Peters


#include "Sentinel/Public/Pawns/SAEditLevelPawn.h"

#include <Camera/CameraComponent.h>
#include <Components/InputComponent.h>
#include <Engine/CollisionProfile.h>
#include <Engine/StaticMeshActor.h>
#include <GameFramework/FloatingPawnMovement.h>
#include <Serialization/BufferArchive.h>
#include <vector>

#include "DataAssets/SAPowerDataAsset.h"
#include "GameMode/SAGameModeBase.h"
#include "PlayerControllers/SAPlayerControllerEdit.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerRobotPawn.h"
#include "Sentinel/Public/Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "Sentinel.h"
#include "Terrain/SATerrainActor.h"
#include "Utility/SAHuffmanCompression.h"
#include "Utility/SARunLengthCompression.h"

#include <DrawDebugHelpers.h>


// Sets default values
ASAEditLevelPawn::ASAEditLevelPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a CameraComponent	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetRelativeLocation(FVector(.0f, .0f, .0f)); // Position the camera
	CameraComponent->bUsePawnControlRotation = true;
	RootComponent = CameraComponent;

	MovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingMovementComponent"));

	CollisionParams.AddIgnoredActor(this);
}

// Called when the game starts or when spawned
void ASAEditLevelPawn::BeginPlay()
{
	Super::BeginPlay();

	ResetRequired = true;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->TerrainGeneratedCB.AddDynamic(this, &ASAEditLevelPawn::OnTerrainGenerated);
}

void ASAEditLevelPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TimeDelta = DeltaTime;

	UpdateRayTest();
}

// Called to bind functionality to input
void ASAEditLevelPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Tree", IE_Pressed, this, &ASAEditLevelPawn::TrySpawnObject<EPowerObjectType::Tree>);
	PlayerInputComponent->BindAction("Sentinel", IE_Pressed, this, &ASAEditLevelPawn::TrySpawnObject<EPowerObjectType::Sentinel>);
	PlayerInputComponent->BindAction("Robot", IE_Pressed, this, &ASAEditLevelPawn::TrySpawnObject< EPowerObjectType::Robot>);
	PlayerInputComponent->BindAction("Sentry", IE_Pressed, this, &ASAEditLevelPawn::TrySpawnObject< EPowerObjectType::Sentry>);

	PlayerInputComponent->BindAction("RemoveObject", IE_Pressed, this, &ASAEditLevelPawn::RemoveObject);

	PlayerInputComponent->BindAction("Reset", IE_Pressed, this, &ASAEditLevelPawn::ResetTerrain);

	PlayerInputComponent->BindAction<FRaiseTerrainDelegate>("RaiseTerrain", IE_Pressed, this, &ASAEditLevelPawn::RaiseTerrain, true);
	PlayerInputComponent->BindAction<FRaiseTerrainDelegate>("LowerTerrain", IE_Pressed, this, &ASAEditLevelPawn::RaiseTerrain, false);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASAEditLevelPawn::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASAEditLevelPawn::LookUpAtRate);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASAEditLevelPawn::AddForwardMovementInput);
	//PlayerInputComponent->BindAxis("MoveForwardRate", this, &ASAEditLevelPawn::ForwardMovementAtRate);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASAEditLevelPawn::AddRightMovementInput);
	//PlayerInputComponent->BindAxis("MoveRightRate", this, &ASAEditLevelPawn::RightMovementAtRate);

	PlayerInputComponent->BindAxis("MoveUp", this, &ASAEditLevelPawn::AddUpMovementInput);
	//PlayerInputComponent->BindAxis("MoveUpRate", this, &ASAEditLevelPawn::UpMovementAtRate);
}

void ASAEditLevelPawn::AddUpMovementInput(float Rate)
{
	AddMovementInput(FVector::UpVector, Rate);
}

void ASAEditLevelPawn::UpMovementAtRate(float Rate)
{
}

void ASAEditLevelPawn::AddForwardMovementInput(float Rate)
{
	AddMovementInput(GetActorForwardVector(), Rate);
}

void ASAEditLevelPawn::ForwardMovementAtRate(float Rate)
{
}

void ASAEditLevelPawn::AddRightMovementInput(float Rate)
{
	AddMovementInput(GetActorRightVector(), Rate);
}

void ASAEditLevelPawn::RightMovementAtRate(float Rate)
{
}

void ASAEditLevelPawn::RaiseTerrain(const bool Raise)
{
	if (!bHasSelectedTerrain)
		return;

	ASATerrainActor* TA = Chunk->GetTerrainActor();
	int32 Tiles = TA->GetTiles();
	const int32 CellX = SelectedTerrainIndex / Tiles;
	const int32 CellY = SelectedTerrainIndex % Tiles;
	const int32 NextCellX = CellX < Tiles ? CellX + 1 : CellX - 1;
	const int32 NextCellY = CellY < Tiles ? CellY + 1 : CellY - 1;
	Tiles++; //SelectedTerrainIndex is the cell e.g 8x8 we need to convert this into triangle point 9x9
	//const float Height = FMath::Clamp(TA->GetTileHeight(CellY + (CellX * Tiles)) + (Raise ? LevelSize : -LevelSize), 0.0f, 1.0f);
	const int32 Height = FMath::Clamp(Heights[CellY + (CellX * Tiles)] + (Raise ? 1 : -1), 0, TA->GetLevels());

	for (int32 i = 0; i < Tiles * Tiles; i++)
	{
		Heights[i] = FMath::RandRange(0, TA->GetLevels());
	}

//#if WITH_EDITOR
//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("CellX=%d CellY=%d NextCellX=%d NextCellY=%d"), CellX, CellY, NextCellX, NextCellY));
//#endif
	Heights[CellY + (CellX * Tiles)] = Height;
	Heights[NextCellY + (CellX * Tiles)] = Height;
	Heights[CellY + (NextCellX * Tiles)] = Height;
	Heights[NextCellY + (NextCellX * Tiles)] = Height;

	//std::vector<unsigned char> Bytes;
	//int32 RunLength = 1;
	//int32 RunValue = TA->Heights[0];
	//Bytes.push_back(RunValue);
	//for (int i = 1; i < TA->Heights.Num(); i++)
	//{
	//	if (RunValue != TA->Heights[i])
	//	{
	//		Bytes.push_back(RunLength);
	//		RunValue = TA->Heights[i];
	//		Bytes.push_back(RunValue);
	//		RunLength = 0;
	//	}
	//	RunLength++;
	//}
	//Bytes.push_back(RunLength);

	//FBufferArchive ToBinary;
	//ToBinary << TA->Heights;

	TArray<int32> RunLengeth = USARunLengthCompression::Encode(Heights);
	 
	//FString HeightsString;
	//for (int i = 1; i < Heights.Num(); i++)
	//{
		//HeightsString.AppendInt(Heights[i]);
	//}
	//USAHuffmanCompression::BuildHuffmanTree(RunLengeth);

	ASAGameStateBase* GameState = GetWorld()->GetGameState<ASAGameStateBase>();
	GameState->TryGenerateLevel();
}

void ASAEditLevelPawn::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * TimeDelta);
}

void ASAEditLevelPawn::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * TimeDelta);
}

void ASAEditLevelPawn::TrySpawnObject(EPowerObjectType ObjectType)
{
	if (!bCurrentTileIsValid || !Controller || !Chunk)
		return;

	int32 TileIndex = 0;
	if (!Chunk->GetTileIndexFromLocation(CurrentTile, TileIndex))
		return;

	const TSubclassOf<ASAPowerPawn> DefaultObject = PowerObjectsDataAsset->GetPowerObjectBlueprint(ObjectType);
	if (DefaultObject == nullptr)
	{
		UE_LOG(PowerObjectLog, Error, TEXT("Failed to spawn power object %s. Please check data asset."), *UEnum::GetDisplayValueAsText(ObjectType).ToString())
		return;
	}

	//const ASAPowerPawn* SpawnedObject = GameMode->SpawnPowerObject(DefaultObject, CurrentTile);
	//if (!SpawnedObject)
	//{
		//UE_LOG(PowerObjectLog, Error, TEXT("Failed to spawn power object %s"), *UEnum::GetDisplayValueAsText(ObjectType).ToString())
		//return;
	//}

	switch (ObjectType)
	{
	case EPowerObjectType::Sentinel:
		Chunk->SetSentinelStartIndex(TileIndex);
		break;

	case EPowerObjectType::Robot:
		Chunk->SetPlayerStartIndex(TileIndex);
		break;

	case EPowerObjectType::Tree:
		Chunk->AddTreeStartIndex(TileIndex);
		break;

	case EPowerObjectType::Sentry:
		Chunk->AddSentryStartIndex(TileIndex);
	}
}

void ASAEditLevelPawn::ResetTerrain()
{
	ASATerrainActor* TA = Chunk->GetTerrainActor();
	TA->ResetTileHeights();
	//Terrain->GenerateMesh();
}

void ASAEditLevelPawn::RemoveObject()
{
	if (CurrentPowerObject != nullptr)
	{
		if (Cast<ASAPowerSentinelPawn>(CurrentPowerObject))
		{
			FVector Location = CurrentPowerObject->GetActorLocation();
			CurrentPowerObject->BecomeAbsorbed(true);
			ASAPowerPawn * SentinelPedestal = Chunk->GetTileTopObject(Location);
			if(SentinelPedestal != nullptr)
				SentinelPedestal->BecomeAbsorbed(true);
			Chunk->SetSentinelStartIndex(-1);
			return;
		}
		else if (Cast<ASAPowerRobotPawn>(CurrentPowerObject))
			Chunk->SetPlayerStartIndex(-1);
		else
		{
			int32 StartIndex = 0;
			Chunk->GetTileIndexFromLocation(CurrentPowerObject->GetActorLocation(), StartIndex);
			Chunk->RemoveTreeAtTile(StartIndex);
		}
		CurrentPowerObject->BecomeAbsorbed(true);
	}
}

void ASAEditLevelPawn::UpdateRayTest()
{
	if (!Chunk || !Controller)
		return;

	FHitResult OutHit;
	const FVector Start = CameraComponent->GetComponentLocation();
	FVector End = (CameraComponent->GetForwardVector() * RayDistance) + Start;
	ASAPowerPawn* prevPowerObject = CurrentPowerObject;

	bHasSelectedTerrain = false;

	DrawDebugLine(GetWorld(), Start, End, FColor::Blue);

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		CurrentTile = OutHit.ImpactPoint;

		bHasSelectedTerrain = Chunk->GetTileIndexFromLocation(CurrentTile, SelectedTerrainIndex);

//#if WITH_EDITOR
//		const int32 Tiles = Terrain->GetTiles();
//		const int32 CellX = SelectedTerrainIndex / Tiles;
//		const int32 CellY = SelectedTerrainIndex % Tiles;
//		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("CellX=%d CellY=%d"), CellX, CellY));
//#endif

		if (bHasSelectedTerrain)
		{
			DrawDebugLine(GetWorld(), CurrentTile, CurrentTile + FVector::UpVector * 1000, FColor::Blue);

//#if WITH_EDITOR
//			const float LevelSize = 1.0f / Terrain->LevelData->Levels;
//			const int32 Tiles = Terrain->GetTiles();
//			const int32 CellX = SelectedTerrainIndex / Tiles;
//			const int32 CellY = SelectedTerrainIndex % Tiles;
//			const int32 NextCellX = CellX < Tiles ? CellX + 1 : CellX - 1;
//			const int32 NextCellY = CellY < Tiles ? CellY + 1 : CellY - 1;
//
//			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("CellX=%d NextCellX=%d CellY=%d NextCellY=%d 0=%d 1=%d 2=%d 3=%d s=%d"), 
//				CellX, NextCellX, CellY, NextCellY,
//				CellY + (CellX * (Tiles+1)), 
//				NextCellY + (CellX * (Tiles+1)),
//				CellY + (NextCellX * (Tiles+1)),
//				NextCellY + (NextCellX * (Tiles+1)),
//				SelectedTerrainIndex
//			));
//#endif
		}
	}

	FVector TilePos = CurrentTile;
	if (!Chunk->GetTileCurrentPosition(TilePos) || TilePos.Y < 0)
		return; //terrain section isnt valid so it cant have an object on it.

	FVector OutFoundMapTile = TilePos;
	CurrentPowerObject = Chunk->GetTileTopObject(OutFoundMapTile);
	
	if (CurrentPowerObject != prevPowerObject)
	{
		if (prevPowerObject)
			prevPowerObject->SetSelected(false);

		if (CurrentPowerObject)
			CurrentPowerObject->SetSelected(true);
	}

	bCurrentTileIsValid = Chunk->IsPositionValid(CurrentTile);

	ASAPlayerControllerEdit* CurrentController = Cast<ASAPlayerControllerEdit>(Controller);
	CurrentController->SetPlayerMarkerHidden(!bCurrentTileIsValid);
	if (bCurrentTileIsValid)
		CurrentController->SetPlayerMarkerLocation(CurrentTile);
}

//void ASAEditLevelPawn::OnTerrainComplete()
//{
//	if (ResetRequired)
//	{
//		ResetRequired = false;
//		ResetPosition();
//	}
//}

void ASAEditLevelPawn::ResetPosition()
{
	if (Chunk == nullptr)
		return;

	ASATerrainActor* TA = Chunk->GetTerrainActor();
	TerrainBounds = TA->TerrainBounds;

	const FVector Position = TerrainBounds.GetCenter();
	float Radius = TerrainBounds.GetExtent().Size();
	float AspectToUse = CameraComponent->AspectRatio;

	/**
 * We need to make sure we are fitting the sphere into the viewport completely, so if the height of the viewport is less
 * than the width of the viewport, we scale the radius by the aspect ratio in order to compensate for the fact that we have
 * less visible vertically than horizontally.
 */
	if (AspectToUse > 1.0f)
	{
		Radius *= AspectToUse;
	}

	const float HalfFOVRadians = FMath::DegreesToRadians(CameraComponent->FieldOfView / 2.0f);
	const float DistanceFromSphere = Radius / FMath::Tan(HalfFOVRadians);
	FVector CameraOffsetVector = FVector::UpVector * DistanceFromSphere;

	SetActorLocation(Position + CameraOffsetVector);
	SetActorRotation(FVector::DownVector.Rotation());
	if(Controller != nullptr)
		Controller->SetControlRotation(FVector::DownVector.Rotation());
	
}

void ASAEditLevelPawn::OnTerrainGenerated(const USATerrainChunk* NewChunk)
{
	Chunk = (USATerrainChunk*)NewChunk;

	Heights = NewChunk->GetTerrainActor()->Heights;
}