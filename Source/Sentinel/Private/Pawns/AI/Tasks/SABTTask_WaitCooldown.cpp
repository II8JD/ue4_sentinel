// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_WaitCooldown.h"

#include "AIController.h"

#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
static TAutoConsoleVariable<float> CVarCooldownTime(TEXT("AI.CooldownTime"), 5.0f, TEXT("How long AI waits to cool down."));
#endif

USABTTask_WaitCooldown::USABTTask_WaitCooldown(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Power Absorber Wait Cooldown";
	bNotifyTick = false;

	TimerDelegate = FTimerDelegate::CreateUObject(this, &USABTTask_WaitCooldown::OnCooldownComplete);
}

EBTNodeResult::Type USABTTask_WaitCooldown::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
			return EBTNodeResult::Failed;
	}

	ASAPowerAbsorberPawn* PowerObject = Cast<ASAPowerAbsorberPawn>(MyController->GetPawn());
	if (!PowerObject)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Pawn %s."), *this->GetName())
			return EBTNodeResult::Failed;
	}

	// reset timer handle
	TimerHandle.Invalidate();
	MyOwnerComp = &OwnerComp;

	float delayTime = CooldownTime;
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	float debugWaitTime = CVarCooldownTime.GetValueOnGameThread();
	if (debugWaitTime >= 0.0f)
		delayTime = debugWaitTime;
#endif

	if (delayTime == 0.0f)
		TimerDelegate.Execute();
	else
		MyController->GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDelegate, delayTime, false);

	return EBTNodeResult::InProgress;
}

EBTNodeResult::Type USABTTask_WaitCooldown::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();

	if (MyController && TimerHandle.IsValid())
	{
		MyController->GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	}

	TimerHandle.Invalidate();

	return EBTNodeResult::Aborted;
}

void USABTTask_WaitCooldown::OnCooldownComplete()
{
	if (MyOwnerComp)
	{
		FinishLatentTask(*MyOwnerComp, EBTNodeResult::Succeeded);
	}
}
