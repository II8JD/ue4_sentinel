// Copyright Epic Games, Inc. All Rights Reserved.

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_RotateToTarget.h"

#include <BehaviorTree/Blackboard/BlackboardKeyType_Vector.h>
#include <BehaviorTree/BlackboardComponent.h>
#include <Kismet/KismetMathLibrary.h>

#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"

USABTTask_RotateToTarget::USABTTask_RotateToTarget(const FObjectInitializer& ObjectInitializer) : 
	Super(ObjectInitializer)
{
	NodeName = "SA Rotate To Target";
	INIT_TASK_NODE_NOTIFY_FLAGS();

	bNotifyTick = true;
}

namespace
{
	FORCEINLINE_DEBUGGABLE float CalculateAngleDifferenceDot(const FVector& VectorA, const FVector& VectorB)
	{
		return (VectorA.IsNearlyZero() || VectorB.IsNearlyZero())
			? 1.f
			: VectorA.CosineAngle2D(VectorB);
	}
}

void USABTTask_RotateToTarget::InitializeFromAsset(UBehaviorTree& Asset)
{
	Super::InitializeFromAsset(Asset);

	UBlackboardData* BBAsset = GetBlackboardAsset();
	if (BBAsset)
	{
		TargetBBKey.ResolveSelectedKey(*BBAsset);
	}
	else
	{
		UE_LOG(LogBehaviorTree, Warning, TEXT("Can't initialize task: %s, make sure that behavior tree specifies blackboard asset!"), *GetName());
	}
}

EBTNodeResult::Type USABTTask_RotateToTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	FRotateToTargetMemory* MyMemory = CastInstanceNodeMemory<FRotateToTargetMemory>(NodeMemory);
	MyMemory->PreviousGoalLocation = FAISystem::InvalidLocation;
	MyMemory->MoveRequestID = FAIRequestID::InvalidRequest;

	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName());
		return EBTNodeResult::Failed;
	}

	ASAPowerAbsorberPawn* Pawn = (ASAPowerAbsorberPawn*)MyController->GetPawn();
	if (!Pawn)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find ASAPowerSentryPawn %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	EBTNodeResult::Type result = PerformRotateTask(OwnerComp, NodeMemory);

	if (result == EBTNodeResult::InProgress)
	{
		UBlackboardComponent* BlackboardComp = OwnerComp.GetBlackboardComponent();
		if (ensure(BlackboardComp))
		{
			if (MyMemory->BBObserverDelegateHandle.IsValid())
			{
				//UE_VLOG(MyController, LogBehaviorTree, Warning, TEXT("USABTTask_RotateToTarget::ExecuteTask \'%s\' Old BBObserverDelegateHandle is still valid! Removing old Observer."), *GetNodeName());
				BlackboardComp->UnregisterObserver(TargetBBKey.GetSelectedKeyID(), MyMemory->BBObserverDelegateHandle);
			}
			MyMemory->BBObserverDelegateHandle = BlackboardComp->RegisterObserver(TargetBBKey.GetSelectedKeyID(), this, FOnBlackboardChangeNotification::CreateUObject(this, &USABTTask_RotateToTarget::OnBlackboardValueChange));
		}
	}

	return result;
}

void USABTTask_RotateToTarget::TickTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	FRotateToTargetMemory* MyMemory = CastInstanceNodeMemory<FRotateToTargetMemory>(NodeMemory);
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController || !MyMemory)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName());
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		return;
	}

	ASAPowerAbsorberPawn* absorberPawn = Cast<ASAPowerAbsorberPawn>(MyController->GetPawn());
	if (!absorberPawn)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find ASAPowerAbsorberPawn %s."), *this->GetName());
		FinishLatentTask(OwnerComp, EBTNodeResult::Failed);
		return;
	}

	FRotator lookAtRotation = UKismetMathLibrary::FindLookAtRotation(absorberPawn->GetHeadLocation(), MyMemory->PreviousGoalLocation);
	
	float yaw = 0.0f;
	float pitch = 0.0f;
	UKismetMathLibrary::GetYawPitchFromVector(absorberPawn->GetHeadRotation(), yaw, pitch);

	//rotate yaw
	FRotator newRot = absorberPawn->GetActorRotation();
	float rotateDist = FMath::FInterpTo(yaw, lookAtRotation.Yaw, DeltaSeconds, RotationSpeed);
	newRot.Yaw = rotateDist;
	absorberPawn->SetActorRotation(newRot);

	//rotate pitch
	rotateDist = FMath::FInterpTo(pitch, lookAtRotation.Pitch, DeltaSeconds, RotationSpeed);
	newRot.Pitch = rotateDist;
	absorberPawn->SetHeadPitch(newRot.Pitch);

	FVector headLocation = absorberPawn->GetHeadLocation();
	DrawDebugLine(GetWorld(), headLocation, headLocation + (absorberPawn->GetHeadRotation() * 10000.0f),
		FColor::Green, false, -1, 0, 12.333);
	DrawDebugLine(GetWorld(), MyMemory->PreviousGoalLocation, MyMemory->PreviousGoalLocation + (FVector::UpVector * 1000000.0f),
		FColor::Red, false, -1, 0, 12.333);
	DrawDebugLine(GetWorld(), headLocation, headLocation + (lookAtRotation.Vector() * 10000.0f),
		FColor::Blue, false, -1, 0, 12.333);
	
	if (absorberPawn->IsLookingAtTarget(MyMemory->PreviousGoalLocation))
	{
		FinishLatentTask(OwnerComp, EBTNodeResult::Succeeded);
	}
}

EBTNodeResult::Type USABTTask_RotateToTarget::PerformRotateTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();
	FRotateToTargetMemory* MyMemory = CastInstanceNodeMemory<FRotateToTargetMemory>(NodeMemory);
	AAIController* MyController = OwnerComp.GetAIOwner();
	APawn* Pawn = MyController->GetPawn();

	EBTNodeResult::Type NodeResult = EBTNodeResult::Failed;

	if (MyController && MyBlackboard)
	{
		if (TargetBBKey.SelectedKeyType == UBlackboardKeyType_Vector::StaticClass())
		{
			const FVector TargetLocation = MyBlackboard->GetValue<UBlackboardKeyType_Vector>(TargetBBKey.GetSelectedKeyID());

			DrawDebugLine(GetWorld(), TargetLocation, TargetLocation + FVector::UpVector * 1000.0f, FColor::Red);

			float newDist = CalculateAngleDifferenceDot(Pawn->GetActorForwardVector(), (TargetLocation, Pawn->GetActorLocation()));

			MyMemory->PreviousGoalLocation = TargetLocation;

			if(newDist < 0.0f)
				NodeResult = EBTNodeResult::Succeeded;
			else
				NodeResult = EBTNodeResult::InProgress;
		}
	}

	return NodeResult;
}

EBlackboardNotificationResult USABTTask_RotateToTarget::OnBlackboardValueChange(const UBlackboardComponent& Blackboard, FBlackboard::FKey ChangedKeyID)
{
	UBehaviorTreeComponent* BehaviorComp = Cast<UBehaviorTreeComponent>(Blackboard.GetBrainComponent());
	if (BehaviorComp == nullptr)
	{
		return EBlackboardNotificationResult::RemoveObserver;
	}

	const AAIController* MyController = BehaviorComp->GetAIOwner();
	uint8* RawMemory = BehaviorComp->GetNodeMemory(this, BehaviorComp->FindInstanceContainingNode(this));
	FRotateToTargetMemory* MyMemory = CastInstanceNodeMemory<FRotateToTargetMemory>(RawMemory);

	const EBTTaskStatus::Type TaskStatus = BehaviorComp->GetTaskStatus(this);
	if (TaskStatus != EBTTaskStatus::Active)
	{
		//UE_VLOG(MyController, LogBehaviorTree, Error, TEXT("BT MoveTo \'%s\' task observing BB entry while no longer being active!"), *GetNodeName());

		// resetting BBObserverDelegateHandle without unregistering observer since 
		// returning EBlackboardNotificationResult::RemoveObserver here will take care of that for us
		MyMemory->BBObserverDelegateHandle.Reset(); //-V595

		return EBlackboardNotificationResult::RemoveObserver;
	}

	// this means the move has already started. 
	if (MyMemory != nullptr && BehaviorComp->GetAIOwner() != nullptr)
	{
		bool bUpdateMove = true;
		// check if new goal is almost identical to previous one
		if (TargetBBKey.SelectedKeyType == UBlackboardKeyType_Vector::StaticClass())
		{
			const FVector TargetLocation = Blackboard.GetValue<UBlackboardKeyType_Vector>(TargetBBKey.GetSelectedKeyID());

			APawn* Pawn = MyController->GetPawn();
			float newDist = CalculateAngleDifferenceDot(Pawn->GetActorForwardVector(), (TargetLocation, Pawn->GetActorLocation()));

			if (newDist < 0.0f)
			{
				FinishLatentTask(*BehaviorComp, EBTNodeResult::Succeeded);
			}
		}
	}

	return EBlackboardNotificationResult::ContinueObserving;
}

void USABTTask_RotateToTarget::PostInitProperties()
{
	Super::PostInitProperties();

	PrecisionDot = FMath::Cos(FMath::DegreesToRadians(Precision));
}

void USABTTask_RotateToTarget::PostLoad()
{
	Super::PostLoad();

	PrecisionDot = FMath::Cos(FMath::DegreesToRadians(Precision));
}

#if WITH_EDITOR

FName USABTTask_RotateToTarget::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
