// Copyright Epic Games, Inc. All Rights Reserved.

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_RotateFractionCooldown.h"

#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"
#include "BehaviorTree/BlackboardComponent.h"

USABTTask_RotateFractionCooldown::USABTTask_RotateFractionCooldown(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	NodeName = "Power Absorber Search Task";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_RotateFractionCooldown::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
			return EBTNodeResult::Failed;
	}

	ASAPowerAbsorberPawn* Pawn = (ASAPowerAbsorberPawn*)MyController->GetPawn();
	if (!Pawn)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find ASAPowerSentryPawn %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	const float RotationDistance = Pawn->GetRotateDistance();

	//rotate
	FQuat YourRoation(Pawn->GetActorRotation().Quaternion());
	// Yaw
	FQuat YawQuat(FVector(0.0f, 0.0f, 1.0f), FMath::DegreesToRadians(RotationDistance));
	YourRoation *= YawQuat;
	YourRoation.Normalize();
	Pawn->SetActorRotation(YourRoation);

	UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();
	const float NewRotationTotal = MyBlackboard->GetValueAsFloat(RotationBlackboardKey.SelectedKeyName) + RotationDistance;
	MyBlackboard->SetValueAsFloat(RotationBlackboardKey.SelectedKeyName, NewRotationTotal);

	return Super::ExecuteTask(OwnerComp, NodeMemory);
}

#if WITH_EDITOR
FName USABTTask_RotateFractionCooldown::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}
#endif	// WITH_EDITOR
