// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_MeanieTurnIntoTree.h"

#include "AIController.h"
#include "Pawns/PowerObjects/SAPowerMeaniePawn.h"

USABTTask_MeanieTurnIntoTree::USABTTask_MeanieTurnIntoTree(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Spawn Meanie";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_MeanieTurnIntoTree::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}
	
	ASAPowerMeaniePawn* Meanie = Cast<ASAPowerMeaniePawn>(MyController->GetPawn());
	if (!Meanie)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Meanie Pawn %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	Meanie->TurnIntoTree();
		
	return EBTNodeResult::Succeeded;
}

#if WITH_EDITOR

FName USABTTask_MeanieTurnIntoTree::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
