// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_RobotHyperspace.h"

#include "AIController.h"
#include "Pawns/PowerObjects/SAPowerRobotPawn.h"

USABTTask_RobotHyperspace::USABTTask_RobotHyperspace(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Robot Teleport";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_RobotHyperspace::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}
	
	ASAPowerRobotPawn* Robot = Cast<ASAPowerRobotPawn>(MyController->GetPawn());
	if (!Robot)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Robot Pawn %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	Robot->OnInputHyperspace();
		
	return EBTNodeResult::Succeeded;
}

#if WITH_EDITOR

FName USABTTask_RobotHyperspace::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
