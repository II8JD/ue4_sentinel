// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_Absorb.h"

#include "AIController.h"
#include "Pawns/PowerObjects/SAPowerAbsorberPawn.h"

USABTTask_Absorb::USABTTask_Absorb(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Power Absorber Wait Then Absorb";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_Absorb::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	MyOwnerComp = &OwnerComp;

	if(ASAPowerAbsorberPawn* powerObject = GetValidPowerObject())
		return Super::ExecuteTask(OwnerComp, NodeMemory);

	return EBTNodeResult::Failed;
}

ASAPowerAbsorberPawn* USABTTask_Absorb::GetValidPowerObject()
{
	const AAIController* MyController = MyOwnerComp->GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return nullptr;
	}

	ASAPowerAbsorberPawn* PowerObject = Cast<ASAPowerAbsorberPawn>(MyController->GetPawn());
	if (!PowerObject)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Pawn %s."), *this->GetName())
		return nullptr;
	}

	if (!PowerObject->HasAbsorbTarget())
		return nullptr;

	if (!PowerObject->CanAbsorbTarget())
		return nullptr;

	return PowerObject;
}

void USABTTask_Absorb::OnCooldownComplete()
{
	if(ASAPowerAbsorberPawn* powerObject = GetValidPowerObject())
		powerObject->AbsorbTarget();

	Super::OnCooldownComplete();
}

#if WITH_EDITOR
FName USABTTask_Absorb::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}
#endif	// WITH_EDITOR
