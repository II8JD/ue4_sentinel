// Copyright Epic Games, Inc. All Rights Reserved.

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_ScanForTargets.h"

#include "PlayerControllers\SAAIControllerBase.h"

USABTTask_ScanForTargets::USABTTask_ScanForTargets(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Power Absorber Scan Task";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_ScanForTargets::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASAAIControllerBase* const MyController = Cast<ASAAIControllerBase>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	//ASAPowerAbsorberPawn* PowerObject = Cast<ASAPowerAbsorberPawn>(MyController->GetPawn());
	//if (!PowerObject)
	//{
	//	UE_LOG(SentinelAILog, Error, TEXT("Failed to find Pawn %s."), *this->GetName())
	//		return EBTNodeResult::Failed;
	//}

	MyController->ScanForTargets();

	return EBTNodeResult::Succeeded;
}

#if WITH_EDITOR

FName USABTTask_ScanForTargets::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
