// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_SpawnPowerObject.h"

#include "AIController.h"

USABTTask_SpawnPowerObject::USABTTask_SpawnPowerObject(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Spawn power object";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_SpawnPowerObject::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* const MyController = OwnerComp.GetAIOwner();
	if (!MyController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}
	
	ASAPowerAbsorberPawn* PowerObject = Cast<ASAPowerAbsorberPawn>(MyController->GetPawn());
	if (!PowerObject)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Pawn %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	{
		FVector tileLocation = PowerObject->GetCurrentTileLocation();
		DrawDebugLine(GetWorld(), tileLocation, tileLocation + FVector::UpVector * 1000.0f, FColor::Red);
	}
	if(!PowerObject->GetCurrentTileIsValid())
		return EBTNodeResult::Failed;

	FVector SpawnPos = PowerObject->GetCurrentTileLocation();
	PowerObject->TrySpawnObjectAtLocation(PowerObjectType, SpawnPos);

	return EBTNodeResult::Succeeded;
}

#if WITH_EDITOR

FName USABTTask_SpawnPowerObject::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
