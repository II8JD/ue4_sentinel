// Carl Peters

#include "Sentinel/Public/Pawns/AI/Tasks/SABTTask_SpawnMeanie.h"

#include "Sentinel/Public/PlayerControllers/SAAIControllerSentinel.h"

USABTTask_SpawnMeanie::USABTTask_SpawnMeanie(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Spawn Meanie";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_SpawnMeanie::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASAAIControllerSentinel* const myController = Cast<ASAAIControllerSentinel>(OwnerComp.GetAIOwner());
	if (!myController)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName())
		return EBTNodeResult::Failed;
	}

	myController->ConvertClosestTreeIntoMeanie();

	return EBTNodeResult::Succeeded;
}

#if WITH_EDITOR

FName USABTTask_SpawnMeanie::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
