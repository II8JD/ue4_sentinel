// Carl Peters

#include "Sentinel/Public/Pawns/SAPreviewLevelPawn.h"

#include <Camera/CameraComponent.h>
#include <Components/InputComponent.h>
#include <Engine/EngineBaseTypes.h>
#include <DrawDebugHelpers.h>

#include <Debug/DebugDrawService.h> //test
#include <Engine/Canvas.h>

#include "GameMode/SAGameModeBaseGame.h"
#include "Terrain/SATerrainActor.h"

// Sets default values
ASAPreviewLevelPawn::ASAPreviewLevelPawn()
{
	bReplicates = true;

 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetRelativeLocation(FVector(.0f, .0f, .0f)); // Position the camera
	CameraComponent->bUsePawnControlRotation = false;

	RootComponent = CameraComponent;

	////rotate
	//FQuat YourRoation(GetActorRotation().Quaternion());
	//// Yaw
	//FQuat YawQuat(FVector(0.0f, 1.0f, 0.0f), PitchAngle);
	//YourRoation *= YawQuat;
	//YourRoation.Normalize();
	//SetActorRotation(YourRoation);
}

void ASAPreviewLevelPawn::RPCPossessGamePawn_Implementation(AController* NewController, ASAPowerPawn* PawnToPossess)
{
	NewController->UnPossess();
	NewController->Possess(PawnToPossess);

	Destroy();
}

// Called every frame
void ASAPreviewLevelPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateRotation(DeltaTime);
}

void ASAPreviewLevelPawn::UpdateRotation(float DeltaTime)
{
	if (!bBoundsSet)
	{
		USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
		USATerrainChunk* chunk = objectManager->GetChunk();
		if (chunk == nullptr || !chunk->GetTerrainComplete())
			return;

		ASATerrainActor* Terrain = chunk->GetTerrainActor();
		TerrainBounds = Terrain->TerrainBounds;
		bBoundsSet = true;
	}

	Rotation += DeltaTime * RotateSpeed;

	//rotate
	FQuat YawQuat = FQuat(FVector(0.0f, 0.0f, 1.0f), FMath::DegreesToRadians(Rotation)) * FQuat(FVector(0.0f, 1.0f, 1.0f), PitchAngle);

	SetActorRotation(YawQuat);

	const FVector Position = TerrainBounds.GetCenter();
	//const float Radius = FMath::Max(TerrainBounds.GetExtent().Size(), 10.f);
	float Radius = TerrainBounds.GetExtent().Size() * RadiusScale;
	float AspectToUse = CameraComponent->AspectRatio;

	/**
 * We need to make sure we are fitting the sphere into the viewport completely, so if the height of the viewport is less
 * than the width of the viewport, we scale the radius by the aspect ratio in order to compensate for the fact that we have
 * less visible vertically than horizontally.
 */
	if (AspectToUse > 1.0f)
	{
		Radius *= AspectToUse;
	}

	const float HalfFOVRadians = FMath::DegreesToRadians(CameraComponent->FieldOfView / 2.0f);
	const float DistanceFromSphere = Radius / FMath::Tan(HalfFOVRadians);
	FVector CameraOffsetVector = YawQuat.GetForwardVector() * -DistanceFromSphere;

	SetActorLocation(Position + CameraOffsetVector);

	/*
	UWorld *World = GetWorld();
	DrawDebugCamera(World, GetActorLocation(), GetActorRotation(), CameraComponent->FieldOfView);
	DrawDebugSphere(World, Position, Radius, 32, FColor::Red);
	//DrawDebugFrustum(World,
	DrawDebugLine(GetWorld(), GetActorLocation(), GetActorLocation() + YawQuat.GetForwardVector() * 1000, FColor::Blue);
	*/
}