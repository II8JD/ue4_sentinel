// Carl Peters

#include "Sentinel/Public/Pawns/SAMainMenuPawn.h"

#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "SAGameStateBase.h"
#include "Terrain/SATerrainActor.h"
#include "Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "DrawDebugHelpers.h"
#include "Sentinel.h"
#include "Engine/LocalPlayer.h"
#include "Engine/GameViewportClient.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASAMainMenuPawn::ASAMainMenuPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetRelativeLocation(FVector(.0f, .0f, .0f)); // Position the camera
	CameraComponent->bUsePawnControlRotation = false;
	RootComponent = CameraComponent;
}

void ASAMainMenuPawn::BeginDestroy()
{
	Super::BeginDestroy();
}

// Called every frame
void ASAMainMenuPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateCameraLocation();
}

void ASAMainMenuPawn::UpdateCameraLocation()
{
	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	if (objectManager == nullptr)
		return;

	if (!bBoundsSet)
	{
		USATerrainChunk* Chunk = objectManager->GetChunk();
		if (Chunk == nullptr)
			return;

		ASATerrainActor* TA = Chunk->GetTerrainActor();
		if (!TA->GetTerrainGenerationComplete())
			return;

		FBox TerrainBounds = TA->GetComponentsBoundingBox();
		TerrainCenter = TerrainBounds.GetCenter();

		bBoundsSet = true;
	}

	const TArray<ASAPowerPawn*> sentinels = objectManager->GetPowerObjects(EPowerObjectType::Sentinel);
	if (sentinels.Num() == 0 || !sentinels[0]->IsActive())
		return;

	const FBox SentinelBounds = sentinels[0]->GetComponentsBoundingBox();
	if (!SentinelBounds.IsValid)
		return;

	const FVector SentinelLocation = SentinelBounds.GetCenter();

	/**
	* We need to make sure we are fitting the sphere into the viewport completely, so if the height of the viewport is less
	* than the width of the viewport, we scale the radius by the aspect ratio in order to compensate for the fact that we have
	* less visible vertically than horizontally.
	*/
	const float AspectToUse = CameraComponent->AspectRatio;
	float Radius = SentinelBounds.GetExtent().Size() * RadiusScale;
	if (AspectToUse > 1.0f)
	{
		Radius *= AspectToUse;
	}

	const float HalfFOVRadians = FMath::DegreesToRadians(CameraComponent->FieldOfView / 2.0f);
	const float DistanceOffset = Radius / FMath::Tan(HalfFOVRadians);

	//calculate position close to the sentinel looking back at the middle of the terrain (the sentinel could be in the middle too)
	FVector Direction = SentinelLocation - TerrainCenter;
	FVector TargetLocation = SentinelLocation + (Direction.GetSafeNormal() * DistanceOffset);

	//make sure new position isnt inside the terrain
	//FVector TerrainPos = TargetPos;
	//Terrain->IsPositionValid(TerrainPos);
	//TargetPos.Z = fmaxf(TargetPos.Z, TerrainPos.Z);

	//set initial position and rotation
	//we will use this as a starting point to move again with the sentinel offset to the right of the screen
	//SetActorLocation(TargetPos);
	Direction = (SentinelLocation - TargetLocation).GetSafeNormal();
	SetActorRotation(FRotationMatrix::MakeFromX(Direction).Rotator());

	//what are the unreal guys thinking why is this so hard? who cares about a player concept?
	ULocalPlayer* LocalPlayer = GEngine->FindFirstLocalPlayerFromControllerId(0);
	if (LocalPlayer != nullptr)
	{
		FSceneViewProjectionData ProjectionData;
		if (LocalPlayer->GetProjectionData(LocalPlayer->ViewportClient->Viewport, ProjectionData))
		{
			ProjectionData.ViewOrigin = TargetLocation; //i dont understand when this should be updated but SetActorLocation doesnt do it

			const FIntRect ViewRect = ProjectionData.GetConstrainedViewRect();
			const FMatrix InvViewProjMatrix = ProjectionData.ComputeViewProjectionMatrix().InverseFast();
			FVector WorldDirection = FVector::ZeroVector;
			FSceneView::DeprojectScreenToWorld(FVector2D(ViewRect.Width() * (1.0f - HorizontalOffset), ViewRect.Height() * 0.5f), ViewRect, InvViewProjMatrix, TargetLocation, WorldDirection);

			FPlane Plane = FPlane(SentinelLocation, Direction);
			//DrawDebugSolidPlane(GetWorld(), Plane, SentinelPos, FVector2D(1000, 1000), FColor::Blue);
			FVector OffsetLocation = FMath::RayPlaneIntersection(TargetLocation, WorldDirection, Plane);
			TargetLocation = OffsetLocation - (Direction * DistanceOffset);
		}
	}

	SetActorLocation(TargetLocation);
}
