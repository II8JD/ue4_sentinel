// Carl Peters

#include "Sentinel/Public/ActorComponents/SAWorldAlignScreenActorComponent.h"

#include "Engine/LocalPlayer.h"
#include "Camera/PlayerCameraManager.h"
#include "GameMode/SAGameModeBaseGame.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
USAWorldAlignScreenActorComponent::USAWorldAlignScreenActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USAWorldAlignScreenActorComponent::BeginPlay()
{
	Super::BeginPlay();

	FViewport::ViewportResizedEvent.AddUObject(this, &USAWorldAlignScreenActorComponent::OnViewportResized);
	PositionObject();
}


// Called every frame
void USAWorldAlignScreenActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	PositionObject();
}

void USAWorldAlignScreenActorComponent::OnViewportResized(FViewport* Viewport, uint32 Unused)
{
	PositionObject();
}

void USAWorldAlignScreenActorComponent::PositionObject()
{
	ULocalPlayer* LocalPlayer = GEngine->FindFirstLocalPlayerFromControllerId(0);
	if (LocalPlayer == nullptr)
		return;

	FSceneViewProjectionData ProjectionData;
	if (!LocalPlayer->GetProjectionData(LocalPlayer->ViewportClient->Viewport, ProjectionData))
		return;

	const APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController == nullptr)
		return;

	const APlayerCameraManager* CameraManager = PlayerController->PlayerCameraManager;

	FIntPoint Rect = ProjectionData.GetViewRect().Size();
	const float ViewAspectRatio = Rect.X / Rect.Y;
	float DistanceOffset = 0.0f;
	const FBox TargetBounds = GetOwner()->GetComponentsBoundingBox();
	float Radius = TargetBounds.GetExtent().Size() * RadiusScale;

	/**
	* We need to make sure we are fitting the sphere into the viewport completely, so if the height of the viewport is less
	* than the width of the viewport, we scale the radius by the aspect ratio in order to compensate for the fact that we have
	* less visible vertically than horizontally.
	*/
	if (ViewAspectRatio > 1.0f)
	{
		Radius *= ViewAspectRatio;
	}

	const float HalfFOVRadians = FMath::DegreesToRadians(CameraManager->GetFOVAngle() / 2.0f);
	DistanceOffset = Radius / FMath::Tan(HalfFOVRadians);

	const FIntRect ViewRect = ProjectionData.GetConstrainedViewRect();
	const FMatrix InvViewProjMatrix = ProjectionData.ComputeViewProjectionMatrix().InverseFast();
	FVector WorldDirection = FVector::ZeroVector;
	FVector WorldPos = FVector::ZeroVector;
	FSceneView::DeprojectScreenToWorld(FVector2D(ViewRect.Width()* (1.0f - HorizontalOffset), ViewRect.Height() * VerticalOffset),
		ViewRect, InvViewProjMatrix, /*OUT*/WorldPos, /*Out*/WorldDirection);

	FVector CameraDirection = CameraManager->GetCameraRotation().Vector();
	const FVector CameraPos = CameraManager->GetCameraLocation();
	const FVector PlanePos = CameraPos + (CameraDirection * DistanceOffset);
	
	const FPlane Plane = FPlane(PlanePos, CameraDirection);
	//DrawDebugSolidPlane(GetWorld(), Plane, PlanePos, FVector2D(1000, 1000), FColor::Blue);
	GetOwner()->SetActorLocation(FMath::RayPlaneIntersection(WorldPos, WorldDirection, Plane));

	CameraDirection = CameraDirection.RotateAngleAxis(OffsetAngle.X, FVector(1.0f, 0.0f, 0.0f));
	//FQuat YawQuat(FVector(0.0f, 0.0f, 1.0f), FMath::DegreesToRadians(RotationOffset));
	GetOwner()->SetActorRotation(FRotationMatrix::MakeFromX(CameraDirection).Rotator());
}