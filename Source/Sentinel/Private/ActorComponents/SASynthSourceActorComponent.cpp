// Carl Peters

#include "Sentinel/Public/ActorComponents/SASynthSourceActorComponent.h"

#include "DrawDebugHelpers.h"
#include "SynthComponents/EpicSynth1Component.h"

// Sets default values for this component's properties
USASynthSourceActorComponent::USASynthSourceActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	ModularSynthComponent = CreateDefaultSubobject<UModularSynthComponent>(TEXT("ModularSynthComponent"));
}

void USASynthSourceActorComponent::PlayClip()
{
	//ModularSynthComponent->SetSynthPreset(Preset);
	//ModularSynthComponent->NoteOn(Note, Velocity, Duration);
}