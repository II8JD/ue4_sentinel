// Carl Peters

#include "Sentinel/Public/GameMode/SAGameModeBaseEditor.h"

#include "DataAssets/SAPowerDataAsset.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "Pawns/PowerObjects/SAPowerRobotPawn.h"
#include "Pawns/SAEditLevelPawn.h"
#include "PlayerControllers/SAPlayerControllerEdit.h"
#include "SAObjectManager.h"
#include "Terrain/SATerrainActor.h"

void ASAGameModeBaseEditor::BeginPlay()
{
	LevelDataAsset = NewObject<USALevelDataAsset>();
	LevelDataAsset->LevelData.Material = Material;
	LevelDataAsset->LevelData.PlayerStartIndex = 0;
	LevelDataAsset->LevelData.SentinelStartIndex = 1;
	LevelDataAsset->LevelData.RandomiserInterface = RandomiserInterface;
	//LevelDataAsset->LevelData.Heights.Init(0, LevelDataAsset->LevelData.Tiles * LevelDataAsset->LevelData.Tiles);

	Super::BeginPlay();

	//Terrain->OnCompletionCB.AddDynamic(this, &ASAGameModeBaseEditor::OnTerrainComplete);

	//ResetCamera = true;

	//SetSeedAndLoadLevel(0, LevelDataAsset);
}

void ASAGameModeBaseEditor::OnTerrainGenerated(const USATerrainChunk* Chunk)
{
	//ResetLevel();

	//Super::OnTerrainComplete();

	//SpawnGameObjects(Terrain);

	SpawnGameObjects(Chunk);

	ASAGameStateBase* State = Cast<ASAGameStateBase>(GameState);
	State->SetExpectedObjectCount();
}

//void ASAGameModeBaseEditor::SpawnGameObjects(const USATerrainChunk* Terrain)
//{
//	Super::SpawnGameObjects(Terrain);
//
//	EditorPlayer = GetWorld()->SpawnActor<ASAEditLevelPawn>(DefaultPawnClass, PlayerPosition, PlayerRotation);
//	if (EditorPlayer != nullptr)
//	{
//		APlayerController* Controller = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
//		Controller->UnPossess();
//		Controller->Possess(EditorPlayer);
//
//		if (ResetCameraOnTerrain)
//		{
//			ResetCameraOnTerrain = false;
//			EditorPlayer->ResetPosition();
//		}
//	}
//}

ASAPowerPawn* ASAGameModeBaseEditor::SpawnPowerObject(const TSubclassOf<ASAPowerPawn>& ObjectType, FVector& SpawnPos, bool bSetPossession)
{
	//ASAPowerPawn** TargetObject = nullptr;

	if (!PlayMode)
	{
		//if we are trying to spawn a sentinel remove the old one and spawn base object too
		if (Cast<ASAPowerPawn>(ObjectType.GetDefaultObject()) != nullptr)
		{
			//ASAGameStateBase* State = GetGameState<ASAGameStateBase>();
			//ASAPowerPawn* Sentinel = Cast<ASAPowerPawn>(ASAPowerSentinelPawn::GetSentinel(GetWorld()));
			//TargetObject = &Sentinel;

			////remove old sentinel
			//if (Sentinel != nullptr)
			//	Terrain->AddPowerObject(Sentinel->GetActorLocation(), *Sentinel, false);

			////reposition or generate pedestal
			//if (SentinelPedestal != nullptr && !SentinelPedestal->IsPendingKillPending())
			//{
			//	Terrain->AddPowerObject(SentinelPedestal->GetActorLocation(), *SentinelPedestal, false);
			//	SentinelPedestal->SetActorLocation(SpawnPos);
			//	Terrain->AddPowerObject(SpawnPos, *SentinelPedestal, true);
			//}
			//else
			//	SentinelPedestal = SpawnPowerObject(PowerObjectsDataAsset->SentinelPedestal, SpawnPos);

			//Terrain->IsPositionValid(SpawnPos);

			//if (Sentinel != nullptr && !Sentinel->IsPendingKillPending())
			//{
			//	Sentinel->SetActorLocation(SpawnPos);
			//	Terrain->AddPowerObject(SpawnPos, *Sentinel, true);
			//	return Sentinel; //nothing more to do
			//}
		}
		else if (Cast<ASAPowerRobotPawn>(ObjectType.GetDefaultObject()) != nullptr)
		{
			if (USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>())
			{
				const TArray<ASAPowerPawn*> robots = objectManager->GetPowerObjects(EPowerObjectType::Robot);
				if (robots.Num() > 0)
				{
					//TargetObject = &robots[0];

					USATerrainChunk* Chunk = objectManager->GetChunkAtLocation(SpawnPos);
					Chunk->AddPowerObject(*robots[0], false);
					robots[0]->SetActorLocation(SpawnPos);
					Chunk->AddPowerObject(*robots[0], true);
					return robots[0];
				}
			}
		}
	}

	ASAPowerPawn *Pawn = Super::SpawnPowerObject(ObjectType, SpawnPos);

	//if (TargetObject != nullptr)
		//*TargetObject = Pawn;

	return Pawn;
}

void ASAGameModeBaseEditor::GenerateLevel()
{
	StoreEditPlayerLocation();
}

void ASAGameModeBaseEditor::StoreEditPlayerLocation()
{
	APlayerController* Controller = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
	if (Controller != nullptr)
	{
		APawn* Pawn = Controller->GetPawn();
		if (Pawn != nullptr)
		{
			PlayerPosition = Pawn->GetActorLocation();
			PlayerRotation = Pawn->GetActorRotation();
		}
	}
}

void ASAGameModeBaseEditor::TogglePlayMode()
{
	//if (!PlayMode)
	//	StoreEditPlayerLocation();

	//ResetLevel();

	//SpawnGameObjects();

	//PlayMode = !PlayMode;

	//APlayerController* Controller = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());

	//if (PlayMode)
	//{
	//	Controller->UnPossess();
	//	Controller->Possess(Robot);
	//}
	//else
	//{
	//	Controller->UnPossess();
	//	Controller->Possess(EditorPlayer);
	//}
}

bool ASAGameModeBaseEditor::IsPlayMode()
{
	return PlayMode;
}