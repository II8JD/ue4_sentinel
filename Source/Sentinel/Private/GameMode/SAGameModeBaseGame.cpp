// Carl Peters


#include "GameMode/SAGameModeBaseGame.h"
#include "PlayerControllers/SAPlayerControllerGame.h"
#include "Pawns/SAPreviewLevelPawn.h"
#include "Pawns/SAMainMenuPawn.h"
#include "Terrain/SATerrainActor.h"
#include "SAGameStateBase.h"

ASAGameModeBaseGame::ASAGameModeBaseGame()
{
	//GameSession

	LevelSkipData.Sort([](const FSALevelSkipData& A, const FSALevelSkipData& B) { return A.AbsorbPower < B.AbsorbPower; });
}

void ASAGameModeBaseGame::LevelHyperspace(const int PlayerPower)
{
	float EndPower = PlayerPower / (float)TotalWorldPower;
	int SkipLevels = 0;

	for (auto It = LevelSkipData.CreateConstIterator(); It; ++It)
		if (EndPower > It->AbsorbPower)
			SkipLevels = It->SkipLevels;

	UnlockedSeed += SkipLevels;

	//ResetLevel();

	ASAPlayerControllerGame* Controller = Cast<ASAPlayerControllerGame>(GetWorld()->GetFirstPlayerController());
	Controller->SetGameOver(false);
}