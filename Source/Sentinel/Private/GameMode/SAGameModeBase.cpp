// Carl Peters

#include "Sentinel/Public/GameMode/SAGameModeBase.h"

#include <AIController.h>
#include <Blueprint/AIBlueprintHelperLibrary.h>
#include <Containers/Map.h>
#include <DrawDebugHelpers.h>
#include <DataAssets/SAPowerDataAsset.h>
#include <Engine/LevelScriptActor.h>
#include <Kismet/GameplayStatics.h>
#include <Math/NumericLimits.h>
#include <Misc/OutputDeviceNull.h>
#include <OnlineSubsystemUtils.h>

#include "GameFramework/PlayerState.h"
#include "Pawns/PowerObjects/SAPowerPawn.h"
#include "Pawns/PowerObjects/SAPowerSentinelPawn.h"
#include "Pawns/PowerObjects/SAPowerPedestalPawn.h"
#include "Sentinel/Public/Sentinel.h"
#include "SAGameStateBase.h"
#include "SAObjectManager.h"
#include "Terrain/SATerrainChunk.h"
#include "Utility/BPFL_SAColour.h"

void ASAGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->TerrainGeneratedCB.AddDynamic(this, &ASAGameModeBase::OnTerrainGenerated);

	SetSeedAndLoadLevel(0, LevelDataAsset);
}

void ASAGameModeBase::SetSeedAndLoadLevel(const int NewSeed, USALevelDataAsset* NewLevelDataAsset)
{
	UE_LOG(GameModeLog, Log, TEXT("SetSeedAndLoadLevel"));

	ASAGameStateBase* State = Cast<ASAGameStateBase>(GameState);
	State->SetSeed(NewSeed);
	State->SetLevelData(NewLevelDataAsset->LevelData);
}

void ASAGameModeBase::OnTerrainGenerated(const USATerrainChunk* Chunk)
{
	UE_LOG(GameModeLog, Log, TEXT("OnTerrainGenerated"));

	ASAGameStateBase* State = Cast<ASAGameStateBase>(GameState);
	FMath::RandInit(State->GetSeed());

	//ResetLevel();

	//Decide object positions
	//sentinel
	DecideObjectLocation(Chunk, LevelDataAsset->LevelData.SentinelStartIndex, false, true, Chunk->GetMaxHeight());
	//sentry
	if (LevelDataAsset->LevelData.SentryCountCurve != nullptr)
	{
		DecideObjectArrayLocations(Chunk, LevelDataAsset->LevelData.SentryStartIndex, false, true, LevelDataAsset->LevelData.SentryCountCurve->GetVectorValue(0.0f));
	}
	//player
	DecideObjectLocation(Chunk, LevelDataAsset->LevelData.PlayerStartIndex, true, false, 0.0f);
	//trees
	if (LevelDataAsset->LevelData.TreeCountCurve != nullptr)
	{
		DecideObjectArrayLocations(Chunk, LevelDataAsset->LevelData.TreeStartIndex, true, false, LevelDataAsset->LevelData.TreeCountCurve->GetVectorValue(0.0f));
	}

	SpawnGameObjects(Chunk);

	State->SetExpectedObjectCount();
}

void ASAGameModeBase::DecideObjectLocation(const USATerrainChunk* Chunk, int32& LocationIndex, const bool bSearchUp, const bool bFavourHigher, float MaxHeight)
{
	FVector RandomPos;
	if (!Chunk->GetRandomPosition(RandomPos, true, bSearchUp, bFavourHigher, MaxHeight))
	{
		UE_LOG(LogTemp, Error, TEXT("Aborting DecideObjectLocation. Failed to find random position!"));
		return;
	}
	else
	{
		Chunk->GetTileIndexFromLocation(RandomPos, LocationIndex);
	}
}

void ASAGameModeBase::DecideObjectArrayLocations(const USATerrainChunk* Chunk, TArray<int32>& LocationIndexArray, const bool bSearchUp, const bool bFavourHigher, FVector MinMaxNumCurve)
{
	const int32 ObjectNum = FMath::RandRange(MinMaxNumCurve.Y, MinMaxNumCurve.X);
	LocationIndexArray.Empty();
	for (int i = 0; i < ObjectNum; i++)
	{
		FVector RandomPos;
		if (!Chunk->GetRandomPosition(RandomPos, true, bSearchUp, bFavourHigher, -1.0f))
		{
			UE_LOG(LogTemp, Error, TEXT("Aborting DecideObjectArrayLocations. Failed to find random position! i=%d"), i);
			return;
		}
		else
		{
			int32 LocationIndex = 0;
			Chunk->GetTileIndexFromLocation(RandomPos, LocationIndex);
			LocationIndexArray.Add(LocationIndex);
		}
	}
}

void ASAGameModeBase::SpawnGameObjects(const USATerrainChunk* Chunk)
{
	UE_LOG(GameModeLog, Log, TEXT("SpawnGameObjects"));

	//robot
	SpawnSingleObject(Chunk, PowerObjectsDataAsset->Robot.Blueprint, LevelDataAsset->LevelData.PlayerStartIndex);

	//sentinel
	SpawnSingleObject(Chunk, PowerObjectsDataAsset->Sentinel.Blueprint, LevelDataAsset->LevelData.SentinelStartIndex);

	//trees
	SpawnObjectList(Chunk, PowerObjectsDataAsset->Tree.Blueprint, LevelDataAsset->LevelData.TreeStartIndex);

	//Sentry
	SpawnObjectList(Chunk, PowerObjectsDataAsset->Sentry.Blueprint, LevelDataAsset->LevelData.SentryStartIndex);
}

void ASAGameModeBase::SpawnObjectList(const USATerrainChunk* Chunk, const TSubclassOf<ASAPowerPawn>& ObjectType, const TArray<int32>& StartPositions)
{
	FVector Position = FVector::ZeroVector;

	for (int i = 0, SentryCount = StartPositions.Num(); i < SentryCount; i++)
	{
		if (Chunk->GetTileLocationFromIndex(StartPositions[i], Position))
		{
			if (Chunk->IsPositionValid(Position))
				SpawnPowerObject(ObjectType, Position);
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Failed to spawn %s. Index %d position is not valid."), ObjectType, i)
			}
		}
	}
}

ASAPowerPawn* ASAGameModeBase::SpawnSingleObject(const USATerrainChunk* Chunk, const TSubclassOf<ASAPowerPawn>& ObjectType, int32 StartPosition)
{
	FVector Position = FVector::ZeroVector;
	if (StartPosition >= 0 &&
		Chunk->GetTileLocationFromIndex(StartPosition, Position))
	{
		if (Chunk->IsPositionValid(Position))
			return SpawnPowerObject(ObjectType, Position);
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to spawn %s. Position is not valid."), ObjectType)
		}
	}

	return nullptr;
}

ASAPowerPawn* ASAGameModeBase::SpawnPowerObject(const TSubclassOf<ASAPowerPawn>& ObjectType, FVector& SpawnPos, bool bSetPossession)
{
	USAObjectManager* ObjectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();

	USATerrainChunk* Chunk = ObjectManager->GetChunkAtLocation(SpawnPos);
	Chunk->IsPositionValid(SpawnPos);

	//if we are trying to spawn a sentinel remove the old one and spawn base object too
	if (Cast<ASAPowerSentinelPawn>(ObjectType.GetDefaultObject()) != nullptr)
	{
		//Destroy old sentinel
		AActor* FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), ASAPowerSentinelPawn::StaticClass());
		if (FoundActor != nullptr)
		{
			FoundActor->Destroy();
		}

		//Destroy old sentinel pedestal
		FoundActor = UGameplayStatics::GetActorOfClass(GetWorld(), ASAPowerPedestalPawn::StaticClass());
		if (FoundActor != nullptr)
		{
			FoundActor->Destroy();
		}

		//create a new pedestal
		ASAPowerPawn* Pedestal = SpawnPowerObject(PowerObjectsDataAsset->SentinelPedestal.Blueprint, SpawnPos);
		SpawnPos.Z += Pedestal->GetMeshHeight();
	}

	FActorSpawnParameters SpawnParams;
	SpawnParams.Owner = NULL;
	SpawnParams.bNoFail = true;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ASAPowerPawn* Instance = GetWorld()->SpawnActor<ASAPowerPawn>(ObjectType, SpawnPos, FRotator::ZeroRotator, SpawnParams);

	if (Cast<ASAPowerSentinelPawn>(ObjectType.GetDefaultObject()) != nullptr)
	{
		
	}

	if(bSetPossession)
		SetAIPossession(Instance);


	//if (Instance)
	//{
	//	//Instance->DisablePhysics();
	//	Terrain->AddPowerObject(SpawnPos, *Instance, true);
	//	Instance->SetOwner(EmptyActor);

	//	//AActor *currentOwner = Instance->GetOwner();
	//	//UE_LOG(LogTemp, Warning, TEXT("Failed to spawn object. %s"), *GetNameSafe(currentOwner))
	//}
	//else
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Failed to spawn object."))
	//}

	return Instance;
}

const int32 ASAGameModeBase::GetUnlockedLevel() const
{
	return UnlockedSeed;
}

void ASAGameModeBase::UpdateVote(const FUniqueNetIdRepl& UniqueId, const bool bSentinel)
{
	UE_LOG(GameModeLog, Log, TEXT("UpdateVote"));

	if (PlayerSentinelVotes.Contains(UniqueId))
		return; //dont accept a second vote

	int32& Vote = PlayerSentinelVotes.Add(UniqueId);
	Vote = bSentinel ? 1 : 0;

	int32 TargetPlayers = 0;

	//get the expected number of players in this game mode
	//i am not sure if this is correct
	//if i run the geme in editor two players automatically connect but
	//I have no session to query for a player count so added GetPlayNumberOfClients
	//surely an API must exist to just get max players without all of this?
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get(); 
	if (OnlineSub != nullptr)
	{
		IOnlineSessionPtr SessionInterface = Online::GetSessionInterface(GetWorld());
		if (SessionInterface.IsValid())
		{
			int32 NumSessions = SessionInterface->GetNumSessions();
			FOnlineSessionSettings* CurrentSettings = SessionInterface->GetSessionSettings(NAME_GameSession);
			if (CurrentSettings != nullptr)
			{
				TargetPlayers = CurrentSettings->NumPublicConnections;
			}
#if WITH_EDITOR
			else
			{
				ULevelEditorPlaySettings* PlayInSettings = Cast<ULevelEditorPlaySettings>(ULevelEditorPlaySettings::StaticClass()->GetDefaultObject());
				PlayInSettings->GetPlayNumberOfClients(TargetPlayers);
			}
#endif
		}
	}

	UE_LOG(GameModeLog, Log, TEXT("UpdateVote players %d target players %d"), PlayerSentinelVotes.Num(), TargetPlayers);

	if (PlayerSentinelVotes.Num() < TargetPlayers)
		return;

	USAObjectManager* objectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	const int32 PlayerNum = GameState->PlayerArray.Num();
	const TArray<ASAPowerPawn*> robotPawns = objectManager->GetPowerObjects(EPowerObjectType::Robot);
	if (!ensure(robotPawns.Num() > 0))
		return;

	const TArray<ASAPowerPawn*> sentinelPawns = objectManager->GetPowerObjects(EPowerObjectType::Sentinel);
	if (!ensure(sentinelPawns.Num() > 0))
		return;

	//single player mode
	if (TargetPlayers == 1)
	{
		TArray<int32> VotesArray;
		PlayerSentinelVotes.GenerateValueArray(VotesArray);

		bool bPlayerIsSentinel = VotesArray[0] > 0;

		for (int i = 0; i < PlayerNum; i++)
		{
			if (!GameState->PlayerArray[i]->IsABot())
			{
				AController* Controller = Cast<AController>(GameState->PlayerArray[i]->GetOwner());

				if (bPlayerIsSentinel)
				{
					SetAIPossession(robotPawns[0]);
					SetPlayerPossession(sentinelPawns[0], Controller);
				}
				else
				{
					SetAIPossession(sentinelPawns[0]);
					SetPlayerPossession(robotPawns[0], Controller);
				}
				return;
			}
		}
	}

	//multiple players so count votes
	//1) calculate the sum of all the weights
	//2) pick a random number that is 0 or greater and is less than the sum of the weights
	//3) go through the items one at a time, subtracting their weight from your random number, 
	//until you get the item where the random number is less than that item's weight
	FUniqueNetIdRepl SentinelPlayerId = GameState->PlayerArray[FMath::RandRange(0, PlayerNum-1)]->GetUniqueId();
	TArray<FUniqueNetIdRepl> SentinelVotes;
	int32 TotalWeight = 0;
	for (const TPair<FUniqueNetIdRepl /*PlayerId*/, int32 /*SentinelVotes*/>& pair : PlayerSentinelVotes)
		TotalWeight += pair.Value;
	int32 Random = FMath::RandRange(0, TotalWeight);
	for (const TPair<FUniqueNetIdRepl /*PlayerId*/, int32 /*SentinelVotes*/>& pair : PlayerSentinelVotes)
	{
		if (Random < pair.Value)
		{
			SentinelPlayerId = pair.Key;
			break;
		}
		else
			Random -= pair.Value;
	}

	bool bRobotIsPossessed = false;
	bool bSentinelIsPossessed = false;
	for (int i = 0; i < PlayerNum; i++)
	{
		AController* Controller = Cast<AController>(GameState->PlayerArray[i]->GetOwner());

		if (GameState->PlayerArray[i]->GetUniqueId() == SentinelPlayerId)
		{
			if (!bSentinelIsPossessed)
			{
				SetPlayerPossession(sentinelPawns[0], Controller);
				bSentinelIsPossessed = true;
			}
		}
		else
		{
			if (!bRobotIsPossessed)
			{
				SetPlayerPossession(robotPawns[0], Controller);
				bRobotIsPossessed = true;
			}
		}
	}
}

void ASAGameModeBase::SetAIPossession(ASAPowerPawn* Pawn) const
{
	Pawn->SpawnDefaultController();

	AAIController* AIC = UAIBlueprintHelperLibrary::GetAIController(Pawn);
	if (!ensureMsgf(AIC != nullptr, TEXT("Failed to find AI Controller %s."), *this->GetName()))
	{
		return;
	}

	ASAPowerAbsorberPawn* PowerPawn = Cast<ASAPowerAbsorberPawn>(Pawn);
	UBehaviorTree* BehaviorTree = PowerPawn->GetBehaviorTree();
	if (!ensureMsgf(BehaviorTree != nullptr, TEXT("BehaviourRree is null please chack blueprint %s."), *this->GetName()))
	{
		return;
	}

	AIC->RunBehaviorTree(BehaviorTree);
}

void ASAGameModeBase::SetPlayerPossession(ASAPowerPawn* Pawn, AController* Controller) const 
{
	Controller->UnPossess();
	Controller->Possess(Pawn);
	Pawn->SetOwner(Controller);
}

void ASAGameModeBase::InvokeTravel(const FString& FURL)
{
	bUseSeamlessTravel = true;

	//if (!CanServerTravel(FURL, true))
	//{
	//	bUseSeamlessTravel = false;
	//	//GetWorld()->GetFirstPlayerController()->ClientTravel(*FURL, TRAVEL_Relative);
	//	UGameplayStatics::OpenLevel(this, *FURL);
	//}
	//else
	GetWorld()->ServerTravel(*FURL);
	//GEngine->Exec(GetWorld(), *FURL);
}

void ASAGameModeBase::DebugSpectate()
{
	USAObjectManager* ObjectManager = GetGameInstance()->GetSubsystem<USAObjectManager>();
	const TArray<ASAPowerPawn*> robotPawns = ObjectManager->GetPowerObjects(EPowerObjectType::Robot);
	if(ensure(robotPawns.Num() > 0))
		SetAIPossession(robotPawns[0]);

	TArray<ASAPowerPawn*> sentinelPawns = ObjectManager->GetPowerObjects(EPowerObjectType::Sentinel);
	SetAIPossession(sentinelPawns[0]);
}