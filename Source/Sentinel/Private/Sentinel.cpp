// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Sentinel.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Sentinel, "Sentinel" );

DEFINE_LOG_CATEGORY(TerrainLog)
DEFINE_LOG_CATEGORY(PowerObjectLog)
DEFINE_LOG_CATEGORY(SentinelAILog)
DEFINE_LOG_CATEGORY(MenuPawnLog)
DEFINE_LOG_CATEGORY(GameModeLog)