// Carl Peters

#include "Sentinel/Public/UI/Tasks/SABTTask_ShowWidget.h"

#include "PlayerControllers/SAPlayerControllerBase.h"

#include <AIController.h>

USABTTask_ShowWidget::USABTTask_ShowWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Show UI Widget";
	bNotifyTick = false;
}

EBTNodeResult::Type USABTTask_ShowWidget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AActor* const MyActor = OwnerComp.GetOwner();
	if (!MyActor)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find AIController %s."), *this->GetName());
		return EBTNodeResult::Failed;
	}

	ASAPlayerControllerBase* Controller = Cast<ASAPlayerControllerBase>(MyActor->GetOwner());
	if (!Controller)
	{
		UE_LOG(SentinelAILog, Error, TEXT("Failed to find Pawn %s."), *this->GetName());
		return EBTNodeResult::Failed;
	}

	Controller->ShowMenu(MenuWidget, bShowCursor);

	return EBTNodeResult::InProgress;
}

#if WITH_EDITOR

FName USABTTask_ShowWidget::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Task.Wait.Icon");
}

#endif	// WITH_EDITOR
