//Carl Peters

#include "Sentinel/Public/UI/Decorators/SABTDecorator_IsGeneratingAssets.h"

#include <AIController.h>

#include "SAGameStateBase.h"

USABTDecorator_IsGeneratingAssets::USABTDecorator_IsGeneratingAssets(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Is Generating Assets";

	// can't abort, it's not observing anything
	bAllowAbortLowerPri = false;
	bAllowAbortNone = false;
	bAllowAbortChildNodes = false;
	FlowAbortMode = EBTFlowAbortMode::None;
}

bool USABTDecorator_IsGeneratingAssets::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	ASAGameStateBase* GameState = GetWorld()->GetGameState<ASAGameStateBase>();
	return GameState->GetState() == ESAGameState::GenerateAssets;
}

#if WITH_EDITOR

FName USABTDecorator_IsGeneratingAssets::GetNodeIconName() const
{
	return FName("BTEditor.Graph.BTNode.Decorator.ConeCheck.Icon");
}

#endif	// WITH_EDITOR
