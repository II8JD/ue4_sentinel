// Carl Peters

#include "Sentinel/Public/Terrain/SATerrainActor.h"

#include <DrawDebugHelpers.h>
#include <Engine/CollisionProfile.h>
#include <FastNoise/UFastNoise.h>
#include <Kismet/GameplayStatics.h>
#include <KismetProceduralMeshLibrary.h>
#include <Materials/MaterialInstanceDynamic.h>

#include "Utility/BPFL_SAColour.h"

// Sets default values
ASATerrainActor::ASATerrainActor()
{
	bReplicates = false;
	bNetLoadOnClient = true;
	bAlwaysRelevant = false;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CustomMesh = CreateDefaultSubobject<UProceduralMeshComponent>("CustomMesh");
	SetRootComponent(CustomMesh);
	CustomMesh->bUseAsyncCooking = true;
	CustomMesh->SetCollisionProfileName(UCollisionProfile::BlockAll_ProfileName);
}

void ASATerrainActor::BeginPlay()
{
	Super::BeginPlay();

	UBPFL_SAColour::OnColoursGeneratedCB.AddDynamic(this, &ASATerrainActor::ApplyColours);

	TerrainParams.Noise = NewObject<UFastNoise>(this, TEXT("Noise"));
}

void ASATerrainActor::GenerateMesh(int32 Seed, FSATerrainData& NewTerrainData)
{
	if (bGenerationInProgress)
	{
		UE_LOG(TerrainLog, Error, TEXT("Generation already in progress."));
		return;
	}

	bGenerationInProgress = true;

	TerrainParams.TerrainData = &NewTerrainData;

	int32 UnchamferedTiles = TerrainParams.TerrainData->Tiles;
	int32 Tiles = (UnchamferedTiles * 2) + 1;// (TerrainData.Tiles * 4) + (FMath::Sqrt(TerrainData.Tiles) * 4) + 1;

	int32 FloorSectionCount = UnchamferedTiles * UnchamferedTiles;
	FloorPoints.Reserve(FloorSectionCount);
	FloorPoints.Init(FVector::ZeroVector, FloorSectionCount);

	int32 triangleCount = (Tiles + 2)*(Tiles + 2) * 2 * 3;
	TerrainParams.Triangles.Reserve(triangleCount);
	TerrainParams.Triangles.Init(0, triangleCount);

	int vertexCount = (Tiles + 3)*(Tiles + 3) * 2;
	TerrainParams.Vertices.Reserve(vertexCount);
	TerrainParams.Vertices.Init(FVector::ZeroVector, vertexCount);
	TerrainParams.UV0.Reserve(vertexCount);
	TerrainParams.UV0.Init(FVector2D::ZeroVector, vertexCount);
	TerrainParams.Colours.Reserve(vertexCount);
	TerrainParams.Colours.Init(FColor::Black, vertexCount);

	int heightCount = (UnchamferedTiles + 1) * (UnchamferedTiles + 1);
	if(Heights.Num() != heightCount)
		Heights.Init(0, heightCount);

	if (Randomiser != nullptr)
		Randomiser->Destroy();

	Randomiser = GetWorld()->SpawnActor<AActor>(TerrainParams.TerrainData->RandomiserInterface);

	//FRandomStream rand = FRandomStream(Seed/*(GLOBAL_SEED << 12) ^ (world_x << 8) ^ (world_z << 4)*/);
	//TerrainData.Noise->SetNoiseType(ENoiseType::Simplex); // Set the desired noise type
	//TerrainData.Noise->SetSeed(Seed);
	FMath::RandInit(Seed);

	FTaskCompletionCB onCompletion = FTaskCompletionCB();
	onCompletion.BindUObject(this, &ASATerrainActor::OnCalculationsComplete);

	(new FAutoDeleteAsyncTask<TerrainGenerateTask>(FloorPoints, TerrainParams, Seed, Randomiser, this, onCompletion))->StartBackgroundTask();
}

void ASATerrainActor::OnCalculationsComplete()
{
	FGraphEventRef Task = FFunctionGraphTask::CreateAndDispatchWhenReady([&]()
	{
		// Code placed here will run in the game thread

		CustomMesh->CreateMeshSection(0, TerrainParams.Vertices, TerrainParams.Triangles, TerrainParams.Normals, TerrainParams.UV0, TerrainParams.Colours, TerrainParams.Tangents, true);
		
		if (Randomiser != nullptr)
			Randomiser->Destroy();

		DynamicMaterial = UMaterialInstanceDynamic::Create(TerrainParams.TerrainData->Material, this);
		CustomMesh->SetMaterial(0, DynamicMaterial);
		ISAColourApplicatorInterface::Execute_ApplyColours(this);

		TerrainBounds = GetComponentsBoundingBox();

		bGenerationComplete = true;

		bGenerationInProgress = false;

		CompletionCB.Broadcast(this);

	}, TStatId(), NULL, ENamedThreads::GameThread);
}

const int32 ASATerrainActor::GetTiles() const 
{ 
	if (TerrainParams.TerrainData == nullptr)
		return 0;

	return TerrainParams.TerrainData->Tiles;
}

const float ASATerrainActor::GetMaxHeight() const 
{ 
	return TerrainParams.TerrainData->Levels * TerrainParams.TerrainData->TileSize;
}

const float ASATerrainActor::GetMeshHeight(const int index) const
{
	if (index < 0 || index >= Heights.Num())
	{
		UE_LOG(TerrainLog, Error, TEXT("Height index is not valid=%d"), index);
		return 0;
	}

	return Heights[index];//FMath::Abs(FMath::FloorToFloat(Levels * Heights[index]));
}

//=======================================================================

void TerrainGenerateTask::DoWork()
{
	//Randomise();
	if (Randomiser != nullptr && 
		Randomiser->GetClass()->ImplementsInterface(USALevelRandomiserInterface::StaticClass()))
	{
		ISALevelRandomiserInterface::Execute_GenerateHeights(Randomiser, Seed, *TerrainParams.TerrainData, TerrainActor);
	}
	else
	{
		UE_LOG(TerrainLog, Error, TEXT("Actor does not implement LevelRandomiserInterface"));
	}
	Randomiser = nullptr;

	GenerateUVs();
	GenerateVertices();
	GenerateTriangles();
	//GenerateNormals();

	CalculateFloorSections();

	OnCompletion.ExecuteIfBound();
}

void TerrainGenerateTask::Randomise()
{	
	int32 levels = TerrainParams.TerrainData->Levels;
	int32 Tiles = TerrainParams.TerrainData->Tiles;
	float sampleDistance = TerrainParams.TerrainData->SampleDistance;
	int32 index = 0;
	for (int x = 0; x <= Tiles; x++)
	{
		for (int y = 0; y <= Tiles; y++)
		{
			FVector2D samplePos = FVector2D(((float)x / (float)Tiles) * sampleDistance, ((float)y / (float)Tiles) * sampleDistance);
			//float random = rand.FRand();
			float random = TerrainParams.Noise->GetValue(samplePos.X, samplePos.Y);
			//float random = FMath::PerlinNoise2D(samplePos);
			//float height = FMath::FloorToFloat(levels * random);
			TerrainActor->Heights[index] = random;// FMath::Abs(height);
			index++;
		}
	}
}

void TerrainGenerateTask::GenerateVertices()
{
	int32 UnchamferedTiles = TerrainParams.TerrainData->Tiles;
	float TileSize = TerrainParams.TerrainData->TileSize;
	float SmallTileSize = 0.0f;
	int32 Tiles = (UnchamferedTiles * 2) + 1; //TerrainParams.Tiles;
	FVector2D ScaleOffset = FVector2D::ZeroVector;
	//int HeightsOffset = 0;
	int HeightCount = 0;
	//float heightRatio = TerrainParams.Tiles / (float)Tiles;
	int CellX = 0;
	int CellY = 0;
	float totalHeight = (UnchamferedTiles * TileSize)+((UnchamferedTiles * SmallTileSize));

	for (int x = 0; x < Tiles + 3; x++)
	{
		int x_idx = x * (Tiles + 3);
		CellX = (int)((ScaleOffset.X / totalHeight) * UnchamferedTiles);
		if (CellX > UnchamferedTiles)
			CellX = UnchamferedTiles;
		for (int y = 0; y < Tiles + 3; y++)
		{
			int _x = x - 1;
			int _y = y - 1;
			if (_x < 0)
				_x = 0;
			if (_x >= Tiles + 1)
				_x = Tiles;
			if (_y < 0)
				_y = 0;
			if (_y >= Tiles + 1)
				_y = Tiles;
			//UE_LOG(MeshLog, Log, TEXT("_x=%f x=%f _z=%f z=%f"), _x, x, _z, z);
			if (_x != x - 1 || _y != y - 1)
			{
				if (_y == 0)
					ScaleOffset.Y = 0.0f;

				TerrainParams.Vertices[x_idx + y] = FVector(ScaleOffset.X, ScaleOffset.Y, -10);
			}
			else
			{
				if (_y == 0)
					ScaleOffset.Y = 0.0f;

				CellY = (int)((ScaleOffset.Y / totalHeight) * UnchamferedTiles);
				if (CellY > UnchamferedTiles)
					CellY = UnchamferedTiles;
				//int heightIndex = FMath::RoundToInt((_x * heightRatio) * TerrainParams.Tiles) + FMath::RoundToInt(_y * heightRatio);
				int heightIndex = (UnchamferedTiles + 1) * CellX + CellY;

				float height = TerrainActor->GetMeshHeight(heightIndex) * TileSize;
				TerrainParams.Vertices[x_idx + y] = FVector(ScaleOffset.X, ScaleOffset.Y, height);

				UE_LOG(TerrainLog, Verbose, TEXT("CellX=%d CellY=%d %d"), CellX, CellY, heightIndex);

				//HeightCount++;
				//if (!(_x == 0 && _y == 0) && _x % 2 == 0 && _y % 2 == 0)
				//{
				//	HeightsOffset++;
				//}
			}

			if (y < Tiles+1)
				ScaleOffset.Y += y % 2 ? SmallTileSize : TileSize;
		}
		ScaleOffset.Y = 0.0f;
		if(x>0 && x<Tiles+1)
			ScaleOffset.X += x % 2 ? SmallTileSize : TileSize;
	}
}

void TerrainGenerateTask::GenerateTriangles()
{
	int32 Tiles = (TerrainParams.TerrainData->Tiles * 2) + 1;

	// there are SIZE*SIZE squares
	// each square has 2 triangles
	// each triangle has 3 corners...
	//int32 triangleCount = (Tiles + 2)*(Tiles + 2) * 2 * 3;
	//Triangles.Reserve(triangleCount);
	//Triangles.Init(0, triangleCount);
	//triangles.Init(0, (SIZE + 2)*(SIZE + 2) * 2 * 3]);
	// loop over each square
	for (int x = 0; x < Tiles + 2; x++) {
		int x_idx = x * (Tiles + 3);
		for (int y = 0; y < Tiles + 2; y++) {
			// calculate the indexes into the vertex 
			// array for the 4 corners of this square
			int v_00 = x_idx + y;
			int v_01 = x_idx + y + 1;
			int v_10 = x_idx + (Tiles + 3) + y;
			int v_11 = x_idx + (Tiles + 3) + y + 1;
			// starting index into the triangles array
			int t_idx = ((x * (Tiles + 2)) + y) * 6;
			// alternate between triangle styles
			//if(z % 2 == 0){
			// lower left triangle
			TerrainParams.Triangles[t_idx + 0] = v_00;
			TerrainParams.Triangles[t_idx + 1] = v_01;
			TerrainParams.Triangles[t_idx + 2] = v_10;
			// upper right triangle
			TerrainParams.Triangles[t_idx + 3] = v_10;
			TerrainParams.Triangles[t_idx + 4] = v_01;
			TerrainParams.Triangles[t_idx + 5] = v_11;
		}
	}
}

void TerrainGenerateTask::GenerateUVs()
{
	int32 Tiles = (TerrainParams.TerrainData->Tiles * 2) + 1;

	for (int x = 0; x < Tiles + 3; x++)
	{
		int x_idx = x * (Tiles + 3);
		for (int z = 0; z < Tiles + 3; z++)
		{
			TerrainParams.UV0[x_idx + z] = FVector2D(x / (float)Tiles * Tiles, z / (float)Tiles * Tiles);
		}
	}
}

void TerrainGenerateTask::GenerateNormals()
{
	UKismetProceduralMeshLibrary::CalculateTangentsForMesh(TerrainParams.Vertices, TerrainParams.Triangles,
		TerrainParams.UV0, TerrainParams.Normals, TerrainParams.Tangents);
}

void TerrainGenerateTask::CalculateFloorSections()
{
	const float TileSize = TerrainParams.TerrainData->TileSize;
	//TerrainParams.FloorSections.Empty();
	const int32 TerrainLipOffset = 0;// (TerrainParams.Tiles + 3) * 3 * 2;
	const float HalfTileSize = TileSize * 0.5f;
	const float EdgeTileSize = TileSize * 0.1f;
	int EdgeOffset = 0;
	int StartTriangle = 0;
	int32 Index = 0;
	for (int32 triangle = TerrainLipOffset, total = TerrainParams.Triangles.Num() - TerrainLipOffset; triangle < total;)
	{
		StartTriangle = triangle;

		FVector P1 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N1 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;
		FVector P2 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N2 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;
		FVector P3 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N3 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;

		FVector P4 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N4 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;
		FVector P5 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N5 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;
		FVector P6 = TerrainParams.Vertices[TerrainParams.Triangles[triangle]];
		//FVector N6 = TerrainParams.Normals[TerrainParams.Triangles[triangle]];
		triangle++;

		//FVector faceNormal = ((N1 + N2 + N3) / 3).GetSafeNormal();
		//FVector faceNormal1 = (-FVector::CrossProduct(P2 - P1, P3 - P1)).GetSafeNormal();
		//FVector faceNormal2 = (-FVector::CrossProduct(P5 - P4, P6 - P4)).GetSafeNormal();

		if ( FVector2D::Distance(FVector2D(P1), FVector2D(P2)) > EdgeTileSize &&
			FVector2D::Distance(FVector2D(P1), FVector2D(P3)) > EdgeTileSize)
		{
			FVector Center = ((P1 + P2 + P3 + P6) / 4);

			//if (FVector::DotProduct(faceNormal1, FVector::UpVector) != 1 &&
				//FVector::DotProduct(faceNormal2, FVector::UpVector) != 1)
				//Center.Z = -1; //not walkable
			if (FMath::Min(FMath::Min(P1.Z, P2.Z), FMath::Min(P3.Z, P6.Z)) != Center.Z ||
				FMath::Max(FMath::Max(P1.Z, P2.Z), FMath::Max(P3.Z, P6.Z)) != Center.Z)
			{
				Center.Z = -1; //not walkable
			}
			else
			{
				FColor TileColour = FColor::Red;
				int TempX = (int)((Center.X - HalfTileSize) / TileSize) % 2;
				int TempY = (int)((Center.Y - HalfTileSize) / TileSize) % 2;
				if ((TempX == 0 && TempY != 0) ||
					(TempX != 0 && TempY == 0))
					TileColour.R = 191;
				TerrainParams.Colours[TerrainParams.Triangles[StartTriangle]] = TileColour;
				TerrainParams.Colours[TerrainParams.Triangles[StartTriangle + 1]] = TileColour;
				TerrainParams.Colours[TerrainParams.Triangles[StartTriangle + 2]] = TileColour;
				TerrainParams.Colours[TerrainParams.Triangles[StartTriangle + 5]] = TileColour;
			}

			FloorPoints[Index] = Center;
			Index++;
		}
	}
}