// Carl Peters

#include "Sentinel/Public/Terrain/SATerrainTileData.h"

void FSATerrainTileData::AddPowerObject(ASAPowerPawn& PowerObject, bool Add)
{
	if (Add)
	{
		SectionPowerObjects.Add(&PowerObject);
		CurrentHeight += PowerObject.GetMeshHeight();
	}
	else
	{
		for (ASAPowerPawn* Elem : SectionPowerObjects)
		{
			if (Elem == &PowerObject)
			{
				SectionPowerObjects.RemoveSwap(&PowerObject);
				CurrentHeight -= PowerObject.GetMeshHeight();
				UE_LOG(LogTemp, Log, TEXT("AddPowerObject Decrease height %f"), CurrentHeight)
				return;
			}
		}
	}
}

float FSATerrainTileData::GetPowerObjectHeight(ASAPowerPawn& PowerObject) const
{
	float Height = Location.Z;
	for (ASAPowerPawn* Elem : SectionPowerObjects)
	{
		if (Elem == &PowerObject)
			return Height;
		Height += Elem->GetMeshHeight();
	}
	UE_LOG(LogTemp, Warning, TEXT("Failed to find power object in list."))
		return Height;
}

void FSATerrainTileData::DeleteAllPowerObjects()
{
	const int32 Count = SectionPowerObjects.Num();
	if (Count == 0)
		return;

	for(int i = Count-1; i >= 0; i--)
		if (SectionPowerObjects[i] != nullptr)
			SectionPowerObjects[i]->Destroy();

	SectionPowerObjects.Empty();
}

ASAPowerPawn* FSATerrainTileData::GetTopObject() const
{
	if (CurrentHeight < 0)
		return nullptr;

	const int32 Count = SectionPowerObjects.Num();
	if (Count == 0)
		return nullptr;

	return SectionPowerObjects[Count - 1];
}

void FSATerrainTileData::InitCollisionParams(struct FCollisionQueryParams& InCollisionParams)
{
	InCollisionParams.ClearIgnoredActors();

	for (ASAPowerPawn* Elem : SectionPowerObjects)
		InCollisionParams.AddIgnoredActor(Elem);
}

bool FSATerrainTileData::LocationCanStack(bool bExcludeSpawningInTiles) const
{
	if (CurrentHeight < 0)
		return false; //this is an invalid position (not flat) 

	const ASAPowerPawn* TopObject = GetTopObject();

	if (IsValid(TopObject))
	{
		if (!TopObject->CanStack())
			return false;

		if(bExcludeSpawningInTiles && TopObject->IsSpawningIn())
			return false;
	}

	return true;
}