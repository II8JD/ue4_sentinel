// Carl Peters

#include "Sentinel/Public/Terrain/SATerrainChunk.h"

#include "DataAssets/SALevelDataAsset.h"
#include "SAObjectManager.h"
#include "Sentinel/Public/DataAssets/SATerrainDataAsset.h"

// Sets default values for this component's properties
USATerrainChunk::USATerrainChunk()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}

void USATerrainChunk::BeginPlay()
{
	Super::BeginPlay();

	Terrain = Cast<ASATerrainActor>(GetOwner());
	Terrain->CompletionCB.AddDynamic(this, &USATerrainChunk::OnTerrainGenerationComplete);

	USAObjectManager* objectManager = GetWorld()->GetGameInstance()->GetSubsystem<USAObjectManager>();
	objectManager->RegisterTerrainObject(*this, true);
}

void USATerrainChunk::SetLevelData(FSALevelData* NewLevelData)
{ 
	LevelData = *NewLevelData;
}

void USATerrainChunk::BeginDestroy()
{
	Super::BeginDestroy();

	UWorld* world = GetWorld();
	if (!world)
		return;

	UGameInstance* instance = world->GetGameInstance();
	if (!instance)
		return;

	USAObjectManager* objectManager = instance->GetSubsystem<USAObjectManager>();
	if (objectManager)
	{
		objectManager->RegisterTerrainObject(*this, false);
	}
}

void USATerrainChunk::OnTerrainGenerationComplete(ASATerrainActor* CompletedTerrain)
{
	if (CompletedTerrain == Terrain)
	{
		TArray<FVector> Points = CompletedTerrain->GetFloorPoints();
		int32 SectionCount = Points.Num();
		FloorSections.Reserve(SectionCount);
		FloorSections.Init(FSATerrainTileData(), SectionCount);

		for (int i = 0; i < SectionCount; i++)
		{
			FloorSections[i].InitLocation(Points[i]);
		}
	}
}

void USATerrainChunk::AddPowerObject(ASAPowerPawn& PowerObject, bool Add)
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(PowerObject.GetActorLocation(), Index))
		return;

	FloorSections[Index].AddPowerObject(PowerObject, Add);

	if (Add)
	{
		if (!PowerObjectTiles.Contains(Index))
			PowerObjectTiles.Add(Index);

		InitTileCollisionParams(PowerObject.GetActorLocation(), PowerObject.GetCollisionParams());
	}
	else
		PowerObjectTiles.Remove(Index);

	PowerObjectUpdateCB.Broadcast();
}

const uint32 USATerrainChunk::GetTilePowerObjectCount(uint32 Index) const
{
	const uint32 SectionCount = FloorSections.Num();
	if (SectionCount == 0 || SectionCount <= Index)
		return 0;

	return FloorSections[Index].GetPowerObjectNum();
}

const bool USATerrainChunk::GetTileCurrentPosition(FVector& Pos) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return false;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(Pos, Index))
		return false;

	//clamp position to grid
	Pos = FloorSections[Index].GetCurrentLocation();

	return true;
}

const bool USATerrainChunk::GetTileOriginalPosition(FVector& Pos) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return false;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(Pos, Index))
		return false;

	//clamp position to grid
	Pos = FloorSections[Index].GetOriginalLocation();

	return true;
}

const bool USATerrainChunk::IsPositionInTile(const FVector& Pos, const FVector& TargetPos) const
{
	if (FloorSections.Num() == 0)
		return false;

	int32 IndexA = 0;
	if (!GetTileIndexFromLocation(Pos, IndexA))
		return false;

	int32 IndexB = 0;
	if (!GetTileIndexFromLocation(TargetPos, IndexB))
		return false;

	return IndexA == IndexB;
}

float USATerrainChunk::GetObjectTargetHeight(ASAPowerPawn& PowerObject) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return 0.0f;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(PowerObject.GetActorLocation(), Index))
		return 0.0f;

	return FloorSections[Index].GetPowerObjectHeight(PowerObject);
}

float USATerrainChunk::GetMaxHeight() const
{
	return Terrain->GetMaxHeight();
}

void USATerrainChunk::InitTileCollisionParams(const FVector& Location, struct FCollisionQueryParams& InCollisionParams)
{
	int32 Index = 0;
	if (!GetTileIndexFromLocation(Location, Index))
		return;

	FloorSections[Index].InitCollisionParams(InCollisionParams);
}

const bool USATerrainChunk::GetTileIndexFromLocation(const FVector& Location, int32& OutIndex) const
{
	const int32 Tiles = Terrain->GetTiles();
	const float TileSize = Terrain->GetTileSize();
	const float TerrainSize = TileSize * Tiles;
	const int32 CellX = (Tiles * (Location.X / TerrainSize));
	const int32 CellY = (Tiles * (Location.Y / TerrainSize));

	OutIndex = CellY + (CellX * Tiles);
	if (OutIndex < 0)
	{
		OutIndex = 0;
		UE_LOG(TerrainLog, Warning, TEXT("Invalid index (%d) X:%f/%d Y:%f/%d"), OutIndex, Location.X, CellX, Location.Y, CellY)
			return false;
	}
	else if ((int32)OutIndex >= FloorSections.Num())
	{
		OutIndex = FloorSections.Num() - 1;
		UE_LOG(TerrainLog, Warning, TEXT("Invalid index (%d) X:%f/%d Y:%f/%d"), OutIndex, Location.X, CellX, Location.Y, CellY)
			return false;
	}

	return true;
}

const bool USATerrainChunk::GetTileLocationFromIndex(const uint32 Index, FVector& OutLocation) const
{
	if ((int32)Index < FloorSections.Num())
	{
		OutLocation = FloorSections[Index].GetCurrentLocation();
		return true;
	}
	else
	{
		UE_LOG(TerrainLog, Warning, TEXT("Invalid index (%d)"), Index)
			return false;
	}
}

bool USATerrainChunk::GetRandomPosition(FVector& OutValue, const bool bEmptyPositionOnly, const bool bSearchUp, const bool bFavourHigher, float MaxHeight) const
{
	const float StartHeight = MaxHeight;
	const float MapMax = GetMaxHeight();
	const float TileSize = Terrain->GetTileSize();
	TArray<FSATerrainTileData> ValidHeights;
	int32 FloorSectionCount = 0;

	while (FloorSectionCount == 0 && ((bSearchUp && MaxHeight < MapMax) || (!bSearchUp && MaxHeight >= 0)))
	{
		ValidHeights.Empty();
		GetValidPositions(ValidHeights, bEmptyPositionOnly, bFavourHigher, MaxHeight);
		FloorSectionCount = ValidHeights.Num();

		if (bSearchUp)
			MaxHeight += TileSize;
		else
			MaxHeight -= TileSize;
	}

	//found none so look in opposite direction
	if (FloorSectionCount == 0)
	{
		MaxHeight = StartHeight;
		while (FloorSectionCount == 0 && ((bSearchUp && MaxHeight < MapMax) || (!bSearchUp && MaxHeight >= 0)))
		{
			ValidHeights.Empty();
			GetValidPositions(ValidHeights, bEmptyPositionOnly, bFavourHigher, MaxHeight);
			FloorSectionCount = ValidHeights.Num();

			if (bSearchUp)
				MaxHeight -= TileSize;
			else
				MaxHeight += TileSize;
		}
	}

	if (FloorSectionCount == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to find random position."));
		return false;
	}

	auto It = ValidHeights.CreateConstIterator();
	for (int i = 0, target = FMath::RandRange(0, FloorSectionCount - 1); i < target; i++)
		It.operator++();
	OutValue = It->GetCurrentLocation();

	return true;
}

void USATerrainChunk::GetValidPositions(TArray<FSATerrainTileData>& OutValues, const bool bEmptyPositionOnly, const bool bFavourHigher, float const MaxHeight) const
{
	for (auto& Elem : FloorSections)
	{
		const float currentHeight = Elem.GetCurrentHeight();

		if (currentHeight < 0 || !Elem.LocationCanStack()) //if Tile is not valid
			continue;

		if (bEmptyPositionOnly && !Elem.LocationIsEmpty())
			continue;

		if (MaxHeight >= 0)
		{
			if (bFavourHigher)
			{
				if (currentHeight < MaxHeight)
					continue;
			}
			else
			{
				if (currentHeight > MaxHeight)
					continue;
			}
		}

		OutValues.Add(Elem);
	}
}

bool USATerrainChunk::GetTerrainComplete() const
{ 
	if (Terrain == nullptr)
	{
		return false;
	}

	return Terrain->GetTerrainGenerationComplete();
}

ASAPowerPawn* USATerrainChunk::GetTileTopObject(FVector& Pos) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return nullptr;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(Pos, Index))
		return nullptr;

	return GetTileTopObject(Index);
}

ASAPowerPawn* USATerrainChunk::GetTileTopObject(const uint32 Index) const
{
	if (Index >= (uint32)FloorSections.Num())
	{
		UE_LOG(TerrainLog, Error, TEXT("Attempting to access tile with invalid index %d."), Index);
		return nullptr;
	}

	return FloorSections[Index].GetTopObject();
}

ASAPowerPawn* USATerrainChunk::GetTileAbsorbObject(FVector& Pos) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount == 0)
		return nullptr;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(Pos, Index))
		return nullptr;

	return FloorSections[Index].GetAbsorbObject();
}

const bool USATerrainChunk::IsPositionValid(FVector& Pos, bool bExcludeSpawningInTiles /*false*/) const
{
	const int32 SectionCount = FloorSections.Num();
	if (SectionCount <= 0)
		return false;

	int32 Index = 0;
	if (!GetTileIndexFromLocation(Pos, Index))
		return false;

	//clamp position to grid
	Pos = FloorSections[Index].GetCurrentLocation();

	if (Pos.Y < 0)
		return false;

	return FloorSections[Index].LocationCanStack(bExcludeSpawningInTiles);
}