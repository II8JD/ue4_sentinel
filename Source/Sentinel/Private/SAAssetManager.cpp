#include "SAAssetManager.h"

#include <AbilitySystemGlobals.h>


USAAssetManager& USAAssetManager::Get()
{
	USAAssetManager* Singleton = Cast<USAAssetManager>(GEngine->AssetManager);

	if (Singleton)
	{
		return *Singleton;
	}
	else
	{
		UE_LOG(LogTemp, Fatal, TEXT("Invalid AssetManager in DefaultEngine.ini, must be GDAssetManager!"));
		return *NewObject<USAAssetManager>();	 // never calls this
	}
}


void USAAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();
	UAbilitySystemGlobals::Get().InitGlobalData();
}
