// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Sentinel : ModuleRules
{
	public Sentinel(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(
            new string[] 
            { 
                "Core", 
                "CoreUObject", 
                "Engine", 
                "InputCore", 
                "ProceduralMeshComponent", 
                "UnrealFastNoisePlugin", 
                "UMG", 
                "Text3D", 
                "Niagara", 
                "ProceduralMusicPlugin", 
                "UIControllerGraphPlugin", 
                "Synthesis", 
                "AIModule", 
                "GameplayTasks", 
                "OnlineSubsystem", 
                "OnlineSubsystemUtils" 
            });

		PrivateDependencyModuleNames.AddRange(new string[] { });

        if (Target.Type == TargetType.Editor)//4.16+
        {
            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "SentinelEditor",
                    "UIControllerGraphEditorPlugin",
                    "UnrealEd",
                    "GameplayAbilities",
                    "GameplayTags", 
                    "GameplayTasks"
                });
        }

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
